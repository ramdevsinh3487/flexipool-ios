//
//  AppDelegate.h
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "defines.h"
#import "AlertViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "APIManager.h"
#import "Database.h"
#import <UIImageView+WebCache.h>
#import <UserNotifications/UserNotifications.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "Notification.h"
#import "MenuView.h"
#import <MessageUI/MessageUI.h>
@import Firebase;

typedef enum LOGGED_USER_TYPE{
    USER_TYPE_PASSENGER,//0
    USER_TYPE_DRIVER,//1
    USER_TYPE_BOTH,//2
}LoggedUserType;

typedef enum RUNNIG_RIDE_STATUS_UPDATE{
    RIDE_STATUS_NONE,//0
    RIDE_STATUS_REQUEST,//1
    RIDE_STATUS_ACCEPTED,//2
    RIDE_STATUS_ARRIVAL,//3
    RIDE_STATUS_ON_GOING,//4
    RIDE_STATUS_CANCELED,//5
    RIDE_STATUS_FINISHED,//6
    RIDE_STATUS_DECLINED//7
}RunningRideStatusUpdate;


@interface AppDelegate : UIResponder <UIApplicationDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, AlertViewControllerDelegate, CLLocationManagerDelegate, MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate>
{
//    UIView *viewBackground;
//    UIView *viewHeader;
//    UILabel *lblName;
//    UILabel *lblEmail;
//    UIImageView *imgViewUser;
//    UIButton *btnWalletAmount;
    
    NSMutableArray *arrImageIconNames;
    NSMutableArray *arrMenutitleNames;
    NSMutableDictionary *dictMenuList;
}

@property(nonatomic,retain)NSString *strCommuteTo;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
@property NSMutableArray * arrayHomeScreenStatus;
//Custom Functions
- (void) setupPushNoification;
-(void)updateHomeStatusArrayWithIndex:(NSString *)stringIndex;
-(void)NavigateToNotification;
-(void)updateDeviceTokenToServer;
-(void)updateFavoriteUserListArray;
-(void)updateTheArrayOfCancelReasons;
-(void)speakStringForNavigation:(NSString *)textToSpeech;

@property BOOL canNavigateToLogin;
@property Database *db;
@property APIManager * apiManager;

@property (nonatomic, retain) NSString *strActiveNotification;

#pragma mark - User Location Corner
@property CLLocationManager * locationManager;
@property CLLocationCoordinate2D userLocation;
@property (nonatomic,retain)NSString *strLatitude;
@property (nonatomic,retain)NSString *strLongitude;
@property CLLocationDirection trueHeading;
@property CLLocationDirection magneticeading;
-(void)getCurrentLocation;
-(BOOL)CheckLocationPermissionWithSourceView:(UIViewController *)sourceView;
// Store User Current Location
@property (nonatomic,retain)NSTimer *timer1;
@property (nonatomic,retain)NSTimer *timer2;

#pragma mark - LeftMenu Corner
@property MenuView * viewLeftMenu;

//@property (nonatomic, retain) UIView *viewMenu;
//@property (nonatomic, retain) UITableView *tblMenu;

- (void) showMenu;
- (void) hideMenu;
- (void) logoutTheUserWithSouldShowAlert:(BOOL)shouldShowAlert;


#pragma mark - Multi-Languages Corner
@property NSMutableArray *arrLanguage;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
- (void)saveContext;


#pragma mark - NSUserDefaults Corner
@property BOOL isInitial;
@property BOOL isUserLoggedIn;
@property BOOL isDemoPresented;
@property NSString * loggedUserId;
@property LoggedUserType loggedUserType;
@property NSString * userPassword;
@property BOOL isUserModeDriver;
@property NSDictionary * dictUserDetails;
@property NSString * tokenString;
@property NSDictionary * dictLastSelectedLocation;
@property NSString * currencySymbol;
@property NSString * decimapPoints;
@property NSMutableArray * arrayCancelationReasons;
@property NSString * WalletAmount;
@property NSString * badgeValue;
@property NSMutableArray * arrayFavoriteUsers;

@property NSString * phoneMessageNumber;
@property NSString * facebookURL;
@property NSString * whatsApp;

//Ride Management Corner
@property BOOL canAvoidTolls;
@property BOOL canAvoidHighways;
@property BOOL canSpeakNavigation;
-(void)clearPreviousRideDetailsFromTheAPP;
-(NSString *)findActualRouteDistance;
@property NSArray * arrayActualRoute;
@property double rideRates;
@property int globalTimeout;
@property BOOL isRideRunning;
@property NSString * runningRideId;
@property NSString * recieverId;
@property RunningRideStatusUpdate runningRideStatus;
@property CLLocationCoordinate2D tripStartLocation;//NSUserDefaults
@property NSDictionary * dictPickupDetails;
@property CLLocationCoordinate2D tripEndLocation;//NSUserDefaults
@property NSDictionary * dictDropoffDetails;

@property NSString * tripDuration;
@property NSString * tripDistance;
@property NSString * tripCost;

@property NSString * tripFormatedDuration;
@property NSString * tripFormatedDistance;
@property BOOL canNavigateToNotifications;


@property NSDictionary * dictTripDetails;
@property NSDate *sendRequestDate;

#pragma mark - NearByUser Corner
-(NSString *)countEstimatedCostFromETAInMinutes:(int)duration DistanceInKM:(float)distance isModeValue:(BOOL)isModeValue;
-(NSString *)formatTheCostWithInputValue:(double)totalCost isModeValue:(BOOL)isModeValue;
@property NSMutableArray * arrayNearByDrivers;
-(void)moveMarker:(GMSMarker *)marker WithAnimationFromCoordinate:(CLLocationCoordinate2D)oldLocation toCoordinate:(CLLocationCoordinate2D)newLocation;
- (void) createDynamicLink : (NSString *)Link;
-(void)shareMyRideUsingDefaultMessageApp;
@end

