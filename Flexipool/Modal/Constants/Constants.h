//
//  Constants.h
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#define DEBUG_MODE true
#define NAVIGATION_MODE true

#pragma mark - NSNotificationCenter
extern NSString * const NOTI_USER_LOCATION_UPDATES;
extern NSString * const NOTI_UPDATE_NEAR_BY_USERS;
extern NSString * const NOTI_BADGE_COUNT_UPDATED;
extern NSString * const NOTI_RIDE_STATUS_UPDATED;
extern NSString * const NOTI_MENU_ITEM_SELECTED;
extern NSString * const NOTI_STEPS_UPDATED;
#pragma LocalPreferances
extern NSString * const DEFAULT_WALLET_AMOUNT;
extern NSString * const DEFAULT_LAST_SELECTION;

extern NSString * const DEFAULT_IS_LOGGED;
extern NSString * const DEFAULT_DEMO_PRESENTED;
extern NSString * const DEFAULT_USER_DATA;
extern NSString * const DEFAULT_USER_PASSWORD;
extern NSString * const DEFAULT_USER_ID;
extern NSString * const DEFAULT_TOKEN_STRING;
extern NSString * const DEFAULT_USER_TYPE;
extern NSString * const DEFAULT_IS_MODE_SRIVER;
extern NSString * const DEFAULT_CURRENY_SYMBOL;
extern NSString * const DEFAULT_DECIMAL_POINT;
extern NSString * const DEFAULT_PUSH_TOKEN;
extern NSString * const DEFAULT_CANCEL_REASONS;
extern NSString * const DEFAULT_FAVORITE_USERS;

extern NSString * const DEFAULT_PHONE_MESSAGE;
extern NSString * const DEFAULT_FACEBOOK_URL;
extern NSString * const DEFAULT_WHATS_APP;

extern NSString * const DEFAULT_CAN_AVOID_TOLLS;
extern NSString * const DEFAULT_CAN_AVOID_HIGHWAYS;
extern NSString * const DEFAULT_CAN_SPEAK_NAVIGATION;

//Ride Management Corner
extern NSString * const DEFAULT_ACTUAL_ROUTE;
extern NSString * const DEFAULT_RIDE_RATES;
extern NSString * const DEFAULT_GLOBAL_TIMEOUT;
extern NSString * const DEFAULT_IS_RIDE_RUNNING;
extern NSString * const DEFAULT_RIDE_ID;
extern NSString * const DEFAULT_RECIEVER_ID;
extern NSString * const DEFAULT_RIDE_STATUS;
extern NSString * const DEFAULT_TRIP_START_LOCATION;
extern NSString * const DEFAULT_TRIP_START_DETAILS;
extern NSString * const DEFAULT_TRIP_END_LOCATION;
extern NSString * const DEFAULT_TRIP_END_DETAILS;
extern NSString * const DEFAULT_SEND_REQUEST_DATE;
extern NSString * const DEFAULT_TRIP_DETAILS;

#pragma mark - APIManagerCorner
extern NSString * const API_BASE_URL;
extern NSString * const API_REGISTER_USER;
extern NSString * const API_VERIFY_MOBILE;
extern NSString * const API_LOGIN_USER;
extern NSString * const API_UPDATE_PROFILE;
extern NSString * const API_UPDATE_USER_MODE;
extern NSString * const API_ADD_FAV_SPOT;
extern NSString * const API_FAV_SPOT_LIST;
extern NSString * const API_DELETE_FAV_LOCATION;
extern NSString * const API_UPDATE_TOKEN;
extern NSString * const API_FORGET_PASSWORD;
extern NSString * const API_PESSANGER_LOCATION;
extern NSString * const API_DRIVER_LOCATION;
extern NSString * const API_CHANGE_PASSWORD;
extern NSString * const API_NOTIFICATIONS_LIST;
extern NSString * const API_UPDATE_SETTINGS;
extern NSString * const API_FAQ_LIST;
extern NSString * const API_REDEEM_WALLET;
extern NSString * const API_CLEAR_BADGE;

extern NSString * const API_SEND_RIDE_REQUEST;
extern NSString * const API_VERIFY_OTP;
extern NSString * const API_UPDATE_RIDE_STATUS;
extern NSString * const API_CONFIRM_RIDE;
extern NSString * const API_CANCEL_REASONS;
extern NSString * const API_SUBMIT_PAYMENT;
extern NSString * const API_SUBMIT_FEEDBACK;
extern NSString * const API_PLAN_A_RIDE;
extern NSString * const API_DISCUSSION_BOARD;
extern NSString * const API_FAVOITE_USERS;
extern NSString * const API_RIDE_HISTORY;
extern NSString * const API_DELETE_RIDE;
extern NSString * const API_ADD_FAVORUITE_USER;
extern NSString * const API_ADD_BLOCK_USER;
extern NSString * const API_ADD_MONEY_WALLET;
extern NSString * const API_CMS_INFO;
extern NSString * const API_AUTHORIZED_TRANSACTION;
extern NSString * const API_LOGOUT_USER;
extern NSString * const API_SOS_EMERGANCY;
extern NSString * const API_VIEW_SETTINGS;
extern NSString * const API_SHARE_RIDE;

extern NSString * const ItunesAppLink;

