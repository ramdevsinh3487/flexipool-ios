//
//  Constants.m
//  VCab
//
//  Created by Vishal Gohil on 18/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Constants.h"
#pragma mark - NSNotificationCenter
NSString * const NOTI_USER_LOCATION_UPDATES = @"NOTI_USER_LOCATION_UPDATES";
NSString * const NOTI_UPDATE_NEAR_BY_USERS = @"NOTI_UPDATE_NEAR_BY_USERS";
NSString * const NOTI_BADGE_COUNT_UPDATED = @"NOTI_BADGE_COUNT_UPDATED";
NSString * const NOTI_RIDE_STATUS_UPDATED = @"NOTI_RIDE_STATUS_UPDATED";
NSString * const NOTI_MENU_ITEM_SELECTED = @"NOTI_MENU_ITEM_SELECTED";
NSString * const NOTI_STEPS_UPDATED = @"NOTI_STEPS_UPDATED";
#pragma LocalPreferances
NSString * const DEFAULT_WALLET_AMOUNT = @"DEFAULT_WALLET_AMOUNT";
NSString * const DEFAULT_LAST_SELECTION = @"DEFAULT_LAST_SELECTION";
NSString * const DEFAULT_IS_LOGGED = @"DEFAULT_IS_LOGGED";
NSString * const DEFAULT_DEMO_PRESENTED = @"DEFAULT_DEMO_PRESENTED";
NSString * const DEFAULT_USER_PASSWORD = @"DEFAULT_USER_PASSWORD";
NSString * const DEFAULT_USER_DATA = @"DEFAULT_USER_DATA";
NSString * const DEFAULT_USER_ID = @"DEFAULT_USER_ID";
NSString * const DEFAULT_TOKEN_STRING = @"DEFAULT_TOKEN_STRING";
NSString * const DEFAULT_USER_TYPE = @"DEFAULT_USER_TYPE";
NSString * const DEFAULT_IS_MODE_SRIVER = @"DEFAULT_IS_MODE_SRIVER";
NSString * const DEFAULT_CURRENY_SYMBOL = @"DEFAULT_CURRENY_SYMBOL";
NSString * const DEFAULT_DECIMAL_POINT = @"DEFAULT_DECIMAL_POINT";
NSString * const DEFAULT_PUSH_TOKEN = @"DEFAULT_PUSH_TOKEN";
NSString * const DEFAULT_CANCEL_REASONS = @"DEFAULT_CANCEL_REASONS";
NSString * const DEFAULT_FAVORITE_USERS = @"DEFAULT_FAVORITE_USERS";

NSString * const DEFAULT_PHONE_MESSAGE = @"DEFAULT_PHONE_MESSAGE";
NSString * const DEFAULT_FACEBOOK_URL = @"DEFAULT_FACEBOOK_URL";
NSString * const DEFAULT_WHATS_APP = @"DEFAULT_WHATS_APP";
NSString * const DEFAULT_CAN_AVOID_TOLLS = @"DEFAULT_CAN_AVOID_TOLLS";
NSString * const DEFAULT_CAN_AVOID_HIGHWAYS = @"DEFAULT_CAN_AVOID_HIGHWAYS";
NSString * const DEFAULT_CAN_SPEAK_NAVIGATION = @"DEFAULT_CAN_SPEAK_NAVIGATION";
//Ride Management
NSString * const DEFAULT_ACTUAL_ROUTE = @"DEFAULT_ACTUAL_ROUTE";
NSString * const DEFAULT_RIDE_RATES = @"DEFAULT_RIDE_RATES";
NSString * const DEFAULT_GLOBAL_TIMEOUT = @"DEFAULT_GLOBAL_TIMEOUT";
NSString * const DEFAULT_IS_RIDE_RUNNING = @"DEFAULT_IS_RIDE_RUNNING";
NSString * const DEFAULT_RIDE_ID = @"DEFAULT_RIDE_ID";
NSString * const DEFAULT_RIDE_STATUS = @"DEFAULT_RIDE_STATUS";
NSString * const DEFAULT_RECIEVER_ID = @"DEFAULT_RECIEVER_ID";
NSString * const DEFAULT_TRIP_START_LOCATION = @"DEFAULT_TRIP_START_LOCATION";
NSString * const DEFAULT_TRIP_START_DETAILS = @"DEFAULT_TRIP_START_DETAILS";
NSString * const DEFAULT_TRIP_END_LOCATION = @"DEFAULT_TRIP_END_LOCATION";
NSString * const DEFAULT_TRIP_END_DETAILS = @"DEFAULT_TRIP_END_DETAILS";
NSString * const DEFAULT_SEND_REQUEST_DATE = @"DEFAULT_SEND_REQUEST_DATE";
NSString * const DEFAULT_TRIP_DETAILS = @"DEFAULT_TRIP_DETAILS";
#pragma mark - APIManagerCorner


NSString * const API_BASE_URL = @"https://app.ahoyride.com/api/";
//NSString * const API_BASE_URL = @"https://app.ahoyride.com/beta/api/";
NSString * const ItunesAppLink = @"https://www.ahoyride.com/";

NSString * const API_REGISTER_USER = @"register.php";
NSString * const API_VERIFY_MOBILE = @"mobile_check.php";
NSString * const API_LOGIN_USER = @"login.php";
NSString * const API_UPDATE_PROFILE = @"update_profile.php";
NSString * const API_UPDATE_USER_MODE = @"update_login_roll.php";
NSString * const API_ADD_FAV_SPOT = @"add_favourite_spots.php";
NSString * const API_FAV_SPOT_LIST = @"favourite_spots_list.php";
NSString * const API_DELETE_FAV_LOCATION = @"delete_favourite_spots.php";
NSString * const API_UPDATE_TOKEN = @"device_register.php";
NSString * const API_FORGET_PASSWORD = @"forgot_password.php";
NSString * const API_PESSANGER_LOCATION = @"passanger_currentlocation.php";
NSString * const API_DRIVER_LOCATION = @"driver_currentlocation.php";
NSString * const API_CHANGE_PASSWORD = @"change_password.php";
NSString * const API_NOTIFICATIONS_LIST = @"notifications_list.php";
NSString * const API_UPDATE_SETTINGS = @"update_settings.php";
NSString * const API_FAQ_LIST = @"faq.php";
NSString * const API_REDEEM_WALLET = @"redeem_wallet.php";
NSString * const API_CLEAR_BADGE = @"notifications_read.php";
NSString * const API_SEND_RIDE_REQUEST = @"ride_request.php";
NSString * const API_VERIFY_OTP = @"verify_otp.php";
NSString * const API_UPDATE_RIDE_STATUS = @"ride_update_status.php";
NSString * const API_CONFIRM_RIDE = @"confirm_ride.php";
NSString * const API_CANCEL_REASONS = @"cancellation_reasons.php";
NSString * const API_SUBMIT_PAYMENT = @"payment.php";
NSString * const API_SUBMIT_FEEDBACK = @"add_feedback.php";
NSString * const API_PLAN_A_RIDE = @"plan_a_ride.php";
NSString * const API_DISCUSSION_BOARD = @"discussion_board.php";
NSString * const API_FAVOITE_USERS = @"favourite_user_list.php";
NSString * const API_RIDE_HISTORY = @"trip_history.php";
NSString * const API_DELETE_RIDE = @"delete_ride.php";
NSString * const API_ADD_FAVORUITE_USER = @"add_user_as_favourite.php";
NSString * const API_ADD_BLOCK_USER = @"block_onblock_user.php";
NSString * const API_ADD_MONEY_WALLET = @"add_amount_in_wallet.php";
NSString * const API_CMS_INFO = @"cms.php";
NSString * const API_AUTHORIZED_TRANSACTION = @"auth_net.php";
NSString * const API_LOGOUT_USER = @"logout.php";
NSString * const API_SOS_EMERGANCY = @"sos.php";
NSString * const API_VIEW_SETTINGS = @"view_settings.php";
NSString * const API_SHARE_RIDE = @"share_my_info.php";

