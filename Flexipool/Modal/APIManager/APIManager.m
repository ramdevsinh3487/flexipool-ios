//
//  APIManager.m
//  VCab
//
//  Created by Vishal Gohil on 25/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "APIManager.h"
#import "defines.h"


@implementation APIManager

-(instancetype)init{
    manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    return self;
}

#pragma mark - Google API Corner
-(void)findAllRoutesBeteweenTwoPlaces:(CLLocationCoordinate2D)originPlace destination:(CLLocationCoordinate2D)destination withCallBack:(void (^) (int etaSeconds, float distanceKM, NSString * formattedETA, NSString * formatedDistnace, NSString * polylinePoints, NSArray * allRoutes))callback {
    NSDictionary * dictPera;
    if (NAVIGATION_MODE){
        
        NSString * avoids = @"";
        
        if (APP_DELEGATE.canAvoidTolls){
            avoids = @"tolls";
        }
        
        if (APP_DELEGATE.canAvoidTolls){
            if ([avoids isEqualToString:@""]){
                avoids = @"highways";
            } else {
                avoids = [NSString stringWithFormat:@"%@|highways", avoids];
            }
        }
        
        dictPera = @{
                     @"origin":[NSString stringWithFormat:@"%f,%f", originPlace.latitude, originPlace.longitude],
                     @"destination":[NSString stringWithFormat:@"%f,%f", destination.latitude, destination.longitude],
                     @"key":GOOGLE_DIRECTION_API_KEY,
                     @"alternatives":NAVIGATION_MODE ? @"true" : @"false",
                     @"mode":@"driving",
                     @"units":@"imperial",
                     @"avoid":avoids
                     };
    } else {
        dictPera = @{
                     @"origin":[NSString stringWithFormat:@"%f,%f", originPlace.latitude, originPlace.longitude],
                     @"destination":[NSString stringWithFormat:@"%f,%f", destination.latitude, destination.longitude],
                     @"key":GOOGLE_DIRECTION_API_KEY,
                     @"mode":@"driving",
                     };
    }
    
    NSString * serverURL = @"https://maps.googleapis.com/maps/api/directions/json?";
    
    [manager GET:serverURL parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSDictionary* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSArray *routesArray = [response objectForKey:@"routes"];
        if (routesArray != nil && routesArray.count > 0){
            NSString * formatedDistance = @"";
            NSString * formatedETA = @"";
            NSString *points = @"";
            float distance = 0.0;
            int totalSeconds = 0;
            
            if (NAVIGATION_MODE){
                for (NSDictionary *routeDict in routesArray) {
                    NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                    points = [routeOverviewPolyline objectForKey:@"points"];
                    
                    distance = [[routeDict objectForKey:@"legs"][0][@"distance"][@"value"]floatValue];
                    totalSeconds = [[routeDict objectForKey:@"legs"][0][@"duration"][@"value"] intValue];
                    
                    formatedDistance = [routeDict objectForKey:@"legs"][0][@"distance"][@"text"];
                    formatedETA = [routeDict objectForKey:@"legs"][0][@"duration"][@"text"];
                }
                
                callback(totalSeconds, distance, formatedETA, formatedDistance, points, routesArray);
            } else {
                for (NSDictionary *routeDict in routesArray) {
                    NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                    points = [routeOverviewPolyline objectForKey:@"points"];
                    
                    distance = [[routeDict objectForKey:@"legs"][0][@"distance"][@"value"]floatValue];
                    totalSeconds = [[routeDict objectForKey:@"legs"][0][@"duration"][@"value"] intValue];
                    
                    formatedDistance = [routeDict objectForKey:@"legs"][0][@"distance"][@"text"];
                    formatedETA = [routeDict objectForKey:@"legs"][0][@"duration"][@"text"];
                }
                
                callback(totalSeconds, distance, formatedETA, formatedDistance, points, @[]);
            }
        } else {
            callback(0, 0, @"0 min away", @"0 KM", @"", @[]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(0, 0, @"0 min", @"0 KM", @"", @[]);
    }];
}

-(void)findAddressUsingLocationCoordinate:(CLLocationCoordinate2D)location withCallBack:(void (^)(BOOL success, NSDictionary * result, NSString * error))callback{
    NSString * serverURL = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f",location.latitude, location.longitude];
    
    [manager GET:serverURL parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSDictionary* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        
        NSDictionary * dictAddressInfo = ((NSArray *)response[@"results"]).firstObject;
        NSString * title =  (((NSArray *)dictAddressInfo[@"address_components"]).firstObject)[@"long_name"];
        
        if ([title length] < 5){
            NSString *title2 = (((NSArray *)dictAddressInfo[@"address_components"])[1])[@"long_name"];
            if (title2 && ![title2 isEqualToString:@""]){
                title = [NSString stringWithFormat:@"%@ %@",title,title2];
            }
        }
        
        NSString * fullAddress = dictAddressInfo[@"formatted_address"];
        
        if (title && ![title isEqualToString:@""] && fullAddress && ![fullAddress isEqualToString:@""]){
            callback(true, @{@"title":title, @"fullAddress":fullAddress}, nil);
        } else {
            callback(false, nil, @"No Address Founds");
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)shouldGetNearByPlacesForLocation:(CLLocationCoordinate2D)location withCallBack:(void (^) (BOOL success, NSArray * nearbyPlaces))callback{
    NSDictionary * dictPera = @{
                                @"location":[NSString stringWithFormat:@"%f,%f", location.latitude, location.longitude],
                                @"rankby":@"distance",
                                @"mode":@"driving",
                                @"key":GOOGLE_API_KEY
                                };
    NSString * serverURL = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json";
    
    
    [manager GET:serverURL parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSDictionary* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSArray *routesArray = [response objectForKey:@"results"];
#define ARC4RANDOM_MAX      0x100000000
        NSMutableArray *  arrayTorespnse = [[NSMutableArray alloc]init];
        int i = 0;
        for (NSDictionary * dictData in routesArray) {
            i = i + 1;
            
            if (i % 2 == 0){
                NSDictionary * dictToAdd;
                int val = (round)((double)arc4random() / ARC4RANDOM_MAX);
                int isFavorite = (round)((double)arc4random() / ARC4RANDOM_MAX);
                if (val == 0){
                    
                    dictToAdd = @{
                                  @"lattitude":dictData[@"geometry"][@"location"][@"lat"],
                                  @"longitude":dictData[@"geometry"][@"location"][@"lng"],
                                  @"gender":@"female",
                                  @"favorite":isFavorite == 1 ? @"true":@"false",
                                  @"address":dictData[@"vicinity"]
                                  };
                    
                    
                } else {
                    dictToAdd = @{
                                  @"lattitude":dictData[@"geometry"][@"location"][@"lat"],
                                  @"longitude":dictData[@"geometry"][@"location"][@"lng"],
                                  @"gender":@"male",
                                  @"favorite":isFavorite == 1 ? @"true":@"false",
                                  @"address":dictData[@"vicinity"]
                                  };
                }
                [arrayTorespnse addObject:dictToAdd];
            }
            
            if (arrayTorespnse.count >= 5){
                break;
            }
        }
        
        if (DEBUG_MODE){
            NSLog(@"Data : %@",arrayTorespnse);
        }
        
        if (arrayTorespnse != nil && arrayTorespnse.count > 0){
            callback(true, arrayTorespnse);
        } else {
            callback(false, nil);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil);
    }];
}

#pragma mark - User API Corner
-(void)registerUserWithFirstName:(NSString *)firstName LastName:(NSString *)lastName CountryCode:(NSString *)countryCode mobileNumber:(NSString *)mobileNumber Password:(NSString *)password Gender:(NSString *)gender DateOfBirth:(NSString *)birthDate UserType:(NSString *)userType MetroArea:(NSString *)metroArea SSNNumber:(NSString *)ssnNumber UserEmail:(NSString *)email allowedRiders:(NSString *)allowedRiders vehicleModel:(NSString *)Vehiclemodel VehicleYear:(NSString *)vehicleYear VehicleState:(NSString *)vehicleState VehiclePlate:(NSString *)vehiclePlate withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse, NSString * errorMessage))callback{

    NSDictionary * dictPara = @{
                                @"first_name":firstName,
                                @"last_name":lastName,
                                @"country_code":countryCode,
                                @"mobile_number":mobileNumber,
                                @"password":password,
                                @"gender":gender,
                                @"date_of_birth":birthDate,
                                @"user_type":userType,
                                @"metro_area":metroArea,
                                @"ssn_number":ssnNumber,
                                @"email":email,
                                @"allow_riders":allowedRiders,
                                @"car_model":Vehiclemodel,
                                @"vehicle_year":vehicleYear,
                                @"vehicle_plate_no":vehiclePlate,
                                @"vehicle_state":vehicleState
                                };
    
    if (DEBUG_MODE) {
        NSLog(@"DIctPAra : %@", dictPara);
    }
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_REGISTER_USER] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Register User Response : %@",dictResponse);
        }
        

        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"][0], nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
        }
        else if ([strCode integerValue] == -3) {
            callback(false, nil, @"You can not sign into more then one device with same Mobile No.");
        }
        else
        {
            callback(false, nil, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)verifyUserEneteredMobile:(NSString *)mobileNumber CountryCode:(NSString *)countryCode withCallBack:(void (^) (BOOL success, NSString * message))callback {
    NSDictionary * dictPara = @{
                               @"mobile_number":mobileNumber
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_VERIFY_MOBILE] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"VERIFY MOBILE Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)loginTheUserWithCountryCode:(NSString *)countryCode MobileNumber:(NSString *)mobileNumber Password:(NSString *)password withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse, NSString * errorMessage))callback{
    
    NSDictionary * dictPara = @{
                                @"country_code":countryCode,
                                @"mobile_number":mobileNumber,
                                @"password":password,
                                @"device_token":[[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_PUSH_TOKEN]
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_LOGIN_USER] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Login User Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"][0], nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)updateUserProfileWithUserName:(NSString *)userName UserProfession:(NSString *)userProfession DateOfBirth:(NSString *)birthDate MetroArea:(NSString *)metroArea MobileNumber:(NSString *)mobileNumber gender:(NSString *)gender EmergancyMobile:(NSString *)emergancyMobile PrefersRide:(NSString *)preferRide AllowedRiders:(NSString *)allowedRiders VehcleModel:(NSString *)vehicleModel VehicleYear:(NSString *)vehicleYear VehiclePlate:(NSString *)vehiclePlate vehicleState:(NSString *)vehicleState UserImageName:(NSString *)userImageName commute:(NSString *)strCommuteTo withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback {
    
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"f_name":userName,
                                @"l_name":@"",
                                @"profession":userProfession,
                                @"dob":birthDate,
                                @"metro_area":metroArea,
                                @"mobile_no":mobileNumber,
                                @"gender":gender,
                                @"prefers_ride":preferRide,
                                @"allow_rider":allowedRiders,
                                @"emergecy_mobile_number":emergancyMobile,
                                @"vehicle_model":vehicleModel,
                                @"vehicle_year":vehicleYear,
                                @"vehicle_plate_no":vehiclePlate,
                                @"vehicle_state":vehicleState,
                                @"image":userImageName,
                                @"token":APP_DELEGATE.tokenString,
                                @"commute_to" : strCommuteTo
                                };
    
    
    [manager POST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_UPDATE_PROFILE] parameters:dictPara constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
//        if (imageData){
//            [formData appendPartWithFileData:imageData name:@"image" fileName:[NSString stringWithFormat:@"USRPRFL_%@%@.jpg", APP_DELEGATE.loggedUserId, [[Singleton sharedInstance] getCurrentDate]] mimeType:@"image/jpeg"];
//        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (DEBUG_MODE){
            NSLog(@"Progress : %@",uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update profile will response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"][0], nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil,error.localizedDescription);
    }];
}

-(void)recoverUserPasswordWithUserEmailId:(NSString *)userEmailAddress withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"email":userEmailAddress
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_FORGET_PASSWORD] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Forget Password Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)updateUserModeWithIsModeDriver:(BOOL)isModeDriver withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback{
    
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"login_roll": isModeDriver ? @"2" : @"1",
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_UPDATE_USER_MODE] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Update User Role Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)addNewFavoriteSpotWithFullAddress:(NSString *)fullAddress LocationLattitude:(NSString *)lattitude Longitude:(NSString *)longitude Name:(NSString *)name withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"address":fullAddress,
                                @"latitude":lattitude,
                                @"longitude":longitude,
                                @"address_name":name,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_ADD_FAV_SPOT] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Add Favorite Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)getFavoriteSpotsListWithPageNumber:(NSString *)page withCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int recordCount, NSString * serverMessange))callback{
   
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"page_no": page,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_FAV_SPOT_LIST] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Favorite List Response : %@",dictResponse);
        }
        int count = [dictResponse[@"total_count"] intValue];
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"], count, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, 0, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil, 0, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, 0, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, 0, error.localizedDescription);
    }];
}

-(void)deleteLocationFromFavoriteSpotsListWithLocationId:(NSString *)locationId withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback{
    //?user_id=1&address_id=1&token=UyJg7T
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"address_id": locationId,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_DELETE_FAV_LOCATION] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Delete Favorite Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)upfateDeviceTokenWithToken:(NSString *)deviceToken withCallBack:(void (^) (BOOL success, NSString * messange))callback{
    
    NSDictionary * dictPera = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"device_type":@"1",
                                @"device_id":[[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_PUSH_TOKEN],
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager POST:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_UPDATE_TOKEN] parameters:dictPera progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        NSArray* response = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = response.firstObject;
        if (DEBUG_MODE){
            NSLog(@"Update device token will response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)updateCustomerCurrentLocationWithUserLocationLattitude:(NSString *)userLattitude UserLongitude:(NSString *)userLongitude PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude withCallBack:(void (^) (BOOL success, NSArray * arrayResponse ,NSString * serverMessange))callback {
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"location_lattitude":pickupLattitude,
                                @"location_longitude":pickupLongitude,
                                @"temp_location_lattitude":userLattitude,
                                @"temp_location_longitude":userLongitude,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_PESSANGER_LOCATION] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            
        }
        
        NSLog(@"Update Current Location Response : %@",dictResponse);
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        NSString * message = dictResponse[@"message"];
        
        if ([strCode integerValue] == 1 || ([strCode integerValue] == 0 && [message.lowercaseString containsString:@"success"])) {
            callback(true, arrayServerResponse, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [Singleton showSnakBar:@"Your account is inactive" multiline:true];
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [Singleton showSnakBar:@"You can not sign into more then one device with same Mobile No." multiline:true];
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)updateDriverCurrentLocationWithUserLocationLattitude:(NSString *)userLattitude UserLongitude:(NSString *)userLongitude PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude withCallBack:(void (^) (BOOL success, NSArray * arrayResponse ,NSString * serverMessange))callback {
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"location_lattitude":pickupLattitude,
                                @"location_longitude":pickupLongitude,
                                @"temp_location_lattitude":userLattitude,
                                @"temp_location_longitude":userLongitude,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_DRIVER_LOCATION] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            
        }
        
        NSLog(@"Update Current Location Response : %@",dictResponse);
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        NSString * message = dictResponse[@"message"];
        
        if ([strCode integerValue] == 1 || ([strCode integerValue] == 0 && [message.lowercaseString containsString:@"success"])) {
            callback(true, arrayServerResponse, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [Singleton showSnakBar:@"Your account is inactive" multiline:true];
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [Singleton showSnakBar:@"You can not sign into more then one device with same Mobile No." multiline:true];
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)updateUserCurrentLocationWithUserLocationLattitude:(NSString *)userLattitude UserLongitude:(NSString *)userLongitude withCallBack:(void (^) (BOOL success, NSArray * arrayResponse ,NSString * serverMessange))callback {
    
//    temp_location_lattitude=23.064997&temp_location_longitude=72.556602
    
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"location_lattitude":userLattitude,
                                @"location_longitude":userLongitude,
                                @"token":APP_DELEGATE.tokenString,
                                @"temp_location_lattitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.latitude],
                                @"temp_location_longitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.longitude]
                                };
    
    NSString * currentLocationURL = APP_DELEGATE.isUserModeDriver ? API_DRIVER_LOCATION : API_PESSANGER_LOCATION;
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,currentLocationURL] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            
        }
        
        NSLog(@"Update Current Location Response : %@",dictResponse);
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        NSString * message = dictResponse[@"message"];
        
        if ([strCode integerValue] == 1 || ([strCode integerValue] == 0 && [message.lowercaseString containsString:@"success"])) {
            callback(true, arrayServerResponse, nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [Singleton showSnakBar:@"Your account is inactive" multiline:true];
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [Singleton showSnakBar:@"You can not sign into more then one device with same Mobile No." multiline:true];
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)updateUserPasswordWithOldPassword:(NSString *)oldPassword NewPassword:(NSString *)newPassword withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback {
   
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"old_password":oldPassword,
                                @"new_password":newPassword,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_CHANGE_PASSWORD] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Change Password Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)getMyAllNotificationsListWithPageNumber:(NSString * )pageNumber WithCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int totalRecords,NSString * serverMessange))callback {

    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"page_no":pageNumber,
                                @"login_roll":APP_DELEGATE.isUserModeDriver ? @"2" : @"1",
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_NOTIFICATIONS_LIST] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Notifications list response : %@",dictResponse);
        }
        int totalCounts = [dictResponse[@"total_count"] intValue];
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"], totalCounts,dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, 0, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil, 0,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, 0,dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, 0,error.localizedDescription);
    }];
}

-(void)updateMyAllSettingsWithOffProfileDate:(NSString *)offProfileDate notificationType:(NSString *)notificationsType isNotificationOn:(NSString *)isNotificationOn OffProfileType:(NSString *)offProfileType withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback{
    
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"off_profile_date":offProfileDate,
                                @"notification_type":notificationsType,
//                                @"is_notification_on":isNotificationOn,
                                @"off_profile_type":offProfileType,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_UPDATE_SETTINGS] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Update settings response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)listFAQuestionsForReferanceWithPageNumber:(NSString *)pageNumber withCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int recordCount, NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"page_no":pageNumber,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_FAQ_LIST] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"List FAQ response : %@",dictResponse);
        }
        int count = [dictResponse[@"total_count"] intValue];
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"], count,dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, 0, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil, 0, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, 0, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, 0, error.localizedDescription);
    }];
}

-(void)getCMSRelatedInformationWithCMSId:(NSString *)cmsId withCallBack:(void (^) (BOOL success, NSArray * arrayResponse,NSString * serverMessange))callback {
    NSDictionary * dictPara = @{
                                @"cms_id":cmsId
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_CMS_INFO] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"CMS API response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"],dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)logoutTheLoggedUserFromTheAppWithCallBack:(void (^) (BOOL success, NSString * serverMessange))callback{
//    http://ec2-34-231-167-130.compute-1.amazonaws.com/api/logout.php?user_id=1&token=3VsnfG
    
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_LOGOUT_USER] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"List FAQ response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
    
}

-(void)fireEmergencySOSWithRideId:(NSString *)rideId emergencyMobile:(NSString *)emetgencyMobie withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"ride_id":rideId,
                                @"login_roll":APP_DELEGATE.isUserModeDriver ? @"2" : @"1",
                                @"emergency_contact_no":emetgencyMobie,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_SOS_EMERGANCY] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"List FAQ response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)viewAllMySettingsWithCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_VIEW_SETTINGS] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"View My Settings will response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"][0], nil);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)redeemAhoyWalletWithWalletAmount:(NSString *)walletAmount CallBack:(void (^) (BOOL success,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"amount":walletAmount,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_REDEEM_WALLET] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"View My Settings will response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, @"Successfully redeem");
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)clearTheNotificationBadgeWithCallBack:(void (^) (BOOL success,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_CLEAR_BADGE] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"View My Settings will response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}



#pragma mark - Ride Control Center
-(void)sendRideRequestWithReciverUserId:(NSString *)reciverId PickupTitle:(NSString *)pickupTitle PickupAddress:(NSString *)pickupAddress PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude DropTitle:(NSString *)dropTitle DropAddress:(NSString *)dropAddress DropLattitude:(NSString *)dropLattitude DropLongitude:(NSString *)dropLongitude PickupDateTime:(NSString *)pikcupTime TripDistance:(NSString *)distance TripDuation:(NSString *)duration TripCost:(NSString *)tripCost withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback{
    
    NSDictionary * dictPara = @{
                                @"user_id": APP_DELEGATE.loggedUserId,
                                @"pickup_address":pickupAddress,
                                @"drop_address":dropAddress,
                                @"pickup_lat":pickupLattitude,
                                @"pickup_log":pickupLongitude,
                                @"drop_lat":dropLattitude,
                                @"drop_log":dropLongitude,
                                @"pickup_date_time":pikcupTime,
                                @"trip_km":distance,
                                @"trip_duration":duration,
                                @"reciver_id":reciverId,
                                @"ride_cost":tripCost,
                                @"pickup_title":pickupTitle,
                                @"drop_title":dropTitle,
                                @"login_roll":APP_DELEGATE.isUserModeDriver ? @"2" : @"1",
                                @"token":APP_DELEGATE.tokenString
                                };

    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_SEND_RIDE_REQUEST] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Send Request Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse,dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)verifyOTPConfirmaionNumberWithNumber:(NSString *)number withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"ride_id":APP_DELEGATE.runningRideId,
                                @"confirmation_number":number,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_VERIFY_OTP] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Update Ride Status Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse,dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
    
//    ?
}

-(void)updateRideStatusWithRideId:(NSString *)rideId RideStatus:(NSString *)rideStatus PessangerId:(NSString *)pessangerId DriverId:(NSString *)driverId CancelReasonId:(NSString *)cancelReasonId TotalKm:(NSString *)totalKm withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback{

    NSDictionary * dictPara = @{
                                @"customer_user_id": pessangerId,
                                @"driver_id":driverId,
                                @"ride_id":rideId,
                                @"ride_status":rideStatus,
                                @"user_type":APP_DELEGATE.isUserModeDriver ? @"2" : @"1",
                                @"ride_cancel_reason":cancelReasonId,
                                @"trip_km":totalKm,
                                @"token":APP_DELEGATE.tokenString
                                };
    
     NSLog(@"%@",[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_UPDATE_RIDE_STATUS]);
    NSLog(@"%@",dictPara);
   
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_UPDATE_RIDE_STATUS] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Update Ride Status Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse,dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)confirmUpdatedStatusWithRideId:(NSString *)rideId RideStatus:(NSString *)rideStatus withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback {
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"ride_id":rideId,
                                @"ride_status":rideStatus,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_CONFIRM_RIDE] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Confirm Ride Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse,dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)getAllCancelationReasonsWithPageNumber:(NSString *)pageNumber withCallBack:(void (^) (BOOL success, NSArray * arrayResponse,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"page_no":pageNumber,
                                @"user_type":APP_DELEGATE.isUserModeDriver ? @"2" : @"1",
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_CANCEL_REASONS] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Cancel Reasons Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"],dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)submitThePaymentForCostOfTheRideWithRideId:(NSString *)rideId paymentType:(NSString *)paymentTypes PayableAmount:(NSString *)payableAmount withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback{
    
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"ride_id":rideId,
                                @"payment_type":paymentTypes,
                                @"payment_amount":payableAmount,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_SUBMIT_PAYMENT] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Submit payment response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)submitTheUserFeedbackWithReceiverId:(NSString *)receiverID RideId:(NSString *)rideId RateSymbolType:(NSString *)symbolType WhatWrongType:(NSString *)wrongType FeedbackMessage:(NSString *)feedbackMessage isBlock:(NSString *)isBlockUser isFavoriteUser:(NSString *)isFavoriteUser withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback{
  
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"feedback_user_id":receiverID,
                                @"ride_id":rideId,
                                @"symbol_type":symbolType,
                                @"whats_wrong":wrongType,
                                @"feedback":feedbackMessage,
                                @"is_block":isBlockUser,
                                @"is_favourite":isFavoriteUser,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_SUBMIT_FEEDBACK] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Submit Feedback response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)planARideWithUserID:(NSString *)UserID pickup_location:(NSString *)pickup_location drop_off_location:(NSString *)drop_off_location pickup_latitude:(NSString *)pickup_latitude pickup_longitude:(NSString *)pickup_longitude drop_off_latitude:(NSString *)drop_off_latitude drop_off_longitude:(NSString *)drop_off_longitude pickup_date:(NSString *)pickup_date pickup_time:(NSString *)pickup_time request_type:(NSString *)request_type withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback {
    
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"pickup_location" : pickup_location,
                                @"drop_off_location" : drop_off_location,
                                @"pickup_latitude" : pickup_latitude,
                                @"pickup_longitude" : pickup_longitude,
                                @"drop_off_latitude" : drop_off_latitude,
                                @"drop_off_longitude" : drop_off_longitude,
                                @"pickup_date" : pickup_date,
                                @"pickup_time" : pickup_time,
                                @"request_type" : request_type,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_PLAN_A_RIDE] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Plan a ride response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)getDiscussionBoardDataWithSearchDate:(NSString *)searchableDate SearchUserId:(NSString *)searchableUserId PageNumber:(NSString *)pageNo post:(NSString *)strMyPost withCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int totalItems, NSString * serverMessange))callback{

    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"search_date":searchableDate,
                                @"search_user":searchableUserId,
                                @"page_no":pageNo,
                                @"token":APP_DELEGATE.tokenString,
                                @"is_my_post" : strMyPost
                                };
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_DISCUSSION_BOARD] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Discussion Board Response : %@",dictResponse);
        }
        
        int itemsCount = [dictResponse[@"total_count"] intValue];
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"], itemsCount, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, 0, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil, 0,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, 0, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, 0, error.localizedDescription);
    }];
}

-(void)getMyAllFavoriteUsersListWithCallBack:(void (^) (BOOL success, NSArray * arrayResponse,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_FAVOITE_USERS] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Favoruite Users list response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"],dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)getRideHistoryDetailsWithCallBack:(void (^) (BOOL success, NSArray * arrayResponse,NSString * serverMessange))callback{
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"token":APP_DELEGATE.tokenString,
                                @"user_type":APP_DELEGATE.isUserModeDriver ? @"2" : @"1"
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_RIDE_HISTORY] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Ride Histroey Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"result"],dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, nil, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, nil, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, nil, error.localizedDescription);
    }];
}

-(void)deleteTheRideInfoFromRideHistoryWithRideId:(NSString *)rideId IsModeDeleteAll:(BOOL)isModeDeleteAll withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback {

    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"ride_id":rideId,
                                @"delete_all":isModeDeleteAll ? @"1" : @"0",
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_DELETE_RIDE] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Delete Ride Response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)addOrRemoveUserFromMyFavoruiteListWithUserId:(NSString *)userId IsModeFavoruite:(BOOL)isModeFavoruite withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback {

    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"favorite_user_id":userId,
                                @"is_favourite":isModeFavoruite ? @"1" : @"0",
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_ADD_FAVORUITE_USER] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Add / Remove Favoruite response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)addOrRemoveUserFromMyBlockedUserListWithUserId:(NSString *)userId IsModeBlock:(BOOL)isModeBlock withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback {
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"block_user_id":userId,
                                @"is_block":isModeBlock ? @"1" : @"0",
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_ADD_BLOCK_USER] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Block unblock response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

-(void)addMoneyToMyWallerWithAmountToAdd:(NSString *)amount withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback {
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"amount":amount,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_ADD_MONEY_WALLET] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Add money response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}

// Vishal D
-(void)AuthorizedBankTransaction:(NSDictionary *)dict withCallBack:(void (^) (BOOL success, NSMutableArray * response, NSString * errorMessage))callback{
    
    
    NSLog(@"%@",[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_AUTHORIZED_TRANSACTION]);
    NSLog(@"%@",dict);
    
    START_HUD
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_AUTHORIZED_TRANSACTION] parameters:dict progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        
        
        NSError *jsonParserError;
        NSMutableArray* dictResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        
        if (DEBUG_MODE){
            NSLog(@"Aurthorized.net response : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"code"] objectAtIndex:0]];
        if ([strCode integerValue] == -2) {
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, nil,@"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        }
        else if ([strCode integerValue] == 0) {
            callback(false, nil,@"Oops..!! Try another card/payment method");
           
        }
        else
        {
            callback(true, dictResponse, nil);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(true, nil, error.localizedDescription);
    }];
}

-(void)didShareMyRideInfoWithRideId:(NSString *)rideId ContactNumber:(NSString *)contactNumber withCallBack:(void (^) (BOOL success, NSString * serverMessage))callback {
    
    NSString * phone = [contactNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSDictionary * dictPara = @{
                                @"user_id":APP_DELEGATE.loggedUserId,
                                @"ride_id":rideId,
                                @"contact_no":phone,
                                @"token":APP_DELEGATE.tokenString
                                };
    
    START_HUD
    [manager GET:[NSString stringWithFormat:@"%@%@",API_BASE_URL,API_SHARE_RIDE] parameters:dictPara progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSError *jsonParserError;
        
        NSMutableArray* arrayServerResponse = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&jsonParserError];
        NSDictionary * dictResponse = arrayServerResponse[0];
        if (DEBUG_MODE){
            NSLog(@"Share My Ride : %@",dictResponse);
        }
        
        NSString *strCode = [NSString stringWithFormat:@"%@",[dictResponse objectForKey:@"code"]];
        if ([strCode integerValue] == 1) {
            callback(true, dictResponse[@"sms_message"]);
        } else if ([strCode integerValue] == -2) {
            callback(false, @"Your account is inactive.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else if ([strCode integerValue] == -3) {
            callback(false, @"You can not sign into more then one device with same Mobile No.");
            [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
        } else {
            callback(false, dictResponse[@"message"]);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callback(false, error.localizedDescription);
    }];
}


#pragma mark End
@end
