//
//  APIManager.h
//  VCab
//
//  Created by Vishal Gohil on 25/05/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

#import <AFNetworking.h>

@interface APIManager : NSObject {
    AFHTTPSessionManager *manager;
}

#pragma mark - Google API Corner
-(void)findAllRoutesBeteweenTwoPlaces:(CLLocationCoordinate2D)originPlace destination:(CLLocationCoordinate2D)destination withCallBack:(void (^) (int etaSeconds, float distanceKM, NSString * formattedETA, NSString * formatedDistnace, NSString * polylinePoints, NSArray * allRoutes))callback;

-(void)findAddressUsingLocationCoordinate:(CLLocationCoordinate2D)location withCallBack:(void (^)(BOOL success, NSDictionary * result, NSString * error))callback;

-(void)shouldGetNearByPlacesForLocation:(CLLocationCoordinate2D)location withCallBack:(void (^) (BOOL success, NSArray * nearbyPlaces))callback;


#pragma mark - User API Corner
-(void)registerUserWithFirstName:(NSString *)firstName LastName:(NSString *)lastName CountryCode:(NSString *)countryCode mobileNumber:(NSString *)mobileNumber Password:(NSString *)password Gender:(NSString *)gender DateOfBirth:(NSString *)birthDate UserType:(NSString *)userType MetroArea:(NSString *)metroArea SSNNumber:(NSString *)ssnNumber UserEmail:(NSString *)email allowedRiders:(NSString *)allowedRiders vehicleModel:(NSString *)Vehiclemodel VehicleYear:(NSString *)vehicleYear VehicleState:(NSString *)vehicleState VehiclePlate:(NSString *)vehiclePlate withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse, NSString * errorMessage))callback;

-(void)verifyUserEneteredMobile:(NSString *)mobileNumber CountryCode:(NSString *)countryCode withCallBack:(void (^) (BOOL success, NSString * message))callback;



-(void)loginTheUserWithCountryCode:(NSString *)countryCode MobileNumber:(NSString *)mobileNumber Password:(NSString *)password withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse, NSString * errorMessage))callback;

-(void)updateUserProfileWithUserName:(NSString *)userName UserProfession:(NSString *)userProfession DateOfBirth:(NSString *)birthDate MetroArea:(NSString *)metroArea MobileNumber:(NSString *)mobileNumber gender:(NSString *)gender EmergancyMobile:(NSString *)emergancyMobile PrefersRide:(NSString *)preferRide AllowedRiders:(NSString *)allowedRiders VehcleModel:(NSString *)vehicleModel VehicleYear:(NSString *)vehicleYear VehiclePlate:(NSString *)vehiclePlate vehicleState:(NSString *)vehicleState UserImageName:(NSString *)userImageName commute:(NSString *)strCommuteTo withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback;

-(void)updateUserModeWithIsModeDriver:(BOOL)isModeDriver withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback;

-(void)addNewFavoriteSpotWithFullAddress:(NSString *)fullAddress LocationLattitude:(NSString *)lattitude Longitude:(NSString *)longitude Name:(NSString *)name withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback;

-(void)getFavoriteSpotsListWithPageNumber:(NSString *)page withCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int recordCount,NSString * serverMessange))callback;

-(void)deleteLocationFromFavoriteSpotsListWithLocationId:(NSString *)locationId withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback;

-(void)recoverUserPasswordWithUserEmailId:(NSString *)userEmailAddress withCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback;

-(void)upfateDeviceTokenWithToken:(NSString *)deviceToken withCallBack:(void (^) (BOOL success, NSString * messange))callback;

//-(void)updateUserCurrentLocationWithUserLocationLattitude:(NSString *)userLattitude UserLongitude:(NSString *)userLongitude withCallBack:(void (^) (BOOL success, NSArray * arrayResponse ,NSString * serverMessange))callback;

-(void)updateCustomerCurrentLocationWithUserLocationLattitude:(NSString *)userLattitude UserLongitude:(NSString *)userLongitude PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude withCallBack:(void (^) (BOOL success, NSArray * arrayResponse ,NSString * serverMessange))callback;

-(void)updateDriverCurrentLocationWithUserLocationLattitude:(NSString *)userLattitude UserLongitude:(NSString *)userLongitude PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude withCallBack:(void (^) (BOOL success, NSArray * arrayResponse ,NSString * serverMessange))callback;

-(void)updateUserPasswordWithOldPassword:(NSString *)oldPassword NewPassword:(NSString *)newPassword withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)getMyAllNotificationsListWithPageNumber:(NSString * )pageNumber WithCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int totalRecords,NSString * serverMessange))callback;

-(void)updateMyAllSettingsWithOffProfileDate:(NSString *)offProfileDate notificationType:(NSString *)notificationsType isNotificationOn:(NSString *)isNotificationOn OffProfileType:(NSString *)offProfileType withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)listFAQuestionsForReferanceWithPageNumber:(NSString *)pageNumber withCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int recordCount, NSString * serverMessange))callback;

-(void)getCMSRelatedInformationWithCMSId:(NSString *)cmsId withCallBack:(void (^) (BOOL success, NSArray * arrayResponse,NSString * serverMessange))callback;

-(void)addOrRemoveUserFromMyFavoruiteListWithUserId:(NSString *)userId IsModeFavoruite:(BOOL)isModeDeleteAll withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)addOrRemoveUserFromMyBlockedUserListWithUserId:(NSString *)userId IsModeBlock:(BOOL)isModeBlock withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)addMoneyToMyWallerWithAmountToAdd:(NSString *)amount withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)AuthorizedBankTransaction:(NSDictionary *)dict withCallBack:(void (^) (BOOL success, NSMutableArray * response, NSString * errorMessage))callback;

-(void)logoutTheLoggedUserFromTheAppWithCallBack:(void (^) (BOOL success, NSString * serverMessange))callback;

-(void)fireEmergencySOSWithRideId:(NSString *)rideId emergencyMobile:(NSString *)emetgencyMobie withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)viewAllMySettingsWithCallBack:(void (^) (BOOL success, NSDictionary * dictResponse ,NSString * serverMessange))callback;

-(void)redeemAhoyWalletWithWalletAmount:(NSString *)walletAmount CallBack:(void (^) (BOOL success,NSString * serverMessange))callback;

-(void)clearTheNotificationBadgeWithCallBack:(void (^) (BOOL success,NSString * serverMessange))callback;

#pragma mark - Ride Control Center
-(void)sendRideRequestWithReciverUserId:(NSString *)reciverId PickupTitle:(NSString *)pickupTitle PickupAddress:(NSString *)pickupAddress PickupLattitude:(NSString *)pickupLattitude PickupLongitude:(NSString *)pickupLongitude DropTitle:(NSString *)dropTitle DropAddress:(NSString *)dropAddress DropLattitude:(NSString *)dropLattitude DropLongitude:(NSString *)dropLongitude PickupDateTime:(NSString *)pikcupTime TripDistance:(NSString *)distance TripDuation:(NSString *)duration TripCost:(NSString *)tripCost withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback;

-(void)verifyOTPConfirmaionNumberWithNumber:(NSString *)number withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback;

-(void)updateRideStatusWithRideId:(NSString *)rideId RideStatus:(NSString *)rideStatus PessangerId:(NSString *)pessangerId DriverId:(NSString *)driverId CancelReasonId:(NSString *)cancelReasonId TotalKm:(NSString *)totalKm withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback;

-(void)confirmUpdatedStatusWithRideId:(NSString *)rideId RideStatus:(NSString *)rideStatus withCallBack:(void (^) (BOOL success, NSDictionary* dictResponse,NSString * serverMessange))callback;

-(void)getAllCancelationReasonsWithPageNumber:(NSString *)pageNumber withCallBack:(void (^) (BOOL success, NSArray * arrayResponse,NSString * serverMessange))callback;

-(void)submitThePaymentForCostOfTheRideWithRideId:(NSString *)rideId paymentType:(NSString *)paymentTypes PayableAmount:(NSString *)payableAmount withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)submitTheUserFeedbackWithReceiverId:(NSString *)receiverID RideId:(NSString *)rideId RateSymbolType:(NSString *)symbolType WhatWrongType:(NSString *)wrongType FeedbackMessage:(NSString *)feedbackMessage isBlock:(NSString *)isBlockUser isFavoriteUser:(NSString *)isFavoriteUser withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)planARideWithUserID:(NSString *)UserID pickup_location:(NSString *)pickup_location drop_off_location:(NSString *)drop_off_location pickup_latitude:(NSString *)pickup_latitude pickup_longitude:(NSString *)pickup_longitude drop_off_latitude:(NSString *)drop_off_latitude drop_off_longitude:(NSString *)drop_off_longitude pickup_date:(NSString *)pickup_date pickup_time:(NSString *)pickup_time request_type:(NSString *)request_type withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)getDiscussionBoardDataWithSearchDate:(NSString *)searchableDate SearchUserId:(NSString *)searchableUserId PageNumber:(NSString *)pageNo post:(NSString *)strMyPost withCallBack:(void (^) (BOOL success, NSArray * arrayResponse, int totalItems, NSString * serverMessange))callback;





-(void)getMyAllFavoriteUsersListWithCallBack:(void (^) (BOOL success, NSArray * arrayResponse, NSString * serverMessange))callback;

-(void)getRideHistoryDetailsWithCallBack:(void (^) (BOOL success, NSArray * arrayResponse,NSString * serverMessange))callback;

-(void)deleteTheRideInfoFromRideHistoryWithRideId:(NSString *)rideId IsModeDeleteAll:(BOOL)isModeDeleteAll withCallBack:(void (^) (BOOL success ,NSString * serverMessange))callback;

-(void)didShareMyRideInfoWithRideId:(NSString *)rideId ContactNumber:(NSString *)contactNumber withCallBack:(void (^) (BOOL success, NSString * serverMessage))callback;
@end
