//
//  NoNetworkViewController.m
//  ShareBite
//
//  Created by William Falcon on 1/15/15.
//  Copyright (c) 2015 HACStudios. All rights reserved.
//

#import "NoNetworkViewController.h"
#import "Reachability.h"

@interface NoNetworkViewController()
@property (strong, nonatomic) IBOutlet UIButton *actionButton;
@property (assign) BOOL originalNavigationBarState;
@end

@implementation NoNetworkViewController

#pragma mark - View Lifecycle
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.actionButton.layer.cornerRadius = 18.0f;
    self.actionButton.layer.borderColor = [UIColor colorWithRed:149.0/255.0f green:154.0/255.0f blue:153.0/255.0f alpha:1.0].CGColor;
    self.actionButton.layer.borderWidth = 0.5;
    
    _spinnerForNoInternet = [[UIActivityIndicatorView alloc] init];
    _spinnerForNoInternet.frame = CGRectMake(30, 10, 20, 20);
    [_spinnerForNoInternet setColor:[UIColor whiteColor]];
    
    [self blurBackground];
    
    if (self.isModeNetworkService){
         self.imageViewServiceIcon.image = [UIImage imageNamed:@"PHInternetService"];
        self.lableServiceTitle.text = @"No internet connection!";
        self.lableServiceMessager.text = @"You must connect to a Wi-Fi or cellular data network to use Flexipool";
        [self.buttonRetry setTitle:@"Retry" forState:UIControlStateNormal];
        self.spinnerForNoInternet.hidden = false;
    } else {
        self.imageViewServiceIcon.image = [UIImage imageNamed:@"PHLocationService"];
        self.lableServiceTitle.text = @"Location Service Disabled !";
        self.lableServiceMessager.text = @"You must turn on Location Service to use Flexipool";
        [self.buttonRetry setTitle:@"Setting" forState:UIControlStateNormal];
        self.spinnerForNoInternet.hidden = true;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //hide nav bar
    self.originalNavigationBarState = self.navigationController.navigationBar.hidden;
    self.navigationController.navigationBar.hidden = YES;
    
//    if (APP_DELEGATE.swipeLeft.enabled)
//        [APP_DELEGATE hideShowMenu];
//    [APP_DELEGATE sideMenuEnabled:NO];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [APP_DELEGATE sideMenuEnabled:YES];
    
   // BOOL isFound = NO;
    
//    for (UIViewController *viewC in APP_DELEGATE.navigation.viewControllers) {
//        
//        if ([viewC isKindOfClass:[HomeVC class]]) {
//            
//            isFound = YES;
//        }
//    }
//
//    if (!isFound) {
//        
//        [APP_DELEGATE createSlideView];
//    }
    
    //show nav bar
    self.navigationController.navigationBar.hidden = self.originalNavigationBarState;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)mainActionPressed:(UIButton *)sender
{
    if (self.isModeNetworkService){
        [sender addSubview:_spinnerForNoInternet];
        
        Reachability* reachability;
        NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
        if(remoteHostStatus != NotReachable)
        {
            self.navigationController.navigationBar.hidden = self.originalNavigationBarState;
        }
        [_spinnerForNoInternet startAnimating];
        [sender setTitle:@"Wait..." forState:UIControlStateNormal];
        [self performSelector:@selector(waitIsOverForRefreshBtn:) withObject:sender afterDelay:4];
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}

-(void) waitIsOverForRefreshBtn:(UIButton *) sender
{
    [sender setTitle:@"Retry" forState:UIControlStateNormal];
    [_spinnerForNoInternet stopAnimating];
}

#pragma mark - UI Utils
- (void)blurBackground {
    self.view.backgroundColor = [UIColor clearColor];
    UIBlurEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *blurView = [[UIVisualEffectView alloc]initWithEffect:effect];
    blurView.frame = self.view.bounds;
    [self.view insertSubview:blurView atIndex:0];
}

@end






