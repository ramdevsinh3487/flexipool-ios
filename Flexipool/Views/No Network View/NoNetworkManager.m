//
//  NoNetworkManager.m
//  ShareBite
//
//  Created by William Falcon on 1/15/15.
//  Copyright (c) 2015 HACStudios. All rights reserved.
//

#import "NoNetworkManager.h"
#import "NoNetworkViewController.h"
#import "Reachability.h"

static float S_AUTO_TAKEOVER_DELAY = 1.0f;

typedef NS_ENUM(int, NoNetworkViewControllerState) {
    NoNetworkViewControllerStateHidden,
    NoNetworkViewControllerStateShowing
};

@interface NoNetworkManager ()
@property (nonatomic, strong) NSTimer *timerLocationService;
@property (nonatomic, strong) Reachability *reachability;
@property (nonatomic, strong) NoNetworkViewController *takeoverViewController;
@property (assign) NoNetworkViewControllerState currentNoNetworkVCState;
@end

@implementation NoNetworkManager


#pragma mark - Public class methods
+ (void)enableLackOfNetworkTakeover {

    
    //init the network change observer
    NoNetworkManager *observer = [NoNetworkManager sharedInstance];
    [observer addReachabilityNotifications];
    
    //check current status on launch
    NetworkStatus remoteHostStatus = [observer.reachability currentReachabilityStatus];
    BOOL noNetwork = remoteHostStatus == NotReachable;
    
    //if not network, takeover screen after a slight delay
    if (noNetwork) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(S_AUTO_TAKEOVER_DELAY * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            observer.takeoverViewController.isModeNetworkService = true;
            [observer showTakeoverVC:true];
        });
    }
    
    [observer.timerLocationService invalidate];
    observer.timerLocationService = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(checkLocationServiceStatus) userInfo:nil repeats:true];
}



+(void)checkLocationServiceStatus {
    NoNetworkManager *observer = [NoNetworkManager sharedInstance];
    BOOL takeoverVCShowing = observer.currentNoNetworkVCState == NoNetworkViewControllerStateShowing;
    BOOL isGPSEneble = [CLLocationManager locationServicesEnabled] &&
    [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied;
    
    NetworkStatus remoteHostStatus = [observer.reachability currentReachabilityStatus];
    BOOL noNetwork = remoteHostStatus == NotReachable;
    
    if (!isGPSEneble && !takeoverVCShowing) {
//        [observer showTakeoverVC:false];
    }else if (isGPSEneble && takeoverVCShowing && observer.takeoverViewController.isModeNetworkService == false) {
        [observer hideTakeoverVC];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            BOOL takeoverVCShowing2 = observer.currentNoNetworkVCState == NoNetworkViewControllerStateShowing;
            if (noNetwork && !takeoverVCShowing2){
                [observer showTakeoverVC:true];
            }
        });
    }
}

/**
 Singleton
 */
+ (id)sharedInstance {
    static id sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}
#pragma mark - Reachability utils

/**
 Main way to observe changes in the connectivity status
 */
- (void)addReachabilityNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNetworkChange:) name:kReachabilityChangedNotification object:nil];
    
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
}


- (void)handleNetworkChange:(NSNotification *)notification {
    NetworkStatus remoteHostStatus = [self.reachability currentReachabilityStatus];
    
    BOOL noNetwork = remoteHostStatus == NotReachable;
    BOOL takeoverVCShowing = self.currentNoNetworkVCState == NoNetworkViewControllerStateShowing;
//    BOOL isGPSEneble = [CLLocationManager locationServicesEnabled] &&
//    [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied;
    
    if (noNetwork && !takeoverVCShowing) {
        [self showTakeoverVC:true];
    } else if (!noNetwork && takeoverVCShowing) {
        [self hideTakeoverVC];
    }
}

#pragma mark - Takeover push/pop
/**
 Pushes takeover VC on the current view controller visible
 */
- (void)showTakeoverVC:(BOOL)isModeNetwork{
    self.currentNoNetworkVCState = NoNetworkViewControllerStateShowing;
    
    NoNetworkManager *observer = [NoNetworkManager sharedInstance];
    observer.currentNoNetworkVCState = NoNetworkViewControllerStateShowing;


    //init takeover VC
    self.takeoverViewController = [[NoNetworkViewController alloc]initWithNibName:@"NoNetworkViewController" bundle:nil];
    self.takeoverViewController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    self.takeoverViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    self.takeoverViewController.isModeNetworkService = isModeNetwork;
    //find topmost VC and put inside a nav controller
    UIViewController *topViewController = [self getTopMostViewController];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:self.takeoverViewController];
    
    //present the nav controller
    [topViewController presentViewController:nav animated:true completion:^{
        
    }];
}
/**
 Dismisses the takeover viewcontroller
 */
- (void)hideTakeoverVC{
    self.currentNoNetworkVCState = NoNetworkViewControllerStateHidden;
    
    NoNetworkManager *observer = [NoNetworkManager sharedInstance];
    observer.currentNoNetworkVCState = NoNetworkViewControllerStateHidden;
    
    [self.takeoverViewController dismissViewControllerAnimated:true completion:^{
        self.takeoverViewController = nil;
    }];
}

#pragma mark - Root VC Finding helpers

- (UIViewController*) getTopMostViewController
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    if (window.windowLevel != UIWindowLevelNormal) {
        NSArray *windows = [[UIApplication sharedApplication] windows];
        for(window in windows) {
            if (window.windowLevel == UIWindowLevelNormal) {
                break;
            }
        }
    }
    
    for (UIView *subView in [window subviews])
    {
        UIResponder *responder = [subView nextResponder];
        
        //added this block of code for iOS 8 which puts a UITransitionView in between the UIWindow and the UILayoutContainerView
        if ([responder isEqual:window])
        {
            //this is a UITransitionView
            if ([[subView subviews] count])
            {
                UIView *subSubView = [subView subviews][0]; //this should be the UILayoutContainerView
                responder = [subSubView nextResponder];
            }
        }
        
        if([responder isKindOfClass:[UIViewController class]]) {
            return [self topViewController: (UIViewController *) responder];
        }
    }
    
    return nil;
}

- (UIViewController *) topViewController: (UIViewController *) controller
{
    BOOL isPresenting = NO;
    do {
        // this path is called only on iOS 6+, so -presentedViewController is fine here.
        UIViewController *presented = [controller presentedViewController];
        isPresenting = presented != nil;
        if(presented != nil) {
            controller = presented;
        }
        
    } while (isPresenting);
    
    return controller;
}
@end
