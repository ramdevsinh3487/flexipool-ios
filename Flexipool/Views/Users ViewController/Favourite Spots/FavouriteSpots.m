//
//  FavouriteSpots.m
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "FavouriteSpots.h"
#import "FavouriteSpotsCell.h"
@interface FavouriteSpots ()

@end

@implementation FavouriteSpots

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:true];
    
    [lblNoDataFound setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 5]];
    
    [self setupInitialView];
    txtNameOfPlace.backgroundColor = [UIColor clearColor];
    
    tblView.tableFooterView = [UIView new];
    
    if (self.isSelectionMode){
        [btnNotifications setImage:[UIImage imageNamed:@"btn-true"] forState:UIControlStateNormal];
        [btnNotifications setTitle:@"" forState:UIControlStateNormal];
        [btnmenu setImage:[UIImage imageNamed:@"back-arrow"] forState:UIControlStateNormal];
        [btnNotifications.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
        btnNotifications.hidden = true;
    } else {
        [btnNotifications setImage:[UIImage imageNamed:@"notification-bell"] forState:UIControlStateNormal];
        [btnNotifications setTitle:@"" forState:UIControlStateNormal];
        [btnmenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
        btnNotifications.hidden = false;
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
    
    isGettingList = true;
    totalCount = 0;
    pageNumber = 1;
    [self getAndReloadFavoriteSpotList];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    if (!self.isSelectionMode){
        [btnNotifications setShouldAnimateBadge:true];
        btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
    }
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)getAndReloadFavoriteSpotList {
    if (totalCount && totalCount > 0){
        START_HUD
    } else {
        [arrayFavoriteSpot removeAllObjects];
        arrayFavoriteSpot = [[NSMutableArray alloc]init];
        [tblView reloadData];
        btnNotifications.hidden = true;
    }
    
    isGettingList = true;
    
    [APP_DELEGATE.apiManager getFavoriteSpotsListWithPageNumber:[NSString stringWithFormat:@"%d",pageNumber] withCallBack:^(BOOL success, NSArray *arrayResponse, int recordCount, NSString *serverMessange) {
        
        isGettingList = false;
        if (success){
            if (totalCount && totalCount > 0){
                [arrayFavoriteSpot addObjectsFromArray:arrayResponse];
                STOP_HUD
            } else {
                arrayFavoriteSpot = [[NSMutableArray alloc]initWithArray:arrayResponse];
                totalCount = recordCount;
            }
            
            pageNumber = pageNumber + 1;
        }
        
        selectedIndex = arrayFavoriteSpot.count;
        [tblView reloadData];
    }];
}

- (void) setupInitialView {
    
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    [lblFavouriteSpot setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblAddFavouriteSpot setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [btnAdd.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    
    lblHeader.text = GET_Language_Value(@"fav_spots");
    lblAddFavouriteSpot.text = GET_Language_Value(@"add_favourite_spot");
    lblFavouriteSpot.text = GET_Language_Value(@"your_favourite_spot");

    [btnAdd setTitle:GET_Language_Value(@"add") forState:UIControlStateNormal];
    
}

#pragma mark - UITableView Datasource Method -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isGettingList){
        tblView.hidden = true;
        lblNoDataFound.hidden = false;
        lblNoDataFound.text = @"Loading data, Please Wait...";
        btnNotifications.hidden = true;
        return 0;
    } else {
        if (arrayFavoriteSpot.count > 0){
            tblView.hidden = false;
            lblNoDataFound.hidden = true;
            btnNotifications.hidden = false;
            return arrayFavoriteSpot.count;
        } else {
            tblView.hidden = true;
            lblNoDataFound.hidden = false;
            lblNoDataFound.text = @"No Data Found";
            btnNotifications.hidden = true;
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier;
    
    simpleTableIdentifier = @"FavouriteSpotsCell";
    FavouriteSpotsCell *cell = (FavouriteSpotsCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        if (!self.isSelectionMode){
            cell.rightUtilityButtons = @[];//[self rightButtons];
        }
        cell.delegate = self;
    }
    
    if (self.isSelectionMode){
        if (indexPath.row == selectedIndex){
            cell.imgRadioSelection.image = [UIImage imageNamed:@"PHRadioFilled"];
        } else {
            cell.imgRadioSelection.image = [UIImage imageNamed:@"PHRadioUnfilled"];
        }
    } else {
        cell.imgRadioSelection.image = [UIImage imageNamed:@"delete-black"];
    }
    
    cell.lblLocation.text = arrayFavoriteSpot[indexPath.row][@"address"];
    cell.lblLocationName.text = arrayFavoriteSpot[indexPath.row][@"address_name"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;

    
    [cell.btnSelection addTarget:self action:@selector(cellSelectionActionWithButton:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnSelection.tag = indexPath.row;
    
    if (totalCount > arrayFavoriteSpot.count) {
        if (indexPath.row == arrayFavoriteSpot.count - 1 && isGettingList == false){
            [self getAndReloadFavoriteSpotList];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [self showSearchLocationWithModeAddLocation:false centerLattitude:arrayFavoriteSpot[indexPath.row][@"latitude"] CenterLongitude:arrayFavoriteSpot[indexPath.row][@"longitude"]];
    
//    if (self.isSelectionMode){
//        int oldIndex = (int)selectedIndex >= arrayFavoriteSpot.count ? 0 : (int)selectedIndex;
//        selectedIndex = indexPath.row;
//        [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:oldIndex inSection:0], indexPath] withRowAnimation:UITableViewRowAnimationFade];
//    } else {
////        FavouriteSpotsCell *cell = [tableView cellForRowAtIndexPath:indexPath];
////        [cell showRightUtilityButtonsAnimated:true];
//        
//        
//    }
    
}

-(void)cellSelectionActionWithButton:(UIButton *)sender{
    int oldIndex = (int)selectedIndex >= arrayFavoriteSpot.count ? 0 : (int)selectedIndex;
    selectedIndex = sender.tag;
    if (self.isSelectionMode){
        [tblView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:oldIndex inSection:0], [NSIndexPath indexPathForRow:sender.tag inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    } else {
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.delegate = self;
        alert.colorScheme = [UIColor darkGrayColor];
        
        
        alert.detachButtons = YES;
        [alert bounceAnimations];
        alert.dismissOnOutsideTouch = TRUE;
        
        [alert showAlertInView:APP_DELEGATE.navigationController
                     withTitle:AlertTitle
                  withSubtitle:@"Are you sure you want to delete this favorite spot ?"
               withCustomImage:nil
           withDoneButtonTitle:@"OK"
                    andButtons:@[@"CANCEL"]];
    }
}

- (NSArray *)rightButtons{
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    UIImage *img = [UIImage imageNamed:@"delete"];
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:76.0/255.0 green:170.0/255.0 blue:7.0/255.0 alpha:1.0] icon:img];
    
    
    return rightUtilityButtons;
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [tblView indexPathForCell:cell];
    
    selectedIndex = indexPath.row;
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    alert.colorScheme = [UIColor darkGrayColor];
    
    
    alert.detachButtons = YES;
    [alert bounceAnimations];
    alert.dismissOnOutsideTouch = TRUE;
    
    [alert showAlertInView:APP_DELEGATE.navigationController
                 withTitle:AlertTitle
              withSubtitle:@"Are you sure you want to delete this spot ?"
           withCustomImage:nil
       withDoneButtonTitle:@"OK"
                andButtons:@[@"CANCEL"]];
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell{
    return YES;
}

- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    START_HUD
    [APP_DELEGATE.apiManager deleteLocationFromFavoriteSpotsListWithLocationId:arrayFavoriteSpot[selectedIndex][@"address_id"] withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        STOP_HUD
        if (success){
            [Singleton showSnakBar:@"Successfully Removed" multiline:NO];
            [arrayFavoriteSpot removeObjectAtIndex:selectedIndex];
            [tblView reloadData];
        } else {
            [Singleton showSnakBar:serverMessange multiline:NO];
        }
    }];
}

#pragma mark - Action Methods -

- (IBAction)actionMenu:(id)sender {
    if (self.isSelectionMode){
        [self dismissViewControllerAnimated:true completion:nil];
    } else {
        [APP_DELEGATE showMenu];
    }
}

- (IBAction)actionNotification:(id)sender {
    if (self.isSelectionMode){
        if (selectedIndex >= arrayFavoriteSpot.count || selectedIndex < 0){
            [Singleton showSnakBar:@"Please select favourite spot" multiline:false];
        } else {
            NSDictionary * dictData = @{
                                        @"address" : arrayFavoriteSpot[selectedIndex][@"address"],
                                        @"address_id" : arrayFavoriteSpot[selectedIndex][@"address_id"],
                                        @"address_name" : arrayFavoriteSpot[selectedIndex][@"address_name"],
                                        @"lattitude" : arrayFavoriteSpot[selectedIndex][@"latitude"],
                                        @"longitude" : arrayFavoriteSpot[selectedIndex][@"longitude"]
                                        };
            [self dismissViewControllerAnimated:true completion:^{
                [self.delegate shouldSelectFavoriteLocation:dictData];
            }];
        }
    } else {
        [APP_DELEGATE NavigateToNotification];
    }
}

- (IBAction)actionAddPlace:(UIButton *)sender {
    [self.view endEditing:true];
    if (!dictLocationToAdd){
        [Singleton showSnakBar:@"Pleae Select Location First" multiline:NO];
    } else if (![txtNameOfPlace hasText]){
        [Singleton showSnakBar:@"Pleae Enter Name of Place" multiline:NO];
    } else {
        START_HUD
        [APP_DELEGATE.apiManager addNewFavoriteSpotWithFullAddress:dictLocationToAdd[@"fullAddress"] LocationLattitude:dictLocationToAdd[@"lattitude"] Longitude:dictLocationToAdd[@"longitude"] Name:txtNameOfPlace.text withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
            STOP_HUD
            if (success){
                [Singleton showSnakBar:@"Successfully Added" multiline:NO];
                dictLocationToAdd = nil;
                lblAddFavouriteSpot.text = @"Add Favorite Location";
                txtNameOfPlace.text = @"";
                
                totalCount = 0;
                pageNumber = 1;
                [self getAndReloadFavoriteSpotList];
                //Will Reload All Spots
            } else {
                [Singleton showSnakBar:serverMessange multiline:NO];
            }
        }];
    }
    
    sender.selected = false;
}
- (IBAction)actionSearchLocation:(id)sender {
    [self showSearchLocationWithModeAddLocation:true centerLattitude:@"" CenterLongitude:@""];
}

#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)showSearchLocationWithModeAddLocation:(BOOL)isModeAddLocation centerLattitude:(NSString *)lattitude CenterLongitude:(NSString *)longitude {
    SearchLocationViewController * searchView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
    searchView.isModeSearchLocation = true;
    searchView.delegate = self;
    if (isModeAddLocation){
        searchView.searchLocationMode = SLM_SET_FAVORITE_LOCATION;
    } else {
        searchView.searchLocationMode = SLM_SHOW_FAVORITE_LOCATION;
        searchView.centerableCoordinate = CLLocationCoordinate2DMake([lattitude doubleValue], [longitude doubleValue]);
    }
    
    [self presentViewController:searchView animated:true completion:nil];
}

-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo{
    if (DEBUG_MODE){
        NSLog(@"Selected Location : %@",locationInfo);
    }
    
    lblAddFavouriteSpot.text = locationInfo[@"fullAddress"];
    txtNameOfPlace.text = locationInfo[@"title"];
    dictLocationToAdd = locationInfo;
}



@end
