//
//  FavouriteSpotsCell.m
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "FavouriteSpotsCell.h"

@implementation FavouriteSpotsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.lblLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    [self.lblLocationName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
