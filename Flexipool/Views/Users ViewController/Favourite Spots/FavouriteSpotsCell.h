//
//  FavouriteSpotsCell.h
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteSpotsCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblLocationName;
@property (strong, nonatomic) IBOutlet UIImageView *imgRadioSelection;
@property (strong, nonatomic) IBOutlet UIButton *btnSelection;
@end
