//
//  FavouriteSpots.h
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchLocationViewController.h"
//#import <SWTableViewCell.h>

@protocol FavouriteSpotsDelegate <NSObject>

@optional
-(void)shouldSelectFavoriteLocation:(NSDictionary *)favoriteLocation;

@end

@interface FavouriteSpots : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate,FCAlertViewDelegate, SearchLocationViewControllerDelegate>
{
    IBOutlet UIButton *btnAdd;
    IBOutlet UILabel *lblAddFavouriteSpot;
    
    IBOutlet UITextField *txtNameOfPlace;
    IBOutlet UILabel *lblHeader;
    IBOutlet UILabel *lblNoDataFound;
   
    IBOutlet UITableView *tblView;
    IBOutlet UILabel *lblFavouriteSpot;
    
    NSInteger selectedIndex;
    
    NSDictionary * dictLocationToAdd;
    
    NSMutableArray * arrayFavoriteSpot;
    
    BOOL isGettingList;
    IBOutlet UIButton *btnNotifications;
    IBOutlet UIButton *btnmenu;
    
    int totalCount, pageNumber;
}

@property BOOL isSelectionMode;
@property id <FavouriteSpotsDelegate> delegate;
@end
