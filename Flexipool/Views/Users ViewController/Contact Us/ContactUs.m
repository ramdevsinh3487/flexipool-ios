//
//  ContactUs.m
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "ContactUs.h"

@interface ContactUs ()

@end

@implementation ContactUs

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    [self setupInitialView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {

    [btnNotification setShouldAnimateBadge:true];
    btnNotification.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}

- (void) setupInitialView {
    [lblMsg setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblFb setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblReview setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblCall setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    
    lblHeader.text = GET_Language_Value(@"contact_us");
        lblCall.text = GET_Language_Value(@"callUs");
        lblMsg.text = GET_Language_Value(@"message");
        lblFb.text = GET_Language_Value(@"contact_withUs");
        lblReview.text = GET_Language_Value(@"rate_app");
    [btnWhatsAppNumber setTitle:APP_DELEGATE.whatsApp forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Methods -

- (IBAction)actionMenu:(id)sender {
    
    [APP_DELEGATE showMenu];
}

- (IBAction)callUsAction:(id)sender {
    NSString * passenegerMobile = APP_DELEGATE.phoneMessageNumber;
    //Call
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",passenegerMobile]]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",passenegerMobile]]];
    } else {
        SHOW_ALERT_WITH_CAUTION(@"Unable to make phone calls \n Please check your device settings");
        return;
    }
}

- (IBAction)messageAction:(id)sender {
    NSString * passenegerMobile = APP_DELEGATE.phoneMessageNumber;
    //Message
    if(![MFMessageComposeViewController canSendText]) {
        SHOW_ALERT_WITH_CAUTION(@"Unable to send text messages \n Please check your device settings");
        return;
    } else {
        NSString * userName = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.dictUserDetails[@"first_name"], APP_DELEGATE.dictUserDetails[@"last_name"]];
        NSArray *recipents = @[passenegerMobile];
        NSString *message = [NSString stringWithFormat:@"Ahoy user %@ is contacting you.", userName];
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setBody:message];
        [self presentViewController:messageController animated:YES completion:nil];
    }
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    NSLog(@"Result : %ld", (long)result);
    [controller dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)facebookAction:(id)sender {
    
    NSString * strUrl = [APP_DELEGATE.facebookURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    if (![strUrl containsString:@"http://"] && ![strUrl containsString:@"https://"]){
        strUrl = [NSString stringWithFormat:@"http://%@",strUrl];
    }
    
    NSURL * facebookURL = [NSURL URLWithString:strUrl];

    if ([[UIApplication sharedApplication]canOpenURL:facebookURL]){
        [[UIApplication sharedApplication]openURL:facebookURL];
    } else {
        SHOW_ALERT_WITH_CAUTION(@"Unable to load facebook page right now \n Please try again later")
    }
}

- (IBAction)rateAppAction:(id)sender {
    NSURL * appStoreURL = [NSURL URLWithString:ItunesAppLink];
    if ([[UIApplication sharedApplication]canOpenURL:appStoreURL]){
        [[UIApplication sharedApplication]openURL:appStoreURL];
    } else {
        SHOW_ALERT_WITH_CAUTION(@"Unable to find Application right now \n Please try again later")
    }
}

- (IBAction)whatsAppAction:(id)sender {
//    NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=Hello"];
//    urlWhats = [urlWhats stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSURL * whatsappURL = [NSURL URLWithString:urlWhats];

//    NSString * msg1 = @"";
    NSString * urlWhats1 = [NSString stringWithFormat:@"https://api.whatsapp.com/send?phone=%@&text=",APP_DELEGATE.whatsApp];
    NSURL * whatsAppPhone1 = [NSURL URLWithString:[urlWhats1 stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
    
   // if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication]openURL:whatsAppPhone1];
//    } else {
//        NSString * strUrl = [@"https://itunes.apple.com/in/app/whatsapp-messenger/id310633997?mt=8" stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//        NSURL * whatsAppPhone = [NSURL URLWithString:strUrl];
//        if ([[UIApplication sharedApplication] canOpenURL: whatsAppPhone]) {
//            [[UIApplication sharedApplication]openURL:whatsAppPhone];
//        }
//    }
}

#pragma mark - Status Bar
-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
