//
//  ContactUs.h
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface ContactUs : UIViewController <MFMessageComposeViewControllerDelegate>
{
    IBOutlet UILabel *lblHeader;
    IBOutlet UIButton *btnWhatsAppNumber;
    IBOutlet UILabel *lblReview;
    IBOutlet UILabel *lblFb;
    IBOutlet UILabel *lblMsg;
    IBOutlet UILabel *lblCall;
    
    
    IBOutlet UIButton *btnNotification;
    
    
}



- (IBAction)actionMenu:(id)sender;

- (IBAction)callUsAction:(id)sender;
- (IBAction)messageAction:(id)sender;
- (IBAction)facebookAction:(id)sender;
- (IBAction)rateAppAction:(id)sender;
- (IBAction)whatsAppAction:(id)sender;

@end
