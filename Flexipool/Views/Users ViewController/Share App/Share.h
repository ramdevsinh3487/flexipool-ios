//
//  Share.h
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Share : UIViewController
{
    IBOutlet UILabel *lblHeader;
    IBOutlet UILabel *lblMail;
    IBOutlet UILabel *lblWhatsApp;
    IBOutlet UILabel *lblTwitter;
    IBOutlet UILabel *lblInsta;
    IBOutlet UILabel *lblFb;
    IBOutlet UILabel *lblGoogle;
    IBOutlet UIButton *btnNotification;
}
@end
