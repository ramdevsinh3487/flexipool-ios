//
//  Share.m
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Share.h"

@interface Share ()

@end

@implementation Share

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    [self setupInitialView];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotification setShouldAnimateBadge:true];
    btnNotification.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}


- (void) setupInitialView {
    [lblGoogle setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblFb setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblMail setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblInsta setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblTwitter setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblWhatsApp setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    
        lblHeader.text = GET_Language_Value(@"share");
        lblGoogle.text = GET_Language_Value(@"google");
        lblFb.text = GET_Language_Value(@"fb");
        lblInsta.text = GET_Language_Value(@"instagram");
        lblTwitter.text = GET_Language_Value(@"twitter");
        lblWhatsApp.text = GET_Language_Value(@"whatsapp");
        lblMail.text = GET_Language_Value(@"mail");
}

#pragma mark - Action Methods -

- (IBAction)actionGoogle:(id)sender {
    
}

- (IBAction)actionFB:(id)sender {
    
}

- (IBAction)actionInstagram:(id)sender {
    
}

- (IBAction)actionTwitter:(id)sender {
    
}

- (IBAction)actionWhatsapp:(id)sender {
    
}

- (IBAction)actionMail:(id)sender {
    
}

- (IBAction)actionMenu:(id)sender {
    
    [APP_DELEGATE showMenu];
}

#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
