//
//  AhoyWallet.h
//  Flexipool
//
//  Created by Nirav on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import "AddMoney.h"


@interface AhoyWallet : UIViewController <AddMoneyDelegate, AlertViewControllerDelegate>
{
    IBOutlet UILabel *lblWalletBalance;
    IBOutlet UILabel *lblHeader;
    
    IBOutlet UILabel *lblBalance;
    IBOutlet UIButton *btnReedemAmount;
    IBOutlet UIButton *btnRefillWallet;
    
    NSString * walletAmount;
    IBOutlet UIButton *btnMenu;
    IBOutlet UIButton *btnNotification;
}

@property BOOL isModeRefillFromPayment;
@end
