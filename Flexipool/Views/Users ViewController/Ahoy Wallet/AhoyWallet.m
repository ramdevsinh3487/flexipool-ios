//
//  AhoyWallet.m
//  Flexipool
//
//  Created by Nirav on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AhoyWallet.h"

@interface AhoyWallet ()

@end

@implementation AhoyWallet

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    
    
    [self setupInitialView];
     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
    
    [self.navigationController setNavigationBarHidden:true];
    
    if (self.isModeRefillFromPayment){
        [btnMenu setImage:[UIImage imageNamed:@"close-icon"] forState:UIControlStateNormal];
    } else {
        [btnMenu setImage:[UIImage imageNamed:@"menu"] forState:UIControlStateNormal];
    }
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotification setShouldAnimateBadge:true];
    btnNotification.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}


- (void) setupInitialView {
    [lblBalance setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+10]];
    [lblWalletBalance setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+3]];
    
    [btnReedemAmount.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    [btnRefillWallet.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    lblBalance.text = [APP_DELEGATE formatTheCostWithInputValue:[APP_DELEGATE.WalletAmount doubleValue] isModeValue:false];
    
    lblHeader.text = GET_Language_Value(@"ahoy_wallet");
    lblWalletBalance.text = GET_Language_Value(@"wallet_balance");
    
    [btnReedemAmount setTitle:@"Redeem Amount" forState:UIControlStateNormal];
    [btnRefillWallet setTitle:@"Refill Wallet" forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated {
    lblBalance.text = [Singleton getWalletAmount];
    [self showAvailableBalance];
   
}

-(void)showAvailableBalance{
     lblBalance.text = [APP_DELEGATE formatTheCostWithInputValue:[APP_DELEGATE.WalletAmount doubleValue] isModeValue:false];
}


//-(void)getSuccessOnAddMoneyByCreditDebitCard:(NSString *)amount{
//    
//    [self addMoneyToWalletAPI:amount];
//    
//}


#pragma mark - action methods -

- (IBAction)actionReedemAmount:(id)sender {
    if ([APP_DELEGATE.WalletAmount doubleValue] > 0){
        AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
        obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
        obj.delegate = self;
        obj.alertTitles = @"Are you sure you want to redeem wallet amount ?";
        obj.alertMessages = @"";
        obj.yesTitle = @"Yes";
        obj.noTitle = @"No";
        [self presentViewController:obj animated:NO completion:nil];
    } else {
        [Singleton showSnakBar:@"You have insufficient wallet balance" multiline:false];
    }
}

- (void)alertControllerButtonTappedWithIndex:(NSInteger)index {
    if (index == 1){
        START_HUD
        [APP_DELEGATE.apiManager redeemAhoyWalletWithWalletAmount:APP_DELEGATE.WalletAmount CallBack:^(BOOL success, NSString *serverMessange) {
            STOP_HUD
            if (success){
                APP_DELEGATE.WalletAmount = 0;
                walletAmount = @"0";
                lblBalance.text = [APP_DELEGATE formatTheCostWithInputValue:0.0 isModeValue:false];
                [Singleton showSnakBar:serverMessange multiline:true];
            } else {
                [Singleton showSnakBar:serverMessange multiline:true];
            }
        }];
    }
}

- (IBAction)actionRefillAmounr:(id)sender {
    
    AddMoney *obj = (AddMoney *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AddMoney"];
    obj.delegate = self;
    [self presentViewController:obj animated:YES completion:nil];
}

- (IBAction)actionMenu:(id)sender {
    if (self.isModeRefillFromPayment){
        [self dismissViewControllerAnimated:true completion:nil];
    } else {
        [APP_DELEGATE showMenu];
    }
    
}



#pragma mark  - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
