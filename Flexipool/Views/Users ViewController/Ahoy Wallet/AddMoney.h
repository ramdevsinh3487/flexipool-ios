//
//  AddMoney.h
//  Flexipool
//
//  Created by HimAnshu on 15/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import <CardIO/CardIO.h>


@protocol AddMoneyDelegate <NSObject>
@optional
-(void)getSuccessOnAddMoney:(NSString *)amount;
@end

@interface AddMoney : UIViewController<CardIOPaymentViewControllerDelegate, UITextViewDelegate>
{
    IBOutlet UILabel *lblHeader;
    IBOutlet ACFloatingTextField *txtAmount;
    
    __weak IBOutlet UILabel *lblCurrencySign;
    IBOutlet UIButton *btnAmount1;
    IBOutlet UIButton *btnAmount2;
    IBOutlet UIButton *btnAmount3;
    IBOutlet UIButton *btnNext;
    
    IBOutlet UILabel *lblAvailableBalance;
    
    NSInteger selectedPaymentType;
    
    __weak IBOutlet UIButton *btnPaypal;
    __weak IBOutlet UIButton *btnCreditDebitCard;
    
}

- (IBAction)OnClickPaymentOption:(id)sender;

@property id <AddMoneyDelegate> delegate;

- (IBAction)actionDefaultAmount:(UIButton *)sender;
- (IBAction)actionNext:(id)sender;
- (IBAction)actionClose:(id)sender;


@end
