//
//  AddMoney.m
//  Flexipool
//
//  Created by HimAnshu on 15/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AddMoney.h"


@interface AddMoney ()

@end

@implementation AddMoney

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    [self.navigationController setNavigationBarHidden:true];
    
    [self setupInitialView];
}

- (void) setupInitialView {
    
     [txtAmount setPadding:8];

    [txtAmount setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    lblAvailableBalance.font = [UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE+2];
    
    [btnAmount1.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE-1]];
    [btnAmount2.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE-1]];
    [btnAmount3.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE-1]];
    
    [btnNext.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    lblHeader.text = GET_Language_Value(@"refill_wallet");
    [btnNext setTitle:GET_Language_Value(@"add") forState:UIControlStateNormal];
    
    lblAvailableBalance.text = [NSString stringWithFormat:@"Wallet Balance : %@",[APP_DELEGATE formatTheCostWithInputValue:[APP_DELEGATE.WalletAmount doubleValue] isModeValue:false]];
    
    selectedPaymentType = -1;
    
    
    [btnPaypal setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnCreditDebitCard setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
//    [txtAmount becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Methods -

- (IBAction)actionDefaultAmount:(UIButton *)sender {
    
    txtAmount.text = [sender.titleLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
}

- (IBAction)actionNext:(id)sender {
    [self.view endEditing:true];
    
    if (selectedPaymentType < 0){
        [Singleton showSnakBar:@"Please select any one of the payment method" multiline:true];
    } else  if (![txtAmount hasText])
    {
        [Singleton showSnakBar:@"Please enter amount" multiline:false];
        return;
    } else if ([txtAmount.text intValue] > 250 || [txtAmount.text intValue] < 20) {
        [Singleton showSnakBar:[NSString stringWithFormat:@"Amount must be in between %@ 20 to %@ 250", APP_DELEGATE.currencySymbol, APP_DELEGATE.currencySymbol] multiline:true];
    } else  if (selectedPaymentType == 1)
    {
        [[Singleton sharedInstance] PayPalCallWithAmount:txtAmount.text delegatedView:self];
    }
    else
    {
        CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self scanningEnabled:false];
        scanViewController.collectCardholderName = true;
        scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        [self presentViewController:scanViewController animated:YES completion:nil];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if ([string isEqualToString:@""]){
        return true;
    }
    
    if (textField == txtAmount){
        if ([txtAmount.text length] < 3){
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
    return true;
}


#pragma mark - Open Credit/Debit Card -


#pragma mark - CardIOPaymentViewControllerDelegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    if (DEBUG_MODE){
        NSLog(@"Scan succeeded with info: %@",info.cardNumber);
        NSLog(@"Scan succeeded with info: %@",info.cvv);
        NSLog(@"Scan succeeded with info: %lu",(unsigned long)info.expiryYear);
        NSLog(@"Scan succeeded with info: %ld",(long)info.redactedCardNumber);
        NSLog(@"Scan succeeded with info: %lu",(unsigned long)info.expiryMonth);
    }
    
    
    NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc] init];
    [dictInfo setValue:info.cardNumber forKey:@"CardNumber"];
    [dictInfo setValue:[NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictUserDetails[@"first_name"],APP_DELEGATE.dictUserDetails[@"last_name"]] forKey:@"CardHolder"];
    [dictInfo setValue:[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryMonth] forKey:@"CardExpiryMonth"];
    [dictInfo setValue:[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear] forKey:@"CardExpiryYear"];
    [dictInfo setValue:txtAmount.text forKey:@"TotalAmount"];
    [dictInfo setValue:APP_DELEGATE.dictUserDetails[@"first_name"] forKey:@"FirstName"];
    [dictInfo setValue:APP_DELEGATE.dictUserDetails[@"last_name"] forKey:@"LastName"];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self callAPIforTransactionWithAuthorizedNet:dictInfo];
    }];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    if (DEBUG_MODE){
        NSLog(@"User cancelled scan");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - Method for Authorized Bank API For Transaction -
-(void)callAPIforTransactionWithAuthorizedNet:(NSMutableDictionary *)dictCardInfo{    
    NSDictionary *dict = @{@"user_id" : APP_DELEGATE.loggedUserId,
                           @"token" : APP_DELEGATE.tokenString,
                           @"card_number": [dictCardInfo valueForKey:@"CardNumber"],
                           @"card_holder": [dictCardInfo valueForKey:@"CardHolder"],
                           @"card_expire_month": [dictCardInfo valueForKey:@"CardExpiryMonth"],
                           @"card_expire_year": [dictCardInfo valueForKey:@"CardExpiryYear"],
                           @"total_amount": [dictCardInfo valueForKey:@"TotalAmount"],
                           @"first_name": [dictCardInfo valueForKey:@"FirstName"],
                           @"last_name" : [dictCardInfo valueForKey:@"LastName"]};
    
    [APP_DELEGATE.apiManager AuthorizedBankTransaction:dict withCallBack:^(BOOL success, NSMutableArray *response, NSString *errorMessage) {
        
        if (success)
        {
            NSMutableArray *dictResponse = response;
            NSString *strCode = [NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"code"] objectAtIndex:0]];
            
            if ([strCode integerValue] == 1) {
                [self addMoneyToWalletAPI:[NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"amount"] objectAtIndex:0]]];
            } else {
                [Singleton showSnakBar:[[dictResponse valueForKey:@"message"] objectAtIndex:0] multiline:true];
            }
        } else {
            [Singleton showSnakBar:errorMessage multiline:true];
        }

        STOP_HUD;
    }];
}


#pragma mark - PaypalDelegate
- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    [paymentViewController dismissViewControllerAnimated:true completion:^{
        [self addMoneyToWalletAPI:[NSString stringWithFormat:@"%@",completedPayment.amount]];
    }];
}


#pragma mark - API To Add Money To Wallet -
-(void)addMoneyToWalletAPI : (NSString *)strAmount {
    NSLog(@"PayPal Payment Success! : %@",strAmount);
    START_HUD
    [APP_DELEGATE.apiManager addMoneyToMyWallerWithAmountToAdd:[NSString stringWithFormat:@"%@",strAmount]withCallBack:^(BOOL success, NSString *serverMessange) {
        STOP_HUD
        if (success){
            NSString * walletAmount = [NSString stringWithFormat:@"%f", [APP_DELEGATE.WalletAmount doubleValue] + [strAmount doubleValue]];
            
            APP_DELEGATE.WalletAmount = walletAmount;
            [self dismissViewControllerAnimated:true completion:nil];
//            [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:^{
//                
//            }];
//            [Singleton showSnakBar:serverMessange multiline:true];
        } else {
            [Singleton showSnakBar:serverMessange multiline:true];
        }
    }];
}

- (IBAction)actionClose:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)OnClickPaymentOption:(id)sender {
    if ([sender tag] == 1)
    {
        selectedPaymentType = 1;
        
        [btnPaypal setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        [btnCreditDebitCard setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    }
    else
    {
        selectedPaymentType = 2;
        
        [btnCreditDebitCard setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        [btnPaypal setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    }
    
}
@end
