//
//  RideHistoryCell.m
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "RideHistoryCell.h"

@implementation RideHistoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.lblName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [self.lblRideAmount setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [self.lblPickupLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    [self.lblDestinationLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    self.imgUserProfile.layer.cornerRadius = 25;
    self.imgUserProfile.layer.masksToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
