//
//  RideHistory.m
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "RideHistory.h"

@interface RideHistory ()

@end

@implementation RideHistory

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    [self setupInitialView];
}

- (void) setupInitialView {
    
    [btnClearAll.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 1]];
    [lblDataMessage setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 5]];
    
    lblHeader.text = GET_Language_Value(@"ride_history");
    [btnClearAll setTitle:GET_Language_Value(@"clear_all") forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
    
    retryCount = 0;
    tblView.tableFooterView = [UIView new];
    [self updateRideHistoryList];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {

    [btnNotification setShouldAnimateBadge:true];
    btnNotification.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}

-(void)updateRideHistoryList {
    retryCount = retryCount + 1;
    arrayRideHistory = [[NSMutableArray alloc]init];
    isLoadingData = true;
    [tblView reloadData];
    [APP_DELEGATE.apiManager getRideHistoryDetailsWithCallBack:^(BOOL success, NSArray *arrayResponse, NSString *serverMessange) {
        if (success){
            arrayRideHistory = [[NSMutableArray alloc]initWithArray:arrayResponse];
            isLoadingData = false;
            [tblView reloadData];
        } else {
            if (retryCount >= 3){
                isLoadingData = false;
                [tblView reloadData];
            } else {
                [self updateRideHistoryList];
            }
        }
    }];
}

#pragma mark - UITableView Datasource Method -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isLoadingData){
        tableView.hidden = true;
        lblDataMessage.hidden = false;
        lblDataMessage.text = @"Loading data, please wait...";
        return 0;
    } else {
        if (arrayRideHistory.count > 0){
            tableView.hidden = false;
            lblDataMessage.hidden = true;
            lblDataMessage.text = @"";
            return arrayRideHistory.count;
        } else {
            tableView.hidden = true;
            lblDataMessage.hidden = false;
            lblDataMessage.text = @"No data found";
            return 0;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier;
    simpleTableIdentifier = @"RideHistoryCell";
    
    RideHistoryCell *cell = (RideHistoryCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    NSDictionary * dictTripInfo = arrayRideHistory[indexPath.row];
    BOOL isRideTaken = [dictTripInfo[@"ride_taken_given_status"] isEqualToString:@"1"] ? true : false;
    
    NSString * dateOfRide = dictTripInfo[@"date_of_ride"];
    NSDateFormatter * dateFormater = [[NSDateFormatter alloc]init];
    dateFormater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate * convertedDate = [dateFormater dateFromString:dateOfRide];
    dateFormater.dateFormat = @"dd MMM yyyy";
    cell.lblDate.text = [dateFormater stringFromDate:convertedDate];
    dateFormater.dateFormat = @"hh:mm a";
    cell.lblTime.text = [dateFormater stringFromDate:convertedDate];
    
    cell.lblRideAmount.text = [NSString stringWithFormat:@"%@ : %@",GET_Language_Value(@"ride_amount"), [APP_DELEGATE formatTheCostWithInputValue:[dictTripInfo[@"trip_totalcost"] doubleValue] isModeValue:false]];
    
    cell.lblPickupLocation.text = dictTripInfo[@"source_location"];
    cell.lblDestinationLocation.text = dictTripInfo[@"destination_location"];
    cell.lblRideStatus.text = dictTripInfo[@"ride_taken_given_label"];
    
    NSString * imageURL = @"";
    
    if (isRideTaken){
        cell.lblName.text = dictTripInfo[@"driver_name"];
        imageURL = dictTripInfo[@"driver_image"];
    } else {
        cell.lblName.text = dictTripInfo[@"rider_name"];
        imageURL = dictTripInfo[@"rider_image"];
    }

     UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
    
//    UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//    NSString * gender = dictTripInfo[@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, cell.imgUserProfile.frame.size.width, cell.imgUserProfile.frame.size.height)];
    
    activity_indicator.tag = 11111;
    [[cell.imgUserProfile viewWithTag:1111] removeFromSuperview];
    
    [cell.imgUserProfile addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    imageURL = [imageURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:imageURL] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [[cell.imgUserProfile viewWithTag:1111] removeFromSuperview];
    }];
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.rightUtilityButtons = [self rightButtonsWithUserDetails:dictTripInfo];
    cell.delegate = self;
    
    cell.imgUserProfile.layer.cornerRadius = cell.imgUserProfile.frame.size.height / 2;
    cell.imgUserProfile.layer.masksToBounds = true;
    
    cell.lblName.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    cell.lblRideAmount.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE - 3];
    cell.lblPickupLocation.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE - 5];
    cell.lblDestinationLocation.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE - 5];
    cell.lblRideStatus.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE - 5];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [previousCell hideUtilityButtonsAnimated:true];
    RideHistoryCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell showRightUtilityButtonsAnimated:true];
    previousCell = cell;
}

- (NSArray *)rightButtonsWithUserDetails:(NSDictionary *)dictTripInfo {
    
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    UIImage *img1 = [UIImage imageNamed:@"delete"];
    
    UIImage *img2 = [UIImage imageNamed:@"heart-white-filled"];
    if ([dictTripInfo[@"is_favourite"] isEqualToString:@"1"]){
        img2 = [UIImage imageNamed:@"heart-white-filled-cracked"];
    } else {
        img2 = [UIImage imageNamed:@"heart-white-filled"];
    }
    
    UIImage * img3 = [UIImage imageNamed:@"cancle"];
    if ([dictTripInfo[@"is_block"] isEqualToString:@"1"]){
        img3 = [UIImage imageNamed:@"cancle"];
    } else {
        img3 = [UIImage imageNamed:@"cancle"];
    }
    
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:76.0/255.0 green:170.0/255.0 blue:7.0/255.0 alpha:1.0] icon:img1];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:76.0/255.0 green:170.0/255.0 blue:7.0/255.0 alpha:1.0] icon:img2];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:76.0/255.0 green:170.0/255.0 blue:7.0/255.0 alpha:1.0] icon:img3];
    
    return rightUtilityButtons;
}


- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index
{
    isModeClearAll = false;
    
    NSIndexPath *indexPath = [tblView indexPathForCell:cell];
    selectedIndex = indexPath.row;
    selectedButtonIndex = index;
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.delegate = self;
    alert.colorScheme = [UIColor darkGrayColor];
    
    
    alert.detachButtons = YES;
    [alert bounceAnimations];
    alert.dismissOnOutsideTouch = TRUE;
    
    NSDictionary * dictTripInfo = arrayRideHistory[selectedIndex];
    
    BOOL isBlocked = [dictTripInfo[@"is_block"] isEqualToString:@"1"] ? true : false;
    BOOL isFavoruite = [dictTripInfo[@"is_favourite"] isEqualToString:@"1"] ? true : false;
    
    if (index == 0){
        //Delete
        [alert showAlertInView:APP_DELEGATE.navigationController
                     withTitle:AlertTitle
                  withSubtitle:@"Are you sure want to delete ride ?"
               withCustomImage:nil
           withDoneButtonTitle:@"OK"
                    andButtons:@[@"CANCEL"]];
    } else if (index == 1){
        //Favoruite
        if (isFavoruite){
            [alert showAlertInView:APP_DELEGATE.navigationController
                         withTitle:AlertTitle
                      withSubtitle:@"Are you sure want to remove user from favourite ?"
                   withCustomImage:nil
               withDoneButtonTitle:@"OK"
                        andButtons:@[@"CANCEL"]];
        } else {
            [alert showAlertInView:APP_DELEGATE.navigationController
                         withTitle:AlertTitle
                      withSubtitle:@"Are you sure want to add user as favourite ?"
                   withCustomImage:nil
               withDoneButtonTitle:@"OK"
                        andButtons:@[@"CANCEL"]];
        }
    } else {
        //block
        if (isBlocked){
            [alert showAlertInView:APP_DELEGATE.navigationController
                         withTitle:AlertTitle
                      withSubtitle:@"Are you sure want to unblock user ?"
                   withCustomImage:nil
               withDoneButtonTitle:@"OK"
                        andButtons:@[@"CANCEL"]];
        } else {
            [alert showAlertInView:APP_DELEGATE.navigationController
                         withTitle:AlertTitle
                      withSubtitle:@"Are you sure want to block user ?"
                   withCustomImage:nil
               withDoneButtonTitle:@"OK"
                        andButtons:@[@"CANCEL"]];
        }
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell{
    return YES;
}

- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    if (isModeClearAll){
        START_HUD
        [APP_DELEGATE.apiManager deleteTheRideInfoFromRideHistoryWithRideId:@"" IsModeDeleteAll:true withCallBack:^(BOOL success, NSString *serverMessange) {
            STOP_HUD
            if (success){
                [Singleton showSnakBar:@"Delete successfully" multiline:false];
                [APP_DELEGATE.navigationController popViewControllerAnimated:true];
            } else {
                [Singleton showSnakBar:serverMessange multiline:true];
            }
        }];
    } else {
        NSDictionary * dictTripInfo = arrayRideHistory[selectedIndex];
        
        BOOL isBlocked = [dictTripInfo[@"is_block"] isEqualToString:@"1"] ? true : false;
        BOOL isFavoruite = [dictTripInfo[@"is_favourite"] isEqualToString:@"1"] ? true : false;
        NSString * operationalUserId = @"";
        if (APP_DELEGATE.isUserModeDriver){
            operationalUserId = dictTripInfo[@"rider_id"];
        } else {
            operationalUserId = dictTripInfo[@"driver_id"];
        }
        
        if (selectedButtonIndex == 0){
            //Delete the ride
            NSString * rideId = dictTripInfo[@"history_id"];
            START_HUD
            [APP_DELEGATE.apiManager deleteTheRideInfoFromRideHistoryWithRideId:rideId IsModeDeleteAll:false withCallBack:^(BOOL success, NSString *serverMessange) {
                STOP_HUD
                if (success){
                    [arrayRideHistory removeObjectAtIndex:selectedIndex];
                    [tblView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:selectedIndex inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                } else {
                    [Singleton showSnakBar:serverMessange multiline:true];
                }
            }];
        } else if (selectedButtonIndex == 1){
            START_HUD
            [APP_DELEGATE.apiManager addOrRemoveUserFromMyFavoruiteListWithUserId:operationalUserId IsModeFavoruite:!isFavoruite withCallBack:^(BOOL success, NSString *serverMessange) {
                STOP_HUD
                if(success){
                    NSString * comparableId = APP_DELEGATE.isUserModeDriver ? dictTripInfo[@"rider_id"] : dictTripInfo[@"driver_id"];
                    for (int i = 0; i < arrayRideHistory.count; i++){
                        NSMutableDictionary * dictData = [[NSMutableDictionary alloc]initWithDictionary:arrayRideHistory[i]];
                        NSString * comparableIdTwo = APP_DELEGATE.isUserModeDriver ? dictData[@"rider_id"] : dictData[@"driver_id"];
                        
                        if ([comparableId isEqualToString:comparableIdTwo]){
                            if(isFavoruite){
                                [dictData setObject:@"0" forKey:@"is_favourite"];
                            } else {
                                [dictData setObject:@"1" forKey:@"is_favourite"];
                            }
                            [arrayRideHistory replaceObjectAtIndex:i withObject:dictData];
                            [tblView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        }
                    }
                    
                    if(isFavoruite){
                        [Singleton showSnakBar:@"Removed from Favourite successfully" multiline:false];
                    } else {
                        [Singleton showSnakBar:@"Added Favourite successfully" multiline:false];
                    }
                    
                    [APP_DELEGATE updateFavoriteUserListArray];
                } else {
                    [Singleton showSnakBar:serverMessange multiline:true];
                }
                [APP_DELEGATE updateFavoriteUserListArray];
            }];
        } else {
            START_HUD
            [APP_DELEGATE.apiManager addOrRemoveUserFromMyBlockedUserListWithUserId:operationalUserId IsModeBlock:!isBlocked withCallBack:^(BOOL success, NSString *serverMessange) {
                STOP_HUD
                if(success){
                    
                    NSString * comparableId = APP_DELEGATE.isUserModeDriver ? dictTripInfo[@"rider_id"] : dictTripInfo[@"driver_id"];
                    for (int i = 0; i < arrayRideHistory.count; i++){
                        NSMutableDictionary * dictData = [[NSMutableDictionary alloc]initWithDictionary:arrayRideHistory[i]];
                        NSString * comparableIdTwo = APP_DELEGATE.isUserModeDriver ? dictData[@"rider_id"] : dictData[@"driver_id"];
                        
                        if ([comparableId isEqualToString:comparableIdTwo]){
                            if(isBlocked){
                                [dictData setObject:@"0" forKey:@"is_block"];
                            } else {
                                [dictData setObject:@"1" forKey:@"is_block"];
                            }
                            
                            [arrayRideHistory replaceObjectAtIndex:i withObject:dictData];
                            [tblView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        }
                    }
                    
                    if(isBlocked){
                        [Singleton showSnakBar:@"Unblock successfully" multiline:false];
                    } else {
                        [Singleton showSnakBar:@"Block successfully" multiline:false];
                    }
                    
                } else {
                    [Singleton showSnakBar:serverMessange multiline:true];
                }
            }];
        }
    }
    
}

#pragma mark - Action Methods -

- (IBAction)actionMenu:(id)sender {
    
    [APP_DELEGATE showMenu];
}

- (IBAction)actionClearAll:(id)sender {
    if (arrayRideHistory && arrayRideHistory.count > 0){
        isModeClearAll = true;
        FCAlertView *alert = [[FCAlertView alloc] init];
        alert.delegate = self;
        alert.colorScheme = [UIColor darkGrayColor];
        
        
        alert.detachButtons = YES;
        [alert bounceAnimations];
        alert.dismissOnOutsideTouch = TRUE;
        
        [alert showAlertInView:APP_DELEGATE.navigationController
                     withTitle:AlertTitle
                  withSubtitle:@"Are you sure want to delete all rides ?"
               withCustomImage:nil
           withDoneButtonTitle:@"OK"
                    andButtons:@[@"CANCEL"]];
    } else {
        [Singleton showSnakBar:@"You have not taken any rides" multiline:false];
    }
    
}

#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
