//
//  RideHistory.h
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RideHistoryCell.h"
//#import <SWTableViewCell.h>

@interface RideHistory : UIViewController<UITableViewDelegate,UITableViewDataSource,SWTableViewCellDelegate,FCAlertViewDelegate>
{
    IBOutlet NSLayoutConstraint *constraintOfTblvwTop;
    IBOutlet UITableView *tblView;
    
    IBOutlet UIButton *btnNotification;
    IBOutlet UILabel *lblHeader;
    IBOutlet UIButton *btnClearAll;
    IBOutlet UILabel *lblDataMessage;
    
    NSInteger selectedIndex;
    NSUInteger selectedButtonIndex;
    
    NSMutableArray * arrayRideHistory;
    BOOL isLoadingData;
    int retryCount;
    
    BOOL isModeClearAll;
    RideHistoryCell * previousCell;
}
@end
