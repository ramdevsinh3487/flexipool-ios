//
//  RideHistoryCell.h
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RideHistoryCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;


@property (weak, nonatomic) IBOutlet UILabel *lblRideStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblRideAmount;
@property (strong, nonatomic) IBOutlet UILabel *lblPickupLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblDestinationLocation;
@property (strong, nonatomic) IBOutlet UIImageView *imgUserProfile;

@end
