//
//  PlanRide.h
//  Flexipool
//
//  Created by Krishna on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchLocationViewController.h"
//#import "FavouriteSpots.h"
@interface PlanRide : UIViewController<UITextFieldDelegate, SearchLocationViewControllerDelegate>
{
  

    IBOutlet UIView *viewBorderPickupDate;
    IBOutlet UIView *viewBorderPickupTime;
    
    __weak IBOutlet UIButton *btnPost;
    __weak IBOutlet ACFloatingTextField *txtPickUpTime;
    __weak IBOutlet ACFloatingTextField *txtPickupDate;
    
    __weak IBOutlet UIButton *btnDiscussionBoard;
    __weak IBOutlet ACFloatingTextField *txtPickupLocation;
    IBOutlet UILabel *lblPickupLocation;
    __weak IBOutlet ACFloatingTextField *txtDropOffLocation;
    IBOutlet UILabel *lblDropOffLocation;
    __weak IBOutlet UILabel *lblHeader;
    
    NSString *strFromDate,*strTime;
    NSDate * prevousDate;
    UITextField *activeTextField;
    BOOL isModePickBeginLocation;
    IBOutlet UIButton *btnNotification;
    
    NSDictionary * dictPickupLocation;
    NSDictionary * dictDropoffLocation;
    
    IBOutlet UIImageView *imgPickupHeart;
    IBOutlet UIButton *btnPickupFavoruite;
    IBOutlet UIImageView *imgDestinationHeart;
    IBOutlet UIButton *btnDestinationFavoruite;
}

@end
