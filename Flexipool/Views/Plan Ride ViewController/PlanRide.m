//
//  PlanRide.m
//  Flexipool
//
//  Created by Krishna on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "PlanRide.h"
#import "FavouriteSpots.h"
@interface PlanRide () <FavouriteSpotsDelegate>

@end

@implementation PlanRide 
#pragma mark - viewDidLoad -
- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    // Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
    [self setupInitialView];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotification setShouldAnimateBadge:true];
    btnNotification.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}


- (void) setupInitialView {
    
    [txtPickupDate setPadding:8];
    [txtPickUpTime setPadding:8];
    [txtPickupLocation setPadding:8];
    [txtDropOffLocation setPadding:8];

    
    [txtPickupDate setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txtPickUpTime setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txtPickupLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txtDropOffLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    [txtPickupDate addTarget:self action:@selector(txtDateTouch:) forControlEvents:UIControlEventEditingDidBegin];
    [txtPickupDate addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfPickupDate) shouldShowPlaceholder:YES];
    
    [txtPickUpTime addTarget:self action:@selector(txtDateTouch:) forControlEvents:UIControlEventEditingDidBegin];
    [txtPickUpTime addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfPickupTime) shouldShowPlaceholder:YES];
    
    [btnPost.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    [txtPickupLocation setRightPaddingWhiteHeart];
    [txtDropOffLocation setRightPaddingWhiteHeart];
    [txtPickupDate setRightPaddingCalender];
    
    txtPickupLocation.placeholder = @"Pick up location";
    txtDropOffLocation.placeholder = @"Drop off location";
    txtPickupDate.placeholder = @"Pick up date";
    txtPickUpTime.placeholder = @"Drop off date";
    [btnPost setTitle:GET_Language_Value(@"post") forState:UIControlStateNormal];
    lblHeader.text = @"Plan Ride";
    prevousDate = [NSDate date];
    
    [self setupTheBorderView];
}

#pragma mark - UITextField Delegates -

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == txtPickupLocation || textField == txtDropOffLocation){
        SearchLocationViewController * searchView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
        searchView.isModeSearchLocation = true;
        searchView.delegate = self;
        [textField resignFirstResponder];
        if (textField == txtPickupLocation){
            isModePickBeginLocation = true;
            searchView.searchLocationMode = SLM_SET_START_LOCATION;
        } else {
            isModePickBeginLocation = false;
            searchView.searchLocationMode = SLM_SET_END_LOCATION;
        }
        
        [self presentViewController:searchView animated:true completion:nil];
        [textField resignFirstResponder];
    } else if(textField == txtPickUpTime && !txtPickupDate.text.hash){
        [textField resignFirstResponder];
    }
    
    if (textField != txtPickupLocation && textField != txtDropOffLocation){
        activeTextField = textField;
        [self setBorderViewShow:YES];
    }
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    lblPickupLocation.text = txtPickupLocation.text;
    lblDropOffLocation.text = txtDropOffLocation.text;
    [self setBorderViewShow:NO];
    return YES;
}

-(void)setupTheBorderView {
    viewBorderPickupDate.layer.borderWidth = 1.0;
    viewBorderPickupDate.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderPickupDate.alpha = 0;
    
    viewBorderPickupTime.layer.borderWidth = 1.0;
    viewBorderPickupTime.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderPickupTime.alpha = 0;
}

- (void) setBorderViewShow:(BOOL)isShow {
    UIView * editableView;
    if (activeTextField == txtPickupDate){
        editableView = viewBorderPickupDate;
    } else if (activeTextField == txtPickUpTime) {
        editableView = viewBorderPickupTime;
    }
    
    [UIView animateWithDuration:isShow ? 0.5 : 0.2 animations:^{
        if (isShow){
            editableView.alpha = isShow;
        } else {
            editableView.alpha = activeTextField.hasText;
        }
    }];
}
- (void) resignActiveInputField {
    
    [self.view endEditing:YES];
}
#pragma mark - Custom Methods -

- (void) txtDateTouch : (UITextField *) textField {
    
    activeTextField = textField;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FORMATE_YYYY_MM_DD];
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    
    textField.inputView.backgroundColor = [UIColor whiteColor];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    
    if(textField == txtPickupDate)
    {
        datePicker.datePickerMode = UIDatePickerModeDate;
        NSDateFormatter *dateFormate1 = [[NSDateFormatter alloc] init];
        [dateFormate1 setDateFormat:DATE_FORMATE_YYYY_MM_DD];
        [dateFormate1 setDefaultDate:[NSDate date]];
        [datePicker setDate:prevousDate];
        [datePicker setMinimumDate:[NSDate date]];
        strFromDate = [dateFormat stringFromDate:[datePicker date]];
        [datePicker addTarget:self action:@selector(updateTextFieldDate:)
             forControlEvents:UIControlEventValueChanged];
        
    }
    else if(textField == txtPickUpTime)
    {
        if (txtPickupDate.text.hash){
            datePicker.datePickerMode = UIDatePickerModeTime;
            NSDateFormatter *dateFormate1 = [[NSDateFormatter alloc] init];
            if ([txtPickUpTime hasText]) {
                NSString *str = txtPickUpTime.text;
                [dateFormate1 setDateFormat:TIME_FORMAT];
                NSDate *date = [dateFormate1 dateFromString:str];
                [datePicker setDate:date];
            }
            else
            {
                [dateFormate1 setDateFormat:TIME_FORMAT];
                [dateFormate1 setDefaultDate:[NSDate date]];
                [datePicker setDate:[dateFormate1 defaultDate]];
            }
            strTime = [dateFormate1 stringFromDate:[datePicker date]];
            [datePicker addTarget:self action:@selector(updateTextFieldDate:)
                 forControlEvents:UIControlEventValueChanged];
            //        NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
            //        datePicker.locale = twelveHourLocale;
        } else {
            [Singleton showSnakBar:@"Please select date first" multiline:false];
            return;
        }
    }
    
    [textField setInputView:datePicker];
}

-(void)updateTextFieldDate:(UIDatePicker *)sender {
    if(activeTextField == txtPickupDate)
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:DATE_FORMATE_YYYY_MM_DD];
        strFromDate = [dateFormat stringFromDate:sender.date];
        prevousDate = sender.date;
    }
    else
    {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:TIME_FORMAT];
        strTime = [dateFormat stringFromDate:sender.date];
    }
}

- (void) doneOfPickupDate {
    txtPickupDate.text = strFromDate;
    [txtPickupDate resignFirstResponder];
}
- (void) doneOfPickupTime
{
    txtPickUpTime.text = strTime;
    [txtPickUpTime resignFirstResponder];
}

#pragma mark - Action methods -

- (IBAction)actionPost:(id)sender {
    
    [self.view endEditing:YES];
    
    if(![txtPickupLocation hasText]) {
        
        [Singleton showSnakBar:@"Please selecte pickup location" multiline:NO];
    }
    else if(![txtDropOffLocation hasText]) {
        
        [Singleton showSnakBar:@"Please select drop off location" multiline:NO];
    }
    else if(![txtPickupDate hasText]) {
        
        [Singleton showSnakBar:@"Please select date" multiline:NO];
    }
    else if(![txtPickUpTime hasText]) {
        
        [Singleton showSnakBar:@"Please select time" multiline:NO];
    }
    else {
       
        NSDateFormatter * dateFormater = [[NSDateFormatter alloc]init];
        dateFormater.dateFormat = @"yyyy-MM-dd";
        NSString * fullFormatedDate = [dateFormater stringFromDate:prevousDate];
        
        START_HUD
        [APP_DELEGATE.apiManager planARideWithUserID:APP_DELEGATE.loggedUserId pickup_location:lblPickupLocation.text drop_off_location:lblDropOffLocation.text pickup_latitude:dictPickupLocation[@"lattitude"] pickup_longitude:dictPickupLocation[@"longitude"] drop_off_latitude:dictDropoffLocation[@"lattitude"] drop_off_longitude:dictDropoffLocation[@"longitude"] pickup_date:fullFormatedDate pickup_time:strTime request_type:APP_DELEGATE.isUserModeDriver ? @"2" : @"1" withCallBack:^(BOOL success, NSString *serverMessange) {
            
            STOP_HUD
            if (success){
                dispatch_async(dispatch_get_main_queue(), ^{
                    lblPickupLocation.text = @"";
                    txtPickupLocation.text = @"";
                    lblDropOffLocation.text = @"";
                    txtDropOffLocation.text = @"";
                    txtPickupDate.text = @"";
                    txtPickUpTime.text = @"";
                    strTime = @"";
                    strFromDate = @"";
                    dictPickupLocation = @{};
                    dictDropoffLocation = @{};
                    
                    [btnPickupFavoruite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                    [btnDestinationFavoruite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                
                    [self actionDiscussionBOard:btnDiscussionBoard];
                });
            } else {
                SHOW_ALERT_WITH_CAUTION(serverMessange);
            }
        }];
    }
}

- (IBAction)actionMenu:(id)sender {
    
    [APP_DELEGATE showMenu];
}
- (IBAction)actionDiscussionBOard:(id)sender {
    
    DiscussionBoard *obj = (DiscussionBoard *)[[self storyboard]instantiateViewControllerWithIdentifier:@"DiscussionBoard"];
    [self.navigationController pushViewController:obj animated:YES];
}


#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SearchLocationViewControllerDelegate
-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo{
    if (DEBUG_MODE){
        NSLog(@"Selected Location : %@",locationInfo);
    }

    if (isModePickBeginLocation){
        txtPickupLocation.text = locationInfo[@"fullAddress"];
        dictPickupLocation = locationInfo;
        imgPickupHeart.image = [UIImage imageNamed:@"PHFavoriteDeactive"];
    } else {
        txtDropOffLocation.text = locationInfo[@"fullAddress"];
        dictDropoffLocation = locationInfo;
        imgDestinationHeart.image = [UIImage imageNamed:@"PHFavoriteDeactive"];
    }
    
    
    lblPickupLocation.text = txtPickupLocation.text;
    lblDropOffLocation.text = txtDropOffLocation.text;
}

-(void)findAddressForCoordinate:(CLLocationCoordinate2D)location WithCallback:(void (^) (BOOL success, NSDictionary * dictAddress))callback{
    [APP_DELEGATE.apiManager findAddressUsingLocationCoordinate:location withCallBack:^(BOOL success, NSDictionary *result, NSString *error) {
        if (success){
            callback(true,result);
        } else {
            [[GMSGeocoder geocoder] reverseGeocodeCoordinate:location completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
                if (response.results.count > 0){
                    
                    NSLog(@"%@",response);
                    
                    GMSAddress* addressObj =  response.firstResult;
                    NSString * fullAddress = @"";
                    for (NSString * title in addressObj.lines){
                        fullAddress = [fullAddress stringByAppendingString:title];
                    }
                    
                    NSLog(@"%@", addressObj.lines);
                    
                    NSString *title;
                    if (addressObj.thoroughfare){
                        title = addressObj.thoroughfare;
                    } else{
                        title = addressObj.addressLine1;
                    }
                    
                    callback(true, @{@"title":title,@"fullAddress":fullAddress});
                } else {
                    callback(false, @{@"title":@"Current Location",@"fullAddress":@""});
                }
            }];
        }
    }];
}

- (IBAction)selectLocationFromFavoriteTapped:(id)sender {
    if ([sender tag] == 101){
        [self showFavoriteLocationControllerWithIsModePickup:true];
    } else {
        [self showFavoriteLocationControllerWithIsModePickup:false];
    }
}


#pragma mark - FavoruiteSoptsDelegate
-(void)showFavoriteLocationControllerWithIsModePickup:(BOOL)isModePickup {
    if (isModePickup){
        isModePickBeginLocation = true;
    } else {
        isModePickBeginLocation = false;
    }
    
    FavouriteSpots * favSpotsView = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteSpots"];
    favSpotsView.isSelectionMode = true;
    favSpotsView.delegate = self;
    UINavigationController * navController = [[UINavigationController alloc]initWithRootViewController:favSpotsView];
    [self presentViewController:navController animated:true completion:nil];
}

-(void)shouldSelectFavoriteLocation:(NSDictionary *)favoriteLocation{
    if (isModePickBeginLocation){
        imgPickupHeart.image = [UIImage imageNamed:@"PHHeartFilled"];
        txtPickupLocation.text = favoriteLocation[@"address"];
        lblPickupLocation.text = txtPickupLocation.text;
        dictPickupLocation = favoriteLocation;
    } else {
        imgDestinationHeart.image = [UIImage imageNamed:@"PHHeartFilled"];
        txtDropOffLocation.text = favoriteLocation[@"address"];
        lblDropOffLocation.text = txtDropOffLocation.text;
        dictDropoffLocation = favoriteLocation;
    }
}


@end
