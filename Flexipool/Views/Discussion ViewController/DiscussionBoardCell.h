//
//  DiscussionBoardCell.h
//  Flexipool
//
//  Created by Krishna on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscussionBoardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblRiderName;
@property (weak, nonatomic) IBOutlet UILabel *lblProfessional;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupPlace;
@property (weak, nonatomic) IBOutlet UILabel *lblDropPlace;

@property (weak, nonatomic) IBOutlet UILabel *lblSenderDate;
@property (weak, nonatomic) IBOutlet UIButton *buttonUserProfile;
@property (strong, nonatomic) IBOutlet UIImageView *imgUserProfileImage;

@end
