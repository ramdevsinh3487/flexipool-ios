//
//  DiscussionBoard.h
//  Flexipool
//
//  Created by Krishna on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FilterViewController.h"
#import <MessageUI/MessageUI.h>
@interface DiscussionBoard : UIViewController<UITableViewDelegate,UITableViewDataSource, FilterViewControllerDelegate, MFMessageComposeViewControllerDelegate>
{
    IBOutlet UILabel *lblHeader;
    IBOutlet UITableView *tblView;

    IBOutlet UIView *viewUserProfileContainer;
    IBOutlet UIButton *imageViewProfile;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblRideDate;
    IBOutlet UILabel *lblUserAge;
    IBOutlet UILabel *lblGender;
    IBOutlet UILabel *lblUserDecupation;
    IBOutlet UIButton *btnCallUser;
    IBOutlet UIButton *btnMessagerUser;
    IBOutlet NSLayoutConstraint *profileContainerBottomSpace;
    NSString * userMobile;
    
    NSArray * filteredUsers;
    NSDate * filteredDate;
    NSMutableArray * arrayPlanedRides;
    BOOL isLoadingData;
    
    IBOutlet UILabel *lblDataMessage;
    
//    int totalItemsCount;
//    int currentPage;
//    BOOL canClearRecords;
    
    NSString *strMyPost;
    IBOutlet UIButton *btnMyPost;
    
}


-(IBAction)ActionMyPost:(id)sender;
- (IBAction)actionBack:(id)sender;


@end
