//
//  DiscussionBoardCell.m
//  Flexipool
//
//  Created by Krishna on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "DiscussionBoardCell.h"

@implementation DiscussionBoardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
     [self.lblDate setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
     [self.lblDropPlace setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
     [self.lblRiderName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
     [self.lblPickupPlace setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    [self.lblProfessional setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
