//
//  DiscussionBoard.m
//  Flexipool
//
//  Created by Krishna on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "DiscussionBoard.h"
#import "DiscussionBoardCell.h"
@interface DiscussionBoard ()

@end

@implementation DiscussionBoard

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    lblHeader.text = @"Carpool Offers";
    [lblDataMessage setFont:[UIFont fontWithName:AppFont_Regular size:POPUP_HEADER_FONTS_SIZE]];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureFoundForHideProfileView:)];
    [viewUserProfileContainer addGestureRecognizer:tapGesture];
    tblView.tableFooterView = [UIView new];
    
    
    //This Code Will Enables Pagination
//    currentPage = 1;
//    canClearRecords = false;
//    totalItemsCount = 0;
    
    isLoadingData = true;
    arrayPlanedRides = [[NSMutableArray alloc]init];
    [tblView reloadData];
    
    
    strMyPost = @"1";
    [self ActionMyPost:nil];

   // [self getDiscussionDataFromServerWithSelectedDate:@"" selectedUser:@""];

}

-(IBAction)ActionMyPost:(id)sender{
    
    if ([strMyPost isEqualToString:@"0"])
    {
        [btnMyPost setImage:[UIImage imageNamed:@"checkbox-active"] forState:UIControlStateNormal];
        strMyPost = @"1";
    }
    else
    {
        [btnMyPost setImage:[UIImage imageNamed:@"checkbox-deactive"] forState:UIControlStateNormal];
        strMyPost = @"0";
    }
    
    
    START_HUD
     [self getDiscussionDataFromServerWithSelectedDate:@"" selectedUser:@""];
}

-(void)getDiscussionDataFromServerWithSelectedDate:(NSString *)selectedDate selectedUser:(NSString *)selectedUser{
    
//    if (totalItemsCount > 0){
//        START_HUD
//    }
    

    [APP_DELEGATE.apiManager getDiscussionBoardDataWithSearchDate:selectedDate SearchUserId:selectedUser PageNumber:@"0" post:strMyPost withCallBack:^(BOOL success, NSArray *arrayResponse, int totalItems, NSString *serverMessange) {
        isLoadingData = false;
        arrayPlanedRides = [[NSMutableArray alloc] init];
        if (success){
            
            STOP_HUD
            
            arrayPlanedRides = [[NSMutableArray alloc]initWithArray:arrayResponse];
            
//            if (!totalItemsCount || totalItemsCount == 0){
//                arrayPlanedRides = [[NSMutableArray alloc]initWithArray:arrayResponse];
//                totalItemsCount = totalItems;
//            } else {
//                [arrayPlanedRides addObjectsFromArray:arrayResponse];
//                STOP_HUD
//            }
        } else {
            
            STOP_HUD
            
            [Singleton showSnakBar:serverMessange multiline:true];
        }
        [tblView reloadData];
    }];
    
}

#pragma mark - UITableView Datasource Method -
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (isLoadingData){
        lblDataMessage.hidden = false;
        tblView.hidden = true;
        lblDataMessage.text = @"Loading data, Please wait...";
        return 0;
    } else {
        if (arrayPlanedRides.count > 0){
            lblDataMessage.hidden = true;
            tblView.hidden = false;
            return arrayPlanedRides.count;
        } else {
            lblDataMessage.hidden = false;
            tblView.hidden = true;
            lblDataMessage.text = @"No data found";
            return 0;
        }
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isLoadingData){
        lblDataMessage.hidden = false;
        tblView.hidden = true;
        lblDataMessage.text = @"Loading data, Please wait...";
        return 0;
    } else {
        if (arrayPlanedRides.count > 0){
            lblDataMessage.hidden = true;
            tblView.hidden = false;
            return ((NSArray *)((NSDictionary *)arrayPlanedRides[section])[@"rides"]).count;
        } else {
            lblDataMessage.hidden = false;
            tblView.hidden = true;
            lblDataMessage.text = @"No data found";
            return 0;
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView * headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 100, 45)];
    headerView.backgroundColor = [UIColor clearColor];
    
    UILabel * lableDate = [[UILabel alloc]initWithFrame:CGRectMake(100, 5, SCREEN_WIDTH - 200, 35)];
    lableDate.backgroundColor = [UIColor lightGrayColor];
    lableDate.textColor = [UIColor blackColor];
    [lableDate setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    NSDateFormatter * dateFormter = [[NSDateFormatter alloc]init];
    [dateFormter setDateFormat:@"yyyy-MM-dd"];
    NSDate * dateToConvert = [dateFormter dateFromString:arrayPlanedRides[section][@"date"]];
    [dateFormter setDateFormat:@"yyyy-MM-dd"];
    NSString * dateToDisplay = [dateFormter stringFromDate:dateToConvert];
    
    lableDate.text = dateToDisplay;
    [lableDate setTextAlignment:NSTextAlignmentCenter];
    lableDate.layer.cornerRadius = 17.5;
    lableDate.layer.masksToBounds = true;
    [headerView addSubview:lableDate];
    headerView.clipsToBounds = true;
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * dictData = arrayPlanedRides[indexPath.section][@"rides"][indexPath.row];
    
    if (DEBUG_MODE){
        NSLog(@"Planed Ride Details : %@",dictData);
    }

    static NSString *simpleTableIdentifier;
    
    simpleTableIdentifier = @"DiscussionBoardCell";
    
    DiscussionBoardCell *cell = (DiscussionBoardCell *)[tblView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if([APP_DELEGATE.loggedUserId isEqualToString:dictData[@"user_id"]]) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DiscussionBoardSenderCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }  else {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DiscussionBoardReceiverCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    [cell.buttonUserProfile setTag:indexPath.row];
    [cell.buttonUserProfile addTarget:self action:@selector(userProfileButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    cell.buttonUserProfile.tag = indexPath.row;
    cell.buttonUserProfile.restorationIdentifier = [NSString stringWithFormat:@"%ld",(long)indexPath.section];
    
    UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
    
//    NSString * gender = dictData[@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, cell.imgUserProfileImage.frame.size.width, cell.imgUserProfileImage.frame.size.height)];
    
    activity_indicator.tag = 11111;
    [[cell.imgUserProfileImage viewWithTag:1111] removeFromSuperview];
    
    [cell.imgUserProfileImage addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    NSString *strUrl = dictData[@"user_image"];
    strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [cell.imgUserProfileImage sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [[cell.imgUserProfileImage viewWithTag:1111] removeFromSuperview];
    }];
    
    NSString * dateWithTime =[NSString stringWithFormat:@"%@ %@", dictData[@"pickup_date"], dictData[@"pickup_time"]];
    NSDateFormatter * formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSLocale *twelveHourLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    formatter.locale = twelveHourLocale;
    
    NSDate * rideDate = [formatter dateFromString:dateWithTime];
    formatter.dateFormat = [NSString stringWithFormat:@"%@, hh:mm a", DATE_FORMATE_YYYY_MM_DD];
    NSString * formatedDate = [formatter stringFromDate:rideDate];
    
    
    cell.imgUserProfileImage.layer.cornerRadius = cell.imgUserProfileImage.frame.size.height / 2;
    cell.imgUserProfileImage.backgroundColor = [UIColor whiteColor];
    cell.imgUserProfileImage.layer.borderWidth = 0.5;
    cell.imgUserProfileImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    cell.imgUserProfileImage.layer.masksToBounds = true;
    cell.imgUserProfileImage.clipsToBounds = true;
    
    NSString * requestType = @"";
    if ([dictData[@"request_type"] isEqualToString:@"1"]){
        requestType = @"(Requesting Ride)";
    } else {
        requestType = @"(Offering Ride)";
    }
    
    cell.lblRiderName.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE - 3];
    
    cell.lblDropPlace.text = [NSString stringWithFormat:@"%@", dictData[@"drop_off_location"]];
    cell.lblRiderName.text = [NSString stringWithFormat:@"%@ %@",dictData[@"user_name"], requestType];
    cell.lblSenderDate.text = [NSString stringWithFormat:@"%@",formatedDate];
    cell.lblPickupPlace.text = [NSString stringWithFormat:@"%@",dictData[@"pickup_location"]];
    cell.lblProfessional.text = [NSString stringWithFormat:@"%@",dictData[@"occupation"]];

    //This Code Will Enables Pagination
//    if (indexPath.section == arrayPlanedRides.count - 1){
//        if (DEBUG_MODE){
//            NSLog(@"This is the last index");
//        }
//        
//        if (totalItemsCount != arrayPlanedRides.count && isLoadingData == false){
//            canClearRecords = false;
//            [self didApplyFilterWithDateOfTravel:filteredDate favoriteUsers:filteredUsers];
//        }
//    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    [self showUserInfoForIndex:(int)indexPath.row withSection:(int)indexPath.section];
}

- (IBAction)userProfileButtonTapped:(UIButton *)sender {
    //Get User data from array, Your index will be sender.tag
    
    int section = [sender.restorationIdentifier intValue];
    int index = (int)sender.tag;
    
    [self showUserInfoForIndex:index withSection:section];
}

-(void)showUserInfoForIndex:(int)index withSection:(int)section{
    NSDictionary * dictData = arrayPlanedRides[section][@"rides"][index];
    
    
    UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
    
//    UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//    
//    NSString * gender = APP_DELEGATE.dictUserDetails[@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    [imageViewProfile setImage:dummyImage forState:UIControlStateNormal];
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imageViewProfile.imageView.frame.size.width, imageViewProfile.imageView.frame.size.height)];
    
    activity_indicator.tag = 11111;
    [[imageViewProfile viewWithTag:1111] removeFromSuperview];
    
    [imageViewProfile addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    UIImageView * dummyImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    dummyImageView.hidden = true;
    [self.view addSubview:dummyImageView];
    
    
    NSString *strUrl = dictData[@"user_image"];
    strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [dummyImageView sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            [imageViewProfile setImage:image forState:UIControlStateNormal];
        } else {
            [imageViewProfile setImage:dummyImage forState:UIControlStateNormal];
        }
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [[imageViewProfile viewWithTag:1111] removeFromSuperview];
    }];
    
    lblUserName.text = [NSString stringWithFormat:@"%@",dictData[@"user_name"]];
    lblRideDate.text = [NSString stringWithFormat:@"Date of ride : %@",dictData[@"pickup_date"]];
    lblUserAge.text = [NSString stringWithFormat:@"Age : %@",dictData[@"age"]];
    lblGender.text = [NSString stringWithFormat:@"Gender : %@",dictData[@"gender"]];
    lblUserDecupation.text = [NSString stringWithFormat:@"Occupation : %@",dictData[@"occupation"]];
    userMobile = dictData[@"mobile_no"];
    
    viewUserProfileContainer.hidden = false;
    [UIView animateWithDuration:0.3 animations:^{
        viewUserProfileContainer.alpha = 1.0;
    } completion:^(BOOL finished) {
        profileContainerBottomSpace.constant = 10;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }];
}

-(void)tapGestureFoundForHideProfileView:(UIGestureRecognizer *)gesture{
    profileContainerBottomSpace.constant = -250;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            viewUserProfileContainer.alpha = 0.0;
        }completion:^(BOOL finished) {
            viewUserProfileContainer.hidden = true;
        }];
    }];
}

#pragma mark - Action Methods -

- (IBAction)actionBack:(id)sender {
    
    backAction()
}

- (IBAction)phoneButtonTapped:(id)sender {
    //Will Open the call
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",userMobile]]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",userMobile]]];
    } else {
        SHOW_ALERT_WITH_CAUTION(@"Unable to make phone calls \n Please check your device settings");
        return;
    }
}

- (IBAction)messageButtonTapped:(id)sender {
    //Will Open the message
    
    if(![MFMessageComposeViewController canSendText]) {
        SHOW_ALERT_WITH_CAUTION(@"Unable to send message \n Please check your device settings");
        return;
    } else {
        NSArray *recipents = @[userMobile];
        NSString * userName = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictUserDetails[@"first_name"],
                               APP_DELEGATE.dictUserDetails[@"last_name"]];
        
        NSString *message = [NSString stringWithFormat:@"Ahoy user <%@> is contacting you \n\n",userName];
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setBody:message];
        [self presentViewController:messageController animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            if (DEBUG_MODE){
                NSLog(@"Failed To send SMS");
            }
            break;
        }
            
        case MessageComposeResultSent:
            break;
            if (DEBUG_MODE){
                NSLog(@"Your message has been sent");
            }
        default:
            break;
    }
    
    [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)filterButtonTapAction:(id)sender {
    FilterViewController* filterVc = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterViewController"];
    filterVc.delegate = self;
    filterVc.arraySelectedUsers = [[NSMutableArray alloc]initWithArray:filteredUsers];
    filterVc.selectedDate = filteredDate;
    [self presentViewController:filterVc animated:true completion:nil];
//    canClearRecords = true;
}

-(void)didApplyFilterWithDateOfTravel:(NSDate *)dateOfTravels favoriteUsers:(NSArray *)arrayFavoriteUsers {
    
//    if (canClearRecords){
//        canClearRecords = false;
//        isLoadingData = true;
//        arrayPlanedRides = [[NSMutableArray alloc]init];
//        [tblView reloadData];
//    }
    
    isLoadingData = true;
    arrayPlanedRides = [[NSMutableArray alloc]init];
    [tblView reloadData];
    
    filteredUsers = arrayFavoriteUsers;
    filteredDate = dateOfTravels;
    NSString * userIds = @"";
    if (arrayFavoriteUsers && arrayFavoriteUsers.count > 0){
        for (NSDictionary * dictUser in arrayFavoriteUsers){
            userIds = [NSString stringWithFormat:@"%@%@,",userIds,dictUser[@"user_id"]];
        }
        
        userIds = [userIds substringToIndex:[userIds length] - 1];
    }
    
    NSString * dateString = @"";
    if (dateOfTravels){
        NSDateFormatter * dateFormate = [[NSDateFormatter alloc]init];
        dateFormate.dateFormat = @"yyyy-MM-dd";
        dateString = [dateFormate stringFromDate:dateOfTravels];
    }
    
    [self getDiscussionDataFromServerWithSelectedDate:dateString selectedUser:userIds];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
