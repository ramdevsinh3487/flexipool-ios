//
//  PaymentViewController.m
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "PaymentViewController.h"
#import "PayPalMobile.h"

@interface PaymentViewController ()

@end

@implementation PaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    selectedPayment = -1;
    self.payableAmount = [self.dictTripDetails[@"trip_totalcost"] doubleValue];
    confirmationRetryCount = 0;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setupInitialView];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [APP_DELEGATE updateHomeStatusArrayWithIndex:@""];
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RIDE_STATUS_UPDATED object:nil];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    
    [btnNotifications setShouldAnimateBadge:true];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}

- (void) setupInitialView {
    [lblYourRideCost setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 4]];
    [lblRidePrice setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 6]];
    
    [lblPayUsingWallet setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    [btnPayUsingWallet.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnPayPal.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnCash.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnPay.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [btnCard.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionBack:) name:@"dismissParentView" object:nil];
    
    lblHeader.text = GET_Language_Value(@"payment");
    lblYourRideCost.text = GET_Language_Value(@"your_ride_cost");
    lblRidePrice.text = [APP_DELEGATE formatTheCostWithInputValue:self.payableAmount isModeValue:false];
    
    lblPayUsingWallet.text = [NSString stringWithFormat:@"(%@. %@)",[GET_Language_Value(@"bal") lowercaseString],[APP_DELEGATE formatTheCostWithInputValue:[APP_DELEGATE.WalletAmount doubleValue] isModeValue:false]];

//    [btnPayUsingWallet setTitle:GET_Language_Value(@"pay_using_wallet") forState:UIControlStateNormal];
//     [btnCash setTitle:GET_Language_Value(@"cash") forState:UIControlStateNormal];
//    [btnCard setTitle:GET_Language_Value(@"Credit/Debit Card") forState:UIControlStateNormal];
    [btnPay setTitle:GET_Language_Value(@"pay") forState:UIControlStateNormal];
    
    rideId = self.dictTripDetails[@"ride_id"];
    if (APP_DELEGATE.isUserModeDriver){
        receiverId = self.dictTripDetails[@"customer_user_id"];
    } else {
        receiverId = self.dictTripDetails[@"driver_id"];
    }
}

#pragma mark - Action Methods -

- (IBAction)ActionPaymentMode:(UIButton *)sender {
    
    [btnPayUsingWallet setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnCash setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnCard setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnPayPal setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    
    [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
    
    selectedPayment = [sender tag];
    
//    if (sender == btnPayUsingWallet && ([APP_DELEGATE.WalletAmount doubleValue] < self.payableAmount)){
//        [btnPay setTitle:@"Refill" forState:UIControlStateNormal];
//        btnPay.tag = 1111;
//    } else {
//        [btnPay setTitle:@"Pay" forState:UIControlStateNormal];
//        btnPay.tag = 0000;
//    }
}

- (IBAction)actionPay:(id)sender {
    if (selectedPayment <= 0){
        [Singleton showSnakBar:@"Please select any one of payment method" multiline:false];
    } else {
        AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
        obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
        obj.delegate = self;
        obj.alertTitles = @"Are you sure want to continue with selected payment method";
        obj.alertMessages = @"";
        obj.yesTitle = @"Yes";
        obj.noTitle = @"No";
        [self presentViewController:obj animated:NO completion:nil];
    }
}

- (IBAction)actionRefill:(id)sender {
    AddMoney * walletVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMoney"];
    UINavigationController * navC = [[UINavigationController alloc]initWithRootViewController:walletVC];
//    walletVC.isModeRefillFromPayment = true;
    [self presentViewController:navC animated:true completion:nil];
}

- (void)alertControllerButtonTappedWithIndex:(NSInteger)index {
    if (index == 1){
        NSString * amount = [NSString stringWithFormat:@"%f",self.payableAmount];
        if (selectedPayment == 1){
            paymentType = @"1";
            if ([APP_DELEGATE.WalletAmount doubleValue] < self.payableAmount){
                [Singleton showSnakBar:@"You don't have sufficient balance in your wallet to pay for this ride" multiline:true];
                return;
            } else {
                [self callWebserviceForMakePayment:paymentType];
            }
        } else if (selectedPayment == 2){
            //Selected payment over paypal
            paymentType = @"2";
            [[Singleton sharedInstance] PayPalCallWithAmount:amount delegatedView:self];
        } else if (selectedPayment == 3){
            //Selected payment over Credit / Debit card
            paymentType = @"4";
            [self OpenCardIOViewForCreditDebitCard];
        } else {
            //Selected payment over cash
            paymentType = @"3";
        }

    } else {
        
    }
}



#pragma mark - Open Credit/Debit Card -
-(void)OpenCardIOViewForCreditDebitCard{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self scanningEnabled:false];
    scanViewController.collectCardholderName = true;
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

#pragma mark - CardIOPaymentViewControllerDelegate
- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController {
    if (DEBUG_MODE){
        NSLog(@"Scan succeeded with info: %@",info.cardNumber);
        NSLog(@"Scan succeeded with info: %@",info.cvv);
        NSLog(@"Scan succeeded with info: %lu",(unsigned long)info.expiryYear);
        NSLog(@"Scan succeeded with info: %ld",(long)info.redactedCardNumber);
        NSLog(@"Scan succeeded with info: %lu",(unsigned long)info.expiryMonth);
    }
    
    
    NSMutableDictionary *dictInfo = [[NSMutableDictionary alloc] init];
    [dictInfo setValue:info.cardNumber forKey:@"CardNumber"];
    [dictInfo setValue:[NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictUserDetails[@"first_name"],APP_DELEGATE.dictUserDetails[@"last_name"]] forKey:@"CardHolder"];
    [dictInfo setValue:[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryMonth] forKey:@"CardExpiryMonth"];
    [dictInfo setValue:[NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear] forKey:@"CardExpiryYear"];
    [dictInfo setValue:[NSString stringWithFormat:@"%f",self.payableAmount] forKey:@"TotalAmount"];
    [dictInfo setValue:APP_DELEGATE.dictUserDetails[@"first_name"] forKey:@"FirstName"];
    [dictInfo setValue:APP_DELEGATE.dictUserDetails[@"last_name"] forKey:@"LastName"];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self callAPIforTransactionWithAuthorizedNet:dictInfo];
    }];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    if (DEBUG_MODE){
        NSLog(@"User cancelled scan");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark - Method for Authorized Bank API For Transaction -
-(void)callAPIforTransactionWithAuthorizedNet:(NSMutableDictionary *)dictCardInfo{
    
    NSDictionary *dict = @{@"user_id" : APP_DELEGATE.loggedUserId,
                           @"token" : APP_DELEGATE.tokenString,
                           @"card_number": [dictCardInfo valueForKey:@"CardNumber"],
                           @"card_holder": [dictCardInfo valueForKey:@"CardHolder"],
                           @"card_expire_month": [dictCardInfo valueForKey:@"CardExpiryMonth"],
                           @"card_expire_year": [dictCardInfo valueForKey:@"CardExpiryYear"],
                           @"total_amount": [dictCardInfo valueForKey:@"TotalAmount"],
                           @"first_name": [dictCardInfo valueForKey:@"FirstName"],
                           @"last_name" : [dictCardInfo valueForKey:@"LastName"]};
    
    [APP_DELEGATE.apiManager AuthorizedBankTransaction:dict withCallBack:^(BOOL success, NSMutableArray *response, NSString *errorMessage) {
        
        if (success)
        {
            NSMutableArray *dictResponse = response;;
            
            NSString *strCode = [NSString stringWithFormat:@"%@",[[dictResponse valueForKey:@"code"] objectAtIndex:0]];
            
            if ([strCode integerValue] == 1) {
                
                [self callWebserviceForMakePayment:paymentType];
            }
            else {
                
                [Singleton showSnakBar:errorMessage multiline:true];
            }
            
            
        } else {
            [Singleton showSnakBar:errorMessage multiline:true];
        }
        
        STOP_HUD;
    }];
    
}


-(void)callWebserviceForMakePayment:(NSString *)strPaymentType{
    
    NSString * amount = [NSString stringWithFormat:@"%f",self.payableAmount];
    
    START_HUD
    [APP_DELEGATE.apiManager submitThePaymentForCostOfTheRideWithRideId:rideId paymentType:strPaymentType PayableAmount:amount withCallBack:^(BOOL success, NSString *serverMessange) {
        if (success){
            [self confirmTheRideStatusWithRideId];
        } else {
            STOP_HUD
            [Singleton showSnakBar:serverMessange multiline:true];
        }
    }];

}

-(void)confirmTheRideStatusWithRideId {
    

    confirmationRetryCount = confirmationRetryCount + 1;
    [APP_DELEGATE.apiManager confirmUpdatedStatusWithRideId:rideId RideStatus:@"5" withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        
        if (success){
            STOP_HUD
            SHOW_ALERT_WITH_SUCCESS(@"Your Payment has been successfully Received")
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                FeedbackViewController *obj = (FeedbackViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"FeedbackViewController"];
                obj.dictTripDetails = self.dictTripDetails;
                [self presentViewController:obj animated:true completion:nil];
                [self.delegate didSubmitPaymentSuccessfullForRideWithRideDetails:self.dictTripDetails];
            });
        } else {
            if (DEBUG_MODE){
                NSLog(@"Can Not Confirm : %@",serverMessange);
            }
            
            if (confirmationRetryCount <= 3){
                [self confirmTheRideStatusWithRideId];
            } else {
                STOP_HUD
                [Singleton showSnakBar:@"We can not update your payment status \n Kindly please contact Admin" multiline:true];
            }
        }
    }];
}


#pragma mark - PaypalDelegate
- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    [self dismissViewControllerAnimated:true completion:^{
        [self callWebserviceForMakePayment:paymentType];
    }];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark  - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)actionBack:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
