//
//  PaymentViewController.h
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"
#import <CardIO/CardIO.h>
#import "AhoyWallet.h"

@protocol PaymentViewControllerDelegate <NSObject>
@optional
-(void)didSubmitPaymentSuccessfullForRideWithRideDetails:(NSDictionary *)dictTripDetails;
@end

@interface PaymentViewController : UIViewController <CardIOPaymentViewControllerDelegate, AlertViewControllerDelegate>
{
    IBOutlet UILabel *lblHeader;
    IBOutlet UILabel *lblYourRideCost;
    IBOutlet UILabel *lblRidePrice;
    
    IBOutlet UIButton *btnPayUsingWallet;
    IBOutlet UILabel *lblWallet;
    
    IBOutlet UIButton *btnCash;
    IBOutlet UIButton *btnPayPal;
    IBOutlet UIButton *btnPayPalICon;
    IBOutlet UILabel *lblPayUsingWallet;
    IBOutlet UIButton *btnPay;
    IBOutlet UIButton *btnRefill;
    
    IBOutlet UIButton *btnCard;
    
    IBOutlet UIButton *btnNotifications;
    NSInteger selectedPayment;
    
    int confirmationRetryCount;
    
    NSString * rideId, * receiverId;
    
     NSString * paymentType;
}

@property float payableAmount;
@property NSDictionary * dictTripDetails;

- (IBAction)actionBack:(id)sender;
- (IBAction)actionPay:(id)sender;
- (IBAction)actionRefill:(id)sender;

@property id<PaymentViewControllerDelegate> delegate;
@end
