//
//  HomeViewController.h
//  Flexipool
//
//  Created by HimAnshu on 11/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "StartRide.h"
#import "SearchLocationViewController.h"
#import "FavouriteSpots.h"
#import "NavigationStepsView.h"


@interface HomeViewController : UIViewController <GMSMapViewDelegate, AlertViewControllerDelegate, ReasonViewControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, StartRideDelegate, SearchLocationViewControllerDelegate, FavouriteSpotsDelegate, PassengerRequestViewControllerDelegate, PaymentViewControllerDelegate, UIScrollViewDelegate>
{
    
    IBOutlet UIView *viewTransperent;
    IBOutlet NSLayoutConstraint *transperentViewHeight;
    
    IBOutlet UIView *viewCustomNavBar;
    IBOutlet UIButton *btnNotifications;
    IBOutlet UIImageView *imgViewHeaderTab;
    IBOutlet UIImageView *imgViewRider;
    IBOutlet UIImageView *imgViewDriver;
    IBOutlet UILabel *lblRider;
    IBOutlet UILabel *lblDriver;
    
    IBOutlet GMSMapView *viewMap;
    
    NSTimer * timerFindAddress, * timerShowViews;
    
    IBOutlet UIButton *btnSOS;
    IBOutlet NSLayoutConstraint *heightBtnSOS;
    IBOutlet UIButton *btnSound;
    IBOutlet NSLayoutConstraint *heightBtnSound;
    IBOutlet UIButton *btnShareRide;
    IBOutlet NSLayoutConstraint *heightBtnShareRide;
    IBOutlet UIButton *btnRecenter;
    IBOutlet NSLayoutConstraint *heightBtnRecenter;
    
    IBOutlet UIView *viewLocationSelection;
    IBOutlet NSLayoutConstraint *constraintViewLocationTop;
    IBOutlet UILabel *lblPickupLocation;
    IBOutlet UILabel *lblDestinationLocation;
    IBOutlet UIButton *btnPickupFavorite;
    IBOutlet UIButton *btnDestinationFavorite;
    
    
    
    // Driver Info
    IBOutlet UIView *viewDriverInfo;
    IBOutlet UIView *viewDriverInfoInner;
    IBOutlet UIView *viewCarInfo;
    IBOutlet NSLayoutConstraint *carInfoHeight;
    IBOutlet UILabel *lblDriverCurrentLocation;
    IBOutlet UIButton *btnRequestRide1;
    IBOutlet UIButton *btnRequestRide2;
    IBOutlet UICollectionView *collectionDriverList;
    
    IBOutlet UILabel *lblPassRidesGiven;
    IBOutlet UILabel *lblPassRidesTaken;
    IBOutlet UIView *viewPassRidesGiven;
    IBOutlet UIView *viewPassRidesTaken;
    
    IBOutlet UILabel *lblHeadingTO;
    IBOutlet UILabel *lblHeadingToLabel;
    IBOutlet UILabel *lblHeadingToSecond;
    IBOutlet UIButton *btnImgHeading;
    
    
    UIPanGestureRecognizer *panRecognizer;
//    NSInteger selectedDriverIndex;
    NSString * selectedUserId;
    NSString * selectedVehicleYear;
    
    // Current Ride of Passenger (Driver Info)
    IBOutlet UIView *viewCurrentTripDriver;
    IBOutlet NSLayoutConstraint *bottomSpaceCurrentTripDriver;
    IBOutlet UIImageView *imgCurrentTripDriverProfile;
    IBOutlet UILabel *lblCurrentTripDriverName;
    IBOutlet UILabel *lblCurrentTripDriverCarIno;
    IBOutlet UILabel *lblCurrentTripETA;
    IBOutlet UIView *viewNavCurrentTripDriverETA;
    IBOutlet UILabel *lblNavCurrentTripDriverETA;
    IBOutlet UILabel *lblCurrentTripDriverPlate;
    IBOutlet UIButton *btnCurrentTripDriverCancel;
    IBOutlet NSLayoutConstraint *bottomSpaceCurentTripDriverCancelButton;

    // Current Ride of Driver (Passenger Info)
    IBOutlet UIView *viewCurrentTripPessanger;
    IBOutlet NSLayoutConstraint *bottomSpaceCurrentTripPessanger;
    IBOutlet UIImageView *imgCurrentTripPessengerPofile;
    IBOutlet UILabel *lblCurrentTripPessangerName;
    IBOutlet UILabel *lblCurrentTripPessangerETA;
    IBOutlet UILabel *lblCurrentTripPessangerDistance;
    IBOutlet UIButton *btnCurrentTripPessabgerSmallCancel;
    IBOutlet UIButton *btnCurrentTripPessangerSmallConfirm;
    IBOutlet UIButton *btnCurrentTripPessangerBigCancel;
    IBOutlet NSLayoutConstraint *bottomSpaceCurentTripPessangerCancelButton;
    
    // Temporary Boolean
    BOOL isInitialMarkerChanged,isAnimationStarted,isModePickBeginLocation,canHideMapOnDrag, canHideViewHUD;
    
    NSMutableArray * arrayNearByCarMarkers;
    
    __weak IBOutlet UILabel *lblPopUpCarName;
    __weak IBOutlet UILabel *lblPopUpMAxRiders;
    __weak IBOutlet UILabel *lblPopUpRidesGiven;
    __weak IBOutlet UILabel *lblPopUpRidesTaken;
    
    __weak IBOutlet UILabel *lblPopUpDriverName;
    
    __weak IBOutlet UILabel *lblPopUpUsuallyDrivers;
    __weak IBOutlet UILabel *lblPopUpPrefersRide;
    __weak IBOutlet UILabel *lblPopUpOccupation;
    __weak IBOutlet UILabel *lblPopUpGEnder;
    __weak IBOutlet UILabel *lblPopUpAge;
    IBOutlet UILabel *lblPopUpETA;
    

    
    GMSMarker * myLocationMarker;
    //Pulstor Animation
    int remainigTime;
    int totalTimeToWait;
    NSTimer * timerPulstorWaiting;
    BOOL isAlertModeExpire;
    BOOL isAlertModeConfirmation;
    BOOL isAlertModePlanRide;
    BOOL isAlertModeNoUser;
    
    BOOL canShowNoUserAlert;
    
    BOOL isDisplayedNoUserBanner;
    BOOL canCallAPIForRoute;
    BOOL isMapRecentered;

    IBOutlet UIView *viewPulstorContainer;
    IBOutlet NSLayoutConstraint *pulstor1Width;
    IBOutlet NSLayoutConstraint *pulstor2Width;
    IBOutlet NSLayoutConstraint *pulstorContainerHeight;
    IBOutlet UIImageView *imageViewPulstor1;
    IBOutlet UIImageView *imageViewPulstor2;
    IBOutlet UILabel *lableTimeDetails;
    IBOutlet UILabel *lableRemainingTime;
    IBOutlet UIImageView *imageViewUserType;
    
    IBOutlet NSLayoutConstraint *constraintCollectionWidth;
    
    float userContainerHeight;
    
    GMSPolyline * poliLine;
    GMSMarker * userMarker;
    
    //Naviagtions
    IBOutlet UIView *viewBennerETABG;
    IBOutlet UILabel *lblBannerETA;
    
    int activeRouteIndex;
    int activeStepIndex;
    BOOL canUpdateRoutes;
    BOOL isModeETADistance;
    NSString * previusSpeech;
    
    NSMutableArray * arrayAlternativeRoutes;
    NSMutableArray * arrayActiveRoutesSteps;
    NSMutableArray * arrayRespectedPolylines;
    
    IBOutlet UIView *viewNavHeading;
    IBOutlet UILabel *lblNavStepDistance;
    IBOutlet UILabel *lblNavSpetDescription;
    IBOutlet UIButton *btnNavOpitons;
    IBOutlet NSLayoutConstraint *heightBtnNavOptions;
    IBOutlet UIButton *btnSpeakHeader;
    
    
    IBOutlet UIView *viewNavNextDirection;
    IBOutlet UIImageView *imgNavNextDirection;
    IBOutlet UILabel *lblNavNextDirectionTitle;
    IBOutlet NSLayoutConstraint *heightNavNextDirection;
    
    IBOutlet UIButton *btnNavShowSteps;
    IBOutlet NSLayoutConstraint *heightNavShowSteps;
    
    IBOutlet UIView *viewNavOptions;
    IBOutlet UIButton *btnAvoidTolls;
    IBOutlet UILabel *lblAvoidTolls;
    IBOutlet UILabel *lblAvoidHighways;
    IBOutlet NSLayoutConstraint *heightBtnAvoidTolls;
    IBOutlet UIButton *btnAvoidHighways;
    IBOutlet UIButton *btnOkay;
    
    IBOutlet UILabel *lnlConfirmationNumber;
    
    
    IBOutlet UIView *viewTotalUsers;
    IBOutlet UIImageView *imgTotalUserIcon;
    IBOutlet UILabel *lblTotalUserLable;
    
    IBOutlet UIStackView *stackCommuteTo;
    IBOutlet NSLayoutConstraint *topLabelETA;
}


@property int counter;
@property int timerStatus;
@property int tempCount;
- (IBAction)actionUserModeChange:(id)sender;
- (IBAction)actionMenu:(id)sender;
- (IBAction)actionRequestRide:(id)sender;
- (IBAction)btnSoundTapAction:(UIButton *)sender;
- (IBAction)btnShareRideTapAction:(UIButton *)sender;
- (IBAction)btnRecenterTapAction:(UIButton *)sender;


#pragma mark- Navigation Corner
- (IBAction)btnNavOptionTapAction:(UIButton *)sender;
- (IBAction)btnNavShowStepsTapAction:(UIButton *)sender;
- (IBAction)btnNavOptionsTapAction:(UIButton *)sender;
- (IBAction)btnNavOptionOkayButtonTapAction:(UIButton *)sender;
- (IBAction)btnSpeakHeaderTapAction:(id)sender;

@end
