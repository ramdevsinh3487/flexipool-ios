//
//  HomeViewController.m
//  Flexipool
//
//  Created by HimAnshu on 11/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "HomeViewController.h"
#import "DriversCell.h"

#define SHOW_DRIVER_LIST_VIEW_FULL (SCREEN_HEIGHT - viewDriverInfo.frame.size.height)
#define SHOW_DRIVER_LIST_VIEW_HALF SCREEN_HEIGHT-lblPopUpPrefersRide.frame.origin.y-viewDriverInfoInner.frame.origin.y
#define HIDE_DRIVER_LIST_VIEW SCREEN_HEIGHT+10

@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize counter,tempCount,timerStatus;

#pragma mark - ViewControllerDelegate
- (void)viewDidLoad {
    [super viewDidLoad];
    
    activeRouteIndex = 0;
    
    [APP_DELEGATE updateDeviceTokenToServer];
    
    viewPulstorContainer.hidden = true;
    isInitialMarkerChanged = false;
    isAnimationStarted = false;
    canHideMapOnDrag = true;
    canHideViewHUD = false;
    canCallAPIForRoute = true;
    isMapRecentered = false;
    
    [self.view layoutIfNeeded];
    
    [self updateLablesAccordingToLanguage];
    [self setupInitialView];
    
    [self resloadTheMapViewWithClear];
    [self restoreGoogleMapWithMarkersAndRoutes];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNearByUserMarkersWithAnimation) name:NOTI_UPDATE_NEAR_BY_USERS object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationReceivedForUserLocationUpdates:) name:NOTI_USER_LOCATION_UPDATES object:nil];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(centerTheMapToUserLocation) name:NOTI_MENU_ITEM_SELECTED object:nil];
    
    [self updateNotificationsBadgeCount:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationFoundForRideStatusUpdate:) name:NOTI_RIDE_STATUS_UPDATED object:nil];
    
    [self performSelector:@selector(currentLocation) withObject:nil afterDelay:1.0];
//    [self updateCustomerCurrentLocation];
    
    [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
    [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
    
    if (APP_DELEGATE.isUserModeDriver) {
        viewTotalUsers.backgroundColor = APP_YELLOW_COLOR;
        imgTotalUserIcon.image = [UIImage imageNamed:@"chat-ride-info"];
    } else {
        viewTotalUsers.backgroundColor = APP_GREEN_COLOR;
        imgTotalUserIcon.image = [UIImage imageNamed:@"car-info"];
    }
}



-(void)centerTheMapToUserLocation {
    if (APP_DELEGATE.isUserModeDriver){
        [viewMap animateToLocation:APP_DELEGATE.userLocation];
    } else if (APP_DELEGATE.isRideRunning){
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        CLLocationCoordinate2D driverLocation = CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude);
        [viewMap animateToLocation:driverLocation];
    } else {
        [viewMap animateToLocation:APP_DELEGATE.userLocation];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    if (APP_DELEGATE.isInitial) {
        if(arrayNearByCarMarkers.count > 0){
            [self mapView:viewMap didTapMarker:arrayNearByCarMarkers[0]];
        }
        APP_DELEGATE.isInitial = NO;
    }
    [self showLocationDetailsContainerView];
    
    [APP_DELEGATE createDynamicLink:@"7383833795_*_Vrin@123"];
    isDisplayedNoUserBanner = false;
    
    canShowNoUserAlert = true;
    isAlertModeNoUser = false;
    isAlertModeExpire = false;
    isAlertModePlanRide = false;
    isAlertModeConfirmation = false;
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotifications setShouldAnimateBadge:true];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Custome Methods
- (void) setupInitialView {
    
    APP_DELEGATE.isInitial = YES;
    
    [lblRider setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [lblDriver setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    [lblPickupLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblDestinationLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    // ------------------- Transperent Background view -------------------
    viewTransperent.hidden = true;
    viewTransperent.alpha = 0.0;
    transperentViewHeight.constant = SCREEN_HEIGHT;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(transperentViewTapped)];
    [viewTransperent addGestureRecognizer:tapRecognizer];
    
    // ------------------- Driver Selection View -------------------
    [self initializeDriverInfoContainer];
    
    // ------------------- Current Ride Info of Driver -------------------
    [lblCurrentTripDriverName setFont:[UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE]];
    [lblCurrentTripDriverCarIno setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblCurrentTripDriverPlate setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblCurrentTripETA setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [btnCurrentTripDriverCancel.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    // Check viewDidAppear
    
    
    // ------------------- Current Ride Info of Passenger -------------------
    [lblCurrentTripPessangerName setFont:[UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE]];
    [lblCurrentTripPessangerETA setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblCurrentTripPessangerDistance setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [btnCurrentTripPessabgerSmallCancel.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [btnCurrentTripPessangerBigCancel.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [btnCurrentTripPessangerSmallConfirm.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE-(IS_IPHONE_5 ? 2 : 0)]];
    // Check viewDidAppear
}

-(void)updateLablesAccordingToLanguage {
    
    lblCurrentTripDriverPlate.text = [NSString stringWithFormat:@"%@ : VN 01 5454",GET_Language_Value(@"plate_no")];
//    lblCurrentTripETA.text = [NSString stringWithFormat:@"%@ : 08:15 AM (12 min)",GET_Language_Value(@"eta")];
    [btnCurrentTripDriverCancel setTitle:GET_Language_Value(@"cancel") forState:UIControlStateNormal];
    
//    lblCurrentTripPessangerETA.text = [NSString stringWithFormat:@"%@ : 08:15 AM (12 min)",GET_Language_Value(@"eta")];
    
    [btnCurrentTripPessangerBigCancel setTitle:GET_Language_Value(@"end_trip") forState:UIControlStateNormal];
    [btnCurrentTripPessabgerSmallCancel setTitle:GET_Language_Value(@"cancel") forState:UIControlStateNormal];
    [btnCurrentTripPessangerSmallConfirm setTitle:GET_Language_Value(@"confirmation_no") forState:UIControlStateNormal];
    
    if (APP_DELEGATE.loggedUserType == USER_TYPE_PASSENGER){
        lblRider.text = GET_Language_Value(@"ride");
        lblDriver.text = @"";
        imgViewRider.hidden = false;
        
        imgViewDriver.hidden = true;
        [lblRider setTextAlignment:NSTextAlignmentCenter];
        imgViewHeaderTab.image = [UIImage imageNamed:@"PHUserActiveBG"];
        //Hide Icon Image
    } else if (APP_DELEGATE.loggedUserType == USER_TYPE_DRIVER){
        imgViewDriver.hidden = true;
        lblDriver.text = @"";
        
        imgViewRider.hidden = false;
        imgViewRider.image = imgViewDriver.image;
        
        lblRider.text = GET_Language_Value(@"drive");
        [lblRider setTextAlignment:NSTextAlignmentCenter];
        
        imgViewHeaderTab.image = [UIImage imageNamed:@"PHUserActiveBG"];
        
        
    } else {
        lblRider.text = GET_Language_Value(@"ride");
        lblDriver.text = GET_Language_Value(@"drive");
        [lblRider setTextAlignment:NSTextAlignmentLeft];
        [lblDriver setTextAlignment:NSTextAlignmentRight];
        if (APP_DELEGATE.isUserModeDriver){
            imgViewHeaderTab.image = [UIImage imageNamed:@"PHHeaderDriver"];
        } else {
            imgViewHeaderTab.image = [UIImage imageNamed:@"PHHeaderRider"];
        }
        
        imgViewRider.hidden = false;
        imgViewDriver.hidden = false;
    }
    
    if (APP_DELEGATE.isUserModeDriver){
        
        viewPassRidesGiven.hidden = false;
        viewPassRidesTaken.hidden = false;
//        carInfoHeight.constant = 0;
//        [UIView animateWithDuration:0.3 animations:^{
//            [self.view layoutIfNeeded];
//        } completion:^(BOOL finished) {
//            viewCarInfo.hidden = true;
//        }];
    } else {
        viewPassRidesGiven.hidden = true;
        viewPassRidesTaken.hidden = true;
        
//        carInfoHeight.constant = 80;
//        viewCarInfo.hidden = false;
//        [UIView animateWithDuration:0.3 animations:^{
//            [self.view layoutIfNeeded];
//        } completion:^(BOOL finished) {
//            
//        }];
    }
}

-(void)showLocationDetailsContainerView {
    if (APP_DELEGATE.isUserModeDriver && APP_DELEGATE.isRideRunning && NAVIGATION_MODE){
        [self hideLocationDetailsContainerView];
    } else {
        [self shouldHideNavigationForMap];
        
        if(!APP_DELEGATE.isRideRunning) {
            if (constraintViewLocationTop.constant < 15){
                constraintViewLocationTop.constant = 15;
                [UIView animateWithDuration:0.3 animations:^{
                    [self.view layoutIfNeeded];
                }];
            }
        } else {
            [self hideLocationDetailsContainerView];
        }
    }
    
    [self shouldShowRunningRideBannerForPassengerUser:true];
    [self shouldShowRunningRideBannerForDriverUser:true];
}

-(void)hideLocationDetailsContainerView {
    CGFloat requiredX = (0 - viewLocationSelection.frame.size.height) - 150;
    if (constraintViewLocationTop.constant > requiredX){
        constraintViewLocationTop.constant = requiredX;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

#pragma mark - SearchLocationViewControllerDelegate
-(void)showFavoriteLocationControllerWithIsModePickup:(BOOL)isModePickup {
    if (!APP_DELEGATE.isRideRunning){
        if (isModePickup){
            isModePickBeginLocation = true;
        } else {
            isModePickBeginLocation = false;
        }
        
        FavouriteSpots * favSpotsView = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteSpots"];
        favSpotsView.isSelectionMode = true;
        favSpotsView.delegate = self;
        UINavigationController * navController = [[UINavigationController alloc]initWithRootViewController:favSpotsView];
        [self presentViewController:navController animated:true completion:nil];
    }
}

-(void)showSearchLocationControllerWithIsModePickup:(BOOL)isModePickup {
    if (!APP_DELEGATE.isRideRunning){
        SearchLocationViewController * searchView = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchLocationViewController"];
        searchView.isModeSearchLocation = true;
        searchView.delegate = self;
        isModePickBeginLocation = isModePickup;
        if (isModePickup){
            searchView.searchLocationMode = SLM_SET_START_LOCATION;
        } else {
            searchView.searchLocationMode = SLM_SET_END_LOCATION;
        }
        [self presentViewController:searchView animated:true completion:nil];
    }
}

-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo{
    if (DEBUG_MODE){
        NSLog(@"Selected Location : %@",locationInfo);
    }
    double lattitude = [locationInfo[@"lattitude"]doubleValue];
    double longitude = [locationInfo[@"longitude"]doubleValue];
    
    if (isModePickBeginLocation){
        if (locationInfo[@"isFavourite"] && [locationInfo[@"isFavourite"] isEqualToString:@"1"]){
            [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartFilled"] forState:UIControlStateNormal];
        } else {
            [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
        }
        
        APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(lattitude, longitude);
        APP_DELEGATE.dictPickupDetails = locationInfo;
        [viewMap animateToLocation:APP_DELEGATE.tripStartLocation];
    } else {
        if (locationInfo[@"isFavourite"] && [locationInfo[@"isFavourite"] isEqualToString:@"1"]){
            [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartFilled"] forState:UIControlStateNormal];
        } else {
            [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
        }
        
        APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(lattitude, longitude);
        APP_DELEGATE.dictDropoffDetails = locationInfo;
        [viewMap animateToLocation:APP_DELEGATE.tripEndLocation];
    }
    
    isInitialMarkerChanged= true;
    [self restoreGoogleMapWithMarkersAndRoutes];
    [self updateCustomerCurrentLocation];
    
    
    NSUInteger index2 = [APP_DELEGATE.arrayNearByDrivers indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
        BOOL found = [[item objectForKey:@"user_id"] isEqualToString:selectedUserId];
        return found;
    }];
    
    if (index2 == NSNotFound || index2 > APP_DELEGATE.arrayNearByDrivers.count) {
        index2 = 0;
    }
    [self loadDataIntoUserInfoViewWithIndex:(int)index2];
}

#pragma mark - UICollectionView Delegate -
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return APP_DELEGATE.arrayNearByDrivers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    DriversCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DriversCell" forIndexPath:indexPath];
    
    cell.imgActiverDriver.hidden = YES;
    
//    UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
    
     UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
    
    NSLog(@"%@",APP_DELEGATE.arrayNearByDrivers);
    
    NSLog(@"%@",APP_DELEGATE.arrayNearByDrivers[indexPath.row][@"gender"]);
    
    
//    NSString * gender = APP_DELEGATE.arrayNearByDrivers[indexPath.row][@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, cell.imgUserProfile.frame.size.width, cell.imgUserProfile.frame.size.height)];
    
    activity_indicator.tag = 11111;
    [[cell.imgUserProfile viewWithTag:1111] removeFromSuperview];
    
    [cell.imgUserProfile addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    NSString *strUrl = APP_DELEGATE.arrayNearByDrivers[indexPath.row][@"user_image"];
    strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [cell.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [[cell.imgUserProfile viewWithTag:1111] removeFromSuperview];
    }];
    
    
   
    
    CGFloat cellTransformRatio;
    if (APP_DELEGATE.arrayNearByDrivers[indexPath.row][@"user_id"] == selectedUserId) {
        cell.imgActiverDriver.hidden = NO;
        cellTransformRatio = 1.0;
        
//        cell.imgUserProfile.transform = CGAffineTransformScale(cell.transform, cellTransformRatio, cellTransformRatio);
//        cell.imgActiverDriver.transform = CGAffineTransformScale(cell.transform, cellTransformRatio, cellTransformRatio);
        
    } else {
        cell.imgActiverDriver.hidden = YES;
        cellTransformRatio = 0.5;
//        cell.imgUserProfile.transform = CGAffineTransformScale(cell.transform, cellTransformRatio, cellTransformRatio);
//        cell.imgActiverDriver.transform = CGAffineTransformScale(cell.transform, cellTransformRatio, cellTransformRatio);
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedUserId = APP_DELEGATE.arrayNearByDrivers[indexPath.row][@"user_id"];
    
    NSUInteger index2 = [APP_DELEGATE.arrayNearByDrivers indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
        BOOL found = [[item objectForKey:@"user_id"] isEqualToString:selectedUserId];
        return found;
    }];
    
    if (index2 == NSNotFound && index2 >= APP_DELEGATE.arrayNearByDrivers.count) {
        index2 = 0;
    }
    
    [self reloadDriverCollection];
    [collectionDriverList scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    [self loadDataIntoUserInfoViewWithIndex:(int)index2];
}

#pragma mark - IBOutlet Actions
- (IBAction)actionMenu:(id)sender {
    [APP_DELEGATE showMenu];
}

- (IBAction)notificationIcon:(id)sender {
    
    [APP_DELEGATE NavigateToNotification];
}

- (IBAction)buttonPickupLocation:(id)sender {
    if ([APP_DELEGATE CheckLocationPermissionWithSourceView:self]){
        [self showSearchLocationControllerWithIsModePickup:true];
    }
}

- (IBAction)buttonPickupFavorite:(id)sender {
    if ([APP_DELEGATE CheckLocationPermissionWithSourceView:self]){
        [self showFavoriteLocationControllerWithIsModePickup:true];
    }
    //Should Open Favorite Spot View With Selection Mode
}

- (IBAction)buttonDestinationLocation:(id)sender {
    if ([APP_DELEGATE CheckLocationPermissionWithSourceView:self]){
        [self showSearchLocationControllerWithIsModePickup:false];
    }
}

- (IBAction)buttonDestinationFavorite:(id)sender {
    if ([APP_DELEGATE CheckLocationPermissionWithSourceView:self]){
        [self showFavoriteLocationControllerWithIsModePickup:false];
    }
    //Should Open Favorite Spot View With Selection Mode
}

- (IBAction)actionUserModeChange:(id)sender {
    if (APP_DELEGATE.loggedUserType == USER_TYPE_BOTH && !APP_DELEGATE.isRideRunning){
        if ([sender tag] == 101 && APP_DELEGATE.isUserModeDriver) {
            [self didHideDriverInfoContainerWithAnimation];
            START_HUD
            canHideViewHUD = true;
            [APP_DELEGATE.apiManager updateUserModeWithIsModeDriver:false withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
                
                if (success){
                    APP_DELEGATE.isUserModeDriver = false;
                    imgViewHeaderTab.image = [UIImage imageNamed:@"header-tab-active"];
                    
                    for (GMSMarker * marker in arrayNearByCarMarkers){
                        marker.map = nil;
                    }
                    [APP_DELEGATE.arrayNearByDrivers removeAllObjects];
                    [arrayNearByCarMarkers removeAllObjects];
                    selectedUserId = @"";
                    [self updateCustomerCurrentLocation];
                    
//                    carInfoHeight.constant = 80;
//                    viewCarInfo.hidden = false;
//                    [UIView animateWithDuration:0.3 animations:^{
//                        [self.view layoutIfNeeded];
//                    } completion:^(BOOL finished) {
//                        
//                    }];
                    
                    viewPassRidesGiven.hidden = true;
                    viewPassRidesTaken.hidden = true;
                    
                    [APP_DELEGATE updateTheArrayOfCancelReasons];
                    [viewMap setMyLocationEnabled:false];
                    
                      lblTotalUserLable.textColor = [UIColor whiteColor];
                    
                    viewTotalUsers.backgroundColor = APP_GREEN_COLOR;
                    imgTotalUserIcon.image = [UIImage imageNamed:@"car-info"];
                } else {
                    STOP_HUD
                }
            }];
        }
        else if ([sender tag] == 102 && !APP_DELEGATE.isUserModeDriver){
            START_HUD
            canHideViewHUD = true;
            [self didHideDriverInfoContainerWithAnimation];
            [APP_DELEGATE.apiManager updateUserModeWithIsModeDriver:true withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
                if (success){
                    APP_DELEGATE.isUserModeDriver = true;
                    imgViewHeaderTab.image = [UIImage imageNamed:@"header-tab-inactive"];
                    
                    if (APP_DELEGATE.isRideRunning){
                        PassengerRequestViewController *obj = (PassengerRequestViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"PassengerRequestViewController"];
                        obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
                        [[APP_DELEGATE.navigationController.viewControllers firstObject] presentViewController:obj animated:NO completion:nil];
                        [self didHideDriverInfoContainerWithAnimation];
                    }
                    
                    for (GMSMarker * marker in arrayNearByCarMarkers){
                        marker.map = nil;
                    }
                    
                    [APP_DELEGATE.arrayNearByDrivers removeAllObjects];
                    [arrayNearByCarMarkers removeAllObjects];
                    selectedUserId = @"";
                    [self updateCustomerCurrentLocation];
                    
//                    carInfoHeight.constant = 0;
//                    
//                    [UIView animateWithDuration:0.3 animations:^{
//                        [self.view layoutIfNeeded];
//                    } completion:^(BOOL finished) {
//                        viewCarInfo.hidden = true;
//                    }];
                    [APP_DELEGATE updateTheArrayOfCancelReasons];
                    
                    viewPassRidesGiven.hidden = false;
                    viewPassRidesTaken.hidden = false;
                    [viewMap setMyLocationEnabled:false];
                    
                    
                     lblTotalUserLable.textColor = [UIColor blackColor];
                    
                    viewTotalUsers.backgroundColor = APP_YELLOW_COLOR;
                    imgTotalUserIcon.image = [UIImage imageNamed:@"chat-ride-info"];
                } else {
                    STOP_HUD
                }
            }];
        }
    }
}

- (IBAction)actionRequestRide:(id)sender {
    MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
    MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.tripEndLocation);
    
    if ((APP_DELEGATE.tripStartLocation.latitude == 0 && APP_DELEGATE.tripStartLocation.longitude == 0) || (APP_DELEGATE.tripEndLocation.latitude == 0 && APP_DELEGATE.tripEndLocation.longitude == 0)){
        [Singleton showSnakBar:@"Please Select Pick & Drop address to request a ride" multiline:NO];
    } else if (MKMetersBetweenMapPoints(point1, point2) < 800.0){
        [Singleton showSnakBar:@"Minimum distance should be 0.8 KM" multiline:false];
    } else {
        if (DEBUG_MODE){
            NSLog(@"Pickup Data : %@",APP_DELEGATE.dictPickupDetails);
            NSLog(@"Drop-off Data : %@",APP_DELEGATE.dictDropoffDetails);
        }
        
        if (APP_DELEGATE.arrayNearByDrivers.count <= 0){
            [Singleton showSnakBar:@"Something went wrong \n Please try again later" multiline:true];
        }
        START_HUD
        if ([selectedUserId isEqualToString:@""]){
            selectedUserId = APP_DELEGATE.arrayNearByDrivers[0][@"user_id"];
            APP_DELEGATE.recieverId = selectedUserId;
        } else {
            APP_DELEGATE.recieverId = selectedUserId;
        }
        
        if (DEBUG_MODE){
            NSLog(@"reciever user : %@", APP_DELEGATE.recieverId);
        }
        
        
        NSString * pickupAddress = APP_DELEGATE.dictPickupDetails[@"fullAddress"];
        NSString * pickupTitle = APP_DELEGATE.dictPickupDetails[@"title"];
        
        NSString * dropTitle = APP_DELEGATE.dictDropoffDetails[@"title"];
        NSString * dropAddress = APP_DELEGATE.dictDropoffDetails[@"fullAddress"];
        
        
        NSString * pickupLattitude = [NSString stringWithFormat:@"%f", APP_DELEGATE.tripStartLocation.latitude];
        NSString * pickupLongitude = [NSString stringWithFormat:@"%f", APP_DELEGATE.tripStartLocation.longitude];
        
        NSString * dropLattitude = [NSString stringWithFormat:@"%f", APP_DELEGATE.tripEndLocation.latitude];
        NSString * dropLongitude = [NSString stringWithFormat:@"%f", APP_DELEGATE.tripEndLocation.longitude];
        
        NSString * tripDistance = [NSString stringWithFormat:@"%f",[APP_DELEGATE.tripDistance doubleValue] / 1000 ];
        NSString * tripDuration = APP_DELEGATE.tripDuration;
        
        NSDate *currDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss a"];
        NSString * pikcupTime = [dateFormatter stringFromDate:currDate];
        
        NSString * costOfTheRide =[APP_DELEGATE countEstimatedCostFromETAInMinutes:0 DistanceInKM:[APP_DELEGATE.tripDistance doubleValue] isModeValue:true];
        
        [APP_DELEGATE.apiManager sendRideRequestWithReciverUserId:APP_DELEGATE.recieverId PickupTitle:pickupTitle PickupAddress:pickupAddress PickupLattitude:pickupLattitude PickupLongitude:pickupLongitude DropTitle:dropTitle DropAddress:dropAddress DropLattitude:dropLattitude DropLongitude:dropLongitude PickupDateTime:pikcupTime TripDistance:tripDistance TripDuation:tripDuration TripCost:costOfTheRide withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
            if (success){
                APP_DELEGATE.runningRideId = dictResponse[@"ride_id"];
                APP_DELEGATE.runningRideStatus = RIDE_STATUS_REQUEST;
                APP_DELEGATE.isRideRunning = true;
                
                [self didHideDriverInfoContainerWithAnimation];
                APP_DELEGATE.sendRequestDate = [NSDate date];
                [APP_DELEGATE updateHomeStatusArrayWithIndex:@"0"];
                
                [self.view bringSubviewToFront:viewPulstorContainer];
                viewPulstorContainer.hidden = false;
                pulstor1Width.constant = 10;
                pulstor2Width.constant = 0;
                [timerPulstorWaiting invalidate];
                [self shouldStartPulstorAnimation];
                
               // [Singleton showSnakBar:serverMessange multiline:true];
            } else {
                [Singleton showSnakBar:serverMessange multiline:true];
            }
            STOP_HUD
        }];
    }
}

#pragma mark - Navigation Buttons
-(void)shouldManageNavigationButtons {
    if (APP_DELEGATE.isRideRunning){
        
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
            if (APP_DELEGATE.isUserModeDriver){
                btnSound.hidden = false;
                heightBtnSound.constant = 50;
            } else {
                heightBtnSound.constant = 0;
            }
            btnSOS.hidden = false;
            heightBtnSOS.constant = 50;
            
            btnShareRide.hidden = false;
            heightBtnShareRide.constant = 50;
        } else {
            heightBtnSOS.constant = 0;
            heightBtnShareRide.constant = 0;
        }
        
        btnRecenter.hidden = !NAVIGATION_MODE;
        heightBtnRecenter.constant = NAVIGATION_MODE ? 50 : 0;
        
        btnSound.hidden = !NAVIGATION_MODE && APP_DELEGATE.isUserModeDriver;
        heightBtnSound.constant = NAVIGATION_MODE && APP_DELEGATE.isUserModeDriver ? 50 : 0;
        
        if (APP_DELEGATE.canSpeakNavigation == false){
            [btnSound setImage:[UIImage imageNamed:@"PHSNDOff"] forState:UIControlStateNormal];
        } else {
            [btnSound setImage:[UIImage imageNamed:@"PHSNDOn"] forState:UIControlStateNormal];
        }
    } else {
        heightBtnSOS.constant = 0;
        heightBtnSound.constant = 0;
        heightBtnRecenter.constant = 0;
        heightBtnShareRide.constant = 0;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        if (heightBtnRecenter.constant <= 0){
            btnRecenter.hidden = true;
        }
        
        if (heightBtnShareRide.constant <= 0){
            btnShareRide.hidden = true;
        }
        
        if (heightBtnSOS.constant <= 0){
            btnSOS.hidden = true;
        }
        
        if (heightBtnSound.constant <= 0){
            btnSound.hidden = true;
        }
    }];
}

- (IBAction)sosEmergencyButtonTapped:(id)sender {
    NSString * rideId = APP_DELEGATE.runningRideId;
    NSString * mobileNumber = APP_DELEGATE.dictUserDetails[@"emergency_mobile_number"];
    if (!mobileNumber || mobileNumber.length <= 0 || [mobileNumber isEqualToString:@""]){
        
        
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Are you in emergency and need help?" message:@"Enter emergency contact (in user profile) to use this feature." preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        
        [alert addAction:yesAction];
        [self presentViewController:alert animated:true completion:nil];
        
       // [Singleton showSnakBar:@"Enter emergency contact (in user profile) to use this feature" multiline:true];
    } else if (!rideId || rideId.length <= 0 || [rideId isEqualToString:@""]){
        if (DEBUG_MODE){
            NSLog(@"Unable to found Ride Id");
        }
    } else {
        
        NSString *strMessage = @"We are going to text (SMS) your emergency contact on your behalf with your location and ride details.";
        
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Are you in emergency and need help?" message:strMessage preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            START_HUD
            NSString *strMobileNumber = [NSString stringWithFormat:@"+%@%@",[Singleton.sharedInstance findYourCurrentCountryCode],mobileNumber];
            
            [APP_DELEGATE.apiManager fireEmergencySOSWithRideId:rideId emergencyMobile:strMobileNumber withCallBack:^(BOOL success, NSString *serverMessange) {
                STOP_HUD
                if (success){
                    [Singleton showSnakBar:@"SOS sent successfully" multiline:true];
                } else {
                    [Singleton showSnakBar:serverMessange multiline:true];
                }
            }];
        }];
        
        UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:noAction];
        [alert addAction:yesAction];
        
        
        [self presentViewController:alert animated:true completion:nil];
    }
}

- (IBAction)btnSoundTapAction:(UIButton *)sender {
    if (APP_DELEGATE.canSpeakNavigation){
        APP_DELEGATE.canSpeakNavigation = false;
        [btnSound setImage:[UIImage imageNamed:@"PHSNDOff"] forState:UIControlStateNormal];
        [Singleton showSnakBar:@"Muted" multiline:false];
    } else {
        APP_DELEGATE.canSpeakNavigation = true;
        [btnSound setImage:[UIImage imageNamed:@"PHSNDOn"] forState:UIControlStateNormal];
        [Singleton showSnakBar:@"Unmuted" multiline:false];
    }
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

- (IBAction)btnShareRideTapAction:(UIButton *)sender {
    if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING) {
        [APP_DELEGATE shareMyRideUsingDefaultMessageApp];
    } else {
        [Singleton showSnakBar:@"You can share you ride while your ride is running" multiline:true];
    }
}

- (IBAction)btnRecenterTapAction:(UIButton *)sender {
    if (NAVIGATION_MODE){
        isMapRecentered = true;
        canHideMapOnDrag = false;
        double angelToRatate = 0.0;
        if (APP_DELEGATE.trueHeading){
            angelToRatate = APP_DELEGATE.trueHeading;
        } else if (APP_DELEGATE.magneticeading){
            angelToRatate = APP_DELEGATE.magneticeading;
        } else {
            angelToRatate = 0.0;
        }
        
        angelToRatate = NAVI_BEARING_RATE;
        
        CLLocationCoordinate2D userPosiiton;
        if (!APP_DELEGATE.isUserModeDriver && APP_DELEGATE.isRideRunning){
            double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
            double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
            userPosiiton = CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude);
        } else {
            userPosiiton = APP_DELEGATE.userLocation;
        }
        
        GMSCameraPosition * cameraPosition = [[GMSCameraPosition alloc]initWithTarget:userPosiiton zoom:NAVI_ZOOM_RATE bearing:angelToRatate viewingAngle:NAVI_ANGLE_RATE];
        [CATransaction begin];
        [CATransaction setValue:[NSNumber numberWithFloat: 1.0f] forKey:kCATransactionAnimationDuration];
        [viewMap animateToCameraPosition:cameraPosition];
        [CATransaction commit];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            isMapRecentered = true;
        });
    }
}

#pragma mark - Custom Alert Delegate -
- (void)alertControllerButtonTappedWithIndex:(NSInteger)index {
    if (index == 1) {
        if (isAlertModeExpire){
            NSString * driverId;
            NSString * pessangerId;
            if (APP_DELEGATE.isUserModeDriver) {
                pessangerId = APP_DELEGATE.recieverId;
                driverId = APP_DELEGATE.loggedUserId;
            } else {
                pessangerId = APP_DELEGATE.loggedUserId;
                driverId = APP_DELEGATE.recieverId;
            }
            
            [APP_DELEGATE.apiManager updateRideStatusWithRideId:APP_DELEGATE.runningRideId RideStatus:@"6" PessangerId:pessangerId DriverId:driverId CancelReasonId:@"" TotalKm:@"" withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
                if (DEBUG_MODE){
                    NSLog(@"Decline Ride Response : %@",serverMessange);
                }
                
                arrayNearByCarMarkers = [[NSMutableArray alloc]init];
                APP_DELEGATE.arrayNearByDrivers = [[NSMutableArray alloc]init];
                [APP_DELEGATE clearPreviousRideDetailsFromTheAPP];
                [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                
                [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                
                lblPickupLocation.text = @"Pick Up Location";
                lblDestinationLocation.text = @"Drop Off Location";
                
                viewPulstorContainer.hidden = true;
                [timerPulstorWaiting invalidate];
                
                previusSpeech = @"";
                
                [self restoreGoogleMapWithMarkersAndRoutes];
                [self shouldShowRunningRideBannerForDriverUser:false];
                [self shouldShowRunningRideBannerForPassengerUser:false];
                [self updateCustomerCurrentLocation];
            }];
        } else if (isAlertModeConfirmation){
            if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_CANCELED){
                //Confirm with cancel status
                NSString * rideId = APP_DELEGATE.runningRideId;
                NSString * rideStatus = @"4";
                START_HUD
                [APP_DELEGATE.apiManager confirmUpdatedStatusWithRideId:rideId RideStatus:rideStatus withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
                    STOP_HUD
                    if (success){
                        arrayNearByCarMarkers = [[NSMutableArray alloc]init];
                        APP_DELEGATE.arrayNearByDrivers = [[NSMutableArray alloc]init];
                        [APP_DELEGATE clearPreviousRideDetailsFromTheAPP];
                        [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        
                        [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        
                        viewPulstorContainer.hidden = true;
                        [timerPulstorWaiting invalidate];
                        lblPickupLocation.text = @"Pick Up Location";
                        lblDestinationLocation.text = @"Drop Off Location";
                        
                        [self restoreGoogleMapWithMarkersAndRoutes];
                        [self shouldShowRunningRideBannerForDriverUser:false];
                        [self shouldShowRunningRideBannerForPassengerUser:false];
                        [self updateCustomerCurrentLocation];
                        
                        previusSpeech = @"";
                        
                    } else {
                        if (DEBUG_MODE){
                            NSLog(@"Can Not Confirm : %@",serverMessange);
                        }
                    }
                }];
            } else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_FINISHED){
                //Confirm with Finished status
                PaymentViewController *obj = (PaymentViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"PaymentViewController"];
                obj.dictTripDetails = APP_DELEGATE.dictTripDetails.mutableCopy;
                obj.delegate = self;
                [self presentViewController:obj animated:YES completion:^{ }];
            }
            else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_DECLINED){
                //Confirm with Declined status
                NSString * rideId = APP_DELEGATE.runningRideId;
                NSString * rideStatus = @"6";
                START_HUD
                [APP_DELEGATE.apiManager confirmUpdatedStatusWithRideId:rideId RideStatus:rideStatus withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
                    STOP_HUD
                    if (success){
                        arrayNearByCarMarkers = [[NSMutableArray alloc]init];
                        APP_DELEGATE.arrayNearByDrivers = [[NSMutableArray alloc]init];
                        [APP_DELEGATE clearPreviousRideDetailsFromTheAPP];
                        [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        
                        [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                        
                        viewPulstorContainer.hidden = true;
                        [timerPulstorWaiting invalidate];
                        lblPickupLocation.text = @"Pick Up Location";
                        lblDestinationLocation.text = @"Drop Off Location";
                        
                        [self restoreGoogleMapWithMarkersAndRoutes];
                        [self shouldShowRunningRideBannerForDriverUser:false];
                        [self shouldShowRunningRideBannerForPassengerUser:false];
                        [self updateCustomerCurrentLocation];
                        
                        previusSpeech = @"";
                        
                    } else {
                        if (DEBUG_MODE){
                            NSLog(@"Can Not Confirm : %@",serverMessange);
                        }
                    }
                }];
            }
        } else if (isAlertModePlanRide){
            Class className = NSClassFromString(@"PlanRide");
            for (UIViewController *view in APP_DELEGATE.navigationController.viewControllers) {
                if ([view isKindOfClass:className]) {
                    [self.navigationController popToViewController:view animated:NO];
                    return;
                }
            }
            
            id viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlanRide"];
            [self.navigationController pushViewController:viewController animated:NO];
            
        } else if (isAlertModeNoUser){
            //Will Release once User Founds
        } else {
            if (APP_DELEGATE.arrayCancelationReasons.count > 0){
                ReasonViewController *obj = (ReasonViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"ReasonViewController"];
                obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
                obj.delegate = self;
                obj.arrReasons = APP_DELEGATE.arrayCancelationReasons.mutableCopy;
                [self presentViewController:obj animated:NO completion:nil];

            } else {
                [self reasonControllerButtonTappedWithIndex:0];
            }
        }
    } else {
        // No
    }
}

-(void)didSubmitPaymentSuccessfullForRideWithRideDetails:(NSDictionary *)dictTripDetails {
    [APP_DELEGATE clearPreviousRideDetailsFromTheAPP];
    
    lblPickupLocation.text = @"Pick Up Location";
    lblDestinationLocation.text = @"Drop Off Location";
    
    [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
    [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
    
    [self shouldShowRunningRideBannerForDriverUser:false];
    [self shouldShowRunningRideBannerForPassengerUser:false];
    
    APP_DELEGATE.tripStartLocation = APP_DELEGATE.userLocation;
    [self restoreGoogleMapWithMarkersAndRoutes];
    [self updateCustomerCurrentLocation];
    
//    FeedbackViewController *obj = (FeedbackViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"FeedbackViewController"];
//    obj.dictTripDetails = dictTripDetails;
//    [self presentViewController:obj animated:YES completion:nil];
}

- (void)reasonControllerButtonTappedWithIndex:(NSInteger)index
{
    START_HUD
    NSString * selectedReasonId;
    if (index < APP_DELEGATE.arrayCancelationReasons.count){
        selectedReasonId = APP_DELEGATE.arrayCancelationReasons[index][@"reason_id"];
    }else {
        selectedReasonId = @"";
    }
    
    NSString * driver_id;
    NSString * pessangerId;
    if (APP_DELEGATE.isUserModeDriver) {
        pessangerId = APP_DELEGATE.recieverId;
        driver_id = APP_DELEGATE.loggedUserId;
    } else {
        pessangerId = APP_DELEGATE.loggedUserId;
        driver_id = APP_DELEGATE.recieverId;
    }
    
    [APP_DELEGATE.apiManager updateRideStatusWithRideId:APP_DELEGATE.runningRideId RideStatus:@"4" PessangerId:pessangerId DriverId:driver_id CancelReasonId:selectedReasonId TotalKm:@"" withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        STOP_HUD
        if (DEBUG_MODE){
            NSLog(@"Decline Ride Response : %@",serverMessange);
            
        }
        [APP_DELEGATE clearPreviousRideDetailsFromTheAPP];
        [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
        [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
        
        [self restoreGoogleMapWithMarkersAndRoutes];
        [self shouldShowRunningRideBannerForDriverUser:false];
        [self shouldShowRunningRideBannerForPassengerUser:false];
        [self updateCustomerCurrentLocation];
        
        lblPickupLocation.text = @"Pick Up Location";
        lblDestinationLocation.text = @"Drop Off Location";
        
        viewPulstorContainer.hidden = true;
        [timerPulstorWaiting invalidate];
    }];
}

#pragma mark - ******************************************
#pragma mark   ****** Google Developement Section ******
#pragma mark   ******************************************

- (void)resloadTheMapViewWithClear {
    [viewMap clear];
    arrayNearByCarMarkers = [[NSMutableArray alloc]init];
    APP_DELEGATE.arrayNearByDrivers = [[NSMutableArray alloc]init];
    
    isModePickBeginLocation = true;
    lblPickupLocation.text = GET_Language_Value(@"pickup_location");
    lblDestinationLocation.text = GET_Language_Value(@"destination_location");
    
    isInitialMarkerChanged = false;
    
    GMSCameraPosition *camera;
    if (APP_DELEGATE.userLocation.latitude != 00.00000 && APP_DELEGATE.userLocation.longitude != 00.00000){
        camera = [GMSCameraPosition cameraWithLatitude:APP_DELEGATE.userLocation.latitude longitude:APP_DELEGATE.userLocation.longitude zoom:18];
    } else {
        
        
        if (![APP_DELEGATE CheckLocationPermissionWithSourceView:self]) {
            camera = [GMSCameraPosition cameraWithLatitude:00.0000 longitude:00.0000 zoom:18];
        }
        
        
    }
    
    viewMap.camera = camera;
    viewMap.myLocationEnabled = false;
    viewMap.delegate = self;
    viewMap.settings.rotateGestures = NAVIGATION_MODE;
    viewMap.settings.compassButton = false;
    viewMap.settings.zoomGestures = true;
    
    [self updateUserLocationMarker];
}

-(void)updateUserLocationMarker {
    CLLocationCoordinate2D userPosiiton;
    if (!APP_DELEGATE.isUserModeDriver && APP_DELEGATE.isRideRunning){
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        userPosiiton = CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude);
    } else {
        userPosiiton = APP_DELEGATE.userLocation;
    }
    
    if (userPosiiton.latitude != 00.00000 && userPosiiton.longitude != 00.00000){
        if (userMarker){
            userMarker.map = nil;
            userMarker.position = userPosiiton;
            userMarker.map = viewMap;
        } else {
            userMarker = [[GMSMarker alloc] init];
            userMarker.position = userPosiiton;
            userMarker.title = @"My Location";
            [userMarker setDraggable:false];
            userMarker.icon = [UIImage imageNamed:@"PHMarkerCurrent"];
            userMarker.map = viewMap;
        }
        
        if (isMapRecentered){
//            [self btnRecenterTapAction:btnRecenter];
        }
    }
}

-(void)restoreGoogleMapWithMarkersAndRoutes{
    [viewMap clear];
    
    if (DEBUG_MODE){
        NSLog(@"Pickup : %@", APP_DELEGATE.dictPickupDetails);
        NSLog(@"Destination : %@", APP_DELEGATE.dictDropoffDetails);
    }
    
    if (APP_DELEGATE.tripStartLocation.latitude != 00.000 && APP_DELEGATE.tripStartLocation.longitude != 00.000){
        NSString * pickupAddress = !APP_DELEGATE.dictPickupDetails ? @"": APP_DELEGATE.dictPickupDetails[@"fullAddress"];
        if (pickupAddress && pickupAddress.length > 0){
            lblPickupLocation.text = APP_DELEGATE.dictPickupDetails[@"fullAddress"];
            [self addStartMarkerWithTitle:@"Pick Up"];
        } else {
            [self findAddressForCoordinate:APP_DELEGATE.tripStartLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                APP_DELEGATE.dictPickupDetails = dictAddress;
                lblPickupLocation.text = dictAddress[@"fullAddress"];
                [self addStartMarkerWithTitle:@"Pick Up"];
            }];
        }
    }
    
    if (APP_DELEGATE.tripEndLocation.latitude != 00.000 && APP_DELEGATE.tripEndLocation.longitude != 00.000){
        NSString * dropoffAddress = !APP_DELEGATE.dictDropoffDetails ? @"": APP_DELEGATE.dictDropoffDetails[@"fullAddress"];
        if (dropoffAddress && dropoffAddress.length > 0){
            lblDestinationLocation.text = dropoffAddress;
            [self addEndMarkerWithTitle:@"Drop Off"];
        } else {
            [self findAddressForCoordinate:APP_DELEGATE.tripEndLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                APP_DELEGATE.dictDropoffDetails = dictAddress;
                lblDestinationLocation.text = dictAddress[@"fullAddress"];
                [self addEndMarkerWithTitle:@"Drop Off"];
            }];
        }
    }
    
    CLLocationCoordinate2D validUserLocation;
    if (APP_DELEGATE.isUserModeDriver){
        validUserLocation = APP_DELEGATE.userLocation;
    } else {
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
            validUserLocation = CLLocationCoordinate2DMake([APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue], [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue]);
        } else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
            validUserLocation = APP_DELEGATE.tripStartLocation;
        } else {
            validUserLocation = CLLocationCoordinate2DMake([APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue], [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue]);
        }
        
    }
    
    if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.tripStartLocation Destination:validUserLocation isDotedLine:true withCallback:^(BOOL success) {
            //Draw Route Successfull
        }];
    } else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:validUserLocation Destination:APP_DELEGATE.tripEndLocation isDotedLine:true withCallback:^(BOOL success) {
            //Draw Route Successfull
        }];
    }else  if (APP_DELEGATE.tripStartLocation.latitude != 00.000 && APP_DELEGATE.tripStartLocation.longitude != 00.000 && APP_DELEGATE.tripEndLocation.latitude != 00.000 && APP_DELEGATE.tripEndLocation.longitude != 00.000) {
        [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:APP_DELEGATE.tripEndLocation destination:APP_DELEGATE.tripStartLocation withCallBack:^(int etaSeconds, float distanceKM, NSString* formattedETA, NSString *formatedDistnace, NSString * polylinePoints, NSArray * allRoutes) {
            APP_DELEGATE.tripDuration = [NSString stringWithFormat:@"%0.0f",round([[arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"duration"][@"value"] intValue] / 60)];
            APP_DELEGATE.tripDistance = [NSString stringWithFormat:@"%0.1f",[[arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"distance"][@"value"]floatValue]];
            APP_DELEGATE.tripFormatedDuration = [arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"duration"][@"text"];
            APP_DELEGATE.tripFormatedDistance = [arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"distance"][@"text"];
        }];
    }
    
    for (GMSMarker * nearByUser in arrayNearByCarMarkers){
        nearByUser.map = viewMap;
    }
}

-(void)adjustMapviewToViewAllMarkers{
    if (APP_DELEGATE.isRideRunning){
        GMSMutablePath *pathMapPath = [GMSMutablePath path];
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
            CLLocationCoordinate2D driverCoordinates;
            if (APP_DELEGATE.isUserModeDriver){
                driverCoordinates = APP_DELEGATE.userLocation;
            } else {
                driverCoordinates = CLLocationCoordinate2DMake([APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue], [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue]);
            }

            [pathMapPath addCoordinate:APP_DELEGATE.tripStartLocation];
            [pathMapPath addCoordinate:driverCoordinates];
        } else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
            if (APP_DELEGATE.isUserModeDriver){
                [pathMapPath addCoordinate:APP_DELEGATE.userLocation];
                [pathMapPath addCoordinate:APP_DELEGATE.tripEndLocation];
            } else {
                [pathMapPath addCoordinate:APP_DELEGATE.tripStartLocation];
                [pathMapPath addCoordinate:APP_DELEGATE.tripEndLocation];
            }
        }
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:pathMapPath];
        [viewMap animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    }
}

-(void)addStartMarkerWithTitle:(NSString *)title {
    if (APP_DELEGATE.isUserModeDriver){
        if (!APP_DELEGATE.isRideRunning) {
            return;
        }
    }
    CLLocationCoordinate2D markerCoordinates;
    if (APP_DELEGATE.isRideRunning){
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
            if (APP_DELEGATE.isUserModeDriver){
                markerCoordinates = APP_DELEGATE.userLocation;
            } else {
                markerCoordinates = CLLocationCoordinate2DMake([APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue], [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue]);
            }
        } else {
            markerCoordinates = APP_DELEGATE.tripStartLocation;
        }
    } else {
        markerCoordinates = APP_DELEGATE.tripStartLocation;
    }
    
    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
    markerToAdd.position = markerCoordinates;
    markerToAdd.title = title;
    [markerToAdd setDraggable:false];
    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerPickup"];
    markerToAdd.map = viewMap;
}

-(void)addEndMarkerWithTitle:(NSString *)title{
    if (APP_DELEGATE.isUserModeDriver){
        if (!APP_DELEGATE.isRideRunning) {
            return;
        }
    }
    
    CLLocationCoordinate2D markerCoordinates;
    if (APP_DELEGATE.isRideRunning){
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
            markerCoordinates = APP_DELEGATE.tripStartLocation;
        } else {
            markerCoordinates = APP_DELEGATE.tripEndLocation;
        }
    } else {
        markerCoordinates = APP_DELEGATE.tripEndLocation;
    }
    
    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
    markerToAdd.position = markerCoordinates;
    markerToAdd.title = title;
    [markerToAdd setDraggable:false];
    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerDestination"];
    markerToAdd.map = viewMap;
}

-(void)notificationReceivedForUserLocationUpdates:(NSNotification *)notif{
    [viewMap setMyLocationEnabled:false];
    
    [self updateUserLocationMarker];
    
    if (!isInitialMarkerChanged && !APP_DELEGATE.isRideRunning ){
        if (DEBUG_MODE){
            NSLog(@"User Location : %f and %f",APP_DELEGATE.userLocation.latitude,APP_DELEGATE.userLocation.longitude);
        }
        
        if (APP_DELEGATE.tripStartLocation.latitude != APP_DELEGATE.userLocation.latitude || APP_DELEGATE.tripStartLocation.longitude != APP_DELEGATE.userLocation.longitude){
            MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
            MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
            float distance = MKMetersBetweenMapPoints(point1, point2);
            if (distance > 0){
                APP_DELEGATE.dictPickupDetails = @{};
                APP_DELEGATE.tripStartLocation = APP_DELEGATE.userLocation;
                [self restoreGoogleMapWithMarkersAndRoutes];
            }
        }
    }
}



-(void)findAddressForCoordinate:(CLLocationCoordinate2D)location WithCallback:(void (^) (BOOL success, NSDictionary * dictAddress))callback{
    
    NSString * lattitude = [NSString stringWithFormat:@"%f",location.latitude];
    NSString * longitude = [NSString stringWithFormat:@"%f",location.longitude];
    
    [APP_DELEGATE.apiManager findAddressUsingLocationCoordinate:location withCallBack:^(BOOL success, NSDictionary *result, NSString *error) {
        if (success){
            NSString * title = result[@"title"];
            NSString * fullAddress = result[@"fullAddress"];
            callback(true, @{@"title":title,@"fullAddress":fullAddress,@"lattitude":lattitude,@"longitude":longitude});
        } else {
            [[GMSGeocoder geocoder] reverseGeocodeCoordinate:location completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
                if (response.results.count > 0){
                    GMSAddress* addressObj =  response.firstResult;
                    NSString * fullAddress = @"";
                    for (NSString * title in addressObj.lines){
                        fullAddress = [fullAddress stringByAppendingString:title];
                    }
                    
                    NSString *title;
                    if (addressObj.thoroughfare){
                        title = addressObj.thoroughfare;
                    } else{
                        title = addressObj.addressLine1;
                    }
                    
                    callback(true, @{@"title":title,@"fullAddress":fullAddress,@"lattitude":lattitude,@"longitude":longitude});
                } else {
                    callback(false, @{@"title":@"Current Location",@"fullAddress":@"",@"lattitude":lattitude,@"longitude":longitude});
                }
            }];
        }
    }];
}

-(void)drawRouteWithMapViewForOrigin:(CLLocationCoordinate2D)origin Destination:(CLLocationCoordinate2D)destination isDotedLine:(BOOL)isDoted withCallback:(void (^) (BOOL success))callback{
    if (NAVIGATION_MODE && APP_DELEGATE.isUserModeDriver){
        if (canCallAPIForRoute){
            if (DEBUG_MODE){
                NSLog(@"Can call API Avil");
            }
        } else {
            if (DEBUG_MODE){
                NSLog(@"Can call API Restricated");
            }
            
            if (DEBUG_MODE){
//                [Singleton showSnakBar:@"API Call restricated" multiline:false];
            }
            return;
        }
    }
    
    [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:origin destination:destination withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
        if (NAVIGATION_MODE){
            if (allRoutes.count > 0){
                if (canUpdateRoutes){
                    canUpdateRoutes = false;
                    if (!activeRouteIndex || activeRouteIndex >= allRoutes.count || activeRouteIndex < 0){
                        activeRouteIndex = 0;
                    }
                    int index = 0;
                    double lastSmallValue = 0.0;
                    int lastSmallIndex = -1;
                    for (NSDictionary * data in allRoutes){
                        double valueOne = [[data objectForKey:@"legs"][0][@"duration"][@"value"] doubleValue];
                        if (index == 0){
                            lastSmallIndex = index;
                            lastSmallValue = valueOne;
                        } else {
                            if (valueOne < lastSmallValue){
                                lastSmallValue = valueOne;
                                lastSmallIndex = index;
                            } else if (valueOne == lastSmallIndex){
                                double distOne = [[data objectForKey:@"legs"][0][@"distance"][@"value"] doubleValue];
                                double distTwo = [[allRoutes[lastSmallIndex] objectForKey:@"legs"][0][@"distance"][@"value"] doubleValue];
                                if (distOne < distTwo){
                                    lastSmallIndex = index;
                                    lastSmallValue = valueOne;
                                }
                            }
                        }
                        index = index + 1;
                    }
                    
                    activeRouteIndex = lastSmallIndex;
                    activeStepIndex = 0;
                    
                    [self drawRouteWithAllAlterNativeRoutesArray:allRoutes];
                } else {
                    APP_DELEGATE.tripDuration = [NSString stringWithFormat:@"%0.0f",round([[allRoutes[0] objectForKey:@"legs"][0][@"duration"][@"value"] intValue] / 60)];
                    APP_DELEGATE.tripDistance = [NSString stringWithFormat:@"%0.1f",[[allRoutes[0] objectForKey:@"legs"][0][@"distance"][@"value"]floatValue]];
                    APP_DELEGATE.tripFormatedDuration = [allRoutes[0] objectForKey:@"legs"][0][@"duration"][@"text"];
                    APP_DELEGATE.tripFormatedDistance = [allRoutes[0] objectForKey:@"legs"][0][@"distance"][@"text"];
                    
                    [self shouldShowRunningRideBannerForPassengerUser:true];
                    [self shouldShowRunningRideBannerForDriverUser:true];
                }
                callback(true);
            } else {
                callback(false);
            }
        } else {
            if (polylinePoints && ![polylinePoints isEqualToString:@""]) {
                APP_DELEGATE.tripDuration = [NSString stringWithFormat:@"%0.0f",round(etaSeconds / 60)];
                APP_DELEGATE.tripDistance = [NSString stringWithFormat:@"%0.1f",distanceKM];
                APP_DELEGATE.tripFormatedDuration = formattedETA;
                APP_DELEGATE.tripFormatedDistance = formatedDistnace;
                
                poliLine.map = nil;
                poliLine = [GMSPolyline polylineWithPath: [GMSPath pathFromEncodedPath: polylinePoints]];
                poliLine.strokeWidth = 5.0f;
                poliLine.strokeColor = [UIColor colorWithRed:81.0/255 green:81.0/255 blue:81.0/255 alpha:1.0];
                
                if (isDoted){
                    NSArray *styles = @[[GMSStrokeStyle solidColor:[UIColor clearColor]],[GMSStrokeStyle solidColor:[UIColor blackColor]]];
                    NSArray *lengths = @[@5, @5];
                    poliLine.spans = GMSStyleSpans(poliLine.path, styles, lengths, kGMSLengthGeodesic);
                }
                
                poliLine.map = viewMap;

                callback(true);
            } else {
                callback(false);
            }
        }
    }];
}

-(void)drawRouteWithAllAlterNativeRoutesArray:(NSArray *)arrayAltRoutes{
    if (DEBUG_MODE){
        NSLog(@"Will Maintain the Actiave Route with Lowest ETA later");
    }
    
    if (arrayAltRoutes && arrayAltRoutes.count > 0){
        arrayAlternativeRoutes = [[NSMutableArray alloc]initWithArray:arrayAltRoutes];
        
        for (NSDictionary * dictData in arrayRespectedPolylines){
            GMSPolyline * line = dictData[@"polyline"];
            line.map = nil;
        }
        arrayRespectedPolylines = [[NSMutableArray alloc]init];
        
        int i = 0;
        for (NSDictionary * data in arrayAlternativeRoutes){
            if (i == activeRouteIndex && APP_DELEGATE.isUserModeDriver){
                NSDictionary *routeOverviewPolyline = [data objectForKey:@"overview_polyline"];
                NSString * polylinePoints = [routeOverviewPolyline objectForKey:@"points"];
                
                NSArray * stepsArray = data[@"legs"][0][@"steps"];
                arrayActiveRoutesSteps = [[NSMutableArray alloc]initWithArray:stepsArray];
                
                GMSPolyline * activePoliline;
                activePoliline = [GMSPolyline polylineWithPath: [GMSPath pathFromEncodedPath: polylinePoints]];
                activePoliline.strokeWidth = 5.0f;
                activePoliline.strokeColor = NAVI_ACTIVE_ROUTE;
                activePoliline.tappable = APP_DELEGATE.isUserModeDriver;
                activePoliline.map = viewMap;
                activePoliline.zIndex = 1;
                [arrayRespectedPolylines addObject:@{@"index":[NSString stringWithFormat:@"%d",i], @"polyline":activePoliline, @"polypoints":polylinePoints}];
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_STEPS_UPDATED object:nil userInfo:@{@"steps_array":arrayActiveRoutesSteps}];

            } else {
                NSDictionary *routeOverviewPolyline = [data objectForKey:@"overview_polyline"];
                NSString * polylinePoints = [routeOverviewPolyline objectForKey:@"points"];
                
                GMSPolyline * activePoliline;
                activePoliline = [GMSPolyline polylineWithPath: [GMSPath pathFromEncodedPath: polylinePoints]];
                activePoliline.strokeWidth = 5.0f;
                activePoliline.strokeColor = NAVI_ALTERNATE_ROUTE;
                activePoliline.tappable = APP_DELEGATE.isUserModeDriver;
                activePoliline.map = viewMap;
                activePoliline.zIndex = 0;
                
                [arrayRespectedPolylines addObject:@{@"index":[NSString stringWithFormat:@"%d",i], @"polyline":activePoliline, @"polypoints":polylinePoints}];
            }
            
            i = i + 1;
        }
    
        if (APP_DELEGATE.isUserModeDriver && canCallAPIForRoute) {
            [self shouldShowNavigationForMapWithIsInitialMode:true];
        }
        
        if (activeRouteIndex < arrayAlternativeRoutes.count && APP_DELEGATE.isUserModeDriver){
            APP_DELEGATE.tripDuration = [NSString stringWithFormat:@"%0.0f",round([[arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"duration"][@"value"] intValue] / 60)];
            APP_DELEGATE.tripDistance = [NSString stringWithFormat:@"%0.1f",[[arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"distance"][@"value"]floatValue]];
            APP_DELEGATE.tripFormatedDuration = [arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"duration"][@"text"];
            APP_DELEGATE.tripFormatedDistance = [arrayAlternativeRoutes[activeRouteIndex] objectForKey:@"legs"][0][@"distance"][@"text"];
            
            [self shouldShowRunningRideBannerForPassengerUser:true];
            [self shouldShowRunningRideBannerForDriverUser:true];
        }
    }
}

-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    isMapRecentered = false;
    
    if (canHideMapOnDrag){
        [self didHideDriverInfoContainerWithAnimation];
        [self hideLocationDetailsContainerView];
        
        [self shouldShowRunningRideBannerForPassengerUser:false];
        [self shouldShowRunningRideBannerForDriverUser:false];
        
        [timerShowViews invalidate];
        if (APP_DELEGATE.arrayNearByDrivers.count > 0){
            timerShowViews = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(didShowDriverInfoContainerWithAnimation) userInfo:nil repeats:false];
        } else {
            timerShowViews = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(showLocationDetailsContainerView) userInfo:nil repeats:false];
        }
    }
    
    GMSVisibleRegion region = viewMap.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    if (![bounds containsCoordinate:APP_DELEGATE.userLocation]) {
        if(isAnimationStarted == false){
            isAnimationStarted = true;
            //Will Start Animation Here
        }
    } else {
        if (isAnimationStarted == true){
            isAnimationStarted = false;
        }
    }
}

-(void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker{
    if (marker.position.latitude == APP_DELEGATE.tripStartLocation.latitude && marker.position.longitude == APP_DELEGATE.tripStartLocation.longitude){
        isModePickBeginLocation = true;
    } else if (marker.position.latitude == APP_DELEGATE.tripEndLocation.latitude && marker.position.longitude == APP_DELEGATE.tripEndLocation.longitude){
        isModePickBeginLocation = false;
    }
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
}

-(void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    if (isModePickBeginLocation){
        APP_DELEGATE.tripStartLocation = marker.position;
        isInitialMarkerChanged= true;
        [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
    } else {
        APP_DELEGATE.tripEndLocation = marker.position;
        [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
    }
    [self restoreGoogleMapWithMarkersAndRoutes];
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    
    NSLog(@"Tapped Marker Title : %@",marker.title);
    
    NSString * searchableId = marker.title;
    
    NSUInteger index2 = [APP_DELEGATE.arrayNearByDrivers indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
        BOOL found = [[item objectForKey:@"user_id"] isEqualToString:searchableId];
        return found;
    }];
    
    if (index2 != NSNotFound && index2 < APP_DELEGATE.arrayNearByDrivers.count) {
        selectedUserId = searchableId;
        [self reloadDriverCollection];
        [collectionDriverList scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index2 inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        [self loadDataIntoUserInfoViewWithIndex:(int)index2];
    }
    return true;
}

-(void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay {
    NSLog(@"%@",overlay);
    
    GMSPolyline *line = (GMSPolyline *)overlay;
    NSUInteger index2 = [arrayRespectedPolylines indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
        BOOL found = [item objectForKey:@"polyline"] == line;
        return found;
    }];
    
    if (index2 == NSNotFound || index2 > arrayAlternativeRoutes.count) {
        index2 = 0;
    }
    
    activeRouteIndex = (int)index2;
    [self drawRouteWithAllAlterNativeRoutesArray:arrayAlternativeRoutes];
}



#pragma mark - ************************************
#pragma mark   ***** Driver / Rider Info View *****
#pragma mark   ************************************
-(void)initializeDriverInfoContainer {
    viewDriverInfo.frame = CGRectMake(0, HIDE_DRIVER_LIST_VIEW, SCREEN_WIDTH, collectionDriverList.frame.size.height + viewDriverInfoInner.frame.size.height + 20 + 100);
    userContainerHeight = viewDriverInfo.frame.size.height;
    [self.view addSubview:viewDriverInfo];
    
    panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(move:)];
    [panRecognizer setMinimumNumberOfTouches:1];
    [panRecognizer setMaximumNumberOfTouches:1];
    [viewDriverInfo addGestureRecognizer:panRecognizer];
    
    [collectionDriverList registerNib:[UINib nibWithNibName:@"DriversCell" bundle:nil] forCellWithReuseIdentifier:@"DriversCell"];
    collectionDriverList.delegate = self;
    collectionDriverList.dataSource = self;
    
    [btnRequestRide1 setTitle:GET_Language_Value(@"request") forState:UIControlStateNormal];
    [btnRequestRide2 setTitle:GET_Language_Value(@"request") forState:UIControlStateNormal];
}

-(void)loadDataIntoUserInfoViewWithIndex:(int)indexToLoad {
    if (indexToLoad < APP_DELEGATE.arrayNearByDrivers.count){
        NSDictionary * dictUserData = APP_DELEGATE.arrayNearByDrivers[indexToLoad];
        
        canHideMapOnDrag = false;
        double lattitude = [dictUserData[@"latitude"] doubleValue];
        double longitude = [dictUserData[@"longitude"] doubleValue];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            canHideMapOnDrag = true;
        });
        
        
        
        
        lblPopUpCarName.text = [NSString stringWithFormat:@"%@ %@", dictUserData[@"car_year"], dictUserData[@"car_model"]];
        if ([dictUserData[@"allow_rider"] isEqualToString:@"2"]){
            lblPopUpMAxRiders.text = [NSString stringWithFormat:@"Max %@ riders",@"2"];
        } else {
            lblPopUpMAxRiders.text = [NSString stringWithFormat:@"Max %@ riders",@"1"];
        }
        
        lblPopUpRidesGiven.text = [NSString stringWithFormat:@"Rides Given %@",dictUserData[@"total_given_ride"]];
        lblPopUpRidesTaken.text = [NSString stringWithFormat:@"Rides Taken %@",dictUserData[@"total_taken_ride"]];
        lblPassRidesGiven.text = [NSString stringWithFormat:@"Rides Given %@",dictUserData[@"total_given_ride"]];
        lblPassRidesTaken.text = [NSString stringWithFormat:@"Rides Taken %@",dictUserData[@"total_taken_ride"]];

        lblPopUpDriverName.text = [NSString stringWithFormat:@"%@ %@", dictUserData[@"first_name"], dictUserData[@"last_name"]];
        
     
        
        NSString *strCommuteTo = dictUserData[@"commute_to"];
        if ([strCommuteTo length] == 0)
        {
            
    
            topLabelETA.constant = - 30;
            [self.view layoutIfNeeded];
            lblHeadingTO.text = @"";
            lblHeadingToSecond.text = @"";
            
            lblHeadingToSecond.hidden = YES;
            btnImgHeading.hidden = YES;
            lblHeadingToLabel.hidden = YES;
        }
        else
        {
            topLabelETA.constant = 5;
            [self.view layoutIfNeeded];
            
            lblHeadingToLabel.hidden = NO;
            
            NSArray *arrCommute = [strCommuteTo componentsSeparatedByString:@"#"];
            if (arrCommute.count == 1)
            {
                lblHeadingTO.text = [NSString stringWithFormat:@"%@",[arrCommute objectAtIndex:0]];
     
                lblHeadingToSecond.hidden = YES;
                btnImgHeading.hidden = YES;
            }
            else
            {
                lblHeadingToSecond.hidden = NO;
                btnImgHeading.hidden = NO;
                
                 lblHeadingTO.text = [NSString stringWithFormat:@"%@",[arrCommute objectAtIndex:0]];

                lblHeadingToSecond.text = [NSString stringWithFormat:@"%@",[arrCommute objectAtIndex:1]];
                
             
            }
        }
        
      
        
        
        CLLocationCoordinate2D etaStartLocation = CLLocationCoordinate2DMake(00.000000, 00.000000);
        CLLocationCoordinate2D etaEndLocation = CLLocationCoordinate2DMake(00.000000, 00.000000);
        BOOL canCountETA = false;
        BOOL canCountCost = false;
        if (APP_DELEGATE.tripStartLocation.latitude != 00.0000 && APP_DELEGATE.tripStartLocation.longitude != 00.0000){
            etaStartLocation = APP_DELEGATE.tripStartLocation;
            canCountCost = true;
        } else {
            etaStartLocation = APP_DELEGATE.userLocation;
            canCountCost = true;
        }
        
        if (APP_DELEGATE.tripEndLocation.latitude != 00.0000 && APP_DELEGATE.tripEndLocation.longitude != 00.0000){
            etaEndLocation = APP_DELEGATE.tripEndLocation;
            canCountCost = true;
            canCountETA = true;
        } else if (lattitude != 00.0000 && longitude != 00.0000){
            etaEndLocation = CLLocationCoordinate2DMake(lattitude, longitude);
            canCountCost = false;
            canCountETA = true;
        } else {
            canCountCost = false;
            canCountETA = false;
        }
        
        
        if (canCountETA){
            [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:etaStartLocation destination:etaEndLocation withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
                if (polylinePoints && ![polylinePoints isEqualToString:@""]){
                    NSString * cost = @"";
                    if (canCountCost){
                        cost = [APP_DELEGATE countEstimatedCostFromETAInMinutes:0 DistanceInKM:distanceKM isModeValue:false];
                    }
                    
                    [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:etaStartLocation destination:CLLocationCoordinate2DMake(lattitude, longitude) withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
                        if (polylinePoints && ![polylinePoints isEqualToString:@""]){
                            lblPopUpETA.text = [NSString stringWithFormat:@"%@ (%@ away)",cost, formattedETA];
                            if (DEBUG_MODE){
                                NSLog(@"ETA & Cost %@",lblPopUpETA.text);
                            }
                            [viewDriverInfo sizeToFit];
                        } else {
                            lblPopUpETA.text = @"";
                        }
                    }];
                } else {
                    lblPopUpETA.text = @"";
                }
            }];
        }
        
        lblPopUpAge.text = [NSString stringWithFormat:@"Age : %@", dictUserData[@"age"]];
        
        if ([[dictUserData[@"gender"]lowercaseString] isEqualToString:@"male"] || [dictUserData[@"gender"] isEqualToString:@"1"]) {
            lblPopUpGEnder.text = [NSString stringWithFormat:@"Gender : Male"];
        } else {
            lblPopUpGEnder.text = [NSString stringWithFormat:@"Gender : Female"];
        }
        
        if ([dictUserData[@"gender"] length] == 0)
        {
            lblPopUpGEnder.text = @"";
            
        }
        
        
        if (![dictUserData[@"profession"] isEqualToString:@""]){
            lblPopUpOccupation.text = [NSString stringWithFormat:@"Occupation : %@",dictUserData[@"profession"]];
        } else {
            lblPopUpOccupation.text = @"";
        }
        
        if ([dictUserData[@"riding_preferences"] isEqualToString:@"1"]){
            lblPopUpPrefersRide.text = @"Prefers Ride : Male";
        } else if ([dictUserData[@"riding_preferences"] isEqualToString:@"2"]){
            lblPopUpPrefersRide.text = @"Prefers to Ride : Female";
        } else {
            lblPopUpPrefersRide.text = @"Prefers to Ride : male & female";
        }
        
        [self didShowDriverInfoContainerWithAnimation];
    }
}

-(void)didShowDriverInfoContainerWithAnimation {
    if (!APP_DELEGATE.isRideRunning && arrayNearByCarMarkers.count > 0){
        [UIView animateWithDuration:0.3 animations:^{
            if (APP_DELEGATE.isUserModeDriver){
                viewDriverInfo.frame = CGRectMake(0, SHOW_DRIVER_LIST_VIEW_HALF, SCREEN_WIDTH, userContainerHeight);
            } else {
                viewDriverInfo.frame = CGRectMake(0, SHOW_DRIVER_LIST_VIEW_HALF, SCREEN_WIDTH, userContainerHeight);
            }
            
        } completion:^(BOOL finished) {
            viewTransperent.alpha = 0;
            viewTransperent.hidden = true;
            btnRequestRide1.hidden = NO;
            btnRequestRide2.hidden = YES;
        }];
    }
    
    [self showLocationDetailsContainerView];
    [self shouldShowRunningRideBannerForPassengerUser:true];
    [self shouldShowRunningRideBannerForDriverUser:true];
}

-(void)didHideDriverInfoContainerWithAnimation {
    [UIView animateWithDuration:0.3 animations:^{
        viewDriverInfo.frame = CGRectMake(0, HIDE_DRIVER_LIST_VIEW + 100, SCREEN_WIDTH, viewDriverInfo.frame.size.height);
    } completion:^(BOOL finished) {
        viewTransperent.alpha = 0;
        viewTransperent.hidden = true;
    }];
}

-(void)move:(UIPanGestureRecognizer*)sender {
    
    [self.view bringSubviewToFront:sender.view];
    CGPoint translatedPoint = [sender translationInView:sender.view.superview];
    
    if ((sender.view.center.y+translatedPoint.y) < (SCREEN_HEIGHT - viewDriverInfo.frame.size.height - 100 + (viewDriverInfo.frame.size.height/2))) {
        
        goto perform_end;
    }
    else
        translatedPoint = CGPointMake(sender.view.center.x, sender.view.center.y+translatedPoint.y);
    
    [sender.view setCenter:translatedPoint];
    [sender setTranslation:CGPointZero inView:sender.view];
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        perform_end : {
            
            CGFloat velocityX = (0.2*[sender velocityInView:self.view].x);
            CGFloat velocityY = (0.2*[sender velocityInView:self.view].y);
            
            CGFloat finalY = translatedPoint.y + velocityY;// translatedPoint.y + (.35*[(UIPanGestureRecognizer*)sender velocityInView:self.view].y);
            
            if (finalY < (SCREEN_HEIGHT - lblDriverCurrentLocation.frame.origin.y)) {
                finalY = SHOW_DRIVER_LIST_VIEW_FULL;
                viewTransperent.hidden = false;
                [UIView animateWithDuration:0.3 animations:^{
                    viewTransperent.alpha = 1;
                } completion:^(BOOL finished) {
                    btnRequestRide1.hidden = YES;
                    btnRequestRide2.hidden = NO;
                }];
            } else if (finalY > (SCREEN_HEIGHT - lblDriverCurrentLocation.frame.origin.y)) {
                finalY = SHOW_DRIVER_LIST_VIEW_HALF;
                
                [UIView animateWithDuration:0.3 animations:^{
                    viewTransperent.alpha = 0;
                } completion:^(BOOL finished) {
                    btnRequestRide1.hidden = NO;
                    btnRequestRide2.hidden = YES;
                    viewTransperent.hidden = true;
                }];
            }
            
            [panRecognizer setEnabled:NO];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [panRecognizer setEnabled:true];
            });
            
            
            CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
            
            NSLog(@"the duration is: %f", animationDuration);
            
            [UIView beginAnimations:nil context:NULL];
            [UIView setAnimationDuration:animationDuration];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            [UIView setAnimationDelegate:self];
            [[sender view] setFrame:CGRectMake(viewDriverInfo.frame.origin.x, finalY, SCREEN_WIDTH, viewDriverInfo.frame.size.height)];
            [UIView commitAnimations];
            
        }
    }
}

- (void) transperentViewTapped {
   [self didShowDriverInfoContainerWithAnimation];
}

-(void)updateNearByUserMarkersWithAnimation {
    if ( APP_DELEGATE.isRideRunning && NAVIGATION_MODE){
        for (GMSMarker * marker in arrayNearByCarMarkers){
            marker.map = nil;
        }
        
        [arrayNearByCarMarkers removeAllObjects];
        return;
    }
    
    if (!APP_DELEGATE.arrayNearByDrivers || APP_DELEGATE.arrayNearByDrivers.count <= 0){
        if (canShowNoUserAlert){
            isAlertModeConfirmation = false;
            isAlertModeExpire = false;
            isAlertModePlanRide = false;
            isAlertModeNoUser = true;
            canShowNoUserAlert = false;
            
//            AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
//            obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
//            obj.delegate = self;
//            obj.alertTitles = @"No near by users available at the movement";
//            obj.alertMessages = @"";
//            obj.yesTitle = @"OKAY";
//            obj.noTitle = @"";
//        
//            [self presentViewController:obj animated:NO completion:nil];
            [self didHideDriverInfoContainerWithAnimation];
        }
    } else {
        canShowNoUserAlert = true;
    }
    
    BOOL isModeInitial = false;
    if (!arrayNearByCarMarkers || arrayNearByCarMarkers.count == 0) {
        isModeInitial = true;
        //This is for Initial State
        arrayNearByCarMarkers = [[NSMutableArray alloc]init];
        for (NSDictionary * dictLocation in APP_DELEGATE.arrayNearByDrivers){
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake([dictLocation[@"latitude"] doubleValue], [dictLocation[@"longitude"] doubleValue]);
            
            GMSMarker * markerToAdd = [[GMSMarker alloc]init];
            markerToAdd.position = location;
            
//            if ([dictLocation[@"gender"] isEqualToString:@"1"]){
//                if (APP_DELEGATE.isUserModeDriver){
//                    if ([dictLocation[@"is_favourite"] isEqualToString:@"true"] || [dictLocation[@"is_favourite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerBoyFav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerBoy"];
//                    }
//                } else {
//                    if ([dictLocation[@"is_favourite"] isEqualToString:@"true"] || [dictLocation[@"is_favourite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"car-blue-fav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"car-blue"];
//                    }
//                }
//                
//            } else {
//                if (APP_DELEGATE.isUserModeDriver){
//                    if ([dictLocation[@"is_favourite"] isEqualToString:@"true"] || [dictLocation[@"is_favourite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerGirlFav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerGirl"];
//                    }
//                } else {
//                    if ([dictLocation[@"is_favourite"] isEqualToString:@"true"] || [dictLocation[@"is_favourite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"car-pink-fav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"car-pink"];
//                    }
//                }
//            }
            markerToAdd.title = dictLocation[@"user_id"];
            markerToAdd.map = viewMap;
            [arrayNearByCarMarkers addObject:markerToAdd];
        }
        
        
        
        NSUInteger index2 = [APP_DELEGATE.arrayNearByDrivers indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
            BOOL found = [[item objectForKey:@"user_id"] isEqualToString:selectedUserId];
            return found;
        }];
        
        if (index2 == NSNotFound || index2 > APP_DELEGATE.arrayNearByDrivers.count) {
            index2 = 0;
            if (APP_DELEGATE.arrayNearByDrivers.count > 0){
                selectedUserId = APP_DELEGATE.arrayNearByDrivers[index2][@"user_id"];
            }
        }
        [self reloadDriverCollection];
        [self loadDataIntoUserInfoViewWithIndex:(int)index2];
    } else {
        //Remove or Update Driver Marker
        NSMutableArray * arrayManagableMarker = [[NSMutableArray alloc]init];
        for (GMSMarker * userMarkers in arrayNearByCarMarkers){
            NSString * searchableId = userMarkers.title;
            NSUInteger index2 = [APP_DELEGATE.arrayNearByDrivers indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
                BOOL found = [[item objectForKey:@"user_id"] isEqualToString:searchableId];
                return found;
            }];
            
            if (index2 == NSNotFound || index2 > APP_DELEGATE.arrayNearByDrivers.count) {
                [arrayManagableMarker addObject:userMarkers];
            } else {
                static UIImage * oldImage;
                if (APP_DELEGATE.isUserModeDriver && APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
                    oldImage = userMarkers
                    ;
                    userMarkers.icon = [UIImage imageNamed:@"PHMarkerDriver"];
                } else if (oldImage){
                    userMarkers.icon = oldImage;
                    oldImage = nil;
                }
                
                CLLocationCoordinate2D oldPossition = userMarkers.position;
                double newLattitude = [APP_DELEGATE.arrayNearByDrivers[index2][@"latitude"] doubleValue];
                double newLongitude = [APP_DELEGATE.arrayNearByDrivers[index2][@"longitude"] doubleValue];
                CLLocationCoordinate2D newPossition = CLLocationCoordinate2DMake(newLattitude, newLongitude);
                
                MKMapPoint point1 = MKMapPointForCoordinate(oldPossition);
                MKMapPoint point2 = MKMapPointForCoordinate(newPossition);
                float distance = MKMetersBetweenMapPoints(point1, point2);
                if (distance > 0) {
                    [APP_DELEGATE moveMarker:userMarkers WithAnimationFromCoordinate:oldPossition toCoordinate:newPossition];
                }
                //Animate The Marker to New Location
            }
        }
        
        //Remove Marke if Found
        for (GMSMarker * markerToRemove in arrayManagableMarker){
            markerToRemove.map = nil;
            [arrayNearByCarMarkers removeObject:markerToRemove];
        }
        [arrayManagableMarker removeAllObjects];
        arrayManagableMarker = [[NSMutableArray alloc]init];
        
        //Add Found Markers
        for (NSDictionary * dictNearyByUser in APP_DELEGATE.arrayNearByDrivers){
            NSString * searchableId = dictNearyByUser[@"user_id"];
            NSUInteger index2 = [arrayNearByCarMarkers indexOfObjectPassingTest:^BOOL(GMSMarker *item, NSUInteger idx, BOOL *stop) {
                BOOL found = [item.title isEqualToString:searchableId];
                return found;
            }];
            
            if (index2 == NSNotFound || index2 > APP_DELEGATE.arrayNearByDrivers.count) {
                [arrayManagableMarker addObject:dictNearyByUser];
            }
        }
        
        for (NSDictionary * dictNearyByUser in arrayManagableMarker){
            CLLocationCoordinate2D location = CLLocationCoordinate2DMake([dictNearyByUser[@"latitude"] doubleValue], [dictNearyByUser[@"longitude"] doubleValue]);
            
            GMSMarker * markerToAdd = [[GMSMarker alloc]init];
            markerToAdd.position = location;
            
//            if ([dictNearyByUser[@"gender"] isEqualToString:@"1"]){
//                if (APP_DELEGATE.isUserModeDriver){
//                    if ([dictNearyByUser[@"is_favourite"] isEqualToString:@"true"] || [dictNearyByUser[@"is_favourite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerBoyFav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerBoy"];
//                    }
//
//                } else {
//                    if ([dictNearyByUser[@"is_favourite"] isEqualToString:@"true"] || [dictNearyByUser[@"is_favourite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"car-blue-fav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"car-blue"];
//                    }
//                }
//                
//            } else {
//                if (APP_DELEGATE.isUserModeDriver){
//                    if ([dictNearyByUser[@"is_favourite"] isEqualToString:@"true"] || [dictNearyByUser[@"is_favourite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerGirlFav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"PHMarkerGirl"];
//                    }
//                } else {
//                    if ([dictNearyByUser[@"favorite"] isEqualToString:@"true"] || [dictNearyByUser[@"favorite"] isEqualToString:@"1"]){
//                        markerToAdd.icon = [UIImage imageNamed:@"car-pink-fav"];
//                    } else {
//                        markerToAdd.icon = [UIImage imageNamed:@"car-pink"];
//                    }
//                }
//            }
            
            
            
            markerToAdd.title = dictNearyByUser[@"user_id"];
            markerToAdd.map = viewMap;
            [arrayNearByCarMarkers addObject:markerToAdd];
        }
        [arrayManagableMarker removeAllObjects];
    }
    
    if (APP_DELEGATE.arrayNearByDrivers.count > 0 && !APP_DELEGATE.isRideRunning){
        [self reloadDriverCollection];
        NSUInteger index2 = [APP_DELEGATE.arrayNearByDrivers indexOfObjectPassingTest:^BOOL(NSDictionary *item, NSUInteger idx, BOOL *stop) {
            BOOL found = [[item objectForKey:@"user_id"] isEqualToString:selectedUserId];
            return found;
        }];
        
        if (index2 == NSNotFound || index2 > APP_DELEGATE.arrayNearByDrivers.count) {
            index2 = 0;
        }
        [self loadDataIntoUserInfoViewWithIndex:(int)index2];
    }
}

- (void) reloadDriverCollection {
    
    [collectionDriverList reloadData];
    [collectionDriverList layoutIfNeeded];
    if (collectionDriverList.contentSize.width >= SCREEN_WIDTH) {
        
        constraintCollectionWidth.constant = SCREEN_WIDTH;
    }
    else {
        constraintCollectionWidth.constant = collectionDriverList.contentSize.width;
    }
}

#pragma mark - ************************************************
#pragma mark   ****** UPDATE USER CURRENT LOCATION ******
#pragma mark   ************************************************

-(void)notificationFoundForRideStatusUpdate:(NSNotification *)notif {
    [self updateCustomerCurrentLocation];
}

-(void)currentLocation
{
    timerStatus=0;
    counter=0;
    tempCount=0;
    
    APP_DELEGATE.timer1 =nil;
    [APP_DELEGATE.timer1 invalidate];
    
    UIBackgroundTaskIdentifier bgTask = 0;
    UIApplication  *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
    }];
    
    [self updateCustomerCurrentLocation];
    
    APP_DELEGATE.timer1 =
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                   selector:@selector(callWebserviceInBackGround) userInfo:nil
                                    repeats:YES];
    [[NSRunLoop currentRunLoop]addTimer:APP_DELEGATE.timer1 forMode:UITrackingRunLoopMode];
    
}

-(void)callWebserviceInBackGround
{
    [self updateUserLocationMarker];
    
    if (fmod(counter, 3.0) == 00.0 && NAVIGATION_MODE && APP_DELEGATE.isUserModeDriver && APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus != RIDE_STATUS_REQUEST && canCallAPIForRoute){
        [self loadNextStepsDetailsIntoViews];
        if (DEBUG_MODE){
            NSLog(@"All the Steps Reloaded");
        }
    }
    
    if (counter==10 && APP_DELEGATE.isUserLoggedIn){
        counter=0;
        if (APP_DELEGATE.isUserModeDriver && APP_DELEGATE.isRideRunning){
            [self btnRecenterTapAction:btnRecenter];
        }
        [self updateCustomerCurrentLocation];
        
    } else if (!APP_DELEGATE.isUserLoggedIn){
        [APP_DELEGATE.timer1 invalidate];
        [APP_DELEGATE.timer2 invalidate];
    }
    
    if (DEBUG_MODE){
        NSLog(@"Temp Count --->> %d",tempCount);
    }
    
    counter++;
    
    tempCount++;
    if (tempCount==100)
    {
        if (timerStatus==0)
        {
            [APP_DELEGATE.timer1 invalidate ];
            APP_DELEGATE.timer1=nil;
            timerStatus=1;
//            counter=0;
            tempCount=0;
            
            UIBackgroundTaskIdentifier bgTask = 0;
            UIApplication  *app = [UIApplication sharedApplication];
            bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                [app endBackgroundTask:bgTask];
            }];
            
            
            APP_DELEGATE.timer2 =
            [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self selector:@selector(callWebserviceInBackGround)
                                           userInfo:nil repeats:YES];
            
            [[NSRunLoop currentRunLoop]addTimer:APP_DELEGATE.timer2 forMode:UITrackingRunLoopMode];
        }
        else
        {
            [APP_DELEGATE.timer2 invalidate ];
            APP_DELEGATE.timer2=nil;
            timerStatus=0;
//            counter=0;
            tempCount=0;
            
            UIBackgroundTaskIdentifier bgTask = 0;
            UIApplication  *app = [UIApplication sharedApplication];
            bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
                [app endBackgroundTask:bgTask];
            }];
            
            APP_DELEGATE.timer1 =
            [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                           selector:@selector(callWebserviceInBackGround) userInfo:nil
                                            repeats:YES];
            
            [[NSRunLoop currentRunLoop]addTimer:APP_DELEGATE.timer1 forMode:UITrackingRunLoopMode];
        }
    }
    
}

-(void)updateCustomerCurrentLocationAPI {
    double currrentLattitude = APP_DELEGATE.userLocation.latitude;
    double currrentLongitude = APP_DELEGATE.userLocation.longitude;
    if (APP_DELEGATE.isRideRunning){
        double driverLattitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLongitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        if (driverLattitude && driverLattitude != 00.0000 && driverLongitude && driverLongitude != 00.0000){
            currrentLattitude = driverLattitude;
            currrentLongitude = driverLongitude;
        }
    } else {
        currrentLattitude = APP_DELEGATE.userLocation.latitude;
        currrentLongitude = APP_DELEGATE.userLocation.longitude;
    }
    
    double pickupLattitude = APP_DELEGATE.tripStartLocation.latitude;
    double pickupLongitude = APP_DELEGATE.tripStartLocation.longitude;
    
    if (currrentLattitude != 00.00000 && currrentLongitude != 00.000){
        if (pickupLattitude && pickupLattitude != 00.0000 && pickupLongitude && pickupLongitude != 00.0000){
            if (DEBUG_MODE){
                NSLog(@"User Already Selected a Pickup Location");
            }
        } else {
            pickupLattitude = currrentLattitude;
            pickupLongitude = currrentLongitude;
        }
        
        if (DEBUG_MODE){
            NSLog(@"Selected Pickup Location : %f, %f", APP_DELEGATE.tripStartLocation.latitude, APP_DELEGATE.tripStartLocation.longitude);
            NSLog(@"Actual User Location : %f, %f", APP_DELEGATE.userLocation.latitude, APP_DELEGATE.userLocation.longitude);
            NSLog(@"Actual Driver Location : %@, %@", APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"], APP_DELEGATE.dictTripDetails[@"temp_location_longitude"]);
            NSLog(@"API Pickup Location : %f, %f", pickupLattitude, pickupLongitude);
            NSLog(@"API User Location : %f, %f", currrentLattitude, currrentLongitude);
        }
        
        //Will Manage Actual Route Coordinates
        if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
            NSMutableArray * arrayData = APP_DELEGATE.arrayActualRoute.mutableCopy;
            [arrayData addObject:@{
                                   @"lattitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.latitude],
                                   @"longitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.longitude]
                                   }];
            
            APP_DELEGATE.arrayActualRoute = arrayData;
        }
        
        NSString * strCurrentLattitude = [NSString stringWithFormat:@"%f",currrentLattitude];
        NSString * strCurrentLongitude = [NSString stringWithFormat:@"%f",currrentLongitude];
        NSString * strPickupLattitude = [NSString stringWithFormat:@"%f",pickupLattitude];
        NSString * strPickupLongitude = [NSString stringWithFormat:@"%f",pickupLongitude];
        
        [APP_DELEGATE.apiManager updateCustomerCurrentLocationWithUserLocationLattitude:strCurrentLattitude UserLongitude:strCurrentLongitude PickupLattitude:strPickupLattitude PickupLongitude:strPickupLongitude withCallBack:^(BOOL success, NSArray *arrayResponse, NSString *serverMessange) {
            if (canHideViewHUD){
                canHideViewHUD = false;
                STOP_HUD
            }
            
            if (success){
                if (![arrayResponse isKindOfClass:[NSNull class]] && arrayResponse.count > 0)
                {
                    NSMutableArray *arrDetails = [arrayResponse mutableCopy];
                    if (DEBUG_MODE){
                        NSLog(@"Current API Response : %@",arrDetails);
                    }
                    
                    NSDictionary * dictResponse = arrDetails.firstObject;
                    APP_DELEGATE.currencySymbol = [dictResponse objectForKey:@"currency_symbol"];
                    APP_DELEGATE.decimapPoints = [dictResponse objectForKey:@"currency_decimal"];
                    APP_DELEGATE.badgeValue = [dictResponse objectForKey:@"badge_count"];
                    APP_DELEGATE.rideRates = [[dictResponse objectForKey:@"ride_rate"] doubleValue];
                    
                    APP_DELEGATE.phoneMessageNumber = [dictResponse objectForKey:@"call_us"];
                    APP_DELEGATE.facebookURL = [dictResponse objectForKey:@"fb_url"];
                    APP_DELEGATE.whatsApp = [dictResponse objectForKey:@"whats_app"];
                    
                    APP_DELEGATE.strCommuteTo = [dictResponse objectForKey:@"commute_to"];
                    
                    NSLog(@"%@",APP_DELEGATE.whatsApp);
                    
                    
                    int globalTimeOut = [(NSString *)dictResponse[@"global_timeout"] intValue];
                    if (globalTimeOut && globalTimeOut > 0 && globalTimeOut != APP_DELEGATE.globalTimeout) {
                        if (DEBUG_MODE){
                            NSLog(@"Time Out : %d",globalTimeOut);
                        }
                        APP_DELEGATE.globalTimeout = globalTimeOut;
                        totalTimeToWait = APP_DELEGATE.globalTimeout;
                        remainigTime = totalTimeToWait - [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate];
                    }
                    
                    NSString * walletBalance = dictResponse[@"wallet_amount"];
                    if (walletBalance && ![walletBalance isEqualToString:@""]){
                        double walet = [walletBalance doubleValue];
                        walletBalance = [NSString stringWithFormat:@"%f",walet];
                        APP_DELEGATE.WalletAmount = walletBalance;
                    }
                    
                    [self manageViewFromStatus:arrDetails];
                    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_BADGE_COUNT_UPDATED object:nil];
                } else {
                    if (DEBUG_MODE){
                        NSLog(@"Unable to get response array : %@",arrayResponse);
                    }
                }
            } else {
                if (DEBUG_MODE){
                    NSLog(@"Unable to Update Current Location API : %@",serverMessange);
                }
            }
        }];
        
    } else {
        [APP_DELEGATE getCurrentLocation];
        if (canHideViewHUD){
            canHideViewHUD = false;
            STOP_HUD
        }
    }
}

-(void)updateDriverCurrentLocationAPI {
    double currrentLattitude = APP_DELEGATE.userLocation.latitude;
    double currrentLongitude = APP_DELEGATE.userLocation.longitude;
    
    double pickupLattitude = APP_DELEGATE.tripStartLocation.latitude;
    double pickupLongitude = APP_DELEGATE.tripStartLocation.longitude;
    
    if (currrentLattitude != 00.00000 && currrentLongitude != 00.000){
        if (pickupLattitude && pickupLattitude != 00.0000 && pickupLongitude && pickupLongitude != 00.0000){
            if (DEBUG_MODE){
                NSLog(@"User Already Selected a Pickup Location");
            }
        } else {
            pickupLattitude = currrentLattitude;
            pickupLongitude = currrentLongitude;
        }
        
        if (DEBUG_MODE){
            NSLog(@"Selected Pickup Location : %f, %f", APP_DELEGATE.tripStartLocation.latitude, APP_DELEGATE.tripStartLocation.longitude);
            NSLog(@"Actual User Location : %f, %f", APP_DELEGATE.userLocation.latitude, APP_DELEGATE.userLocation.longitude);
            NSLog(@"Actual Driver Location : %@, %@", APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"], APP_DELEGATE.dictTripDetails[@"temp_location_longitude"]);
            NSLog(@"API Pickup Location : %f, %f", pickupLattitude, pickupLongitude);
            NSLog(@"API User Location : %f, %f", currrentLattitude, currrentLongitude);
        }
        
        //Will Manage Actual Route Coordinates
        if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
            NSMutableArray * arrayData = APP_DELEGATE.arrayActualRoute.mutableCopy;
            [arrayData addObject:@{
                                   @"lattitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.latitude],
                                   @"longitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.longitude]
                                   }];
            
            APP_DELEGATE.arrayActualRoute = arrayData;
        }
        
        NSString * strCurrentLattitude = [NSString stringWithFormat:@"%f",currrentLattitude];
        NSString * strCurrentLongitude = [NSString stringWithFormat:@"%f",currrentLongitude];
        NSString * strPickupLattitude = [NSString stringWithFormat:@"%f",pickupLattitude];
        NSString * strPickupLongitude = [NSString stringWithFormat:@"%f",pickupLongitude];
        
        [APP_DELEGATE.apiManager updateDriverCurrentLocationWithUserLocationLattitude:strCurrentLattitude UserLongitude:strCurrentLongitude PickupLattitude:strPickupLattitude PickupLongitude:strPickupLongitude withCallBack:^(BOOL success, NSArray *arrayResponse, NSString *serverMessange) {
            if (canHideViewHUD){
                canHideViewHUD = false;
                STOP_HUD
            }
            
            if (success){
                if (![arrayResponse isKindOfClass:[NSNull class]] && arrayResponse.count > 0)
                {
                    NSMutableArray *arrDetails = [arrayResponse mutableCopy];
                    if (DEBUG_MODE){
                        NSLog(@"Current API Response : %@",arrDetails);
                    }
                    
                    NSDictionary * dictResponse = arrDetails.firstObject;
                    APP_DELEGATE.currencySymbol = [dictResponse objectForKey:@"currency_symbol"];
                    APP_DELEGATE.decimapPoints = [dictResponse objectForKey:@"currency_decimal"];
                    APP_DELEGATE.badgeValue = [dictResponse objectForKey:@"badge_count"];
                    APP_DELEGATE.rideRates = [[dictResponse objectForKey:@"ride_rate"] doubleValue];
                    
                    APP_DELEGATE.phoneMessageNumber = [dictResponse objectForKey:@"call_us"];
                    APP_DELEGATE.facebookURL = [dictResponse objectForKey:@"fb_url"];
                    APP_DELEGATE.whatsApp = [dictResponse objectForKey:@"whats_app"];
                    
                     APP_DELEGATE.strCommuteTo = [dictResponse objectForKey:@"commute_to"];
                    
                    NSLog(@"%@",APP_DELEGATE.whatsApp);
                    
                    int globalTimeOut = [(NSString *)dictResponse[@"global_timeout"] intValue];
                    if (dictResponse[@"trip_details"] && ((NSArray *)dictResponse[@"trip_details"]).count > 0){
                        globalTimeOut = [(NSString *)(dictResponse[@"trip_details"][0][@"global_driver_timeout"]) intValue];
                    }
                    
                    if (globalTimeOut && globalTimeOut > 0 && globalTimeOut != APP_DELEGATE.globalTimeout) {
                        if (DEBUG_MODE){
                            NSLog(@"Time Out : %d",globalTimeOut);
                        }
                        APP_DELEGATE.globalTimeout = globalTimeOut;
                        totalTimeToWait = APP_DELEGATE.globalTimeout;
                        remainigTime = totalTimeToWait - [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate];
                    }
                    
                    NSString * walletBalance = dictResponse[@"wallet_amount"];
                    if (walletBalance && ![walletBalance isEqualToString:@""]){
                        double walet = [walletBalance doubleValue];
                        walletBalance = [NSString stringWithFormat:@"%f",walet];
                        APP_DELEGATE.WalletAmount = walletBalance;
                    }
                    
                    [self manageViewFromStatus:arrDetails];
                    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_BADGE_COUNT_UPDATED object:nil];
                } else {
                    if (DEBUG_MODE){
                        NSLog(@"Unable to get response array : %@",arrayResponse);
                    }
                }
            } else {
                if (DEBUG_MODE){
                    NSLog(@"Unable to Update Current Location API : %@",serverMessange);
                }
            }
        }];
        
    } else {
        [APP_DELEGATE getCurrentLocation];
        if (canHideViewHUD){
            canHideViewHUD = false;
            STOP_HUD
        }
    }
}

-(void)updateCustomerCurrentLocation {
    if (APP_DELEGATE.isUserModeDriver){
        [self updateDriverCurrentLocationAPI];
    } else {
        [self updateCustomerCurrentLocationAPI];
    }
}

/* The Old Method
-(void)updateCustomerCurrentLocation {
    if (DEBUG_MODE){
        NSLog(@"%@", APP_DELEGATE.dictTripDetails);
    }
    double locationLattitude = APP_DELEGATE.userLocation.latitude;
    double locationLongitude = APP_DELEGATE.userLocation.longitude;
    if (!APP_DELEGATE.isUserModeDriver && APP_DELEGATE.isRideRunning && APP_DELEGATE.dictTripDetails){
        locationLattitude = [APP_DELEGATE.dictTripDetails[@"driver_latitude"] doubleValue];
        locationLongitude = [APP_DELEGATE.dictTripDetails[@"driver_longitude"] doubleValue];
        
        if (locationLattitude == 00.0000 || locationLongitude == 00.00000) {
            locationLattitude = APP_DELEGATE.userLocation.latitude;
            locationLongitude = APP_DELEGATE.userLocation.longitude;
        }
    }
    
    NSLog(@"%@",APP_DELEGATE.strLatitude);
    if (locationLattitude == 00.0000 || locationLongitude == 00.00000)
    {
        [APP_DELEGATE getCurrentLocation];
        if (canHideViewHUD){
            canHideViewHUD = false;
            STOP_HUD
        }
    } else
    {
        if (DEBUG_MODE){
            NSLog(@"Pickup Lattitude : %f",APP_DELEGATE.tripStartLocation.latitude);
            NSLog(@"Pickup Longitude : %f",APP_DELEGATE.tripStartLocation.longitude);
            NSLog(@"Destination Lattitude : %f",APP_DELEGATE.tripEndLocation.latitude);
            NSLog(@"Destination Longitude : %f",APP_DELEGATE.tripEndLocation.longitude);
        }
        
        
        
        [APP_DELEGATE.apiManager updateUserCurrentLocationWithUserLocationLattitude:[NSString stringWithFormat:@"%f",locationLattitude] UserLongitude:[NSString stringWithFormat:@"%f",locationLongitude] withCallBack:^(BOOL success, NSArray *arrayResponse, NSString *serverMessange) {
            if (canHideViewHUD){
                canHideViewHUD = false;
                STOP_HUD
            }
            if (success){
                if (![arrayResponse isKindOfClass:[NSNull class]] && arrayResponse.count > 0)
                {
                    NSMutableArray *arrDetails = [arrayResponse mutableCopy];
                    
                    if (DEBUG_MODE){
                        NSLog(@"Current API Response : %@",arrDetails);
                    }
                    
                    NSDictionary * dictResponse = arrDetails.firstObject;
                    APP_DELEGATE.currencySymbol = [dictResponse objectForKey:@"currency_symbol"];
                    APP_DELEGATE.decimapPoints = [dictResponse objectForKey:@"currency_decimal"];
                    APP_DELEGATE.badgeValue = [dictResponse objectForKey:@"badge_count"];
                    APP_DELEGATE.rideRates = [[dictResponse objectForKey:@"ride_rate"] doubleValue];

                    APP_DELEGATE.phoneMessageNumber = [dictResponse objectForKey:@"call_us"];
                    APP_DELEGATE.facebookURL = [dictResponse objectForKey:@"fb_url"];
                    APP_DELEGATE.whatsApp = [dictResponse objectForKey:@"whats_app"];
                    
                    int globalTimeOut = [(NSString *)dictResponse[@"global_timeout"] intValue];
                    if (APP_DELEGATE.isUserModeDriver){
                        if (dictResponse[@"trip_details"] && ((NSArray *)dictResponse[@"trip_details"]).count > 0){
                            globalTimeOut = [(NSString *)(dictResponse[@"trip_details"][0][@"global_driver_timeout"]) intValue];
                        }
                    }
                    
                    if (globalTimeOut && globalTimeOut > 0 && globalTimeOut != APP_DELEGATE.globalTimeout) {
                        if (DEBUG_MODE){
                            NSLog(@"Time Out : %d",globalTimeOut);
                        }
                        APP_DELEGATE.globalTimeout = globalTimeOut;
                        totalTimeToWait = APP_DELEGATE.globalTimeout;
                        remainigTime = totalTimeToWait - [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate];
                    }
                    
                    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_BADGE_COUNT_UPDATED object:nil];
                    
                    NSString * walletBalance = dictResponse[@"wallet_amount"];
                    if (walletBalance && ![walletBalance isEqualToString:@""]){
                        double walet = [walletBalance doubleValue];
                        walletBalance = [NSString stringWithFormat:@"%f",walet];
                        APP_DELEGATE.WalletAmount = walletBalance;
                    }
                    
                    [self manageViewFromStatus:arrDetails];
                }
            } else {
                if (DEBUG_MODE){
                    NSLog(@"Failed to update current locatio : %@",serverMessange);
                }
            }
        }];
    }
}*/

-(void)manageViewFromStatus:(NSArray *)arrDetails
{
    if (DEBUG_MODE){
        NSLog(@"Current Location Response : %@",arrDetails);
    }
    
    NSDictionary * dictCurrentResponse = arrDetails[0];
    NSArray * data = dictCurrentResponse[@"trip_details"];
    NSMutableDictionary * dictTripDetails = data.count > 0 ? [[NSMutableDictionary alloc]initWithDictionary:data[0]] : [[NSMutableDictionary alloc]init];
    
    [APP_DELEGATE.arrayNearByDrivers removeAllObjects];
    APP_DELEGATE.arrayNearByDrivers = [[NSMutableArray alloc]init];
    
    if (dictTripDetails && dictTripDetails.count > 0){
        for (NSString * key in dictTripDetails.allKeys){
            if (dictTripDetails[key] && dictTripDetails[key] != nil && ![dictTripDetails[key] isKindOfClass:[NSNull class]]){
            } else {
                [dictTripDetails setObject:@"" forKey:key];
            }
        }
        
        APP_DELEGATE.dictTripDetails = dictTripDetails;
        [self updateLocalTripInfoWithTripDetails:dictTripDetails withRideStatus:dictCurrentResponse[@"ride_status"]];
        
        if (APP_DELEGATE.isUserModeDriver){
            //Will Manage Local Views For Driver
            [self manageLocalViewsForDriverUser:dictTripDetails withRideStatus:dictCurrentResponse[@"ride_status"]];
        } else {
            //Will Manage Local Views For Pessanger
            [self manageLocalViewsForPessangerUser:dictTripDetails withRideStatus:dictCurrentResponse[@"ride_status"]];
        }
        
    } else {
        
        NSArray * nearByCars = dictCurrentResponse[@"near_by_users"];
        if (![nearByCars isKindOfClass:[NSArray class]]) {
            nearByCars = @[];
        }
        
        if (nearByCars && nearByCars.count > 0){
            APP_DELEGATE.arrayNearByDrivers = [[NSMutableArray alloc]initWithArray:nearByCars];
            if (APP_DELEGATE.isUserModeDriver)
            {
                lblTotalUserLable.textColor = [UIColor blackColor];
                
                lblTotalUserLable.text = [NSString stringWithFormat:@"(%lu) People looking for ride", (unsigned long)APP_DELEGATE.arrayNearByDrivers.count];
            } else {
                lblTotalUserLable.textColor = [UIColor whiteColor];
                
                lblTotalUserLable.text = [NSString stringWithFormat:@"(%lu) People Offering ride", (unsigned long)APP_DELEGATE.arrayNearByDrivers.count];
            }
            isDisplayedNoUserBanner = false;
        } else {
            if (isDisplayedNoUserBanner){
                [Singleton showSnakBar:[NSString stringWithFormat:@"No any %@ available near by you", APP_DELEGATE.isUserModeDriver ? @"Passenger" : @"Driver"] multiline:false];
                isDisplayedNoUserBanner = true;
            }
        }
        
        if (APP_DELEGATE.isRideRunning){
            APP_DELEGATE.isRideRunning = false;
            APP_DELEGATE.runningRideStatus = RIDE_STATUS_NONE;
            APP_DELEGATE.recieverId = @"";
        }
        
        if (!viewPulstorContainer.isHidden){
            viewPulstorContainer.hidden = true;
            [timerPulstorWaiting invalidate];
            
            [self shouldShowRunningRideBannerForDriverUser:false];
            [self shouldShowRunningRideBannerForPassengerUser:false];
        }
        
        [self shouldManageNavigationButtons];
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_UPDATE_NEAR_BY_USERS object:nil];
    }
}

-(void)manageLocalViewsForDriverUser:(NSDictionary *)dictTripDetails withRideStatus:(NSString *)rideStatus {
    if (!rideStatus || [rideStatus isEqualToString:@""]){
        return;
    }
    [self managerTheAcceptedUserMarkerOnMapWithTripDetials:dictTripDetails];
    
    NSString * requestBy = dictTripDetails[@"request_by"];
    BOOL isIncommingRide = ([requestBy isEqualToString:@"1"] == APP_DELEGATE.isUserModeDriver) ? true : false;
    
    if ([rideStatus isEqualToString:@"0"]){
        //Incoming Ride Request
        if ([APP_DELEGATE.arrayHomeScreenStatus[0] integerValue] == 0) {
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"0"];
            if (isIncommingRide){
                [self didHideDriverInfoContainerWithAnimation];
                NSDictionary * dictTripInfo = @{
                                     @"ride_id":dictTripDetails[@"ride_id"],
                                     @"requestee_id":dictTripDetails[@"customer_user_id"],
                                     @"profile":dictTripDetails[@"customer_user_image"],
                                     @"first_name":dictTripDetails[@"customer_first_name"],
                                     @"last_name":dictTripDetails[@"customer_last_name"],
                                     @"age":dictTripDetails[@"customer_age"],
                                     @"gender":dictTripDetails[@"customer_gender"],
                                     @"occupation":dictTripDetails[@"customer_profession"],
                                     @"pikcup_address":dictTripDetails[@"pickup_address"],
                                     @"destination_address":dictTripDetails[@"drop_address"],
                                     @"request_by":dictTripDetails[@"request_by"],
                                     @"trip_cost":dictTripDetails[@"trip_totalcost"],
                                     @"customer_mobile_no":dictTripDetails[@"customer_mobile_no"],
                                     @"driver_mobile_no":dictTripDetails[@"driver_mobile_no"],
                                     @"pickup_title":dictTripDetails[@"pickup_title"],
                                     @"drop_title":dictTripDetails[@"drop_title"]
                                     };
                [self showIncomingRideAlertWithTripDetails:dictTripInfo];
            } else {
                //Requested By Me
                [self.view bringSubviewToFront:viewPulstorContainer];
                viewPulstorContainer.hidden = false;
                pulstor1Width.constant = 10;
                pulstor2Width.constant = 0;
                [timerPulstorWaiting invalidate];
                [self shouldStartPulstorAnimation];
            }
        }
    } else if ([rideStatus isEqualToString:@"1"]){
        //Ride is Accepted
        if ([APP_DELEGATE.arrayHomeScreenStatus[1] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"1"];
            if (viewPulstorContainer.isHidden == false && timerPulstorWaiting.isValid){
                viewPulstorContainer.hidden = true;
                [timerPulstorWaiting invalidate];
            }
            [self restoreGoogleMapWithMarkersAndRoutes];
        }
//        [self shouldShowRunningRideBannerForPassengerUser:true];
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.userLocation Destination:APP_DELEGATE.tripStartLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForPassengerUser:true];
        }];
    }else if ([rideStatus isEqualToString:@"2"]){
        //Ride is in Arrival State
        if ([APP_DELEGATE.arrayHomeScreenStatus[2] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"2"];
        }
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.userLocation Destination:APP_DELEGATE.tripStartLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForPassengerUser:true];
        }];
        
    }else if ([rideStatus isEqualToString:@"3"]){
        //Ride is in On-Going State
        if ([APP_DELEGATE.arrayHomeScreenStatus[3] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"3"];
            [self restoreGoogleMapWithMarkersAndRoutes];
//            [Singleton showSnakBar:@"Your Ride has Been Started" multiline:NO];
            
            isAlertModeConfirmation = false;
            isAlertModeExpire = false;
            isAlertModePlanRide = true;
            isAlertModeNoUser = false;
            
//            AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
//            obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
//            obj.delegate = self;
//            obj.alertTitles = @"Want to book next ride now ?";
//            obj.alertMessages = @"";
//            obj.yesTitle = @"Yes";
//            obj.noTitle = @"No";
//            [self presentViewController:obj animated:NO completion:nil];
        }
        
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.userLocation Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForPassengerUser:true];
        }];
    }else if ([rideStatus isEqualToString:@"4"]){
        //Ride is Canceled by Opposite User
        if ([APP_DELEGATE.arrayHomeScreenStatus[4] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"4"];
            
            isAlertModeExpire = false;
            isAlertModeConfirmation = true;
            isAlertModePlanRide = false;
            isAlertModeNoUser = false;
            [self alertControllerButtonTappedWithIndex:1];
        }
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.userLocation Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForPassengerUser:true];
        }];
    } else if ([rideStatus isEqualToString:@"5"]){
        //Ride is Finished by Opposite User
        if ([APP_DELEGATE.arrayHomeScreenStatus[5] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"5"];
            
            AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
            obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
            obj.delegate = self;
            obj.alertTitles = @"Ride Finished";
            obj.alertMessages = @"Your Ride has been Finished Now";
            obj.yesTitle = @"Okay";
            obj.noTitle = @"";
            [self presentViewController:obj animated:NO completion:nil];
            isAlertModeExpire = false;
            isAlertModeConfirmation = true;
            isAlertModePlanRide = false;
            isAlertModeNoUser = false;
        }
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.userLocation Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForPassengerUser:true];
        }];
    } else if ([rideStatus isEqualToString:@"6"]){
        //Ride is Declined by Opposite User
        if ([APP_DELEGATE.arrayHomeScreenStatus[6] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"6"];
            
            isAlertModeExpire = false;
            isAlertModeConfirmation = true;
            isAlertModePlanRide = false;
            isAlertModeNoUser = false;
            
            [self alertControllerButtonTappedWithIndex:1];
            //[Singleton showSnakBar:@"Your request has been declined by passenger" multiline:true];
        }
        canUpdateRoutes = true;
        [self drawRouteWithMapViewForOrigin:APP_DELEGATE.userLocation Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForPassengerUser:true];
        }];
    }
}

-(void)manageLocalViewsForPessangerUser:(NSDictionary *)dictTripDetails withRideStatus:(NSString *)rideStatus{
    if (!rideStatus || [rideStatus isEqualToString:@""]){
        return;
    }
    [self managerTheAcceptedUserMarkerOnMapWithTripDetials:dictTripDetails];
    NSString * requestBy = dictTripDetails[@"request_by"];
    BOOL isIncommingRide = ([requestBy isEqualToString:@"1"] == APP_DELEGATE.isUserModeDriver) ? true : false;
    
    if ([rideStatus isEqualToString:@"0"]){
        //Incoming Ride Request
        if ([APP_DELEGATE.arrayHomeScreenStatus[0] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"0"];
            if (isIncommingRide){
                [self didHideDriverInfoContainerWithAnimation];
                NSDictionary * dictTripInfo = @{
                                                @"ride_id":dictTripDetails[@"ride_id"],
                                                @"requestee_id":dictTripDetails[@"driver_id"],
                                                @"profile":dictTripDetails[@"driver_user_image"],
                                                @"first_name":dictTripDetails[@"driver_first_name"],
                                                @"last_name":dictTripDetails[@"driver_last_name"],
                                                @"age":dictTripDetails[@"driver_age"],
                                                @"gender":dictTripDetails[@"driver_gender"],
                                                @"occupation":dictTripDetails[@"driver_profession"],
                                                @"pikcup_address":dictTripDetails[@"pickup_address"],
                                                @"destination_address":dictTripDetails[@"drop_address"],
                                                @"request_by":dictTripDetails[@"request_by"],
                                                @"trip_cost":dictTripDetails[@"trip_totalcost"],
                                                @"customer_mobile_no":dictTripDetails[@"customer_mobile_no"],
                                                @"driver_mobile_no":dictTripDetails[@"driver_mobile_no"],
                                                @"vehicle_make":dictTripDetails[@"car_model"],
                                                @"pickup_title":dictTripDetails[@"pickup_title"],
                                                @"drop_title":dictTripDetails[@"drop_title"]
                                                };
                [self showIncomingRideAlertWithTripDetails:dictTripInfo];
            } else {
                [self.view bringSubviewToFront:viewPulstorContainer];
                viewPulstorContainer.hidden = false;
                pulstor1Width.constant = 10;
                pulstor2Width.constant = 0;
                [timerPulstorWaiting invalidate];
                [self shouldStartPulstorAnimation];
            }
        }
    } else if ([rideStatus isEqualToString:@"1"]){
        //Ride is Accepted
        if ([APP_DELEGATE.arrayHomeScreenStatus[1] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"1"];
            if (viewPulstorContainer.isHidden == false && timerPulstorWaiting.isValid){
                viewPulstorContainer.hidden = true;
                [timerPulstorWaiting invalidate];
            }
            [self restoreGoogleMapWithMarkersAndRoutes];
            [self adjustMapviewToViewAllMarkers];
            
            UILocalNotification *notification = [[UILocalNotification alloc] init];
            notification.fireDate = [[NSDate date] dateByAddingTimeInterval:3];
            notification.alertBody = @"Your ride is here.";
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        }
        
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        [self drawRouteWithMapViewForOrigin:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) Destination:APP_DELEGATE.tripStartLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForDriverUser:true];
        }];
    } else if ([rideStatus isEqualToString:@"2"]){
        //Ride is in Arrival State
        if ([APP_DELEGATE.arrayHomeScreenStatus[2] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"2"];
        }
    
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        [self drawRouteWithMapViewForOrigin:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) Destination:APP_DELEGATE.tripStartLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForDriverUser:true];
        }];
    }else if ([rideStatus isEqualToString:@"3"]){
        //Ride is in On-Going State
        if ([APP_DELEGATE.arrayHomeScreenStatus[3] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"3"];
            [self restoreGoogleMapWithMarkersAndRoutes];
//            [Singleton showSnakBar:@"Your Ride has Been Started" multiline:NO];
            [self adjustMapviewToViewAllMarkers];
            
            isAlertModeConfirmation = false;
            isAlertModeExpire = false;
            isAlertModePlanRide = true;
            isAlertModeNoUser = false;
            
//            AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
//            obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
//            obj.delegate = self;
//            obj.alertTitles = @"Want to book next ride now ?";
//            obj.alertMessages = @"";
//            obj.yesTitle = @"Yes";
//            obj.noTitle = @"No";
//            [self presentViewController:obj animated:NO completion:nil];
            
        }
        
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        [self drawRouteWithMapViewForOrigin:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForDriverUser:true];
        }];
    } else if ([rideStatus isEqualToString:@"4"]){
        //Ride is Canceled by Opposite User
        if ([APP_DELEGATE.arrayHomeScreenStatus[4] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"4"];
            [Singleton showSnakBar:@"Driver could not accept your request. Choose another buddy to carpool with." multiline:true];
        
            isAlertModeExpire = false;
            isAlertModeConfirmation = true;
            isAlertModePlanRide = false;
            isAlertModeNoUser = false;
            [self alertControllerButtonTappedWithIndex:1];
        }
        
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        [self drawRouteWithMapViewForOrigin:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForDriverUser:true];
        }];
    } else if ([rideStatus isEqualToString:@"5"]){
        //Ride is Finished by Opposite User
        if ([APP_DELEGATE.arrayHomeScreenStatus[5] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"5"];
            
            PaymentViewController *obj = (PaymentViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"PaymentViewController"];
            obj.dictTripDetails = APP_DELEGATE.dictTripDetails.mutableCopy;
            obj.delegate = self;
            [APP_DELEGATE.navigationController presentViewController:obj animated:true completion:nil];
        }
        
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        [self drawRouteWithMapViewForOrigin:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForDriverUser:true];
        }];
    } else if ([rideStatus isEqualToString:@"6"]){
        //Ride is Declined by Opposite User
        if ([APP_DELEGATE.arrayHomeScreenStatus[6] integerValue] == 0){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@"6"];
            
            isAlertModeExpire = false;
            isAlertModeConfirmation = true;
            isAlertModePlanRide = false;
            isAlertModeNoUser = false;
            [self alertControllerButtonTappedWithIndex:1];
            //[Singleton showSnakBar:@"Your request has been declined by passenger" multiline:false];
        }
        
        double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
        double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
        [self drawRouteWithMapViewForOrigin:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) Destination:APP_DELEGATE.tripEndLocation isDotedLine:!NAVIGATION_MODE withCallback:^(BOOL success) {
            [self shouldShowRunningRideBannerForDriverUser:true];
        }];
    }
}

-(void)updateLocalTripInfoWithTripDetails:(NSDictionary * )dictTripDetails withRideStatus:(NSString *)rideStatus{
    BOOL canRestoreMapView = false;
    
    double pickupLattitude = [dictTripDetails[@"pickup_lat"] doubleValue];
    double pickupLongitude = [dictTripDetails[@"pickup_log"] doubleValue];
    
    double destinationLattitude = [dictTripDetails[@"drop_lat"] doubleValue];
    double destinationLongitude = [dictTripDetails[@"drop_log"] doubleValue];
    
    if (DEBUG_MODE){
        NSLog(@"Current Pickup Lattitude : %f",APP_DELEGATE.tripStartLocation.latitude);
        NSLog(@"Current Pickup Longitude : %f",APP_DELEGATE.tripStartLocation.longitude);
        
        NSLog(@"Changable Pickup Lattitude : %f",pickupLattitude);
        NSLog(@"Changable Pickup Longitude : %f",pickupLongitude);
        
        NSLog(@"Current Dropoff Lattitude : %f",APP_DELEGATE.tripEndLocation.latitude);
        NSLog(@"Current Dropoff Longitude : %f",APP_DELEGATE.tripEndLocation.longitude);
        
        NSLog(@"Changable Dropoff Lattitude : %f",destinationLattitude);
        NSLog(@"Changable Dropoff Longitude : %f",destinationLongitude);
    }
    
    
    MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
    MKMapPoint point2 = MKMapPointForCoordinate(CLLocationCoordinate2DMake(pickupLattitude, pickupLongitude));
    float distance = MKMetersBetweenMapPoints(point1, point2);
    if ((APP_DELEGATE.tripStartLocation.latitude == 00.0000 && APP_DELEGATE.tripStartLocation.longitude == 00.0000) || distance > 2.0) {
        APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(pickupLattitude, pickupLongitude);
        APP_DELEGATE.dictPickupDetails = @{
                                           @"lattitude":dictTripDetails[@"pickup_lat"],
                                           @"longitude":dictTripDetails[@"pickup_log"],
                                           @"title":dictTripDetails[@"pickup_title"],
                                           @"fullAddress":dictTripDetails[@"pickup_address"]
                                           };
        lblPickupLocation.text = dictTripDetails[@"pickup_address"];
        canRestoreMapView = true;
    }
    
    MKMapPoint point3 = MKMapPointForCoordinate(APP_DELEGATE.tripEndLocation);
    MKMapPoint point4 = MKMapPointForCoordinate(CLLocationCoordinate2DMake(destinationLattitude, destinationLongitude));
    float distanc2 = MKMetersBetweenMapPoints(point3, point4);
    if (distanc2 > 2.0) {
        APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(destinationLattitude, destinationLongitude);
        APP_DELEGATE.dictDropoffDetails = @{
                                           @"lattitude":dictTripDetails[@"drop_lat"],
                                           @"longitude":dictTripDetails[@"drop_log"],
                                           @"title":dictTripDetails[@"drop_title"],
                                           @"fullAddress":dictTripDetails[@"drop_address"]
                                           };
        lblDestinationLocation.text = dictTripDetails[@"drop_address"];
        canRestoreMapView = true;
    }
    
    APP_DELEGATE.isRideRunning = true;
    isInitialMarkerChanged = true;
    APP_DELEGATE.runningRideId = dictTripDetails[@"ride_id"];
    
    APP_DELEGATE.recieverId = APP_DELEGATE.isUserModeDriver ? dictTripDetails[@"customer_user_id"] : dictTripDetails[@"driver_id"];
    if (DEBUG_MODE){
        NSLog(@"reciever user : %@", APP_DELEGATE.recieverId);
    }
    
    if ([rideStatus isEqualToString:@"0"]){
        //Incoming / Send Ride Request
        APP_DELEGATE.runningRideStatus = RIDE_STATUS_REQUEST;
        
        btnCurrentTripDriverCancel.hidden = false;
        if (bottomSpaceCurentTripDriverCancelButton.constant < 20){
            bottomSpaceCurentTripDriverCancelButton.constant = 20;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        btnCurrentTripPessabgerSmallCancel.hidden = false;
        btnCurrentTripPessangerSmallConfirm.hidden = false;
        btnCurrentTripPessangerBigCancel.hidden = true;
        [self shouldManageNavigationButtons];
    } else if ([rideStatus isEqualToString:@"1"]){
        //Accepted Ride Request
        APP_DELEGATE.runningRideStatus = RIDE_STATUS_ACCEPTED;
        
        btnCurrentTripDriverCancel.hidden = false;
        if (bottomSpaceCurentTripDriverCancelButton.constant < 20){
            bottomSpaceCurentTripDriverCancelButton.constant = 20;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        
        btnCurrentTripPessabgerSmallCancel.hidden = false;
        btnCurrentTripPessangerSmallConfirm.hidden = false;
        btnCurrentTripPessangerBigCancel.hidden = true;
        [self shouldManageNavigationButtons];
    } else if ([rideStatus isEqualToString:@"2"]){
        //Arrived at Point
        APP_DELEGATE.runningRideStatus = RIDE_STATUS_ARRIVAL;
        
        btnCurrentTripDriverCancel.hidden = false;
        if (bottomSpaceCurentTripDriverCancelButton.constant < 20){
            bottomSpaceCurentTripDriverCancelButton.constant = 20;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        
        btnCurrentTripPessabgerSmallCancel.hidden = false;
        btnCurrentTripPessangerSmallConfirm.hidden = false;
        btnCurrentTripPessangerBigCancel.hidden = true;
        [self shouldManageNavigationButtons];
    } else if ([rideStatus isEqualToString:@"3"]){
        //Start Ride
        APP_DELEGATE.runningRideStatus = RIDE_STATUS_ON_GOING;
        
        btnCurrentTripDriverCancel.hidden = true;
        if (bottomSpaceCurentTripDriverCancelButton.constant > -40){
            bottomSpaceCurentTripDriverCancelButton.constant = -40;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        btnCurrentTripPessabgerSmallCancel.hidden = true;
        btnCurrentTripPessangerSmallConfirm.hidden = true;
        btnCurrentTripPessangerBigCancel.hidden = false;
    } else if ([rideStatus isEqualToString:@"4"]){
        //Cancel Ride
        APP_DELEGATE.runningRideStatus = RIDE_STATUS_CANCELED;
        
        btnCurrentTripDriverCancel.hidden = true;
        bottomSpaceCurentTripDriverCancelButton.constant = - 40;
        btnCurrentTripPessabgerSmallCancel.hidden = true;
        btnCurrentTripPessangerSmallConfirm.hidden = true;
        btnCurrentTripPessangerBigCancel.hidden = true;
    } else if ([rideStatus isEqualToString:@"5"]){
        //Finished Ride
        APP_DELEGATE.runningRideStatus = RIDE_STATUS_FINISHED;
        
        btnCurrentTripDriverCancel.hidden = true;
        if (bottomSpaceCurentTripDriverCancelButton.constant > -40){
            bottomSpaceCurentTripDriverCancelButton.constant = -40;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        btnCurrentTripPessabgerSmallCancel.hidden = true;
        btnCurrentTripPessangerSmallConfirm.hidden = true;
        btnCurrentTripPessangerBigCancel.hidden = true;
    } else if ([rideStatus isEqualToString:@"6"]){
        //Declined
        APP_DELEGATE.runningRideStatus = RIDE_STATUS_DECLINED;
        
        btnCurrentTripDriverCancel.hidden = true;
        if (bottomSpaceCurentTripDriverCancelButton.constant > -40){
            bottomSpaceCurentTripDriverCancelButton.constant = -40;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        btnCurrentTripPessabgerSmallCancel.hidden = true;
        btnCurrentTripPessangerSmallConfirm.hidden = true;
        btnCurrentTripPessangerBigCancel.hidden = true;
    } else {
        //Found Bug
    }
    
    if (canRestoreMapView) {
        [self restoreGoogleMapWithMarkersAndRoutes];
    }
    
    [self shouldManageNavigationButtons];
    [self showLocationDetailsContainerView];
    [self didHideDriverInfoContainerWithAnimation];
}

-(void)managerTheAcceptedUserMarkerOnMapWithTripDetials:(NSDictionary *)dictTripDetails {
    if (APP_DELEGATE.runningRideStatus != RIDE_STATUS_REQUEST){
        NSArray * arryNearBY;
        if (APP_DELEGATE.isUserModeDriver){
            NSString * lattitude;
            NSString * longitude;
            
            if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
                lattitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.latitude];
                longitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.longitude];
            } else {
                if (dictTripDetails[@"temp_location_lattitude"] && dictTripDetails[@"temp_location_longitude"]){
                    lattitude = dictTripDetails[@"passanger_latitude"];
                    longitude = dictTripDetails[@"passanger_longitude"];
                } else {
                    lattitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.latitude];
                    longitude = [NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.longitude];
                }
            }
            
            arryNearBY = @[@{
                               @"ETA":dictTripDetails[@"ETA"],
                               @"address":dictTripDetails[@"pickup_address"],
                               @"age":dictTripDetails[@"customer_age"],
                               @"allow_rider":@"",
                               @"distance":@"",
                               @"first_name":dictTripDetails[@"customer_first_name"],
                               @"gender":dictTripDetails[@"customer_gender"],
                               @"is_favourite":@"0",
                               @"last_name":dictTripDetails[@"customer_last_name"],
                               @"latitude":lattitude,
                               @"longitude":longitude,
                               @"mobile_no":dictTripDetails[@"customer_mobile_no"],
                               @"profession":dictTripDetails[@"customer_profession"],
                               @"riding_preferences":@"0",
                               @"total_given_ride":@"0",
                               @"total_taken_ride":@"0",
                               @"user_id":dictTripDetails[@"customer_user_id"],
                               @"user_image":dictTripDetails[@"customer_user_image"],
                               }];
        } else {
            if (dictTripDetails[@"temp_location_lattitude"] && dictTripDetails[@"temp_location_longitude"]){
                arryNearBY = @[@{
                                   @"ETA":dictTripDetails[@"ETA"],
                                   @"address":dictTripDetails[@"pickup_address"],
                                   @"age":dictTripDetails[@"driver_age"],
                                   @"allow_rider":@"2",
                                   @"car_color":@"",
                                   @"car_id":dictTripDetails[@"car_id"],
                                   @"car_model":dictTripDetails[@"car_model"],
                                   @"car_plat_no":dictTripDetails[@"car_plat_no"],
                                   @"car_year":dictTripDetails[@"car_year"],
                                   @"distance":@"",
                                   @"first_name":dictTripDetails[@"driver_first_name"],
                                   @"gender":dictTripDetails[@"driver_gender"],
                                   @"is_favourite":@"0",
                                   @"last_name":dictTripDetails[@"driver_last_name"],
                                   @"latitude":dictTripDetails[@"temp_location_lattitude"],
                                   @"longitude":dictTripDetails[@"temp_location_longitude"],
                                   @"mobile_no":dictTripDetails[@"driver_mobile_no"],
                                   @"profession":dictTripDetails[@"driver_profession"],
                                   @"riding_preferences":@"0",
                                   @"total_given_ride":@"0",
                                   @"total_taken_ride":@"0",
                                   @"user_id":dictTripDetails[@"driver_id"],
                                   @"user_image":dictTripDetails[@"driver_user_image"],
                                   }];
            }
        }
        
        
        if (arryNearBY && arryNearBY.count > 0){
            APP_DELEGATE.arrayNearByDrivers = [[NSMutableArray alloc]initWithArray:arryNearBY];
        }
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_UPDATE_NEAR_BY_USERS object:nil];
    }
}

#pragma RunningRideBannerCorner
-(void)shouldShowRunningRideBannerForDriverUser:(BOOL)shouldShow {
    //Call when Passenger Logged In
    
    if (shouldShow && APP_DELEGATE.isRideRunning && !APP_DELEGATE.isUserModeDriver && APP_DELEGATE.runningRideStatus != RIDE_STATUS_NONE && APP_DELEGATE.runningRideStatus != RIDE_STATUS_REQUEST && APP_DELEGATE.runningRideStatus != RIDE_STATUS_ON_GOING){
        
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED) {
            double locationLattitude = [APP_DELEGATE.dictTripDetails[@"temp_location_lattitude"] doubleValue];
            double locationLongitude = [APP_DELEGATE.dictTripDetails[@"temp_location_longitude"] doubleValue];
            CLLocationCoordinate2D driverLocation = CLLocationCoordinate2DMake(locationLattitude, locationLongitude);
            MKMapPoint point1 = MKMapPointForCoordinate(driverLocation);
            MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
            double distanceToEnd = MKMetersBetweenMapPoints(point1, point2);
            if (distanceToEnd < NAVI_STEP_DISTANCE_RATE) {
                NSString * otpCode = APP_DELEGATE.dictTripDetails[@"confirmation_number"];
                if (otpCode && ![otpCode isEqualToString:@""]){
                    NSString * otpCode = APP_DELEGATE.dictTripDetails[@"confirmation_number"];
                    lnlConfirmationNumber.text = [NSString stringWithFormat:@"Confirmation no. : %@",otpCode];
                    lnlConfirmationNumber.font = [UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE + 2];
                    lnlConfirmationNumber.textColor = [UIColor colorWithRed:1.0/255.0 green:232/255.0 blue:9/255.0 alpha:1];
                } else {
                    lnlConfirmationNumber.text = @"";
                }
            } else {
                lnlConfirmationNumber.text = @"";
            }
        } else {
            lnlConfirmationNumber.text = @"";
        }
        
        lblCurrentTripDriverName.font = [UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE];
        lblCurrentTripDriverCarIno.font = [UIFont fontWithName:AppFont_Light size:TEXTFIELD_FONTS_SIZE - 4];
        lblCurrentTripDriverPlate.font = [UIFont fontWithName:AppFont_Light size:TEXTFIELD_FONTS_SIZE -4];
        lblCurrentTripETA.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 2];
        
        
        lblCurrentTripDriverName.text = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.dictTripDetails[@"driver_first_name"], APP_DELEGATE.dictTripDetails[@"driver_last_name"]];
        lblCurrentTripDriverCarIno.text = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.dictTripDetails[@"car_year"], APP_DELEGATE.dictTripDetails[@"car_model"]];
        
        if (NAVIGATION_MODE) {
            lblCurrentTripETA.text = @"";
            viewNavCurrentTripDriverETA.hidden = false;
            lblNavCurrentTripDriverETA.hidden = false;
            
//            NSString * mileValue = [NSString stringWithFormat:@"%0.1f mile", [APP_DELEGATE.tripDistance doubleValue] * PER_METRES_TO_MULTIPLY];
            if (APP_DELEGATE.tripFormatedDuration){
                lblNavCurrentTripDriverETA.text = [NSString stringWithFormat:@"ETA : %@", APP_DELEGATE.tripFormatedDuration];
            } else {
                lblNavCurrentTripDriverETA.text = @"ETA : Loading...";
            }
        } else {
            lblNavCurrentTripDriverETA.text = @"";
            viewNavCurrentTripDriverETA.hidden = true;
            lblNavCurrentTripDriverETA.hidden = true;
            if (APP_DELEGATE.tripFormatedDuration){
                lblCurrentTripETA.text = [NSString stringWithFormat:@"ETA : %@", APP_DELEGATE.tripFormatedDuration];
            } else {
                lblCurrentTripETA.text = @"ETA : Loading...";
            }
        }
        
//        
//        lblCurrentTripETA.text = [NSString stringWithFormat:@"ETA : %@", APP_DELEGATE.tripFormatedDuration];
        
//        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
//            double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"driver_latitude"] doubleValue];
//            double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"driver_longitude"] doubleValue];
//            [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) destination:APP_DELEGATE.tripStartLocation withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
//                lblCurrentTripETA.text = [NSString stringWithFormat:@"ETA : %@", formattedETA];
//            }];
//        } else {
//            double driverLAttitude = [APP_DELEGATE.dictTripDetails[@"driver_latitude"] doubleValue];
//            double driverLOngitude = [APP_DELEGATE.dictTripDetails[@"driver_longitude"] doubleValue];
//            [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:CLLocationCoordinate2DMake(driverLAttitude, driverLOngitude) destination:APP_DELEGATE.tripEndLocation withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
//                lblCurrentTripETA.text = [NSString stringWithFormat:@"ETA : %@", formattedETA];
//            }];
//        }
        
        lblCurrentTripDriverPlate.text = [NSString stringWithFormat:@"Licence plate number : %@",APP_DELEGATE.dictTripDetails[@"car_plat_no"]];

         UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
        
//        UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//        NSString * gender = APP_DELEGATE.dictTripDetails[@"driver_gender"];
//        if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//            dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//        }
        
        if (imgCurrentTripDriverProfile.tag == 0){
            imgCurrentTripDriverProfile.tag = 1;
            UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgCurrentTripDriverProfile.frame.size.width, imgCurrentTripDriverProfile.frame.size.height)];
            
            activity_indicator.tag = 11111;
            [[imgCurrentTripDriverProfile viewWithTag:1111] removeFromSuperview];
            
            [imgCurrentTripDriverProfile addSubview:activity_indicator];
            [activity_indicator startAnimating];
            [activity_indicator setColor:[UIColor blackColor]];
            
            imgCurrentTripDriverProfile.layer.cornerRadius = imgCurrentTripDriverProfile.frame.size.height / 2;
            imgCurrentTripDriverProfile.layer.masksToBounds = true;
            imgCurrentTripDriverProfile.clipsToBounds = true;
            
            NSString *strUrl = APP_DELEGATE.dictTripDetails[@"driver_user_image"];
            strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            [imgCurrentTripDriverProfile sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [activity_indicator stopAnimating];
                [activity_indicator removeFromSuperview];
                [[imgCurrentTripDriverProfile viewWithTag:1111] removeFromSuperview];
                imgCurrentTripDriverProfile.tag = 0;
            }];
        }
        
        if (bottomSpaceCurrentTripDriver.constant < 0){
            bottomSpaceCurrentTripDriver.constant = 0;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    } else {
        if (bottomSpaceCurrentTripDriver.constant > -50 - (viewCurrentTripDriver.frame.size.height)){
            bottomSpaceCurrentTripDriver.constant = - 50 - (viewCurrentTripDriver.frame.size.height);
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
    }
}

- (IBAction)passengerCancelButtonTapped:(id)sender {
    //Will Cancel The Ride by Passenger
    AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
    obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
    obj.delegate = self;
    obj.alertTitles = @"Are you sure ?";
    obj.alertMessages = @"";
    obj.yesTitle = @"YES";
    obj.noTitle = @"NO";
    [self presentViewController:obj animated:NO completion:nil];
    isAlertModeExpire = false;
    isAlertModeConfirmation = false;
    isAlertModePlanRide = false;
    isAlertModeNoUser = false;
}

-(void)shouldShowRunningRideBannerForPassengerUser:(BOOL)shouldShow {
    if (shouldShow && APP_DELEGATE.isRideRunning && APP_DELEGATE.isUserModeDriver && APP_DELEGATE.runningRideStatus != RIDE_STATUS_NONE && APP_DELEGATE.runningRideStatus != RIDE_STATUS_REQUEST){
        
        lblCurrentTripPessangerName.text = [NSString stringWithFormat:@"%@ %@", APP_DELEGATE.dictTripDetails[@"customer_first_name"], APP_DELEGATE.dictTripDetails[@"customer_last_name"]];
        if (NAVIGATION_MODE) {
            lblCurrentTripPessangerETA.text = @"";
            viewBennerETABG.hidden = false;
            lblBannerETA.hidden = false;
            
            NSString * mileValue = [NSString stringWithFormat:@"%0.1f mile", [APP_DELEGATE.tripDistance doubleValue] * PER_METRES_TO_MULTIPLY];
            
            if (APP_DELEGATE.tripFormatedDuration && mileValue){
                lblBannerETA.text = [NSString stringWithFormat:@"%@ | %@", APP_DELEGATE.tripFormatedDuration, mileValue];
            } else {
                lblBannerETA.text = @"ETA : Loading...";
            }
        } else {
            lblBannerETA.text = @"";
            viewBennerETABG.hidden = true;
            lblBannerETA.hidden = true;
            
            if (APP_DELEGATE.tripFormatedDuration){
                lblCurrentTripPessangerETA.text = [NSString stringWithFormat:@"ETA : %@", APP_DELEGATE.tripFormatedDuration];
            } else {
                lblCurrentTripPessangerETA.text = @"ETA : Loading...";
            }
        }
        
//        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
//            [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:APP_DELEGATE.userLocation destination:APP_DELEGATE.tripStartLocation withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
//                if (NAVIGATION_MODE) {
//                    NSString * mileValue = [NSString stringWithFormat:@"%0.1f mile", distanceKM * PER_METRES_TO_MULTIPLY];
//                    lblBannerETA.text = [NSString stringWithFormat:@"%@ | %@", formattedETA, mileValue];
//                } else {
//                    lblCurrentTripPessangerETA.text = [NSString stringWithFormat:@"ETA : %@", formattedETA];
//                }
//            }];
//        } else {
//            [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:APP_DELEGATE.userLocation destination:APP_DELEGATE.tripEndLocation withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
//                if (NAVIGATION_MODE) {
//                    NSString * mileValue = [NSString stringWithFormat:@"%0.1f mile", distanceKM * PER_METRES_TO_MULTIPLY];
//                    lblBannerETA.text = [NSString stringWithFormat:@"%@ | %@", formattedETA, mileValue];
//                } else {
//                    lblCurrentTripPessangerETA.text = [NSString stringWithFormat:@"ETA : %@", formattedETA];
//                }
//            }];
//        }

         UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
        
//        UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//        NSString * gender = APP_DELEGATE.dictTripDetails[@"customer_gender"];
//        if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//            dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//        }
        
        imgCurrentTripPessengerPofile.layer.cornerRadius = imgCurrentTripPessengerPofile.frame.size.height / 2;
        imgCurrentTripPessengerPofile.layer.masksToBounds = true;
        imgCurrentTripPessengerPofile.clipsToBounds = true;
        
        if (imgCurrentTripPessengerPofile.tag == 0){
            imgCurrentTripPessengerPofile.tag = 1;
            UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgCurrentTripPessengerPofile.frame.size.width, imgCurrentTripPessengerPofile.frame.size.height)];
            
            activity_indicator.tag = 11111;
            [[imgCurrentTripPessengerPofile viewWithTag:1111] removeFromSuperview];
            
            [imgCurrentTripPessengerPofile addSubview:activity_indicator];
            [activity_indicator startAnimating];
            [activity_indicator setColor:[UIColor blackColor]];
            
            NSString *strUrl = APP_DELEGATE.dictTripDetails[@"customer_user_image"];
            strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
            
            
            [imgCurrentTripPessengerPofile sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                [activity_indicator stopAnimating];
                [activity_indicator removeFromSuperview];
                [[imgCurrentTripPessengerPofile viewWithTag:1111] removeFromSuperview];
                imgCurrentTripPessengerPofile.tag = 0;
            }];
        }
        
        if (bottomSpaceCurrentTripPessanger.constant < 0){
            bottomSpaceCurrentTripPessanger.constant = 0;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        if (APP_DELEGATE.isUserModeDriver && canCallAPIForRoute) {
            if (APP_DELEGATE.isRideRunning && NAVIGATION_MODE){
                [self shouldShowNavigationForMapWithIsInitialMode:false];
            }
        }
    } else {
        if (bottomSpaceCurrentTripPessanger.constant > -50 - (viewCurrentTripPessanger.frame.size.height)){
            bottomSpaceCurrentTripPessanger.constant = - 50 - (viewCurrentTripPessanger.frame.size.height);
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        
        if (APP_DELEGATE.isUserModeDriver && !APP_DELEGATE.isRideRunning){
            [self shouldHideNavigationForMap];
        }
    }
}
- (IBAction)driverCancelButton:(id)sender {
    //Will Cancel The Ride by Passenger
    AlertViewController *obj = (AlertViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"AlertViewController"];
    obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
    obj.delegate = self;
    obj.alertTitles = @"Are you sure ?";
    obj.alertMessages = @"";
    obj.yesTitle = @"YES";
    obj.noTitle = @"NO";
    [self presentViewController:obj animated:NO completion:nil];
    isAlertModeExpire = false;
    isAlertModeConfirmation = false;
    isAlertModePlanRide = false;
    isAlertModeNoUser = false;
}

- (IBAction)driverConfirmationButton:(id)sender {
    //Request a Confirmation Number
    if (!APP_DELEGATE.dictTripDetails[@"confirmation_number"] || [APP_DELEGATE.dictTripDetails[@"confirmation_number"] isEqualToString:@""]){
        [self updateCustomerCurrentLocation];
    } else {
        StartRide *obj = (StartRide *)[[self storyboard]instantiateViewControllerWithIdentifier:@"StartRide"];
        obj.confirmationNumber = APP_DELEGATE.dictTripDetails[@"confirmation_number"];
        obj.delegate = self;
        [self presentViewController:obj animated:YES completion:nil];
    }
}

- (void)getRideConfirmationAfterSuccess {
    
    NSString * rideId = APP_DELEGATE.runningRideId;
    NSString * driver_id;
    NSString * pessangerId;
    
    if (APP_DELEGATE.isUserModeDriver) {
        pessangerId = APP_DELEGATE.recieverId;
        driver_id = APP_DELEGATE.loggedUserId;
    } else {
        pessangerId = APP_DELEGATE.loggedUserId;
        driver_id = APP_DELEGATE.recieverId;
    }
    
    NSString * rideStatus = @"3";
    
    START_HUD
    [APP_DELEGATE.apiManager updateRideStatusWithRideId:rideId RideStatus:rideStatus PessangerId:pessangerId DriverId:driver_id CancelReasonId:@"" TotalKm:@"" withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        STOP_HUD
        if (success){
            previusSpeech = @"";
            canCallAPIForRoute = true;
            APP_DELEGATE.runningRideStatus = RIDE_STATUS_ON_GOING;
            btnCurrentTripPessabgerSmallCancel.hidden = btnCurrentTripPessangerSmallConfirm.hidden = true;
            btnCurrentTripPessangerSmallConfirm.hidden = true;
            [self shouldManageNavigationButtons];
            
            btnCurrentTripPessangerBigCancel.hidden = false;
            [self restoreGoogleMapWithMarkersAndRoutes];
            
            APP_DELEGATE.arrayActualRoute = @[@{
                                                  @"lattitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.latitude],
                                                  @"longitude":[NSString stringWithFormat:@"%f",APP_DELEGATE.userLocation.longitude]
                                                  }];
            [self adjustMapviewToViewAllMarkers];
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessange);
        }
    }];
}

- (IBAction)driverEndRideButton:(id)sender {
    canCallAPIForRoute = true;
    NSString * rideId = APP_DELEGATE.runningRideId;
    NSString * driver_id;
    NSString * pessangerId;
    
    if (APP_DELEGATE.isUserModeDriver) {
        pessangerId = APP_DELEGATE.recieverId;
        driver_id = APP_DELEGATE.loggedUserId;
    } else {
        pessangerId = APP_DELEGATE.loggedUserId;
        driver_id = APP_DELEGATE.recieverId;
    }
    
//    double usableLattitude = APP_DELEGATE.userLocation.latitude != 0.00000 ? APP_DELEGATE.userLocation.latitude : APP_DELEGATE.tripEndLocation.latitude;
//    double usableLongitude = APP_DELEGATE.userLocation.longitude != 0.00000 ? APP_DELEGATE.userLocation.longitude : APP_DELEGATE.tripEndLocation.longitude;
    
    
    NSString * actualDistance = [APP_DELEGATE findActualRouteDistance];
    
    START_HUD
    NSString * rideStatus = @"5";
    
    [APP_DELEGATE.apiManager updateRideStatusWithRideId:rideId RideStatus:rideStatus PessangerId:pessangerId DriverId:driver_id CancelReasonId:@"" TotalKm:actualDistance withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        STOP_HUD
        if (success){
            FeedbackViewController *obj = (FeedbackViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"FeedbackViewController"];
            obj.dictTripDetails = APP_DELEGATE.dictTripDetails;
            [APP_DELEGATE.navigationController presentViewController:obj animated:true completion:^{
                previusSpeech = @"";
                [APP_DELEGATE clearPreviousRideDetailsFromTheAPP];
                [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
                
                APP_DELEGATE.tripStartLocation = APP_DELEGATE.userLocation;
                
                [self restoreGoogleMapWithMarkersAndRoutes];
                [self shouldShowRunningRideBannerForDriverUser:false];
                [self shouldShowRunningRideBannerForPassengerUser:false];
                [self updateCustomerCurrentLocation];
                
                lblPickupLocation.text = @"Pick Up Location";
                lblDestinationLocation.text = @"Drop Off Location";
            }];
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessange);
        }
    }];
}

#pragma mark - IncomingRideAlertController
-(void)showIncomingRideAlertWithTripDetails:(NSDictionary *)tripDetails{
    APP_DELEGATE.sendRequestDate = [NSDate date];
    START_HUD
    [APP_DELEGATE.apiManager findAllRoutesBeteweenTwoPlaces:APP_DELEGATE.tripStartLocation destination:APP_DELEGATE.userLocation withCallBack:^(int etaSeconds, float distanceKM, NSString *formattedETA, NSString *formatedDistnace, NSString *polylinePoints, NSArray * allRoutes) {
        
        NSMutableDictionary * tripData = tripDetails.mutableCopy;
        [tripData setObject:formattedETA forKey:@"ride_eta"];
        [tripData setObject:formatedDistnace forKey:@"ride_distance"];
        
        PassengerRequestViewController *obj = (PassengerRequestViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"PassengerRequestViewController"];
        obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
        obj.dictTripInfo = tripData;
        obj.delegate = self;
        obj.canPerformAciton = true;
        
        [[APP_DELEGATE.navigationController.viewControllers lastObject] dismissViewControllerAnimated:false completion:nil];
        
        [[APP_DELEGATE.navigationController.viewControllers lastObject] presentViewController:obj animated:NO completion:nil];
        STOP_HUD
    }];
}

-(void)didAcceptTheRideWithUsableInfo:(NSDictionary *)dictUsableInfo{
    START_HUD
    NSString * rideId = dictUsableInfo[@"ride_id"];
    NSString * driver_id;
    NSString * pessangerId;
    if (APP_DELEGATE.isUserModeDriver) {
        pessangerId = APP_DELEGATE.recieverId;
        driver_id = APP_DELEGATE.loggedUserId;
    } else {
        pessangerId = APP_DELEGATE.loggedUserId;
        driver_id = APP_DELEGATE.recieverId;
    }
    
    NSString * rideStatus = @"1";
    [APP_DELEGATE.apiManager updateRideStatusWithRideId:rideId RideStatus:rideStatus PessangerId:pessangerId DriverId:driver_id CancelReasonId:@"" TotalKm:@"" withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        STOP_HUD
        if (success){
            canCallAPIForRoute = true;
            previusSpeech = @"";
            APP_DELEGATE.runningRideStatus = RIDE_STATUS_ACCEPTED;
            if (APP_DELEGATE.isUserModeDriver){
                [self shouldShowRunningRideBannerForPassengerUser:true];
            } else {
                [self shouldShowRunningRideBannerForDriverUser:true];
            }
            [self adjustMapviewToViewAllMarkers];
            [self restoreGoogleMapWithMarkersAndRoutes];
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessange);
        }
    }];
}

-(void)didRejectTheRideWithUsableInfo:(NSDictionary *)dictUsableInfo{
    START_HUD
    NSString * rideId = dictUsableInfo[@"ride_id"];
    NSString * driver_id;
    NSString * pessangerId;
    if (APP_DELEGATE.isUserModeDriver) {
        pessangerId = APP_DELEGATE.recieverId;
        driver_id = APP_DELEGATE.loggedUserId;
    } else {
        pessangerId = APP_DELEGATE.loggedUserId;
        driver_id = APP_DELEGATE.recieverId;
    }
    
    NSString * rideStatus = @"6";
    [APP_DELEGATE.apiManager updateRideStatusWithRideId:rideId RideStatus:rideStatus PessangerId:pessangerId DriverId:driver_id CancelReasonId:@"" TotalKm:@"" withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        STOP_HUD
        if (success){
            [APP_DELEGATE clearPreviousRideDetailsFromTheAPP];
            [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
            [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartUnfilled"] forState:UIControlStateNormal];
            
            [self restoreGoogleMapWithMarkersAndRoutes];
            [self shouldShowRunningRideBannerForDriverUser:false];
            [self shouldShowRunningRideBannerForPassengerUser:false];
            [self updateCustomerCurrentLocation];
            lblPickupLocation.text = @"Pick Up Location";
            lblDestinationLocation.text = @"Drop Off Location";
        } else {
            [Singleton showSnakBar:serverMessange multiline:true];
        }
    }];
}

-(void)didCloseTheAlertWithoutAction:(NSDictionary *)dictUsableInfo {
    [APP_DELEGATE updateHomeStatusArrayWithIndex:@""];
    //Will Show The Badge Number
}

#pragma mark - FavoruiteSoptsDelegate
-(void)shouldSelectFavoriteLocation:(NSDictionary *)favoriteLocation{
    double lattitude = [favoriteLocation[@"lattitude"] doubleValue];
    double longitude = [favoriteLocation[@"longitude"] doubleValue];

    
    NSDictionary * dictLocationDetails = @{
                                           @"lattitude":favoriteLocation[@"lattitude"],
                                           @"longitude":favoriteLocation[@"longitude"],
                                           @"title":favoriteLocation[@"address_name"],
                                           @"fullAddress":favoriteLocation[@"address"]
                                           };
    
    if (isModePickBeginLocation){
        APP_DELEGATE.tripStartLocation = CLLocationCoordinate2DMake(lattitude, longitude);
        [btnPickupFavorite setImage:[UIImage imageNamed:@"PHHeartFilled"] forState:UIControlStateNormal];
        APP_DELEGATE.dictPickupDetails = dictLocationDetails;
    } else {
        APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(lattitude, longitude);
        [btnDestinationFavorite setImage:[UIImage imageNamed:@"PHHeartFilled"] forState:UIControlStateNormal];
        
        APP_DELEGATE.dictDropoffDetails = dictLocationDetails;
    }
    isInitialMarkerChanged = true;
    [self restoreGoogleMapWithMarkersAndRoutes];
}

#pragma mark - PulstorAnimation
-(void)shouldStartPulstorAnimation {
    if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_REQUEST){
        totalTimeToWait = APP_DELEGATE.globalTimeout;
        
        remainigTime = totalTimeToWait - [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate];
        if (totalTimeToWait <60)
        {
            lableTimeDetails.text = [NSString stringWithFormat:@"%02d Sec",totalTimeToWait];
        }
        else
        {
            int totalSeconds = totalTimeToWait % 60;
            int totalMinutes = (totalTimeToWait / 60) % 60;
            lableTimeDetails.text = [NSString stringWithFormat:@"%02d:%02d Min", totalMinutes, totalSeconds];
        }
        
        timerPulstorWaiting = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerWillTicked) userInfo:nil repeats:true];
        pulstor1Width.constant = 10;
        pulstor2Width.constant = 10;
        lableRemainingTime.hidden = true;
        lableTimeDetails.hidden = false;
        
        
        
        
        
        [self animateToBigView];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self animateToBigView2];
        });
    }
}

-(void)timerWillTicked{
    if (totalTimeToWait <= [[NSDate date] timeIntervalSinceDate:APP_DELEGATE.sendRequestDate]){
        [timerPulstorWaiting invalidate];
        if (APP_DELEGATE.isRideRunning){
            isAlertModeExpire = true;
            isAlertModeConfirmation = false;
            isAlertModePlanRide = false;
            isAlertModeNoUser = false;
            
            [Singleton showSnakBar:@"Your request has timed-out" multiline:false];
            [self alertControllerButtonTappedWithIndex:1];
        } else {
            [self restoreGoogleMapWithMarkersAndRoutes];
            [self shouldShowRunningRideBannerForDriverUser:false];
            [self shouldShowRunningRideBannerForPassengerUser:false];
            
             APP_DELEGATE.isRideRunning = false;
            APP_DELEGATE.runningRideStatus = RIDE_STATUS_NONE;
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@""];
            
            [self updateCustomerCurrentLocation];
            
            viewPulstorContainer.hidden = true;
        }
    } else {
        
        
        
        remainigTime = remainigTime - 1;
        int totalSeconds = remainigTime % 60;
        int totalMinutes = (remainigTime / 60) % 60;
        lableRemainingTime.text = [NSString stringWithFormat:@"%02d : %02d Mins", totalMinutes, totalSeconds];
        
        
       //  NSString *strCommonText = @"Please wait, contacting other party..";
        
//        if (remainigTime <=60)
//        {
//            lableTimeDetails.text = [NSString stringWithFormat:@"%02d Sec",remainigTime];
//        }
//        else
//        {
//            lableTimeDetails.text = [NSString stringWithFormat:@"%02d:%02d Min", totalMinutes, totalSeconds];
//        }
        
        
        
    }
}

-(void)animateToBigView{
    if (timerPulstorWaiting.isValid){
        pulstor1Width.constant = SCREEN_WIDTH - 50;
        [UIView animateWithDuration:3.0 animations:^{
            imageViewPulstor1.alpha = 0.0;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            imageViewPulstor1.alpha = 1.0;
            pulstor1Width.constant = 10;
            [self.view layoutIfNeeded];
            [self animateToBigView];
        }];
    }
}

-(void)animateToBigView2{
    if (timerPulstorWaiting.isValid){
        pulstor2Width.constant = SCREEN_WIDTH - 50;
        [UIView animateWithDuration:3.0 animations:^{
            imageViewPulstor2.alpha = 0.0;
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            imageViewPulstor2.alpha = 1.0;
            pulstor2Width.constant = 10;
            [self.view layoutIfNeeded];
            [self animateToBigView2];
        }];
    }
}


#pragma mark- Navigation Corner
-(void)shouldShowNavigationForMapWithIsInitialMode:(BOOL)isModeInitial {
    if (!APP_DELEGATE.isRideRunning || APP_DELEGATE.runningRideStatus == RIDE_STATUS_REQUEST || APP_DELEGATE.runningRideStatus == RIDE_STATUS_NONE || !canCallAPIForRoute){
        if (DEBUG_MODE){
            NSLog(@"Will show unwanted navigation");
        }
        return;
    }
    
    if (APP_DELEGATE.isUserModeDriver && NAVIGATION_MODE){
        if (isModeInitial){
            viewNavHeading.hidden = false;
            viewNavNextDirection.hidden = false;
            btnNavShowSteps.hidden = false;
            
            viewNavOptions.hidden = true;
            heightNavNextDirection.constant = 0;
            heightBtnAvoidTolls.constant = 0;
            
            viewNavHeading.backgroundColor = NAVI_DARK_GREEN;
            viewNavNextDirection.backgroundColor = NAVI_LIGH_GREEN;
            btnNavShowSteps.backgroundColor = NAVI_DARK_GREEN;
            viewNavOptions.backgroundColor = NAVI_LIGH_GREEN;
        }
        
        [self loadNextStepsDetailsIntoViews];
        
        heightBtnNavOptions.constant = 50;
        heightNavShowSteps.constant = 40;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    } else {
        [self shouldHideNavigationForMap];
    }
}

-(void)loadNextStepsDetailsIntoViews {
    if (!APP_DELEGATE.isRideRunning || APP_DELEGATE.runningRideStatus == RIDE_STATUS_REQUEST || APP_DELEGATE.runningRideStatus == RIDE_STATUS_NONE || !canCallAPIForRoute){
        if (DEBUG_MODE){
            NSLog(@"Will speak unwanted speech");
        }
        return;
    }
    
    if (arrayActiveRoutesSteps && arrayActiveRoutesSteps.count > 0){
        activeStepIndex = 0;
        int nextStepIndex = 0;
        
        NSDictionary * dictStepsInfo;
        NSDictionary * dictNextStepInfo;
        dictStepsInfo= arrayActiveRoutesSteps[activeStepIndex];
        
        double currentStepLatitude = [dictStepsInfo[@"end_location"][@"lat"] doubleValue];
        double currentStepLongitude = [dictStepsInfo[@"end_location"][@"lng"] doubleValue];
        CLLocationCoordinate2D currentStepEndLocation = CLLocationCoordinate2DMake(currentStepLatitude, currentStepLongitude);
        
        MKMapPoint point1 = MKMapPointForCoordinate(currentStepEndLocation);
        MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
        double distanceToEnd = MKMetersBetweenMapPoints(point1, point2);
        
        if (distanceToEnd <= NAVI_STEP_DISTANCE_RATE && activeStepIndex + 1 < arrayActiveRoutesSteps.count){
            nextStepIndex = activeStepIndex + 1;
        } else {
            nextStepIndex = activeStepIndex;
        }
        
        dictNextStepInfo = arrayActiveRoutesSteps[nextStepIndex];
        
        NSString * detailsTexts = dictNextStepInfo[@"html_instructions"];
        NSString * distance = dictNextStepInfo[@"distance"][@"text"];
        NSString * lableText = dictNextStepInfo[@"html_instructions"];

        NSString * imageName = dictStepsInfo[@"maneuver"];
        if (nextStepIndex != activeStepIndex){
            imageName = dictNextStepInfo[@"maneuver"];
        } else {
            if (activeStepIndex + 1 < arrayActiveRoutesSteps.count){
                imageName = arrayActiveRoutesSteps[activeStepIndex + 1][@"maneuver"];
            } else {
                imageName = dictStepsInfo[@"maneuver"];
            }
        }
        
        if (imageName && ![imageName isEqualToString:@""]){
            viewNavNextDirection.hidden = false;
            imgNavNextDirection.image = [UIImage imageNamed:imageName];
            heightNavNextDirection.constant = 40;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        } else {
            heightNavNextDirection.constant = 0;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:^(BOOL finished) {
                viewNavNextDirection.hidden = true;
            }];
        }
        
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
            MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripStartLocation);
            MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
            double pickupDistance = MKMetersBetweenMapPoints(point1, point2);
            
            if (pickupDistance <= NAVI_DISTANCE_RATE){
                detailsTexts = @"You have arrived at your pickup point";
                lableText = APP_DELEGATE.dictTripDetails[@"pickup_address"];
                canCallAPIForRoute = false;
            }
        } else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
            MKMapPoint point1 = MKMapPointForCoordinate(APP_DELEGATE.tripEndLocation);
            MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
            double destinationDistance = MKMetersBetweenMapPoints(point1, point2);
            
            if (destinationDistance <= NAVI_DISTANCE_RATE){
                lableText = APP_DELEGATE.dictTripDetails[@"drop_address"];
                detailsTexts = @"You have arrived at your destination point";
                canCallAPIForRoute = false;
            }
        }
        
        if (!APP_DELEGATE.canAvoidTolls){
            [btnAvoidTolls setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
        } else {
            [btnAvoidTolls setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
        }
        if (!APP_DELEGATE.canAvoidHighways){
            [btnAvoidHighways setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
        } else {
            [btnAvoidHighways setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
        }
        
        
        double fontRatio = (SCREEN_HEIGHT * 4.5) / 568;
        
        NSString *htmlString = [NSString stringWithFormat:@"<font face='Century Gothic' size='%f' color='white' align='center'>%@",fontRatio, [self stringByDecodingXMLEntities:lableText]];
        NSData * dataFromString = [htmlString dataUsingEncoding:NSUnicodeStringEncoding];
        
        UIColor * color = [UIColor whiteColor];
        NSDictionary * dictData = @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSForegroundColorAttributeName: color};
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:dataFromString options:dictData documentAttributes:nil error:nil];
        lblNavSpetDescription.attributedText = attrStr;
        
        lblNavStepDistance.text = distance;
        
        if ([detailsTexts isEqualToString:@""]){
            if (DEBUG_MODE){
                NSLog(@"Unable to speak next text : %@ :: Previus text : %@", detailsTexts, previusSpeech);
            }
        } else if (([previusSpeech.lowercaseString containsString:@"continue"] || [previusSpeech.lowercaseString containsString:@"head"]) && ([detailsTexts.lowercaseString containsString:@"continue"] || [detailsTexts.lowercaseString containsString:@"head"])) {
            if (DEBUG_MODE){
                NSLog(@"Unable to speak next text : %@ :: Previus text : %@", detailsTexts, previusSpeech);
            }
            previusSpeech = detailsTexts;
        } else if ([previusSpeech isEqualToString:detailsTexts]){
            if (DEBUG_MODE){
                NSLog(@"Unable to speak next text : %@ :: Previus text : %@", detailsTexts, previusSpeech);
            }
            previusSpeech = detailsTexts;
        } else if (APP_DELEGATE.canSpeakNavigation){
            NSString * textToSpeech = [self convertHTML:detailsTexts];
            [APP_DELEGATE speakStringForNavigation:textToSpeech];
            previusSpeech = detailsTexts;
        }
    } else {
        if (DEBUG_MODE){
            NSLog(@"No Steps Found For Load Data");
        }
        [self shouldHideNavigationForMap];
    }
}

-(NSString *)convertHTML:(NSString *)html {
    
    NSScanner *myScanner;
    NSString *text = nil;
    myScanner = [NSScanner scannerWithString:html];
    
    while ([myScanner isAtEnd] == NO) {
        
        [myScanner scanUpToString:@"<" intoString:NULL] ;
        
        [myScanner scanUpToString:@">" intoString:&text] ;
        
        html = [html stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@>", text] withString:@""];
    }
    //
    html = [html stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    return html;
}

-(void)shouldHideNavigationForMap {
    lblNavStepDistance.text = @"";
    lblNavSpetDescription.text = @"";
    heightBtnNavOptions.constant = 0;
    heightNavNextDirection.constant = 0;
    heightNavShowSteps.constant = 0;
    heightBtnAvoidTolls.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        viewNavHeading.hidden = false;
        viewNavNextDirection.hidden = false;
        viewNavOptions.hidden = true;
        btnNavShowSteps.hidden = false;
    }];
}

- (IBAction)btnNavShowStepsTapAction:(UIButton *)sender {
    if (DEBUG_MODE){
        NSLog(@"Will Present the Steps View");
    }
    if (arrayActiveRoutesSteps.count > 0){
        NavigationStepsView * navView =[self.storyboard instantiateViewControllerWithIdentifier:@"NavigationStepsView"];
        navView.arrayActiverSteps = arrayActiveRoutesSteps;
        navView.modalPresentationStyle = UIModalPresentationOverFullScreen;
        [self presentViewController:navView animated:true completion:nil];
    } else {
        [Singleton showSnakBar:@"No Direction available" multiline:false];
    }
}

- (IBAction)btnNavOptionTapAction:(UIButton *)sender {
    if (DEBUG_MODE){
        NSLog(@"Will show / Hides the Options View");
    }
    if (heightBtnAvoidTolls.constant >= 40){
        heightBtnAvoidTolls.constant = 0;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            viewNavOptions.hidden = true;
        }];
    } else {
        viewNavOptions.hidden = false;
        heightBtnAvoidTolls.constant = 40;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (IBAction)btnNavOptionsTapAction:(UIButton *)sender {
    if (DEBUG_MODE){
        NSLog(@"Will Change the Flags for Avoid things in Route");
    }
    
    if (sender == btnAvoidTolls){
        if (APP_DELEGATE.canAvoidTolls){
            APP_DELEGATE.canAvoidTolls = false;
            [btnAvoidTolls setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
        } else {
            APP_DELEGATE.canAvoidTolls = true;
            [btnAvoidTolls setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
        }
    } else {
        if (APP_DELEGATE.canAvoidHighways){
            APP_DELEGATE.canAvoidHighways = false;
            [btnAvoidHighways setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
        } else {
            APP_DELEGATE.canAvoidHighways = true;
            [btnAvoidHighways setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)btnNavOptionOkayButtonTapAction:(UIButton *)sender {
    heightBtnAvoidTolls.constant = 0;
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        //Will Redraw the route with Flags
        viewNavOptions.hidden = true;
        START_HUD
        if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
            canUpdateRoutes = true;
            [self drawRouteWithMapViewForOrigin:APP_DELEGATE.tripStartLocation Destination:APP_DELEGATE.userLocation isDotedLine:true withCallback:^(BOOL success) {
                //Draw Route Successfull
                STOP_HUD
            }];
        } else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
            canUpdateRoutes = true;
            [self drawRouteWithMapViewForOrigin:APP_DELEGATE.userLocation Destination:APP_DELEGATE.tripEndLocation isDotedLine:true withCallback:^(BOOL success) {
                //Draw Route Successfull
                STOP_HUD
            }];
        }
    }];
}

- (IBAction)btnSpeakHeaderTapAction:(id)sender {
    if (NAVIGATION_MODE){
        if (![lblNavSpetDescription.text isEqualToString:@""]){
            [APP_DELEGATE speakStringForNavigation:lblNavSpetDescription.text];
        }
    }
}

-(void)updateRoutesAndStepsIfRequiredOrValidateTheUserIsOnRoute {
    if (arrayRespectedPolylines && arrayRespectedPolylines.count > 0 && arrayAlternativeRoutes && arrayAlternativeRoutes.count > 0 && arrayActiveRoutesSteps && arrayActiveRoutesSteps.count > 0){
        canUpdateRoutes = true;
        if (arrayActiveRoutesSteps.count == 1){
            NSDictionary * stepCoordinates = arrayActiveRoutesSteps.lastObject[@"end_location"];
            double latitude = [stepCoordinates[@"lat"] doubleValue];
            double longitude = [stepCoordinates[@"lng"] doubleValue];
            CLLocationCoordinate2D stepEndLocation = CLLocationCoordinate2DMake(latitude, longitude);
            
            MKMapPoint point1 = MKMapPointForCoordinate(stepEndLocation);
            MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
            double distnace = MKMetersBetweenMapPoints(point1, point2);
            if (distnace <= 15){
                if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
                    [APP_DELEGATE speakStringForNavigation:@"You have arrived at your Pickup location"];
                } else if (APP_DELEGATE.runningRideStatus == RIDE_STATUS_ACCEPTED){
                    [APP_DELEGATE speakStringForNavigation:@"You have arrived at your destination location"];
                }
            }
        }
    } else {
        if (DEBUG_MODE){
            NSLog(@"Unable to Update The Route");
        }
    }
    
//    return;
    
    
//    if (APP_DELEGATE.isRideRunning && NAVIGATION_MODE){
//        if (DEBUG_MODE){
//            NSLog(@"Will Manage the New Routes / Alternatives Here");
//        }
//        
//        if (arrayRespectedPolylines && arrayRespectedPolylines.count > 0 && arrayAlternativeRoutes && arrayAlternativeRoutes.count > 0 && arrayActiveRoutesSteps && arrayActiveRoutesSteps.count > 0){
//            if (!activeRouteIndex || activeRouteIndex >= arrayAlternativeRoutes.count || activeRouteIndex < 0){
//                activeRouteIndex = 0;
//                [self drawRouteWithAllAlterNativeRoutesArray:arrayAlternativeRoutes];
//            }
//            
//            if (!activeStepIndex || activeStepIndex >= arrayActiveRoutesSteps.count || activeStepIndex < 0){
//                activeStepIndex = 0;
//                [self loadNextStepsDetailsIntoViews];
//            }
//            
//            NSString * activeRoutePoints = arrayAlternativeRoutes[activeRouteIndex][@"overview_polyline"][@"points"];
//            BOOL isOnPath = GMSGeometryContainsLocation(APP_DELEGATE.userLocation, [GMSPath pathFromEncodedPath: activeRoutePoints], false);
//            
//            if (isOnPath){
//                canUpdateRoutes = false;
//                
//                NSMutableArray * arrayRemovableSteps = [[NSMutableArray alloc]init];
//                
//                for (int j = 0; j < arrayActiveRoutesSteps.count; j++){
//                    NSDictionary * stepsData = arrayActiveRoutesSteps[j];
//                    NSString * stepLinePoints = stepsData[@"polyline"][@"points"];
//                    BOOL isOnStep = GMSGeometryContainsLocation(APP_DELEGATE.userLocation, [GMSPath pathFromEncodedPath: stepLinePoints], false);
//                    if (isOnStep) {
//                        activeStepIndex = j;
//                        [self loadNextStepsDetailsIntoViews];
//                        break;
//                    } else {
//                        [arrayRemovableSteps addObject:stepsData];
//                    }
//                    j = j+1;
//                }
//                
//                activeStepIndex = activeStepIndex - (int)arrayRemovableSteps.count;
//    
//                for (NSDictionary * stepsData in arrayRemovableSteps){
//                    [arrayActiveRoutesSteps removeObject:stepsData];
//                }
//                
//                if (activeStepIndex == arrayActiveRoutesSteps.count - 1 && APP_DELEGATE.isUserModeDriver && NAVIGATION_MODE){
//                    NSDictionary * stepCoordinates = arrayActiveRoutesSteps.lastObject[@"end_location"];
//                    double latitude = [stepCoordinates[@"lat"] doubleValue];
//                    double longitude = [stepCoordinates[@"lng"] doubleValue];
//                    CLLocationCoordinate2D tripEndLocation = CLLocationCoordinate2DMake(latitude, longitude);
//                    MKMapPoint point1 = MKMapPointForCoordinate(tripEndLocation);
//                    MKMapPoint point2 = MKMapPointForCoordinate(APP_DELEGATE.userLocation);
//                    double distnace = MKMetersBetweenMapPoints(point1, point2);
//                    if (distnace <= 15){
//                        [APP_DELEGATE speakStringForNavigation:@"You have arrived at your destination location"];
//                    }
//                }
//                
//            } else {
//                int i = 0;
//                BOOL isFoundAnyAlterRoute = false;
//                
//                for (NSDictionary * dictData in arrayAlternativeRoutes){
//                    NSString * activeLine = dictData[@"polypoints"][@"points"];
//                    BOOL isOnPath = GMSGeometryContainsLocation(APP_DELEGATE.userLocation, [GMSPath pathFromEncodedPath: activeLine], false);
//                    
//                    if (isOnPath) {
//                        isFoundAnyAlterRoute = true;
//                        activeRouteIndex = i;
//                        break;
//                    }
//                    i = i + 1;
//                }
//                
//                if (isFoundAnyAlterRoute){
//                    canUpdateRoutes = false;
//                    
//                    [self drawRouteWithAllAlterNativeRoutesArray:arrayAlternativeRoutes];
//                    
//                    if (arrayActiveRoutesSteps && arrayActiveRoutesSteps.count > 0){
//                        NSMutableArray * arrayRemovableSteps = [[NSMutableArray alloc]init];
//                        
//                        for (int j = 0; j < arrayActiveRoutesSteps.count; j++){
//                            NSDictionary * stepsData = arrayActiveRoutesSteps[j];
//                            NSString * stepLinePoints = stepsData[@"polyline"][@"points"];
//                            BOOL isOnStep = GMSGeometryContainsLocation(APP_DELEGATE.userLocation, [GMSPath pathFromEncodedPath: stepLinePoints], false);
//                            if (isOnStep) {
//                                activeStepIndex = j;
//                                [self loadNextStepsDetailsIntoViews];
//                                break;
//                            } else {
//                                [arrayRemovableSteps addObject:stepsData];
//                            }
//                            j = j+1;
//                        }
//                        
//                        activeStepIndex = activeStepIndex - (int)arrayRemovableSteps.count;
//                        
//                        for (NSDictionary * stepsData in arrayRemovableSteps){
//                            [arrayActiveRoutesSteps removeObject:stepsData];
//                        }
//                    } else {
//                        if (DEBUG_MODE){
//                            NSLog(@"No Steps Found For Active Route");
//                        }
//                        canUpdateRoutes = true;
//                    }
//                } else {
//                    canUpdateRoutes = true;
//                }
//            }
//        } else {
//            canUpdateRoutes = true;
//        }
//    }
}

- (NSString *)stringByDecodingXMLEntities:(NSString *)inputString {
    
    NSUInteger myLength = [inputString length];
    NSUInteger ampIndex = [inputString rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return inputString;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:inputString];
    
    [scanner setCharactersToBeSkipped:nil];
    
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
    
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            
            if (gotNumber) {
                [result appendFormat:@"%C", (unichar)charCode];
                
                [scanner scanString:@";" intoString:NULL];
            }
            else {
                NSString *unknownEntity = @"";
                
                [scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
                
                
                [result appendFormat:@"&#%@%@", xForHex, unknownEntity];
                
                //[scanner scanUpToString:@";" intoString:&unknownEntity];
                //[result appendFormat:@"&#%@%@;", xForHex, unknownEntity];
                NSLog(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
                
            }
            
        }
        else {
            NSString *amp;
            
            [scanner scanString:@"&" intoString:&amp];  //an isolated & symbol
            [result appendString:amp];
            
            /*
             NSString *unknownEntity = @"";
             [scanner scanUpToString:@";" intoString:&unknownEntity];
             NSString *semicolon = @"";
             [scanner scanString:@";" intoString:&semicolon];
             [result appendFormat:@"%@%@", unknownEntity, semicolon];
             NSLog(@"Unsupported XML character entity %@%@", unknownEntity, semicolon);
             */
        }
        
    }
    while (![scanner isAtEnd]);
    
finish:
    return result;
}


@end
