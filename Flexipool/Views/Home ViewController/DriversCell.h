//
//  DriversCell.h
//  Flexipool
//
//  Created by HimAnshu on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DriversCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgActiverDriver;
@property (weak, nonatomic) IBOutlet UIImageView *imgUserProfile;

@end
