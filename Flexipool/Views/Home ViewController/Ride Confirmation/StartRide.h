//
//  StartRide.h
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol StartRideDelegate <NSObject>
@optional
-(void)getRideConfirmationAfterSuccess;
@end


@interface StartRide : UIViewController<UITextFieldDelegate>
{
    IBOutlet UILabel *lblHeader;
    IBOutlet UILabel *lblEnterConfirmation;
    
    IBOutlet UITextField *txt6;
    IBOutlet UITextField *txt5;
    IBOutlet UITextField *txt4;
    IBOutlet UITextField *txt3;
    IBOutlet UITextField *txt2;
    IBOutlet UITextField *txt1;
    
    IBOutlet UIButton *btnMsg;
    IBOutlet UILabel *lblMsg;
    IBOutlet UIButton *btnStartTrip;
    BOOL canMoveUp;
    
    IBOutlet UIButton *btnNotifications;
    
    UITextField *activeTextfield;
    
    IBOutlet NSLayoutConstraint *hintLableHeight;
    IBOutlet UILabel *lblHint;
}

@property id <StartRideDelegate> delegate;
@property NSString * confirmationNumber;
- (IBAction)actionClose:(id)sender;
- (IBAction)actionStartTrip:(id)sender;

@end
