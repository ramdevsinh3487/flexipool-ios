//
//  StartRide.m
//  Flexipool
//
//  Created by Nirav on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "StartRide.h"

#define GREEN_COLOR_SUCCESS [UIColor colorWithRed:0.0/255.0 green:157.0/255.0 blue:52.0/255.0 alpha:1]

@interface StartRide ()

@end

@implementation StartRide

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    
    
    [self setupInitialView];
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:NO];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
    [self updateNotificationsBadgeCount:nil];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    
    [btnNotifications setShouldAnimateBadge:true];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
}

- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [txt1 becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [[IQKeyboardManager sharedManager] setEnableAutoToolbar:YES];
}

- (void) setupInitialView {
    [self setTextfieldGrey];

    [lblMsg setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblEnterConfirmation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    
    [txt1 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txt2 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txt3 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txt4 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txt5 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txt6 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    [btnStartTrip.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    lblHeader.text = GET_Language_Value(@"start_ride");
    lblEnterConfirmation.text = GET_Language_Value(@"enter_confirmation");
    if (!self.confirmationNumber || self.confirmationNumber.length < 6){
        self.confirmationNumber = APP_DELEGATE.dictTripDetails[@"confirmation_number"];
    }
    
    if (!self.confirmationNumber || self.confirmationNumber.length < 6){
        [Singleton showSnakBar:@"Confirmation number not found" multiline:false];
    } else {
        lblHint.text = @"";
//        lblHint.text = [NSString stringWithFormat:@"Hint : %@",self.confirmationNumber];
    }
}

-(void)doneOfTextfields {
    
    [activeTextfield resignFirstResponder];
    if(![txt1 hasText] || ![txt2 hasText] || ![txt3 hasText] || ![txt4 hasText] || ![txt5 hasText] || ![txt6 hasText])
    {
        [self setTextfieldRedBorder];
    }
}

-(void)setTextfieldGrey
{
    txt1.layer.borderColor = UIColor.lightGrayColor.CGColor;
    txt1.layer.borderWidth = 1;
    txt1.layer.masksToBounds = true;
    
    txt2.layer.borderColor = UIColor.lightGrayColor.CGColor;
    txt2.layer.borderWidth = 1;
    txt2.layer.masksToBounds = true;
    
    txt3.layer.borderColor = UIColor.lightGrayColor.CGColor;
    txt3.layer.borderWidth = 1;
    txt3.layer.masksToBounds = true;
    
    txt4.layer.borderColor = UIColor.lightGrayColor.CGColor;
    txt4.layer.borderWidth = 1;
    txt4.layer.masksToBounds = true;
    
    txt5.layer.borderColor = UIColor.lightGrayColor.CGColor;
    txt5.layer.borderWidth = 1;
    txt5.layer.masksToBounds = true;
    
    txt6.layer.borderColor = UIColor.lightGrayColor.CGColor;
    txt6.layer.borderWidth = 1;
    txt6.layer.masksToBounds = true;
    
    btnStartTrip.enabled = NO;
    btnStartTrip.alpha = 0.5;
    btnMsg.hidden = lblMsg.hidden = YES;
}

-(void)setTextfieldRedBorder
{
    txt1.layer.borderColor = UIColor.redColor.CGColor;
    txt1.layer.borderWidth = 1;
    txt1.layer.masksToBounds = true;
    
    txt2.layer.borderColor = UIColor.redColor.CGColor;
    txt2.layer.borderWidth = 1;
    txt2.layer.masksToBounds = true;
    
    txt3.layer.borderColor = UIColor.redColor.CGColor;
    txt3.layer.borderWidth = 1;
    txt3.layer.masksToBounds = true;
    
    txt4.layer.borderColor = UIColor.redColor.CGColor;
    txt4.layer.borderWidth = 1;
    txt4.layer.masksToBounds = true;
    
    txt5.layer.borderColor = UIColor.redColor.CGColor;
    txt5.layer.borderWidth = 1;
    txt5.layer.masksToBounds = true;
    
    txt6.layer.borderColor = UIColor.redColor.CGColor;
    txt6.layer.borderWidth = 1;
    txt6.layer.masksToBounds = true;
    
    btnStartTrip.enabled = NO;
    btnStartTrip.alpha = 0.5;
    btnMsg.hidden = lblMsg.hidden = NO;
}

-(void)setTextfieldGreenBorder
{
    txt1.layer.borderColor = GREEN_COLOR_SUCCESS.CGColor;
    txt1.layer.borderWidth = 1;
    txt1.layer.masksToBounds = true;
    
    txt2.layer.borderColor = GREEN_COLOR_SUCCESS.CGColor;
    txt2.layer.borderWidth = 1;
    txt2.layer.masksToBounds = true;
    
    txt3.layer.borderColor = GREEN_COLOR_SUCCESS.CGColor;
    txt3.layer.borderWidth = 1;
    txt3.layer.masksToBounds = true;
    
    txt4.layer.borderColor = GREEN_COLOR_SUCCESS.CGColor;
    txt4.layer.borderWidth = 1;
    txt4.layer.masksToBounds = true;
    
    txt5.layer.borderColor = GREEN_COLOR_SUCCESS.CGColor;
    txt5.layer.borderWidth = 1;
    txt5.layer.masksToBounds = true;
    
    txt6.layer.borderColor = GREEN_COLOR_SUCCESS.CGColor;
    txt6.layer.borderWidth = 1;
    txt6.layer.masksToBounds = true;
    
    btnStartTrip.enabled = YES;
    btnStartTrip.alpha = 1;
    btnMsg.hidden = lblMsg.hidden = NO;
}

#pragma mark - UIKeyBoardDelegate -

- (void)keyBoardWillShowNotification:(NSNotification*)notification
{
    if (canMoveUp == true){
        //        NSDictionary* keyboardInfo = [notification userInfo];
        //        NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
        // self.lableBottomSpace.constant = [keyboardFrameBegin CGRectValue].size.height + 20;
        [self.view layoutIfNeeded];
    }
}

#pragma mark - UITextField Delegate -

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setTextfieldGrey];
    activeTextfield = textField;
    
//    if (textField != txt1 && ![txt1 hasText]) {
//        
//        [textField resignFirstResponder];
//        [txt1 becomeFirstResponder];
//    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    int iValue;
    if ([string isEqualToString:@""] || [[NSScanner scannerWithString:string] scanInt:&iValue]){
        NSLog(@"value entered correctly.");
        
    } else {
        return false;
    }
    
    if ([textField.text isEqualToString:@" "]){
        textField.text = @"";
    }
    if (textField.tag == 101){
        if (![string isEqualToString:@""]){
            textField.text = string;
            [txt2 becomeFirstResponder];
            txt2.text = @" ";
            return false;
        }
    }else if (textField.tag == 102){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [txt1 becomeFirstResponder];
            return false;
        } else {
            textField.text = string;
            [textField resignFirstResponder];
            [txt3 becomeFirstResponder];
            txt3.text = @" ";
            return false;
        }
    } else if (textField.tag == 103){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [txt2 becomeFirstResponder];
            return false;
        } else {
            textField.text = string;
            [txt4 becomeFirstResponder];
            txt4.text = @" ";
            return false;
        }
    }
    else if (textField.tag == 104){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [txt3 becomeFirstResponder];
            return false;
        } else {
            textField.text = string;
            [txt5 becomeFirstResponder];
            txt5.text = @" ";
            return false;
        }
    }
    else if (textField.tag == 105){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [txt4 becomeFirstResponder];
            return false;
        } else {
            textField.text = string;
            [txt6 becomeFirstResponder];
            txt6.text = @" ";
            return false;
        }
    }
    
    else if (textField.tag == 106){
        if ([string isEqualToString:@""]){
            textField.text = @"";
            [txt5 becomeFirstResponder];
            return false;
        } else {
            if ([textField.text length] > 0){
                return false;
            } else {
                textField.text = string;
                [textField resignFirstResponder];
                return false;
            }
        }
    }
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if (textField != txt6) {
        return;
    }
    
    NSString * strConfirmation = [[[[[txt1.text stringByAppendingString:txt2.text] stringByAppendingString:txt3.text] stringByAppendingString:txt4.text] stringByAppendingString:txt5.text] stringByAppendingString:txt6.text];
//    START_HUD
//    [APP_DELEGATE.apiManager verifyOTPConfirmaionNumberWithNumber:strConfirmation withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
//        if (success){
//            
//        } else {
//            
//        }
//    }];
    
                                  
    
    if(![strConfirmation isEqualToString:self.confirmationNumber])
    {
        [self setTextfieldRedBorder];
        
        lblMsg.text = GET_Language_Value(@"incorrect_confirmation_number");
        lblMsg.textColor = [UIColor redColor];
        [btnMsg setImage:[UIImage imageNamed:@"confirmation-error-icon"] forState:UIControlStateNormal];
        txt1.text = txt2.text = txt3.text = txt4.text = txt5.text = txt6.text = @"";
        [txt1 becomeFirstResponder];
    }
    else {
        [self setTextfieldGreenBorder];
        
        lblMsg.text = GET_Language_Value(@"verified_successfully");
        lblMsg.textColor = GREEN_COLOR_SUCCESS;
        [btnMsg setImage:[UIImage imageNamed:@"confirmation-success-icon"] forState:UIControlStateNormal];
    }
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action Methods -

- (IBAction)actionClose:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionStartTrip:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        [self.delegate getRideConfirmationAfterSuccess];
    }];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
