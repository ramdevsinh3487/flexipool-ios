//
//  PassengerRequestViewController.m
//  Flexipool
//
//  Created by HimAnshu on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "PassengerRequestViewController.h"

@interface PassengerRequestViewController ()

@end

@implementation PassengerRequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:true];
    self.view.alpha = 1;
    constraintViewX.constant = SCREEN_WIDTH;
    [self setupInitialView];
    
    if (self.canPerformAciton){
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationRecivedForRideStatus) name:NOTI_UPDATE_NEAR_BY_USERS object:nil];
    }
}

- (void) setupInitialView {
    
    [lblName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 5]];
    [lblAge setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblGender setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblOccupation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblHeadingTo setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblFrom setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblTime setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    
    [btnAccept.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [btnReject.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    

    if (self.canPerformAciton){
        lblViewTitle.text = [self.dictTripInfo[@"request_by"]isEqualToString:@"1"] ? @"(Requesting a ride)" : @"(Offering a ride)";
        
        NSString * genderLatter;
        if ([self.dictTripInfo[@"gender"]length] > 0) {
            genderLatter = [NSString stringWithFormat:@"%@",[self.dictTripInfo[@"gender"] isEqualToString:@"1"] ? @"M" : @"F"];
        } else {
            genderLatter = @"";
        }
        
        
        
        lblTime.text = [NSString stringWithFormat:@"%@\n(%@)", [APP_DELEGATE formatTheCostWithInputValue:[self.dictTripInfo[@"trip_cost"] doubleValue] isModeValue:false], self.dictTripInfo[@"ride_eta"]];
        
        lblAge.text = [NSString stringWithFormat:@"%@ %@",genderLatter, self.dictTripInfo[@"age"]];
        lblGender.text = @"";

       
        lblOccupation.text = [NSString stringWithFormat:@"%@ : %@",GET_Language_Value(@"occupation"), self.dictTripInfo[@"occupation"]];
        
        NSString * dropTitle = self.dictTripInfo[@"drop_title"];
        NSString * pickupTitle = self.dictTripInfo[@"pickup_title"];
        
        if (dropTitle && ![dropTitle isEqualToString:@""]) {
            lblHeadingTo.text = [NSString stringWithFormat:@"%@ : \n%@,\n%@",GET_Language_Value(@"heading_to"), dropTitle, self.dictTripInfo[@"destination_address"]];
        } else {
            lblHeadingTo.text = [NSString stringWithFormat:@"%@ : \n%@",GET_Language_Value(@"heading_to"), self.dictTripInfo[@"destination_address"]];
        }
        
        if (pickupTitle && ![pickupTitle isEqualToString:@""]) {
            lblFrom.text = [NSString stringWithFormat:@"%@ : \n%@,\n%@",GET_Language_Value(@"from"), pickupTitle, self.dictTripInfo[@"pikcup_address"]];
        } else {
            lblFrom.text = [NSString stringWithFormat:@"%@ : \n%@",GET_Language_Value(@"from"),self.dictTripInfo[@"pikcup_address"]];
        }
        
        lblName.text = [NSString stringWithFormat:@"%@ %@",self.dictTripInfo[@"first_name"],self.dictTripInfo[@"last_name"]];
        if (APP_DELEGATE.isUserModeDriver == false){
            lblRideStatus.text= [NSString stringWithFormat:@"Vehicle Make & Model : %@", self.dictTripInfo[@"vehicle_make"]];
        }
    } else {
        
        
        NSString * userNam = @"";
        if (![self.dictTripInfo[@"to_user_id"] isEqualToString:APP_DELEGATE.loggedUserId]){
            userNam =  self.dictTripInfo[@"to_user_name"];
            // lblViewTitle.text = [self.dictTripInfo[@"requestee_id"]isEqualToString:@"1"] ? @"(Offering a ride)" : @"(Requesting a ride)";
             lblViewTitle.text = @"(Requesting a ride)";
            
            
        } else {
            userNam = self.dictTripInfo[@"user_name"];
            lblViewTitle.text = @"(Offering a ride)";
        }
        
       
        lblTime.text = @"";
        lblAge.text = [NSString stringWithFormat:@"%@ : %@",GET_Language_Value(@"age"), self.dictTripInfo[@"age"]];
        
        lblGender.text = [NSString stringWithFormat:@"%@ : %@",GET_Language_Value(@"gender"),self.dictTripInfo[@"gender"]];
        lblOccupation.text = [NSString stringWithFormat:@"%@ : %@",GET_Language_Value(@"occupation"), self.dictTripInfo[@"profession"]];
        lblHeadingTo.text = [NSString stringWithFormat:@"%@ : %@",GET_Language_Value(@"heading_to"), self.dictTripInfo[@"destination_location"]];
        lblFrom.text = [NSString stringWithFormat:@"%@ : %@",GET_Language_Value(@"from"),self.dictTripInfo[@"pickup_location"]];
        
        
       
        
        lblName.text = [NSString stringWithFormat:@"%@",userNam];
    }

     UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
    
//    UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//    NSString * gender = self.dictTripInfo[@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    imgViewProfile.layer.cornerRadius = imgViewProfile.frame.size.height / 2;
    imgViewProfile.layer.masksToBounds = true;
    imgViewProfile.clipsToBounds = true;
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgViewProfile.frame.size.width, imgViewProfile.frame.size.height)];
    
    activity_indicator.tag = 11111;
    [[imgViewProfile viewWithTag:1111] removeFromSuperview];
    
    [imgViewProfile addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    NSString * imageUrl;
    if (self.canPerformAciton){
        imageUrl = self.dictTripInfo[@"profile"];
    } else {
        if (![self.dictTripInfo[@"to_user_id"] isEqualToString:APP_DELEGATE.loggedUserId]){
            
             imageUrl = self.dictTripInfo[@"to_image_url"];
        }
        else
        {
             imageUrl = self.dictTripInfo[@"user_image"];
        }
        
    }
    

    imageUrl = [imageUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [imgViewProfile sd_setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [[imgViewProfile viewWithTag:1111] removeFromSuperview];
    }];
    
    [btnAccept setTitle:GET_Language_Value(@"accept") forState:UIControlStateNormal];
    [btnReject setTitle:GET_Language_Value(@"reject") forState:UIControlStateNormal];
    
    if (self.canPerformAciton){
        acceptRejectHeight.constant = 40;
        btnAccept.hidden = false;
        btnReject.hidden = false;
    } else {
        acceptRejectHeight.constant = 0;
        btnAccept.hidden = true;
        btnReject.hidden = true;
    }
    [self.view layoutIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [APP_DELEGATE.window endEditing:YES];
    
    viewPopup.alpha = 1;
    viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.3 animations:^{
        constraintViewX.constant = 0;
        [self.view layoutIfNeeded];
        viewPopup.transform = CGAffineTransformIdentity;
    }];
}

-(void)notificationRecivedForRideStatus {
    if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_REQUEST){
        if (DEBUG_MODE){
            NSLog(@"Request is still available");
        }
    } else {
        [Singleton showSnakBar:[NSString stringWithFormat:@"You missed the request from %@ %@",self.dictTripInfo[@"first_name"],self.dictTripInfo[@"last_name"]] multiline:true];
        [self dismissViewControllerAnimated:true completion:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Action Methods -
- (IBAction)actionClose:(id)sender {
    viewPopup.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 animations:^{
        constraintViewX.constant = -SCREEN_WIDTH;
        [self.view layoutIfNeeded];
        viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:^{
            if (self.delegate){
                [self.delegate didCloseTheAlertWithoutAction:self.dictTripInfo];
            }
        }];
    }];
}

- (IBAction)actionAccept:(id)sender {
    viewPopup.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 animations:^{
        constraintViewX.constant = -SCREEN_WIDTH;
        [self.view layoutIfNeeded];
        viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:^{
            if (self.delegate){
                [self.delegate didAcceptTheRideWithUsableInfo:self.dictTripInfo];
            }
        }];
    }];
}

- (IBAction)actionReject:(id)sender {
    viewPopup.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.2 animations:^{
        constraintViewX.constant = -SCREEN_WIDTH;
        [self.view layoutIfNeeded];
        viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    } completion:^(BOOL finished) {
        [self dismissViewControllerAnimated:NO completion:^{
            if (self.delegate){
                [self.delegate didRejectTheRideWithUsableInfo:self.dictTripInfo];
            }
        }];
    }];
}

- (IBAction)contactPhoneTapped:(id)sender {
    
    NSLog(@"%@",APP_DELEGATE.loggedUserId);
    
    NSString * contactNumber = @"";
    if (self.canPerformAciton){
        contactNumber = APP_DELEGATE.isUserModeDriver ? self.dictTripInfo[@"customer_mobile_no"] : self.dictTripInfo[@"driver_mobile_no"];
    } else {
        
        if ([self.dictTripInfo[@"requestee_id"] isEqualToString:APP_DELEGATE.loggedUserId])
        {
           contactNumber = self.dictTripInfo[@"mobile_no"];
        }
        else
        {
            contactNumber = self.dictTripInfo[@"to_mobile_no"];
        }
        
        
    }
    
    if ([[UIApplication sharedApplication]canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",contactNumber]]]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",contactNumber]]];
    } else {
        SHOW_ALERT_WITH_CAUTION(@"Unable to make phone calls \n Please check your device settings");
        return;
    }
}

- (IBAction)contactMessageTapped:(id)sender {
    //Will Open Discussion Board
    
    if (DEBUG_MODE){
        NSLog(@"Trip Information : %@",self.dictTripInfo);
    }
    NSString * contactNumber = @"";
    if (self.canPerformAciton){
        contactNumber = APP_DELEGATE.isUserModeDriver ? self.dictTripInfo[@"customer_mobile_no"] : self.dictTripInfo[@"driver_mobile_no"];
    } else {
       // contactNumber = self.dictTripInfo[@"mobile_no"];
        
        if ([self.dictTripInfo[@"requestee_id"] isEqualToString:APP_DELEGATE.loggedUserId])
        {
            contactNumber = self.dictTripInfo[@"mobile_no"];
        }
        else
        {
            contactNumber = self.dictTripInfo[@"to_mobile_no"];
        }

    }
    
    if(![MFMessageComposeViewController canSendText]) {
        SHOW_ALERT_WITH_CAUTION(@"Unable to send message \n Please check your device settings");
        return;
    } else {
        NSArray *recipents = @[contactNumber];
        NSString * userName = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictUserDetails[@"first_name"],
                               APP_DELEGATE.dictUserDetails[@"last_name"]];
        
        NSString *message = [NSString stringWithFormat:@"Ahoy user %@ is contacting you \n\n",userName];
        
        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
        messageController.messageComposeDelegate = self;
        [messageController setRecipients:recipents];
        [messageController setBody:message];
        [self presentViewController:messageController animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            if (DEBUG_MODE){
                NSLog(@"Failed To send SMS");
            }
            break;
        }
            
        case MessageComposeResultSent:
            break;
            if (DEBUG_MODE){
                NSLog(@"Your message has been sent");
            }
        default:
            break;
    }
    
    [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end
