//
//  PassengerRequestViewController.h
//  Flexipool
//
//  Created by HimAnshu on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@protocol PassengerRequestViewControllerDelegate <NSObject>

@optional
-(void)didAcceptTheRideWithUsableInfo:(NSDictionary *)dictUsableInfo;

@optional
-(void)didRejectTheRideWithUsableInfo:(NSDictionary *)dictUsableInfo;

@optional
-(void)didCloseTheAlertWithoutAction:(NSDictionary *)dictUsableInfo;
@end

@interface PassengerRequestViewController : UIViewController <MFMessageComposeViewControllerDelegate>
{
    IBOutlet UILabel *lblViewTitle;
    IBOutlet UIView *viewPopup;
    IBOutlet UIImageView *imgViewProfile;
    IBOutlet UILabel *lblName;
    IBOutlet UILabel *lblAge;
    IBOutlet UILabel *lblGender;
    IBOutlet UILabel *lblOccupation;
    IBOutlet UILabel *lblHeadingTo;
    IBOutlet UILabel *lblFrom;
    IBOutlet UILabel *lblRideStatus;
    IBOutlet UILabel *lblTime;
    IBOutlet NSLayoutConstraint *constraintViewX;
    IBOutlet UIButton *btnAccept;
    IBOutlet UIButton *btnReject;
    IBOutlet NSLayoutConstraint *acceptRejectHeight;
}

@property NSDictionary *dictTripInfo;
@property id <PassengerRequestViewControllerDelegate> delegate;
@property BOOL canPerformAciton;

- (IBAction)actionClose:(id)sender;
- (IBAction)actionAccept:(id)sender;
- (IBAction)actionReject:(id)sender;

@end
