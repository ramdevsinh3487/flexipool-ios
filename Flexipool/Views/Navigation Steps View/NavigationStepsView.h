//
//  NavigationStepsView.h
//  Flexipool
//
//  Created by Vishal Gohil on 22/11/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationStepsView : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UILabel *lblTitle;
    IBOutlet UITableView *tblList;
    IBOutlet UIButton *btnOkay;
}
@property NSArray * arrayActiverSteps;
- (IBAction)okayButtonTapped:(id)sender;

@end
