//
//  NavigationStepsView.m
//  Flexipool
//
//  Created by Vishal Gohil on 22/11/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "NavigationStepsView.h"

@interface NavigationStepsView ()

@end

@implementation NavigationStepsView

- (void)viewDidLoad {
    [super viewDidLoad];
    tblList.tableFooterView = [UIView new];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notifStepsArrayUpdated:) name:NOTI_STEPS_UPDATED object:nil];
    [tblList reloadData];
    // Do any additional setup after loading the view.
}

-(void)notifStepsArrayUpdated:(NSNotification *)notif {
    if (notif && notif.userInfo){
        NSArray * stepsArray = (NSArray *)[notif.userInfo objectForKey:@"steps_array"];
        self.arrayActiverSteps = stepsArray;
        [tblList reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrayActiverSteps.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"NavStepsCell"];
    UIImageView * imgDirection = [cell viewWithTag:101];
    UILabel * stepDistance = [cell viewWithTag:102];
    UILabel * stepDescripiton = [cell viewWithTag:103];
    
    NSString * imageName = self.arrayActiverSteps[indexPath.row][@"maneuver"];
    NSString * detailsText = self.arrayActiverSteps[indexPath.row][@"html_instructions"];
    NSString * distance = self.arrayActiverSteps[indexPath.row][@"distance"][@"text"];
    
    if (imageName && ![imageName isEqualToString:@""]){
        imgDirection.image = [UIImage imageNamed:imageName];
    } else {
        imgDirection.image = [UIImage imageNamed:@""];
    }
    
    stepDistance.text = distance;
    
    
    double fontRatio = (SCREEN_HEIGHT * 4.5) / 568;
    NSString *htmlString = [NSString stringWithFormat:@"<font face='Century Gothic' size='%f' color='white' align='center'>%@",fontRatio, [self stringByDecodingXMLEntities:detailsText]];
    NSData * dataFromString = [htmlString dataUsingEncoding:NSUnicodeStringEncoding];
    
    UIColor * color = [UIColor whiteColor];
    NSDictionary * dictData = @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSForegroundColorAttributeName: color};
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:dataFromString options:dictData documentAttributes:nil error:nil];
    stepDescripiton.attributedText = attrStr;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)okayButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (NSString *)stringByDecodingXMLEntities:(NSString *)inputString {
    
    NSUInteger myLength = [inputString length];
    NSUInteger ampIndex = [inputString rangeOfString:@"&" options:NSLiteralSearch].location;
    
    // Short-circuit if there are no ampersands.
    if (ampIndex == NSNotFound) {
        return inputString;
    }
    // Make result string with some extra capacity.
    NSMutableString *result = [NSMutableString stringWithCapacity:(myLength * 1.25)];
    
    // First iteration doesn't need to scan to & since we did that already, but for code simplicity's sake we'll do it again with the scanner.
    NSScanner *scanner = [NSScanner scannerWithString:inputString];
    
    [scanner setCharactersToBeSkipped:nil];
    
    NSCharacterSet *boundaryCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@" \t\n\r;"];
    
    do {
        // Scan up to the next entity or the end of the string.
        NSString *nonEntityString;
        if ([scanner scanUpToString:@"&" intoString:&nonEntityString]) {
            [result appendString:nonEntityString];
        }
        if ([scanner isAtEnd]) {
            goto finish;
        }
        // Scan either a HTML or numeric character entity reference.
        if ([scanner scanString:@"&amp;" intoString:NULL])
            [result appendString:@"&"];
        else if ([scanner scanString:@"&apos;" intoString:NULL])
            [result appendString:@"'"];
        else if ([scanner scanString:@"&quot;" intoString:NULL])
            [result appendString:@"\""];
        else if ([scanner scanString:@"&lt;" intoString:NULL])
            [result appendString:@"<"];
        else if ([scanner scanString:@"&gt;" intoString:NULL])
            [result appendString:@">"];
        else if ([scanner scanString:@"&#" intoString:NULL]) {
            BOOL gotNumber;
            unsigned charCode;
            NSString *xForHex = @"";
            
            // Is it hex or decimal?
            if ([scanner scanString:@"x" intoString:&xForHex]) {
                gotNumber = [scanner scanHexInt:&charCode];
            }
            else {
                gotNumber = [scanner scanInt:(int*)&charCode];
            }
            
            if (gotNumber) {
                [result appendFormat:@"%C", (unichar)charCode];
                
                [scanner scanString:@";" intoString:NULL];
            }
            else {
                NSString *unknownEntity = @"";
                
                [scanner scanUpToCharactersFromSet:boundaryCharacterSet intoString:&unknownEntity];
                
                
                [result appendFormat:@"&#%@%@", xForHex, unknownEntity];
                
                //[scanner scanUpToString:@";" intoString:&unknownEntity];
                //[result appendFormat:@"&#%@%@;", xForHex, unknownEntity];
                NSLog(@"Expected numeric character entity but got &#%@%@;", xForHex, unknownEntity);
                
            }
            
        }
        else {
            NSString *amp;
            
            [scanner scanString:@"&" intoString:&amp];  //an isolated & symbol
            [result appendString:amp];
            
            /*
             NSString *unknownEntity = @"";
             [scanner scanUpToString:@";" intoString:&unknownEntity];
             NSString *semicolon = @"";
             [scanner scanString:@";" intoString:&semicolon];
             [result appendFormat:@"%@%@", unknownEntity, semicolon];
             NSLog(@"Unsupported XML character entity %@%@", unknownEntity, semicolon);
             */
        }
        
    }
    while (![scanner isAtEnd]);
    
finish:
    return result;
}

@end
