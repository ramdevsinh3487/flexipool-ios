//
//  FAQViewController.m
//  Flexipool
//
//  Created by Vishal Gohil on 18/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "FAQViewController.h"

@interface FAQViewController ()

@end

@implementation FAQViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    lblHeader.text = @"FAQs";
    
    [lblDataMessage setFont:[UIFont fontWithName:AppFont_Regular size:POPUP_HEADER_FONTS_SIZE]];
    tblFAQList.tableFooterView = [UIView new];
    
    retryCount = 0;
    totalCounts = 0;
    pageNumber = 1;
    [self reloadFAQList];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
    // Do any additional setup after loading the view.
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotifications setShouldAnimateBadge:true];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
}


-(void)reloadFAQList {
    if (totalCounts && totalCounts > 0){
        START_HUD
    } else {
        arrayFAQs = [[NSMutableArray alloc]init];
        [tblFAQList reloadData];
    }
    
    isLoadingData = true;
    retryCount = retryCount + 1;
    [APP_DELEGATE.apiManager listFAQuestionsForReferanceWithPageNumber:[NSString stringWithFormat:@"%d",pageNumber] withCallBack:^(BOOL success, NSArray *arrayResponse, int recordCount, NSString *serverMessange) {
        if (success){
            NSMutableArray * responseArrayEdited = [[NSMutableArray alloc]init];
            for (NSDictionary * dictFaqData in arrayResponse) {
                NSMutableDictionary *dictFinalData = [[NSMutableDictionary alloc]initWithDictionary:dictFaqData];
                
                [dictFinalData setObject:@"0" forKey:@"isExpanded"];
                [responseArrayEdited addObject:dictFinalData];
            }
            
            if (totalCounts && totalCounts > 0){
                [arrayFAQs addObjectsFromArray:responseArrayEdited];
                STOP_HUD
            } else {
                arrayFAQs = [[NSMutableArray alloc]initWithArray:responseArrayEdited];
                totalCounts = recordCount;
            }
            pageNumber = pageNumber + 1;
            isLoadingData = false;
            [tblFAQList reloadData];
        } else {
            if (totalCounts && totalCounts > 0){
                STOP_HUD
            }
            if (retryCount <= 3){
                SHOW_ALERT_WITH_SUCCESS(serverMessange);
                isLoadingData = false;
                [tblFAQList reloadData];
                totalCounts = (int)arrayFAQs.count;
            } else {
                [self reloadFAQList];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)closeButtonTapAction:(id)sender {
    [APP_DELEGATE showMenu];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (isLoadingData){
        lblDataMessage.text = @"Loading data, Please wait...";
        lblDataMessage.hidden = false;
        tblFAQList.hidden = true;
        return 0;
    } else {
        if (arrayFAQs.count > 0){
            lblDataMessage.hidden = true;
            tblFAQList.hidden = false;
            return arrayFAQs.count;
        } else {
            lblDataMessage.text = @"No data found";
            lblDataMessage.hidden = false;
            tblFAQList.hidden = true;
            return 0;
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FAQTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"FAQTableViewCell"];
    cell.lblQuestionLatter.text = [arrayFAQs[indexPath.row][@"question"] substringToIndex:1];
//    cell.lblQuestionLatter.layer.cornerRadius = cell.lblQuestionLatter.frame.size.height / 2;
//    cell.lblQuestionLatter.layer.masksToBounds = true;
    
    cell.lblQuestion.text = arrayFAQs[indexPath.row][@"question"];
    cell.lblAnswer.text = arrayFAQs[indexPath.row][@"answer"];
    
    if (totalCounts > arrayFAQs.count) {
        if (indexPath.row == arrayFAQs.count - 1 && isLoadingData == false){
            [self reloadFAQList];
        }
    }
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([arrayFAQs[indexPath.row][@"isExpanded"] isEqualToString:@"0"]){
        float willHeight = [self requiredHeight:arrayFAQs[indexPath.row][@"question"]];
        if (willHeight < 20){
            return 55;
        } else  {
            return (willHeight + 30);
        }
        return 60;
    } else {
        return [self requiredHeight:arrayFAQs[indexPath.row][@"answer"]] + 70;
        //Will Count the size for Answer lable
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    
    [tableView beginUpdates];
    NSMutableDictionary * dictdata = [[NSMutableDictionary alloc]initWithDictionary:arrayFAQs[indexPath.row]];
    if ([dictdata[@"isExpanded"] isEqualToString:@"0"]){
        dictdata[@"isExpanded"] = @"1";
    } else {
        dictdata[@"isExpanded"] = @"0";
    }
    
    if (lastSelectedIndex != indexPath.row){
        NSMutableDictionary * dictdata2 = [[NSMutableDictionary alloc]initWithDictionary:arrayFAQs[lastSelectedIndex]];
         dictdata2[@"isExpanded"] = @"0";
        [arrayFAQs replaceObjectAtIndex:lastSelectedIndex withObject:dictdata2];
    }
    lastSelectedIndex = (int)indexPath.row;
    [arrayFAQs replaceObjectAtIndex:indexPath.row withObject:dictdata];
    [tableView endUpdates];
}

- (CGFloat)requiredHeight:(NSString*)textfieldString{
    
    UIFont *font = [UIFont fontWithName:lblHeader.font.fontName size:14.0];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH - 55, CGFLOAT_MAX)];
    label.numberOfLines = 0;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.font = font;
    label.text = textfieldString;
    [label sizeToFit];
    return label.frame.size.height;
}

- (IBAction)notificationButtonTapped:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}


@end
