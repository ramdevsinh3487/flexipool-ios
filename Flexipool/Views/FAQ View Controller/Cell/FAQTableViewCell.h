//
//  FAQTableViewCell.h
//  Flexipool
//
//  Created by Vishal Gohil on 18/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQTableViewCell : UITableViewCell {
    
}

@property (strong, nonatomic) IBOutlet UILabel *lblQuestionLatter;
@property (strong, nonatomic)IBOutlet UILabel *lblQuestion;
@property (strong, nonatomic)IBOutlet UILabel *lblAnswer;
@end
