//
//  FAQViewController.h
//  Flexipool
//
//  Created by Vishal Gohil on 18/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FAQTableViewCell.h"
@interface FAQViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    
    IBOutlet UILabel *lblHeader;
    IBOutlet UITableView *tblFAQList;
    NSMutableArray * arrayFAQs;
    IBOutlet UITextView *txtViewQuery;
    IBOutlet NSLayoutConstraint *queryTxtViewHeight;
    IBOutlet NSLayoutConstraint *queryViewBottomSpace;
    int lastSelectedIndex;
    int retryCount;
    IBOutlet UILabel *lblDataMessage;
    BOOL isLoadingData;
    IBOutlet UIButton *btnNotifications;

    int totalCounts, pageNumber;
}

@end
