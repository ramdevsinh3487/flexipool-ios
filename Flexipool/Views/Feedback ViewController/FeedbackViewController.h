//
//  FeedbackViewController.h
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController<UITextFieldDelegate, UITextViewDelegate>
{
    IBOutlet UILabel *lblHowWasExp;
    IBOutlet UILabel *lblHeader;
    IBOutlet UIImageView *imgvwUserPic;
    IBOutlet UILabel *lblDriverName;
    
    IBOutlet UILabel *lblWhatsWrong;
    IBOutlet UIButton *btn1;
    IBOutlet UIButton *btn2;
    IBOutlet UIButton *btn3;
    
    IBOutlet UIButton *btn4;
    IBOutlet UIView *viewWhatsWronContainer;
    IBOutlet NSLayoutConstraint *whatsWornViewHeight;
    IBOutlet UIButton *btnRude;
    IBOutlet UIButton *btnIndecent;
    IBOutlet UILabel *lblBlockPerson;
    
    IBOutlet UIButton *btnAnnoying;
    
    IBOutlet UILabel *lblFeedBackMsg;
    IBOutlet UIButton *btnYes;
    IBOutlet UILabel *lblProvideFeedback;
    IBOutlet UITextView *txtView;
    
    IBOutlet UIButton *btnNo;
    IBOutlet UIButton *btnSendFeedBack;
    
    NSString * symbolType;
    NSString * wrongType;
    NSString * isBlocked;
    NSString * isFavorite;
    IBOutlet UIButton *btnNotification;
    
    IBOutlet NSLayoutConstraint *blockUserViewHeight;
}

@property NSDictionary * dictTripDetails;
- (IBAction)actionClose:(id)sender;


@end
