//
//  FeedbackViewController.m
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "FeedbackViewController.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    
    txtView.delegate = self;
    imgvwUserPic.hidden = true;
    [self setupInitialView];
      [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    imgvwUserPic.layer.cornerRadius = imgvwUserPic.frame.size.height / 2;
    imgvwUserPic.layer.masksToBounds = true;
    imgvwUserPic.hidden = false;
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    
    [btnNotification setShouldAnimateBadge:true];
    btnNotification.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}

- (void) setupInitialView {
    [lblHowWasExp setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [lblDriverName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+1]];
    
    [lblWhatsWrong setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+1]];
    [lblBlockPerson setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+1]];
    [lblProvideFeedback setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [lblFeedBackMsg setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-4]];
    [txtView setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    [btnRude.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    [btnIndecent.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    [btnAnnoying.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    
    [btnYes.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    [btnNo.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-2]];
    
    [btnSendFeedBack.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    //
    lblHeader.text = GET_Language_Value(@"feedback");
    lblHowWasExp.text = GET_Language_Value(@"how_was_experience");
    lblWhatsWrong.text = GET_Language_Value(@"whats_wrong");
   // lblBlockPerson.text = GET_Language_Value(@"whats_wrong");
    lblProvideFeedback.text = GET_Language_Value(@"provide_feedback");
    lblFeedBackMsg.text = GET_Language_Value(@"we_will_not_share_feedback");
    
    [btnRude setTitle:GET_Language_Value(@"rude") forState:UIControlStateNormal];
    [btnIndecent setTitle:GET_Language_Value(@"indecent") forState:UIControlStateNormal];
    [btnAnnoying setTitle:GET_Language_Value(@"annoying") forState:UIControlStateNormal];

    [btnYes setTitle:GET_Language_Value(@"yes") forState:UIControlStateNormal];
    [btnNo setTitle:GET_Language_Value(@"no") forState:UIControlStateNormal];

    symbolType = @"";
    wrongType = @"";
    isFavorite = @"0";
    isBlocked = @"0";
    btn1.tag = 101;
    
    NSString * imgUrl;
    NSString * gender;
    if (APP_DELEGATE.isUserModeDriver){
        lblDriverName.text = [NSString stringWithFormat:@"%@ %@",self.dictTripDetails[@"customer_first_name"], self.dictTripDetails[@"customer_last_name"]];
        imgUrl = self.dictTripDetails[@"customer_user_image"];
        gender = self.dictTripDetails[@"customer_gender"];
    } else {
        lblDriverName.text = [NSString stringWithFormat:@"%@ %@",self.dictTripDetails[@"driver_first_name"], self.dictTripDetails[@"driver_last_name"]];
        imgUrl = self.dictTripDetails[@"driver_user_image"];
        gender = self.dictTripDetails[@"driver_gender"];
    }

    UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
    
//    UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgvwUserPic.frame.size.width, imgvwUserPic.frame.size.height)];
    
    activity_indicator.tag = 11111;
    [[imgvwUserPic viewWithTag:1111] removeFromSuperview];
    
    [imgvwUserPic addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    imgUrl = [imgUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [imgvwUserPic sd_setImageWithURL:[NSURL URLWithString:imgUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [[imgvwUserPic viewWithTag:1111] removeFromSuperview];
    }];
    
    
    whatsWornViewHeight.constant = 0;
    blockUserViewHeight.constant = 50;
    [self.view layoutIfNeeded];
}

#pragma mark - Action methods -

- (IBAction)actionSendFeedback:(id)sender {
    
    NSLog(@"Experiance Type %@",symbolType);
    NSLog(@"Wrong Type %@",wrongType);
    NSLog(@"Set Favorite %@",isFavorite);
    NSLog(@"Set Block %@",isBlocked);
    
//    if ([symbolType isEqualToString:@""]){
//        [Singleton showSnakBar:@"Please select any one Emoji" multiline:false];
//    } else {
//        NSString * recieverId;
//        if (APP_DELEGATE.isUserModeDriver){
//            recieverId = self.dictTripDetails[@"customer_user_id"];
//        } else {
//            recieverId = self.dictTripDetails[@"driver_id"];
//        }
//        
//        START_HUD
//        [APP_DELEGATE.apiManager submitTheUserFeedbackWithReceiverId:recieverId RideId:self.dictTripDetails[@"ride_id"] RateSymbolType:symbolType WhatWrongType:wrongType FeedbackMessage:txtView.text isBlock:isBlocked isFavoriteUser:isFavorite withCallBack:^(BOOL success, NSString *serverMessange) {
//            STOP_HUD
//            if (success){
//                [APP_DELEGATE updateFavoriteUserListArray];
//                [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:^{
//                }];
//            } else {
//                [Singleton showSnakBar:serverMessange multiline:true];
//            }
//            
//            [APP_DELEGATE updateFavoriteUserListArray];
//        }];
//    }
    
    NSString * recieverId;
    if (APP_DELEGATE.isUserModeDriver){
        recieverId = self.dictTripDetails[@"customer_user_id"];
    } else {
        recieverId = self.dictTripDetails[@"driver_id"];
    }
    
    START_HUD
    [APP_DELEGATE.apiManager submitTheUserFeedbackWithReceiverId:recieverId RideId:self.dictTripDetails[@"ride_id"] RateSymbolType:symbolType WhatWrongType:wrongType FeedbackMessage:txtView.text isBlock:isBlocked isFavoriteUser:isFavorite withCallBack:^(BOOL success, NSString *serverMessange) {
        STOP_HUD
        if (success){
            [APP_DELEGATE updateFavoriteUserListArray];
            [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:^{
            }];
        } else {
            [Singleton showSnakBar:serverMessange multiline:true];
        }
        
        [APP_DELEGATE updateFavoriteUserListArray];
    }];
}

- (IBAction)actionFavoriteUser:(UIButton *)sender {
    if([sender tag] == 102) {
        btn1.tag = 101;
        [btn1 setImage:[UIImage imageNamed:@"PHFavoriteDeactive"] forState:UIControlStateNormal];
        isFavorite = @"0";
        blockUserViewHeight.constant = 50;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    } else {
        btn1.tag = 102;
        [btn1 setImage:[UIImage imageNamed:@"PHFavoriteActive"] forState:UIControlStateNormal];
        isFavorite = @"1";
        isBlocked = @"0";
        blockUserViewHeight.constant = 0;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (IBAction)actionReview:(id)sender {
    [btn2 setImage:[UIImage imageNamed:@"PHSymbolGreatDeactive"] forState:UIControlStateNormal];
    [btn3 setImage:[UIImage imageNamed:@"PHSymbolMediumDeactive"] forState:UIControlStateNormal];
    [btn4 setImage:[UIImage imageNamed:@"PHSymbolSadDeactive"] forState:UIControlStateNormal];
    
    if ([sender tag] == 101) {
        [btn2 setImage:[UIImage imageNamed:@"PHSymbolGreatActive"] forState:UIControlStateNormal];
        symbolType = @"1";
        wrongType = @"";
        whatsWornViewHeight.constant = 0;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
        
    } else if ([sender tag] == 102) {
        [btn3 setImage:[UIImage imageNamed:@"PHSymbolMediumActive"] forState:UIControlStateNormal];
        symbolType = @"2";
        wrongType = @"";
        whatsWornViewHeight.constant = 0;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
        
    } else if ([sender tag] == 103) {
        [btn4 setImage:[UIImage imageNamed:@"PHSymbolSadActive"] forState:UIControlStateNormal];
        symbolType = @"3";
        
        whatsWornViewHeight.constant = 50;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
}

- (IBAction)actionWhatsWrong:(id)sender {
    
    [btnRude setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnAnnoying setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnIndecent setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    
    if ([sender tag] == 101) {
        [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        wrongType = @"1";
    } else if ([sender tag] == 102) {
        [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        wrongType = @"2";
    } else if ([sender tag] == 103) {
        [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        wrongType = @"3";
    }
}

- (IBAction)actionBLock:(id)sender {
    [btnYes setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnNo setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    if ([sender tag] == 101) {
        [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        isBlocked = @"1";
    } else {
        [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        isBlocked = @"0";
    }
}

- (IBAction)actionClose:(id)sender {
//    [self.navigationController popToRootViewControllerAnimated:true];
    [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:^{
    }];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)textViewDidChange:(UITextView *)textView{
    if (textView.hasText){
        lblProvideFeedback.hidden = true;
    } else {
        lblProvideFeedback.hidden = false;
    }
}

@end
