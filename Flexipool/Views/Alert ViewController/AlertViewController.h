//
//  AlertViewController.h
//  Flexipool
//
//  Created by HimAnshu on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertViewControllerDelegate <NSObject>
@optional
-(void)alertControllerButtonTappedWithIndex:(NSInteger)index;
@end


@interface AlertViewController : UIViewController

@property id <AlertViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIView *viewAlert;
@property (weak, nonatomic) IBOutlet UILabel *lblAlertTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAlertMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (strong, nonatomic) IBOutlet UIButton *btnYesCenter;

@property (weak, nonatomic) IBOutlet UIButton *btnNo;

@property NSString * alertTitles;
@property NSString * alertMessages;
@property NSString * yesTitle;
@property NSString * noTitle;

- (IBAction)actionYes:(id)sender;
- (IBAction)actionNo:(id)sender;

@end
