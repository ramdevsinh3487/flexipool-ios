//
//  AlertViewController.m
//  Flexipool
//
//  Created by HimAnshu on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AlertViewController.h"

@interface AlertViewController ()

@end

@implementation AlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewAlert.alpha = 0;
    
    [self.lblAlertTitle setFont:[UIFont fontWithName:AppFont_Regular size:POPUP_HEADER_FONTS_SIZE - 5]];
    [self.lblAlertMessage setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    
    [self.btnYes.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [self.btnNo.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    [_btnYes setTitle:self.yesTitle forState:UIControlStateNormal];
    [_btnYesCenter setTitle:self.yesTitle forState:UIControlStateNormal];
    [_btnNo setTitle:self.noTitle forState:UIControlStateNormal];
    
    if ([self.noTitle isEqualToString:@""]){
        self.btnYes.hidden = true;
        self.btnNo.hidden = true;
        self.btnYesCenter.hidden = false;
        //Show Sigle Button
    } else {
        self.btnYes.hidden = false;
        self.btnNo.hidden = false;
        self.btnYesCenter.hidden = true;
        //Hide Sigle Button
    }
    
    self.lblAlertTitle.text = self.alertTitles;
    self.lblAlertMessage.text = self.alertMessages;
    
    if ([[self.alertMessages lowercaseString]containsString:@"terms"] && [[self.alertMessages lowercaseString]containsString:@"condition"]){
        NSMutableAttributedString* mutableTitle = [[NSMutableAttributedString alloc] initWithString:self.alertMessages];
        [mutableTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE-1] range:[self.alertMessages rangeOfString:@"terms and conditions"]];
        [mutableTitle addAttribute:NSForegroundColorAttributeName value:APP_BLUE_COLOR range:[self.alertMessages rangeOfString:@"terms and conditions"]];
        [self.lblAlertMessage setAttributedText:mutableTitle];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    
    self.viewAlert.alpha = 1;
    self.viewAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.3/2 animations:^{
        self.viewAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            self.viewAlert.transform = CGAffineTransformIdentity;
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)actionYes:(id)sender {
    
    self.viewAlert.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.3/2 animations:^{
        self.viewAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            self.viewAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        } completion:^(BOOL finished) {
            
            [self dismissViewControllerAnimated:false completion:^{
                [self.delegate alertControllerButtonTappedWithIndex:1];
            }];
        }];
    }];
}

- (IBAction)actionNo:(id)sender {
    
    self.viewAlert.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.3/2 animations:^{
        self.viewAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            self.viewAlert.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        } completion:^(BOOL finished) {
            
            [self dismissViewControllerAnimated:false completion:^{
                [self.delegate alertControllerButtonTappedWithIndex:2];
            }];
        }];
    }];
}
@end
