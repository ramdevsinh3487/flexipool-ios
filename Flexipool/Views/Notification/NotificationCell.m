//
//  NotificationCell.m
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "NotificationCell.h"

@implementation NotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
     [self.lblName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+1]];
     [self.lblPickupLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-4]];
     [self.lblDropLocation setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-4]];
     [self.lblDate setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [self.lblRequestigRide setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-4]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

@end
