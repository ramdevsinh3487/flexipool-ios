//
//  NotificationCell.h
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblName;

@property (weak, nonatomic) IBOutlet UILabel *lblDropLocation;
@property (weak, nonatomic) IBOutlet UILabel *lblRequestigRide;
@property (weak, nonatomic) IBOutlet UILabel *lblPickupLocation;
@property (strong, nonatomic) IBOutlet UIButton *imgStatusArrow;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *nameLableTopSpace;

@property (strong, nonatomic) IBOutlet UIImageView *imgPickupPin;
@property (strong, nonatomic) IBOutlet UIImageView *imgDestPin;

@end
