//
//  Notification.h
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BroadcastDetailsView.h"

@interface Notification : UIViewController<UITableViewDelegate,UITableViewDataSource, AlertViewControllerDelegate>
{
    
    IBOutlet UILabel *lblDataMessage;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UILabel *lblHeader;
    
    BOOL isLoadingData;
    NSMutableArray * arrayNotifications;
    int retryCount;
    
    
    int totalCounts, pageNumber;
}

@end
