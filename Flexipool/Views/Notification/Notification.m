//
//  Notification.m
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Notification.h"
#import "NotificationCell.h"
@interface Notification ()

@end

@implementation Notification

- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    // Do any additional setup after loading the view.
    
    
    
    
    lblHeader.text = GET_Language_Value(@"notification");
    [lblDataMessage setFont:[UIFont fontWithName:AppFont_Regular size:POPUP_HEADER_FONTS_SIZE]];
    retryCount = 0;
    tblView.tableFooterView = [UIView new];
    
    pageNumber = 1;
    totalCounts = 0;
    [self reloadNotificationList];
    
    if ([APP_DELEGATE.badgeValue doubleValue] > 0){
        [APP_DELEGATE.apiManager clearTheNotificationBadgeWithCallBack:^(BOOL success, NSString *serverMessange) {
            if (DEBUG_MODE){
                NSLog(@"Message : %@",serverMessange);
                APP_DELEGATE.badgeValue = @"0";
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_BADGE_COUNT_UPDATED object:nil];
            }
        }];
    }
}



-(void)reloadNotificationList {
    if (totalCounts && totalCounts > 0){
        START_HUD
    } else {
        arrayNotifications = [[NSMutableArray alloc]init];
        [tblView reloadData];
    }
    
    isLoadingData = true;
    retryCount = retryCount + 1;
    [APP_DELEGATE.apiManager getMyAllNotificationsListWithPageNumber:[NSString stringWithFormat:@"%d",pageNumber] WithCallBack:^(BOOL success, NSArray *arrayResponse, int totalRecords, NSString *serverMessange) {
        if (success){
            if (totalCounts && totalCounts > 0){
                [arrayNotifications addObjectsFromArray:arrayResponse];
                STOP_HUD
            } else {
                arrayNotifications = [[NSMutableArray alloc]initWithArray:arrayResponse];
                totalCounts = totalRecords;
            }
            
            pageNumber = pageNumber + 1;
            
            isLoadingData = false;
            [tblView reloadData];
            retryCount = 0;
        } else {
            
            if (totalCounts && totalCounts > 0){
                STOP_HUD
            }
            
            if (retryCount >= 3){
                isLoadingData = false;
                totalCounts = (int)arrayNotifications.count;
                [tblView reloadData];
            } else {
                [self reloadNotificationList];
            }
        }
    }];
}

#pragma mark - UITableView Datasource Method -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isLoadingData){
        tblView.hidden = true;
        lblDataMessage.hidden = false;
        lblDataMessage.text = @"Loading data, Please wait...";
        return 0;
    } else {
        if (arrayNotifications.count > 0){
            tblView.hidden = false;
            lblDataMessage.hidden = true;
            return arrayNotifications.count;
        } else {
            tblView.hidden = true;
            lblDataMessage.hidden = false;
            lblDataMessage.text = @"No data found";
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * dictNotification = arrayNotifications[indexPath.row];
    
    static NSString *simpleTableIdentifier;
    
    simpleTableIdentifier = @"NotificationCell";
    static NSString *broadcastTableIdentifier;
    broadcastTableIdentifier = @"NotificationCellBroadcast";
    
    NotificationCell *cell;
    if ([dictNotification[@"isBroadcast"] isEqualToString:@"true"]){
        cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:broadcastTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:broadcastTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
    } else {
        cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
            cell = [nib objectAtIndex:0];
            
        }
    }
    
    
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.lblDate.layer.cornerRadius = cell.lblDate.frame.size.width/2;
    cell.lblDate.layer.masksToBounds = YES;
    
    cell.lblDate.font = [UIFont fontWithName:AppFont_Regular size:(TEXTFIELD_FONTS_SIZE - 4)];
    cell.lblName.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE];
    cell.lblPickupLocation.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 5];
    cell.lblDropLocation.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 5];
    cell.lblRequestigRide.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 5];
    
    NSString * dateOfRide = [NSString stringWithFormat:@"%@",dictNotification[@"date"]];
    NSDateFormatter * dateFormater = [[NSDateFormatter alloc]init];
    dateFormater.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate * convertedDate = [dateFormater dateFromString:dateOfRide];
    dateFormater.dateFormat = @"dd MMM hh:mm a";
    cell.lblDate.text = [dateFormater stringFromDate:convertedDate];
    
    if ([dictNotification[@"isBroadcast"] isEqualToString:@"true"]){
     //   cell.lblDate.text = [NSString stringWithFormat:@"%@",dictNotification[@"date"]];
        cell.lblName.text = [NSString stringWithFormat:@"%@",dictNotification[@"broadcastMsg"]];
        cell.lblPickupLocation.text = @"";
        cell.lblDropLocation.text =@"";
        cell.lblRequestigRide.text = @"";
        cell.nameLableTopSpace.constant = 22;
        cell.imgStatusArrow.hidden = true;
        cell.imgDestPin.hidden = true;
        cell.imgPickupPin.hidden = true;
    } else {
        NSString * userNam = @"";
        if (![dictNotification[@"to_user_id"] isEqualToString:APP_DELEGATE.loggedUserId]){
            userNam = dictNotification[@"to_user_name"];
        } else {
            userNam = dictNotification[@"user_name"];
        }
        
       // cell.lblDate.text = [NSString stringWithFormat:@"%@",dictNotification[@"date"]];
        
        

       
       // dateFormater.dateFormat = @"HH:mm";
        
        cell.lblName.text = [NSString stringWithFormat:@"%@",userNam];
        cell.lblPickupLocation.text = [NSString stringWithFormat:@"%@",dictNotification[@"pickup_location"]];
        cell.lblDropLocation.text = [NSString stringWithFormat:@"%@",dictNotification[@"destination_location"]];
        cell.lblRequestigRide.text = [NSString stringWithFormat:@"%@",dictNotification[@"request_type"]];
        cell.nameLableTopSpace.constant = 8.0;
        if (![dictNotification[@"to_user_id"] isEqualToString:APP_DELEGATE.loggedUserId]){
            //Send Request
            [cell.imgStatusArrow setImage:[UIImage imageNamed:@"right-arrow"] forState:UIControlStateNormal];
        } else {
            //Incoming Request
            [cell.imgStatusArrow setImage:[UIImage imageNamed:@"left-arrow"] forState:UIControlStateNormal];
        }
        
        cell.imgStatusArrow.hidden = false;
        cell.imgDestPin.hidden = false;
        cell.imgPickupPin.hidden = false;
    }
    
    if (totalCounts > arrayNotifications.count) {
        if (indexPath.row == arrayNotifications.count - 1 && isLoadingData == false){
            [self reloadNotificationList];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary * dictNotification = arrayNotifications[indexPath.row];
    if ([dictNotification[@"isBroadcast"] isEqualToString:@"true"]){
        BroadcastDetailsView *obj = (BroadcastDetailsView *)[[self storyboard]instantiateViewControllerWithIdentifier:@"BroadcastDetailsView"];
        obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
        
//        NSDateFormatter *f = [[NSDateFormatter alloc] init];
//        [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//        NSString * todayDate = [f stringFromDate:[NSDate date]];
        
        obj.dictBroadcastData = @{
                                  @"title":@"Broadcast",
                                  @"details":dictNotification[@"broadcastMsg"],
                                  @"date":dictNotification[@"date"]
                                  };
        [self.navigationController pushViewController:obj animated:true];
    } else {
        if ([dictNotification[@"requestee_id"] isEqualToString:APP_DELEGATE.loggedUserId]){
            //Requested by me
            PassengerRequestViewController *obj = (PassengerRequestViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"PassengerRequestViewController"];
            obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
            obj.dictTripInfo = dictNotification;
            obj.canPerformAciton = false;
            
            [[APP_DELEGATE.navigationController.viewControllers firstObject] presentViewController:obj animated:NO completion:nil];
        } else {
            if ([dictNotification[@"ride_status"]doubleValue] == 0){
                [APP_DELEGATE.arrayHomeScreenStatus replaceObjectAtIndex:0 withObject:@"0"];
                [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RIDE_STATUS_UPDATED object:nil];
            } else {
                PassengerRequestViewController *obj = (PassengerRequestViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"PassengerRequestViewController"];
                obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
                obj.dictTripInfo = dictNotification;
                obj.canPerformAciton = false;
                
                [[APP_DELEGATE.navigationController.viewControllers firstObject] presentViewController:obj animated:NO completion:nil];
            }
        }
    }
}

-(void)alertControllerButtonTappedWithIndex:(NSInteger)index{
    
}

#pragma mark - action methods -
-(IBAction)actionMenu:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
