//
//  ShareMyRideCell.m
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "ShareMyRideCell.h"

@implementation ShareMyRideCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.lblName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [self.lblPhoneNum setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
