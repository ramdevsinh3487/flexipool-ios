//
//  ShareMyRideVC.h
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareMyRideVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{

    __weak IBOutlet UILabel *lblHeader;
    __weak IBOutlet UITableView *tblView;
    __weak IBOutlet UISearchBar *SearchRide;
    IBOutlet UILabel *lblMessage;
    
    NSMutableArray *arrContactList,*arrSkypeCOntacts,*arrSearchResult;
    CGRect keyboardFrameBeginRect;
    BOOL isFiltered, isLoadingData;
    IBOutlet UIButton *btnNotifications;
}

@end
