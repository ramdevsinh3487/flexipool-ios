//
//  ShareMyRideVC.m
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "ShareMyRideVC.h"
#import "ShareMyRideCell.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <ContactsWrapper/ContactsWrapper.h>


@interface ShareMyRideVC ()

@end

@implementation ShareMyRideVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    
    tblView.tableFooterView = [UIView new];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(onKeyboardHide:) name:UIKeyboardWillHideNotification object:nil];

    
    arrContactList = [[NSMutableArray alloc] init];
    arrSkypeCOntacts = [[NSMutableArray alloc] init];
    
    
    [lblMessage setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 5]];
    lblHeader.text = GET_Language_Value(@"share_myRide_info");
    lblMessage.text = GET_Language_Value(@"share_ride_message_loading_contacts");
    
    SearchRide.hidden = YES;
    
    tblView.delegate = self;
    tblView.dataSource  = self;
    
    tblView.hidden = false;
    isLoadingData = true;
    [tblView reloadData];
    if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
        SearchRide.hidden = NO;
        CNEntityType entityType = CNEntityTypeContacts;
        if( [CNContactStore authorizationStatusForEntityType:entityType] == CNAuthorizationStatusNotDetermined)
        {
            CNContactStore * contactStore = [[CNContactStore alloc] init];
            [contactStore requestAccessForEntityType:entityType completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if(granted){
                    [self getContactListAndLoadTableView];
                } else {
                    [self showAlertForGrantPermission];
                }
            }];        }
        else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusAuthorized)
        {
            [self getContactListAndLoadTableView];
        } else if( [CNContactStore authorizationStatusForEntityType:entityType]== CNAuthorizationStatusDenied)
        {
            [self showAlertForGrantPermission];
        }
    } else {
        tblView.hidden = false;
        isLoadingData = false;
        [tblView reloadData];
    }
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
}




-(void)showAlertForGrantPermission {
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"Contacts Read Permission denide" message:@"Please turn on contact action from settings" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction * settingsAction = [UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }];
    
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popViewControllerAnimated:true];
    }];
    
    [alertController addAction:settingsAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:true completion:nil];
}

-(NSString *)generateRandomStringIsPhone:(BOOL)isPhone {
    
    NSString *letters;
    int len=2;
    if (isPhone){
        len = 10;
        letters = @"1234567890";
    } else {
        len = 10;
        letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";
    }
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform((int)[letters length])]];
    }
    
    return randomString;
}
-(void)getContactListAndLoadTableView{

    [[ContactsWrapper sharedInstance] getContactsWithContainerId:nil completionBlock:^(NSArray<CNContact *> * _Nullable contacts, NSError * _Nullable error) {
        if (contacts)
        {
            NSMutableArray * dummyData = [[NSMutableArray alloc]init];
            arrContactList = [NSMutableArray arrayWithArray:contacts];
            for(int i = 0 ; i < arrContactList.count ; i++)
            {
                CNContact *contact = arrContactList[i];
                
                CNLabeledValue *phoneNumberValue = contact.phoneNumbers.firstObject;
                CNPhoneNumber *phoneNumber = phoneNumberValue.value;
                NSString *phoneNumberString = phoneNumber.stringValue;
                
                if(!phoneNumberString || [phoneNumberString length] <= 0)
                {
                    [arrSkypeCOntacts addObject:[arrContactList objectAtIndex:i]];
                    [arrContactList removeObjectAtIndex:i];
                    
                } else {
                    NSString * famelyName = contact.familyName;
                    NSString * GivenName = contact.givenName;
                    
                    NSString * finalName = @"";
                    
                    if (famelyName && ![famelyName isEqualToString:@""]) {
                        finalName = famelyName;
                    }
                    
                    if (GivenName && ![GivenName isEqualToString:@""]) {
                        if (finalName && ![finalName isEqualToString:@""]) {
                            finalName = [NSString stringWithFormat:@"%@ %@", finalName, GivenName];
                        } else {
                            finalName = GivenName;
                        }
                    }
                    
                    NSData * imageData = contact.imageData;
                    [dummyData addObject:@{
                                           @"full_name":finalName,
                                           @"phone_number":phoneNumberString,
                                           @"image": !imageData ? @"" : imageData,
                                           @"contact":contact
                                           }];
                }
            }
            
            if (DEBUG_MODE){
                NSLog(@"%@",arrSkypeCOntacts);
            }
            
            arrContactList = [[NSMutableArray alloc]initWithArray:dummyData];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //Your main thread code goes in here
                
                NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:CNContactFamilyNameKey ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                
                NSSortDescriptor* sortDescriptor1 = [NSSortDescriptor sortDescriptorWithKey:@"full_name" ascending:true comparator:^NSComparisonResult(NSString * obj1, NSString * obj2) {
                    NSString* firstStringToCompare = [[obj1 stringByReplacingOccurrencesOfString:@" " withString:@""]lowercaseString]
                    ;
                    NSString* secondStringToCompare = [[obj2 stringByReplacingOccurrencesOfString:@" " withString:@""]lowercaseString];
                    
                    return [firstStringToCompare compare:secondStringToCompare];
                }];
        
                NSArray* sortedArray = [arrContactList sortedArrayUsingDescriptors:@[sortDescriptor1, sortDescriptor]];
                arrContactList = [[NSMutableArray alloc]initWithArray:sortedArray];
                isLoadingData = false;
                [tblView reloadData];
            });
        }
        else
        {
            NSLog(@"%@", error.localizedDescription);
            isLoadingData = false;
            [tblView reloadData];
        }
    }];
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotifications setShouldAnimateBadge:true];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}

#pragma mark - Custom Methods -
-(void)onKeyboardHide:(NSNotification *)notification
{
    [UIView animateWithDuration:1.0 animations:^{
    } completion:^(BOOL finished) {
        tblView.frame = CGRectMake(0, 108, SCREEN_WIDTH,SCREEN_HEIGHT - 108);
    }];
    
    
}
- (void)myNotificationMethod:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameBeginUserInfoKey];
    keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    
    [UIView animateWithDuration:3.0 animations:^{
    } completion:^(BOOL finished) {
        tblView.frame = CGRectMake(0, 108, SCREEN_WIDTH,SCREEN_HEIGHT  - keyboardFrameBeginRect.size.height - 108	);
    }];
    
    
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (searchText.length == 0) {
        
        isFiltered = NO;
        
    }else{
        
        isFiltered = YES;
        
        arrSearchResult = [NSMutableArray array];
        
        for (int i = 0; i < arrContactList.count; i ++) {
            
            NSDictionary *contact = arrContactList[i];
            NSRange dicsRange  = [contact[@"full_name"] rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange dicsRange1  = [contact[@"phone_number"] rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (dicsRange.location != NSNotFound || dicsRange1.location != NSNotFound) {
                [arrSearchResult addObject:contact];
            }
        }
    }
    
    [tblView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    // viewSearch.frame = CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT - 64 - 220 - 44);
    tblView.frame = CGRectMake(0, 108 , SCREEN_WIDTH, SCREEN_HEIGHT - 64);
    [searchBar resignFirstResponder];
    
    searchBar.text = @"";
    searchBar.placeholder = @"Search";
    isFiltered = NO;
    [tblView reloadData];
    
}

#pragma mark - action methods -

- (IBAction)actionMenu:(id)sender {
    
    [APP_DELEGATE showMenu];
}


#pragma mark - UITableView Datasource Method -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isLoadingData){
        lblMessage.text = GET_Language_Value(@"share_ride_message_loading_contacts");
        tblView.hidden = true;
        return 0;
    } else {
        if (isFiltered) {
            if (arrSearchResult.count <= 0){
                lblMessage.text = GET_Language_Value(@"share_ride_message_no_contact");
                tblView.hidden = true;
                return 0;
            } else {
                tblView.hidden = false;
                return arrSearchResult.count;
            }
        }
        else {
            if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING){
                if (arrContactList.count <= 0){
                    lblMessage.text = GET_Language_Value(@"share_ride_message_no_contact");
                    tblView.hidden = true;
                    return 0;
                } else {
                    tblView.hidden = false;
                    return arrContactList.count;
                }
                return arrContactList.count;
            } else {
                lblMessage.text = GET_Language_Value(@"share_ride_message_no_ride");
                tblView.hidden = true;
                return 0;
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    
//    if(isFiltered)
//    {
//
//        if (arrSearchResult.count <= 0) {
//            
//            CUSTOM_NO_DATA_CELL;
//        }
//    }
//    else
//    {
//        
//        if (arrContactList.count <= 0) {
//            
//            CUSTOM_NO_DATA_CELL;
//        }
//    }
    
    static NSString *simpleTableIdentifier;
    
    simpleTableIdentifier = @"ShareMyRideCell";
    
    ShareMyRideCell *cell = (ShareMyRideCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
        cell = [nib objectAtIndex:0];
        
        
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if(isFiltered)
    {
        /*full_name
        phone_number
        image
        contact*/
        
        NSDictionary * dictData = arrSearchResult[indexPath.row];
        cell.lblName.text = dictData[@"full_name"];
        cell.lblPhoneNum.text = dictData[@"phone_number"];
        
        if (dictData[@"image"] && [dictData[@"image"] isKindOfClass:[NSData class]]){
            cell.imgUser.image = [UIImage imageWithData:dictData[@"image"]];
        }
    }
    else
    {
        NSDictionary * dictData = arrContactList[indexPath.row];
        cell.lblName.text = dictData[@"full_name"];
        cell.lblPhoneNum.text = dictData[@"phone_number"];
        
        if (dictData[@"image"] && [dictData[@"image"] isKindOfClass:[NSData class]]){
            cell.imgUser.image = [UIImage imageWithData:dictData[@"image"]];
        }
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.view endEditing:true];
    
    CNContact *contact;
    if (isFiltered){
        contact = arrSearchResult[indexPath.row][@"contact"];
    } else {
        contact = arrContactList[indexPath.row][@"contact"];
    }
    
    CNLabeledValue *phoneNumberValue = contact.phoneNumbers.firstObject;
    CNPhoneNumber *phoneNumber = phoneNumberValue.value;
    NSString *phoneNumberString = phoneNumber.stringValue;
    NSString * personName = [NSString stringWithFormat:@"%@ %@",contact.familyName, contact.givenName];
    NSString * alertMessage = [NSString stringWithFormat:@"%@ \n %@",personName, phoneNumberString];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Share My Ride Info" message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        START_HUD
        [APP_DELEGATE.apiManager didShareMyRideInfoWithRideId:APP_DELEGATE.runningRideId ContactNumber:phoneNumberString withCallBack:^(BOOL success, NSString *serverMessage) {
            if (success){
                [Singleton showSnakBar:@"Successfully shared Ride Info" multiline:true];
            } else {
                [Singleton showSnakBar:@"Unable to share" multiline:true];
            }
            STOP_HUD
        }];
    }];
    
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
    }];
    
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self presentViewController:alert animated:true completion:nil];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
