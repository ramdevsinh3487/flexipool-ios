//
//  Settings.h
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Settings : UIViewController
{
    IBOutlet UILabel *lblHeader;
    IBOutlet UILabel *lblNotification;
    
    IBOutlet UIButton *btnNotify1;
    IBOutlet UILabel *lblNotify1;
    
    IBOutlet UIButton *btnNotify2;
    IBOutlet UILabel *lblNotify2;
    
    IBOutlet UIButton *btnNotify3;
    IBOutlet UILabel *lblNotify3;
    
    IBOutlet UILabel *lblTurnOffProfile;
    IBOutlet UIButton *btnUpdate;
    IBOutlet UIButton *btnShowNotification;
    IBOutlet UILabel *lblProfileSetting;
    IBOutlet UIButton *btn1Month;
    
    IBOutlet UIButton *btn1Week;
    IBOutlet UIButton *btnNone;
    IBOutlet UIButton *btnNotifications;
    
    NSString * offProfileDate, *offProfileType;
    NSString * notificationType1, *notificationType2, *notificationType3, *grandNotificationType, *isNotificationsOn;
    IBOutlet UIButton *btnChangePassword;
}

- (IBAction)ActionChangePassword:(id)sender;

- (IBAction)actionMenu:(id)sender;


@end
