//
//  Settings.m
//  Flexipool
//
//  Created by Nick on 14/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Settings.h"
#import "ChangePassword.h"


@interface Settings ()

@end

@implementation Settings
#pragma mark - viewDidLoad -
- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    
    [self setupInitialView];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
   
    START_HUD
    [APP_DELEGATE.apiManager viewAllMySettingsWithCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
        STOP_HUD
        if (success){
            [self updateUIAccordingaToSettingsDictionary:dictResponse];
        } else {
            [self updateUIAccordingaToSettingsDictionary:APP_DELEGATE.dictUserDetails];
        }
    }];
}




-(void)updateUIAccordingaToSettingsDictionary:(NSDictionary *)dictResponse {
    notificationType1 = [NSString stringWithFormat:@"%@",dictResponse[@"when_app_has_new_feature"]];
    notificationType2 = [NSString stringWithFormat:@"%@",dictResponse[@"when_new_driver_join"]];
    notificationType3 = [NSString stringWithFormat:@"%@",dictResponse[@"when_fav_driver_riding"]];
    
    isNotificationsOn = [NSString stringWithFormat:@"%@",dictResponse[@"is_notification_on"]];
    
    offProfileType = [NSString stringWithFormat:@"%@",dictResponse[@"off_profile_type"]];
    offProfileDate = [NSString stringWithFormat:@"%@",dictResponse[@"expiry_date"]];
    
    btnNotify1.tag = [notificationType1 isEqualToString:@"1"] ? 101 : 102;
    btnNotify2.tag = [notificationType2 isEqualToString:@"1"] ? 201 : 202;
    btnNotify3.tag = [notificationType3 isEqualToString:@"1"] ? 301 : 302;
    
    btnShowNotification.tag = [isNotificationsOn isEqualToString:@"1"] ? 101 : 102;
    
    [self notificationTypeChanged:btnNotify1];
    [self notificationTypeChanged:btnNotify2];
    [self notificationTypeChanged:btnNotify3];
    [self showNotificationsChanged:btnShowNotification];
    
    if ([offProfileType isEqualToString:@"1"]){
        [self turnOffProfileChanged:btn1Week];
    } else if ([offProfileType isEqualToString:@"2"]){
        [self turnOffProfileChanged:btn1Month];
    } else {
        [self turnOffProfileChanged:btnNone];
    }
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotifications setShouldAnimateBadge:true];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
}
- (IBAction)notificationAction:(id)sender {
    [APP_DELEGATE NavigateToNotification];
}
- (void) setupInitialView {
    
    
    [lblNotification setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [lblProfileSetting setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    [lblTurnOffProfile setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    
    [btnNotify1.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [lblNotify1 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [lblNotify2 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [lblNotify3 setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnNotify3.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
     [btn1Month.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
     [btn1Week.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnShowNotification.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 2]];

    [btnUpdate.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
   
     [btnChangePassword.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    
    
    lblNotify1.text = GET_Language_Value(@"when_app_has_newFeatures");
    lblNotify2.text = @"When a new request posted which matches your current ride request";
    lblNotify3.text = GET_Language_Value(@"when_favourite_driverDriving");
    
    lblHeader.text = GET_Language_Value(@"settings");
    lblNotification.text = [NSString stringWithFormat:@"   %@",GET_Language_Value(@"notification")];
    lblProfileSetting.text = [NSString stringWithFormat:@"   %@",GET_Language_Value(@"profile_setting")];
    lblTurnOffProfile.text = GET_Language_Value(@"turnOff_Profile");
    
    [btn1Week setTitle:[NSString stringWithFormat:@"%@",GET_Language_Value(@"1week")] forState:UIControlStateNormal];
    [btn1Month setTitle:[NSString stringWithFormat:@"%@",GET_Language_Value(@"1month")] forState:UIControlStateNormal];
    [btnShowNotification setTitle:[NSString stringWithFormat:@"%@",GET_Language_Value(@"show_notification")] forState:UIControlStateNormal];
    [btnUpdate setTitle:GET_Language_Value(@"update") forState:UIControlStateNormal];
}

#pragma mark - Action methods -
- (IBAction)actionUpdate:(id)sender {
    START_HUD
    [APP_DELEGATE.apiManager updateMyAllSettingsWithOffProfileDate:offProfileDate notificationType:grandNotificationType isNotificationOn:isNotificationsOn OffProfileType:offProfileType withCallBack:^(BOOL success, NSString *serverMessange) {
        STOP_HUD
        if (success){
            [Singleton showSnakBar:@"Updated successfully" multiline:true];
            NSMutableDictionary * dictUserData = [[NSMutableDictionary alloc]initWithDictionary:APP_DELEGATE.dictUserDetails];
            [dictUserData setObject:[notificationType1 stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"when_app_has_new_feature"];
            [dictUserData setObject:[notificationType2 stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"when_new_driver_join"];
            [dictUserData setObject:[notificationType3 stringByReplacingOccurrencesOfString:@"," withString:@""] forKey:@"when_fav_driver_riding"];
            
            [dictUserData setObject:isNotificationsOn forKey:@"is_notification_on"];
            [dictUserData setObject:offProfileType forKey:@"off_profile_type"];
            APP_DELEGATE.dictUserDetails = dictUserData;
            [APP_DELEGATE.navigationController popViewControllerAnimated:true];
        } else {
            SHOW_ALERT_WITH_CAUTION(serverMessange);
        }
    }];
}

- (IBAction)notificationTypeChanged:(UIButton *)sender {
    if (sender.tag == 101){
        notificationType1 = @"1,";
        sender.tag = 102;
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else if (sender.tag == 102){
        notificationType1 = @"";
        sender.tag = 101;
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    } else if (sender.tag == 201){
        notificationType2 = @"2,";
        sender.tag = 202;
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else if (sender.tag == 202){
        notificationType2 = @"";
        sender.tag = 201;
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    } else if (sender.tag == 301){
        notificationType3 = @"3,";
        sender.tag = 302;
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else if (sender.tag == 302){
        notificationType3 = @"";
        sender.tag = 301;
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    }

    
    grandNotificationType = [NSString stringWithFormat:@"%@%@%@",notificationType1, notificationType2, notificationType3];
    
    if ([grandNotificationType length] > 0) {
        grandNotificationType = [grandNotificationType substringToIndex:[grandNotificationType length] - 1];
    }
    
    if(DEBUG_MODE){
        NSLog(@"Notification Types : %@",grandNotificationType);
    }
}

- (IBAction)turnOffProfileChanged:(UIButton *)sender {
    NSDateFormatter * dateFormate = [[NSDateFormatter alloc]init];
    dateFormate.dateFormat = @"yyyy-MM-dd";
    
    if (sender.tag == 101){
        offProfileType = @"1";
        
        NSDateComponents *comp = [NSDateComponents new];
        comp.day = 7;
        NSDate *date = [[NSCalendar currentCalendar] dateByAddingComponents:comp toDate:[NSDate date] options:0];
        offProfileDate = [dateFormate stringFromDate:date];
        
    } else if (sender.tag == 102){
        offProfileType = @"2";
        
        NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
        [dateComponents setMonth:1];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
        offProfileDate = [dateFormate stringFromDate:newDate];
    } else if (sender.tag == 103){
        offProfileType = @"";
        offProfileDate = @"";
    }
    
    if (DEBUG_MODE){
        NSLog(@"Off Profile Type : %@",offProfileType);
        NSLog(@"Off Profile Date : %@", offProfileDate);
    }
    
    [btn1Week setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btn1Month setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnNone setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
}

- (IBAction)showNotificationsChanged:(UIButton *)sender {
    if (sender.tag == 101){
        sender.tag = 102;
        isNotificationsOn = @"1";
         [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else {
        isNotificationsOn = @"0";
        sender.tag = 101;
         [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    }
}

#pragma mark - Action Methods -

- (IBAction)ActionChangePassword:(id)sender {
    
    ChangePassword *obj = (ChangePassword *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"ChangePassword"];
    obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [APP_DELEGATE.navigationController presentViewController:obj animated:true completion:nil];
    
}

- (IBAction)actionMenu:(id)sender {
    [APP_DELEGATE showMenu];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
