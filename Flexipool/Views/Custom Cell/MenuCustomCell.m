//
//  MenuCustomCell.m
//  One Touch
//
//  Created by Vishal Dhamecha on 12/04/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "MenuCustomCell.h"

@implementation MenuCustomCell
@synthesize lblTitle;

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
