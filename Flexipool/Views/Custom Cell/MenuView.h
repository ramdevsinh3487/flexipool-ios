//
//  MenuView.h
//  Flexipool
//
//  Created by Vishal Gohil on 09/11/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuView : UIView
@property (strong, nonatomic) IBOutlet UIImageView *imgUserProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblUserProfession;
@property (strong, nonatomic) IBOutlet UIButton *btnChangePassword;
@property (strong, nonatomic) IBOutlet UILabel *lblWalletAmount;
@property (strong, nonatomic) IBOutlet UIButton *btnWallet;
@property (strong, nonatomic) IBOutlet UITableView *tblMenuOptions;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightTblMenuView;
@property (strong, nonatomic) IBOutlet UIScrollView *scroolView;
@property (strong, nonatomic) IBOutlet UIButton *btnEditProfile;
@property (strong, nonatomic) IBOutlet UIView *viewMenuView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthMenuView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *leftSpaceMenuView;
@end
