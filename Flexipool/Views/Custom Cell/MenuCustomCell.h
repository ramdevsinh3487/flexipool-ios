//
//  MenuCustomCell.h
//  One Touch
//
//  Created by Vishal Dhamecha on 12/04/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCustomCell : UITableViewCell


@property(nonatomic,retain)IBOutlet UILabel *lblTitle;
@property(nonatomic,retain)IBOutlet UILabel *lblCount;
@property (weak, nonatomic) IBOutlet UIImageView *imgSeperator;

@property(nonatomic,retain)IBOutlet UIImageView *imgMenu;
@end
