//
//  ResearchLibraryDetail.m
//  Investment
//
//  Created by Manish on 21/08/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "BroadcastDetailsView.h"

@interface BroadcastDetailsView ()

@end

@implementation BroadcastDetailsView

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialView];
    
    lblTitle.text= self.dictBroadcastData[@"title"];
    NSString *start = [NSString stringWithFormat:@"%@",self.dictBroadcastData[@"date_added"]];
    
    
//    NSDateFormatter *f = [[NSDateFormatter alloc] init];
//    [f setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSDate *startDate = [f dateFromString:start];
//    NSString *str= [Singleton daySuffixForDate:startDate];
    
    lblDays.text=[NSString stringWithFormat:@"%@",self.dictBroadcastData[@"date"]];
    lblDescription.text=self.dictBroadcastData[@"details"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupInitialView {
    [lblHeader setFont:[UIFont fontWithName:AppFont_Semibold size:HEADER_FONTS_SIZE]];
    [lblDays setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+1]];
    [lblTitle setFont:[UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE+3]];
    [lblDescription setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+1]];
}

#pragma mark - back action

- (IBAction)btnBack:(id)sender {
    backAction()
}

#pragma mark - Status bar

-(UIStatusBarStyle)preferredStatusBarStyle{
    
    return UIStatusBarStyleLightContent;
}

@end
