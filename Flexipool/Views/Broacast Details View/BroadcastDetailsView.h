//
//  ResearchLibraryDetail.h
//  Investment
//
//  Created by Manish on 21/08/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HTMLLabel.h"
@interface BroadcastDetailsView : UIViewController
{
    
    IBOutlet UILabel *lblHeader;
    
    IBOutlet UILabel *lblTitle;
    
    
    IBOutlet UILabel *lblDays;
    
    IBOutlet HTMLLabel *lblDescription;
}
- (IBAction)btnBack:(id)sender;

@property(strong,nonatomic)NSDictionary *dictBroadcastData;
@end
