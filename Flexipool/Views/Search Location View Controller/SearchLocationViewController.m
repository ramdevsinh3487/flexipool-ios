//
//  SearchLocationViewController.m
//  Flexipool
//
//  Created by Nick on 15/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "SearchLocationViewController.h"
#import "FavouriteSpots.h"
@interface SearchLocationViewController () <FavouriteSpotsDelegate>

@end

@implementation SearchLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:true];
    
    isAnimationStarted = false;
    isAddressFromPlacePicker = false;
    [self loadMapView];
    [self manageViewInitially];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.lableFavoriteListTitle.text = @"List Favorite";
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

//-(UIStatusBarStyle)preferredStatusBarStyle {
//    return UIStatusBarStyleLightContent;
//}

- (IBAction)trackMyLOcationTApped:(id)sender {
    if ([APP_DELEGATE CheckLocationPermissionWithSourceView:self]){
        [self.viewGMSMapView animateToLocation: APP_DELEGATE.userLocation];
    }
}

- (IBAction)cancelButtonTappedAciton:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_REQUEST){
            [APP_DELEGATE updateHomeStatusArrayWithIndex:@""];
        }
    }];
}

#pragma mark - GoogleSDKSection
#pragma mark ******************
#pragma mark - GoogleMapViewInitializeAndUserOperations
- (void)loadMapView {
    GMSCameraPosition *camera;
    
    double lastSelectedLattitude = [APP_DELEGATE.dictLastSelectedLocation[@"lattitude"] doubleValue];
    double lastSelectedLongitude = [APP_DELEGATE.dictLastSelectedLocation[@"longitude"] doubleValue];
    dictLocationDetails = APP_DELEGATE.dictLastSelectedLocation;
    
    NSString * fullAddress = !dictLocationDetails ? @"": dictLocationDetails[@"fullAddress"];
    if (fullAddress && fullAddress.length > 0){
        isAddressFromPlacePicker = true;
    }

    NSString * title = !dictLocationDetails ? @"": dictLocationDetails[@"title"];
    if (title && title.length > 0){
        self.searchBarSearchForPlace.text = isAddressFromPlacePicker ? title : @"";
    }
    
    if (lastSelectedLattitude != 00.00000 && lastSelectedLongitude != 00.00000){
        camera = [GMSCameraPosition cameraWithLatitude:lastSelectedLattitude longitude:lastSelectedLongitude zoom:18];
    } else if (APP_DELEGATE.userLocation.latitude != 00.00000 && APP_DELEGATE.userLocation.longitude != 00.00000){
        camera = [GMSCameraPosition cameraWithLatitude:APP_DELEGATE.userLocation.latitude longitude:APP_DELEGATE.userLocation.longitude zoom:18];
    } else {
        camera = [GMSCameraPosition cameraWithLatitude:23.0752 longitude:72.5257 zoom:18];
    }
    
    self.viewGMSMapView.camera = camera;
    self.viewGMSMapView.myLocationEnabled = true;
    self.viewGMSMapView.delegate = self;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(notificationReceivedForUserLocationUpdates:) name:NOTI_USER_LOCATION_UPDATES object:nil];
}

-(void)notificationReceivedForUserLocationUpdates:(NSNotification *)notif{
    [self.viewGMSMapView setMyLocationEnabled:true];
}

-(void)findAddressForCoordinate:(CLLocationCoordinate2D)location WithCallback:(void (^) (BOOL success, NSDictionary * dictAddress))callback{
    NSString * lattitude = [NSString stringWithFormat:@"%f",location.latitude];
    NSString * longitude = [NSString stringWithFormat:@"%f",location.longitude];
    
    [APP_DELEGATE.apiManager findAddressUsingLocationCoordinate:location withCallBack:^(BOOL success, NSDictionary *result, NSString *error) {
        if (success){
            NSString * fullAddress = result[@"fullAddress"];
            callback(true, @{@"title":@"",@"fullAddress":fullAddress,@"lattitude":lattitude,@"longitude":longitude});
        } else {
            [[GMSGeocoder geocoder] reverseGeocodeCoordinate:location completionHandler:^(GMSReverseGeocodeResponse* response, NSError* error) {
                if (response.results.count > 0){
                    GMSAddress* addressObj =  response.firstResult;
                    NSString * fullAddress = @"";
                    for (NSString * title in addressObj.lines){
                        fullAddress = [fullAddress stringByAppendingString:title];
                    }
                    
                    callback(true, @{@"title":@"",@"fullAddress":fullAddress,@"lattitude":lattitude,@"longitude":longitude});
                } else {
                    callback(false, @{@"title":@"",@"fullAddress":@"",@"lattitude":lattitude,@"longitude":longitude});
                }
            }];
        }
    }];
}

-(void)findAddressForDropedPinLocation {
//    CGPoint pointToSet = CGPointMake(self.imageViewDestinationDropPin.center.x,self.imageViewDestinationDropPin.center.y + 25);
//    
//    dropedPinLocation = [self.viewGMSMapView.projection coordinateForPoint:pointToSet];
    
    if (isAddressFromPlacePicker){
        isAddressFromPlacePicker = false;
        if (self.imageViewDestinationDropPin.isHidden == false){
            [self shouldShowChangeDestinationBanner:true];
        }
        
        if (self.searchLocationMode == SLM_SET_FAVORITE_LOCATION || isModeLocalFavorite) {
            self.lableSearchedTitle.text = @"Add Favorite";
        } else if (self.searchLocationMode == SLM_SET_START_LOCATION){
            self.lableSearchedTitle.text = @"Pick Up";
        } else if (self.searchLocationMode == SLM_SET_END_LOCATION){
            self.lableSearchedTitle.text = @"Drop Off";
        }
        self.lableSearchedDetails.text = dictLocationDetails[@"fullAddress"];
        self.searchBarSearchForPlace.text = dictLocationDetails[@"title"];
    } else {
        [self findAddressForCoordinate:dropedPinLocation WithCallback:^(BOOL success, NSDictionary *dictAddress) {
            if (success){
                
                dictLocationDetails = dictAddress;
                
                if (self.imageViewDestinationDropPin.isHidden == false){
                    [self shouldShowChangeDestinationBanner:true];
                }
                
                if (self.searchLocationMode == SLM_SET_FAVORITE_LOCATION || isModeLocalFavorite) {
                    self.lableSearchedTitle.text = @"Add Favorite";
                } else if (self.searchLocationMode == SLM_SET_START_LOCATION){
                    self.lableSearchedTitle.text = @"Pick Up";
                } else if (self.searchLocationMode == SLM_SET_END_LOCATION){
                    self.lableSearchedTitle.text = @"Drop Off";
                }
                self.lableSearchedDetails.text = dictLocationDetails[@"fullAddress"];
                self.searchBarSearchForPlace.text = dictLocationDetails[@"title"];
            }
        }];
    }
    
}

#pragma mark - GoogleMapDelegate
-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    if (self.isModeSearchLocation){
        if (self.searchLocationMode != SLM_SHOW_FAVORITE_LOCATION){
            dropedPinLocation = [mapView.projection coordinateForPoint:mapView.center];
            [self shouldShowChangeDestinationBanner:false];
            [timerFindAddress invalidate];
            timerFindAddress = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(findAddressForDropedPinLocation) userInfo:nil repeats:false];
        }
    }
    
    GMSVisibleRegion region = self.viewGMSMapView.projection.visibleRegion;
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:region];
    if (![bounds containsCoordinate:APP_DELEGATE.userLocation]) {
        if(isAnimationStarted == false){
            isAnimationStarted = true;
            [self buttonBlinkAnimationShouldStart];
        }
    } else {
        if (isAnimationStarted == true){
            isAnimationStarted = false;
        }
    }
}

-(void)buttonBlinkAnimationShouldStart{
    self.viewTrackMyLocationShadow.backgroundColor = [UIColor whiteColor];
    if (isAnimationStarted == true){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.viewTrackMyLocationShadow.backgroundColor = [UIColor cyanColor];
        });
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self buttonBlinkAnimationShouldStart];
        });
    } else {
        self.viewTrackMyLocationShadow.alpha = 1.0;
        self.viewTrackMyLocationShadow.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - GooglePlacePickerDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    isAddressFromPlacePicker = true;
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:false completion:^{
        self.imageViewDestinationDropPin.hidden = false;
    }];
    return false;
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:false completion:^{
        dropedPinLocation = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude);
        self.searchBarSearchForPlace.text = place.name;
        NSString * title = place.name;
        NSString * fullAddress = place.formattedAddress;
        NSString * latiitude = [NSString stringWithFormat:@"%f",place.coordinate.latitude];
        NSString * longitude = [NSString stringWithFormat:@"%f",place.coordinate.longitude];
        
        if (fullAddress.length > title.length) {
            NSString * searchable = [fullAddress substringToIndex:(title.length)];
            if (![searchable.lowercaseString containsString:[place.name lowercaseString]]){
                fullAddress = [NSString stringWithFormat:@"%@, %@",place.name, fullAddress];
            }
        } else {
            fullAddress = [NSString stringWithFormat:@"%@, %@",place.name, fullAddress];
        }
        
        dictLocationDetails = @{
                                @"title":title,
                                @"fullAddress":fullAddress,
                                @"lattitude":latiitude,
                                @"longitude":longitude
                                };
        
        if (DEBUG_MODE){
            NSLog(@"Selected Place : %@",dictLocationDetails);
        }
        isAddressFromPlacePicker = true;
        [self.viewGMSMapView animateToLocation:dropedPinLocation];
    }];
}

- (void)viewController:(GMSAutocompleteViewController *)viewController didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:false completion:nil];
    // handle the error.
    if (DEBUG_MODE){
        NSLog(@"Error: %@", [error description]);
    }
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:false completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


#pragma mark - ChangeDestinationBanner
-(void)shouldShowChangeDestinationBanner:(BOOL)shouldShow{
    if (shouldShow){
        if (self.searchedLocationBottomSpace.constant < 1){
            self.searchedLocationBottomSpace.constant = 1;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    } else {
        if (self.searchedLocationBottomSpace.constant > -150){
            self.searchedLocationBottomSpace.constant = -150;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    }
    
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self shouldShowChangeDestinationBanner:false];
}

- (IBAction)doneButtonTapped:(UIButton *)sender {
    if (self.isModeSearchLocation){
        if (isModeLocalFavorite){
            //Should Add As Favorite
            [self shouldShowChangeDestinationBanner:false];
            isModeLocalFavorite = false;
            [self.buttonSearchedDone setTitle:GET_Language_Value(@"done") forState:UIControlStateNormal];
            [self manageViewInitially];
        } else {
            self.imageViewDestinationDropPin.hidden = true;
            [self shouldShowChangeDestinationBanner:false];
            
//            APP_DELEGATE.dictLastSelectedLocation = dictLocationDetails;
            
            [self dismissViewControllerAnimated:true completion:^{
                if (self.searchLocationMode != SLM_SHOW_FAVORITE_LOCATION){
                    [self.delegate didChangeLocationWithLocationInfo:dictLocationDetails];
                }
            }];
        }
    } else {
        self.searchedLocationBottomSpace.constant = -150;
        self.favoriteBannerBottomSpace.constant = -75;
        [self.view layoutIfNeeded];
    }
}

#pragma mark - SelectSearchLocationMethodView
-(void)handleViewShadow{
    if (self.searchedLocationBottomSpace.constant >= 0){
        self.viewSearchedlocationContainer.layer.shadowOpacity = 1.0;
        self.viewFavoriteBanner.layer.shadowOpacity = 0.0;
    } else if (self.favoriteBannerBottomSpace.constant >= 0){
        self.viewFavoriteBanner.layer.shadowOpacity = 1.0;
        self.viewSearchedlocationContainer.layer.shadowOpacity = 0.0;
    }
}

-(void)shouldShowSelectWorkHome:(BOOL)shouldShow{
    if (shouldShow){
        if (self.favoriteBannerBottomSpace.constant < 1){
            self.favoriteBannerBottomSpace.constant = 1;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    } else {
        if (self.favoriteBannerBottomSpace.constant > -75){
            self.favoriteBannerBottomSpace.constant = -75;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            } completion:nil];
        }
    }
    [self handleViewShadow];
}

#pragma mark - ManagerViewShowHideFunctions
-(void)manageViewInitially{
    if (self.isModeSearchLocation){
        self.imageViewDestinationDropPin.hidden = false;
        
        [self shouldShowChangeDestinationBanner:false];
        [timerFindAddress invalidate];
        timerFindAddress = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(findAddressForDropedPinLocation) userInfo:nil repeats:false];
        
        if (self.searchLocationMode == SLM_SET_END_LOCATION){
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerDestination"];
            [self shouldShowSelectWorkHome:true];
        } else  if (self.searchLocationMode == SLM_SET_START_LOCATION){
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerPickup"];
            [self shouldShowSelectWorkHome:true];
        } else  if (self.searchLocationMode == SLM_SET_FAVORITE_LOCATION){
            self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerFav"];
            [self shouldShowSelectWorkHome:false];
        } else if (self.searchLocationMode == SLM_SHOW_FAVORITE_LOCATION){
            self.imageViewDestinationDropPin.hidden = true;
            [self shouldShowSelectWorkHome:false];
            self.buttonSearchedCanacel.hidden = true;
            [self findAddressForCoordinate:self.centerableCoordinate WithCallback:^(BOOL success, NSDictionary *dictAddress) {
                [self addFavoriteSpotMarkerWithTitle:@"Favorite" location:self.centerableCoordinate];
                self.lableSearchedTitle.text = @"Favorite Location";
                self.lableSearchedDetails.text = dictAddress[@"fullAddress"];
                [self shouldShowChangeDestinationBanner:true];
            }];
        }
    } else {
        self.favoriteBannerBottomSpace.constant = -75;
        self.searchedLocationBottomSpace.constant = -150;
        [self.view layoutIfNeeded];
    }
}

- (IBAction)favoriteButtonTapped:(UIButton *)sender {
    if (sender.tag == 101){
        isModeLocalFavorite = true;
        self.lableSearchedTitle.text = @"Add Favorite";
        [self.buttonSearchedDone setTitle:GET_Language_Value(@"done") forState:UIControlStateNormal];
        self.imageViewDestinationDropPin.image = [UIImage imageNamed:@"PHMarkerFav"];
    } else {
        //Should Open Favorite List
        FavouriteSpots * favSpotsView = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteSpots"];
        favSpotsView.isSelectionMode = true;
        favSpotsView.delegate = self;
        UINavigationController * navController = [[UINavigationController alloc]initWithRootViewController:favSpotsView];
        [self presentViewController:navController animated:true completion:nil];
    }
}

#pragma mark - FavoruiteSoptsDelegate
-(void)shouldSelectFavoriteLocation:(NSDictionary *)favoriteLocation{
    
    NSDictionary * dictData = @{
                                           @"lattitude":favoriteLocation[@"lattitude"],
                                           @"longitude":favoriteLocation[@"longitude"],
                                           @"title":favoriteLocation[@"address_name"],
                                           @"fullAddress":favoriteLocation[@"address"],
                                           @"isFavourite":@"1",
                                           };
    
    dictLocationDetails = dictData;
    isAddressFromPlacePicker = true;
    
    double lattitude = [favoriteLocation[@"lattitude"] doubleValue];
    double longitude = [favoriteLocation[@"longitude"] doubleValue];
    [self.viewGMSMapView animateToLocation:CLLocationCoordinate2DMake(lattitude, longitude)];
}

- (IBAction)homeOrWorkButtonTapped:(UIButton *)sender {
//    FavouriteSpots * favSpotsView = [self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteSpots"];
//    favSpotsView.isSelectionMode = true;
////    favSpotsView.delegate = self;
//    UINavigationController * navController = [[UINavigationController alloc]initWithRootViewController:favSpotsView];
//    [self presentViewController:navController animated:true completion:nil];
}

-(void)addFavoriteSpotMarkerWithTitle:(NSString *)title location:(CLLocationCoordinate2D )location{
    GMSMarker * markerToAdd = [[GMSMarker alloc] init];
    markerToAdd.position = location;
    markerToAdd.title = title;
    [markerToAdd setDraggable:false];
    markerToAdd.icon = [UIImage imageNamed:@"PHMarkerFav"];
    markerToAdd.map = self.viewGMSMapView;
    [self.viewGMSMapView animateToLocation:location];
}

@end

