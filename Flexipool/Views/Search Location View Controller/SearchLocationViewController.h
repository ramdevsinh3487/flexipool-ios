//
//  SearchLocationViewController.h
//  Flexipool
//
//  Created by Nick on 15/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
//#import "FavouriteSpots.h"

typedef enum SEARCH_LOCATION_MODE{
    SLM_SET_START_LOCATION,//0
    SLM_SET_END_LOCATION,//1
    SLM_SET_FAVORITE_LOCATION,//2
    SLM_SHOW_FAVORITE_LOCATION
}SearchLocationMode;


@protocol SearchLocationViewControllerDelegate <NSObject>
@optional
-(void)didChangeLocationWithLocationInfo:(NSDictionary *)locationInfo;

@end
@interface SearchLocationViewController : UIViewController <GMSMapViewDelegate, GMSAutocompleteViewControllerDelegate, UISearchBarDelegate>{
    
    CLLocationCoordinate2D dropedPinLocation;
    NSDictionary * dictLocationDetails;
    
    NSTimer * timerFindAddress;
    
    BOOL isAnimationStarted;
    BOOL isModeLocalFavorite;
    BOOL isAddressFromPlacePicker;
}

@property id <SearchLocationViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewDestinationDropPin;
@property (weak, nonatomic) IBOutlet UIView *viewTrackMyLocationShadow;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarSearchForPlace;

@property (weak, nonatomic) IBOutlet GMSMapView *viewGMSMapView;
@property (weak, nonatomic) IBOutlet UIButton *buttonMyLocation;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UIView *viewFavoriteBanner;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *favoriteBannerBottomSpace;
@property (weak, nonatomic) IBOutlet UIButton *buttonFavoriteList;
@property (weak, nonatomic) IBOutlet UILabel *lableFavoriteListTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewFavoriteList;

@property (weak, nonatomic) IBOutlet UIView *viewSearchedlocationContainer;
@property (weak, nonatomic) IBOutlet UILabel *lableSearchedTitle;
@property (weak, nonatomic) IBOutlet UILabel *lableSearchedDetails;
@property (weak, nonatomic) IBOutlet UIButton *buttonSearchedDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonSearchedCanacel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchedLocationBottomSpace;

@property BOOL isModeSearchLocation;
@property SearchLocationMode searchLocationMode;
@property CLLocationCoordinate2D centerableCoordinate;
@end
