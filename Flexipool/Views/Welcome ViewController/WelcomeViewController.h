//
//  WelcomeViewController.h
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterViewController.h"
#import "SignInViewController.h"

@interface WelcomeViewController : UIViewController
{
    IBOutlet NSLayoutConstraint *constraintLogoTop;
    IBOutlet UIView *viewContainer;
    IBOutlet UIButton *btnRegister;
    IBOutlet UIButton *btnSignIn;
    IBOutlet UILabel *lblWelcomeTitle;
    IBOutlet UILabel *lblWelcomeDesc;
    IBOutlet UILabel *lblInfoCarPool;
    IBOutlet UILabel *lblCarPoolLine;
    IBOutlet UILabel *lblInfoHowWorks;
    IBOutlet UIView *viewSaferWayContainer;
    IBOutlet UILabel *lblInfoSaferWay;
    IBOutlet NSLayoutConstraint *leadingPhone2;
    IBOutlet NSLayoutConstraint *leadingPhone3;
    
    IBOutlet UIView *viewPhones;
    IBOutlet UILabel *lblStep1;
    IBOutlet UILabel *lblStep2;
    IBOutlet UILabel *lblStep3;
}

- (IBAction)actionRegister:(id)sender;
- (IBAction)actionSignIn:(id)sender;


@end
