//
//  WelcomeViewController.m
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "WelcomeViewController.h"
#import "AppDemoView.h"

@interface WelcomeViewController ()

@end

@implementation WelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [btnRegister.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [btnSignIn.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [lblWelcomeTitle setFont:[UIFont fontWithName:AppFont_traditional size:DONE_BUTTON_FONTS_SIZE]];
    [lblWelcomeDesc setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-5]];
    
    [btnRegister setTitle:GET_Language_Value(@"register") forState:UIControlStateNormal];
    [btnSignIn setTitle:GET_Language_Value(@"sign_in") forState:UIControlStateNormal];
    
    
    lblInfoCarPool.alpha = 0.0;
    lblInfoCarPool.font = [UIFont fontWithName:AppFont_Bold size:DONE_BUTTON_FONTS_SIZE];
    lblInfoCarPool.textColor = APP_BLUE_COLOR;
    
    lblCarPoolLine.alpha = 0.0;
    
    lblInfoHowWorks.alpha = 0.0;
    lblInfoHowWorks.font = [UIFont fontWithName:AppFont_Bold_Italic size:TEXTFIELD_FONTS_SIZE];
    lblInfoHowWorks.textColor = [UIColor redColor];
    
    lblInfoSaferWay.font = [UIFont fontWithName:AppFont_Bold_Italic size:TEXTFIELD_FONTS_SIZE ];
    lblInfoSaferWay.textColor = APP_GREEN_COLOR;
    
    lblStep1.font = [UIFont fontWithName:AppFont_traditional size:TEXTFIELD_FONTS_SIZE];
    lblStep1.textColor = [UIColor blackColor];
    
    lblStep2.font = [UIFont fontWithName:AppFont_traditional size:TEXTFIELD_FONTS_SIZE];
    lblStep2.textColor = [UIColor blackColor];
    
    lblStep3.font = [UIFont fontWithName:AppFont_traditional size:TEXTFIELD_FONTS_SIZE];
    lblStep3.textColor = [UIColor blackColor];
    
    viewPhones.alpha = 0;
    viewContainer.alpha = 0;
    constraintLogoTop.constant = 120;
    
   
    //Will Enable Demo
    if (!APP_DELEGATE.isDemoPresented) {
        AppDemoView * demoView = [self.storyboard instantiateViewControllerWithIdentifier:@"AppDemoView"];
        [self presentViewController:demoView animated:true completion:nil];
        APP_DELEGATE.isDemoPresented = true;
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (APP_DELEGATE.canNavigateToLogin){
        APP_DELEGATE.canNavigateToLogin = false;
        APP_DELEGATE.isDemoPresented = false;
        SignInViewController * registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
        [APP_DELEGATE.navigationController pushViewController:registerView animated:false];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    constraintLogoTop.constant = 10;
    [UIView animateWithDuration:0.8 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            viewContainer.alpha = 1;
            lblInfoCarPool.alpha = 1.0;
            lblCarPoolLine.alpha = 1.0;
            lblInfoHowWorks.alpha = 1.0;
            viewPhones.alpha = 1.0;
        } completion:^(BOOL finished) {
            [self slideInstructionImages];
        }];
    }];
}

-(void)slideInstructionImages {
    
    leadingPhone2.constant = 0;
    [UIView animateWithDuration:0.8 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        leadingPhone3.constant = 0;
        [UIView animateWithDuration:0.8 animations:^{
            [self.view layoutIfNeeded];
        }];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(UIStatusBarStyle)preferredStatusBarStyle {
//    return UIStatusBarStyleLightContent;
//}


- (IBAction)actionRegister:(id)sender {
    RegisterViewController * registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [APP_DELEGATE.navigationController pushViewController:registerView animated:true];
}

- (IBAction)actionSignIn:(id)sender {
    SignInViewController * registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
    [APP_DELEGATE.navigationController pushViewController:registerView animated:true];
}
@end
