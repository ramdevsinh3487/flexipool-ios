//
//  RegisterViewController.h
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NTMonthYearPicker/NTMonthYearPicker.h>
@interface RegisterViewController : UIViewController <UITextFieldDelegate>//, UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet UIScrollView *scrlView;
    IBOutlet UILabel *lblHeader;
//    IBOutlet UILabel *lblGenderTitle;
    IBOutlet UILabel *lblMale;
    IBOutlet UILabel *lblFemale;
    IBOutlet UIButton *btnMale;
    IBOutlet UIButton *btnFemale;
    
//    IBOutlet UIView *viewBorder;
    IBOutlet ACFloatingTextField *txtFirstName;
    IBOutlet UIView *viewBorderFirstName;
//    IBOutlet ACFloatingTextField *txtLastName;
//    IBOutlet UIView *viewBorderLastName;
    IBOutlet ACFloatingTextField *txtMobileNumber;
    IBOutlet UIView *viewBorderMobileNo;
//    __weak IBOutlet ACFloatingTextField *txtCountryCode;
    IBOutlet ACFloatingTextField *txtPassword;
    IBOutlet UIView *viewBorderPassword;
    IBOutlet ACFloatingTextField *txtConfirmPassword;
    IBOutlet UIView *viewBorderConfirmPassword;
    IBOutlet ACFloatingTextField *txtDOB;
    IBOutlet UIView *viewBorderDOB;
    
    IBOutlet ACFloatingTextField *txtEmaiID;
    IBOutlet UIView *viewBorderEmailId;
    
    IBOutlet ACFloatingTextField *txtMetroArea;
    IBOutlet UIView *viewBorderMetroArea;
    IBOutlet UILabel *lblUserTypeTitle;
    IBOutlet UIButton *btnRider;
    IBOutlet UIButton *btnDriver;
    IBOutlet UIButton *btnBoth;
//    IBOutlet UIButton *btnCheckUncheck;
//    IBOutlet A_ClickableLabel *lblTermsAndCondition;
    IBOutlet UIButton *btnRegister;
    
    NSString *strFromDate, *userGender, *userType;
//    NSString *allowedRiders;
//    NSString * selectedYear;
    NSDate * previouseDate;

    
//    IBOutlet UIView *viewCarDetailsContainer;
//    IBOutlet NSLayoutConstraint *carContainerHeight;
//    IBOutlet UILabel *lblAllowedRiders;
//    IBOutlet UIButton *btnAllowedRideOne;
//    IBOutlet UIButton *btnAllowedRiderOnePlus;
//    IBOutlet UILabel *lblVehicleInfo;
//    IBOutlet ACFloatingTextField *txtVehicleModel;
//    IBOutlet UIView *viewBorderModel;
//    IBOutlet ACFloatingTextField *txtVehicleYear;
//    IBOutlet UIView *viewBorderVehicleYear;
//    IBOutlet ACFloatingTextField *txtVehicleState;
//    IBOutlet UIView *viewBorderVehicleState;
//    IBOutlet ACFloatingTextField *txtVehiclePlate;
//    IBOutlet UIView *viewBorderVehiclePlate;
    
    UILabel * lblCountryCode;
    
//    NSMutableArray * arrayYears;
    UITextField * activeTextField;
}
- (IBAction)actionGender:(id)sender;

- (IBAction)actionCheckUncheck:(UIButton *)sender;
- (IBAction)actionTermsCondition:(id)sender;
- (IBAction)actionRegister:(id)sender;
- (IBAction)actionClose:(id)sender;
- (IBAction)actionUserTypeSelection:(UIButton *)sender;

@end
