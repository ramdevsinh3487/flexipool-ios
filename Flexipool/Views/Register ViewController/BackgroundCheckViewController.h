//
//  BackgroundCheckViewController.h
//  Flexipool
//
//  Created by HimAnshu on 11/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadURLViewController.h"
#import <FLAnimatedImage/FLAnimatedImage.h>
@interface BackgroundCheckViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
{
    IBOutlet UILabel *lblHeader;
    IBOutlet UIButton *btnBack;
    IBOutlet UIView *viewVehicleContainer;
    IBOutlet NSLayoutConstraint *heightVehicleContaine;
    IBOutlet UILabel *lblTitleVehicleInfo;
    IBOutlet ACFloatingTextField *txtVehicleModel;
    IBOutlet UIView *viewModelBorder;
    
    IBOutlet ACFloatingTextField *txtVehicleYear;
    IBOutlet UIView *viewBorderYear;
    
    IBOutlet ACFloatingTextField *txtVehicleState;
    IBOutlet UIView *viewBorderState;
    
    IBOutlet ACFloatingTextField *txtVehiclePlateNo;
    IBOutlet UIView *viewBorderPlate;
    
    IBOutlet UIButton *btnBGCheckMark;
    IBOutlet A_ClickableLabel *lblBGCheckLable;
    
    IBOutlet UIButton *btnTCCheckMark;
    IBOutlet A_ClickableLabel *lblTCCheckLable;
    
    IBOutlet UIButton *btnPIICheckMark;
    IBOutlet UILabel *lblPIICheckLable;
    
    IBOutlet UIButton *btnLICCheckMark;
    IBOutlet UILabel *lblLICCheckLAble;
    IBOutlet UIImageView *imgLICIcon;
    IBOutlet UIButton *btnLICAction;
    
    IBOutlet UIButton *btnRunBackgroundCheck;
    
    IBOutlet FLAnimatedImageView *imgGIFView;
    IBOutlet UIView *viewVerifying;
    IBOutlet UIView *viewVerifyingInner;
    IBOutlet UILabel *lblPopHeader;
    IBOutlet UILabel *lblNoCreditCheck;
    IBOutlet UILabel *lblSeeYouSoon;
    
    IBOutlet UILabel *lblPopUpInstruction;
    IBOutlet UIButton *btnThankYou;
    IBOutlet NSLayoutConstraint *constraintPopupY;
    
    NSMutableArray * arrayYears;
    
    UITextField * activeTextField;
    
    NSString * selectedYear;
    IBOutlet UIView *viewBorderedLable;
    IBOutlet UILabel *lblVerificationMail;
}

@property NSDictionary * dictUserDetails;

- (IBAction)actionBack:(id)sender;
- (IBAction)actionCheckUncheck:(UIButton *)sender;
- (IBAction)actionTermsCondition:(id)sender;
- (IBAction)actionBackgroundCheck:(id)sender;
- (IBAction)actionThankYou:(id)sender;

@end
