//
//  BackgroundCheckViewController.m
//  Flexipool
//
//  Created by HimAnshu on 11/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "BackgroundCheckViewController.h"
#import "RegisterViewController.h"

@interface BackgroundCheckViewController ()

@end

@implementation BackgroundCheckViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   // lblHeader.text = GET_Language_Value(@"background_check");
    
    lblHeader.text = @"Registration (2 of 2)";
    
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    [self setupInitialView];
    
    [self setBorderViewShow:false];
    
    
    if ([self.dictUserDetails[@"userType"] isEqualToString:@"1"]) {
        
        txtVehicleModel.userInteractionEnabled = false;
        txtVehicleState.userInteractionEnabled = false;
        txtVehicleYear.userInteractionEnabled = false;
        txtVehiclePlateNo.userInteractionEnabled = false;
        
        btnLICCheckMark.hidden = true;
        lblLICCheckLAble.hidden = true;
        lblLICCheckLAble.text = @"";
        imgLICIcon.hidden = true;
        btnLICAction.hidden = true;
        
        heightVehicleContaine.constant = 0;
        viewVehicleContainer.hidden = true;
        [self.view layoutIfNeeded];
    } else {
        txtVehicleModel.userInteractionEnabled = true;
        txtVehicleState.userInteractionEnabled = true;
        txtVehicleYear.userInteractionEnabled = true;
        txtVehiclePlateNo.userInteractionEnabled = true;
        
        heightVehicleContaine.constant = (225 * SCREEN_WIDTH) / 320;
        viewVehicleContainer.hidden = false;
        [self.view layoutIfNeeded];
    }
    
    btnBGCheckMark.tag = 0;
    [btnBGCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    btnTCCheckMark.tag = 0;
    [btnTCCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    btnPIICheckMark.tag = 0;
    [btnPIICheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    btnLICCheckMark.tag = 0;
    [btnLICCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void) setupInitialView {
    [lblTitleVehicleInfo setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    lblTitleVehicleInfo.text = @"Vehicle Info";
    
    [btnRunBackgroundCheck setTitle:GET_Language_Value(@"run_background_check") forState:UIControlStateNormal];
    [btnRunBackgroundCheck.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    [txtVehicleModel setPadding:8];
    txtVehicleModel.delegate = self;
    [txtVehicleModel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtVehicleModel.placeholder = @"Vehicle Make & Model";
    
    [txtVehicleState setPadding:8];
    txtVehicleState.delegate = self;
    [txtVehicleState setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtVehicleState.placeholder = @"Vehicle State";
    
    [txtVehicleYear setPadding:8];
    txtVehicleYear.delegate = self;
    [txtVehicleYear setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];

    txtVehicleYear.placeholder = @"Vehicle Year (Optional)";
    [txtVehicleYear addTarget:self action:@selector(txtVehicleYearTouched:) forControlEvents:UIControlEventEditingDidBegin];
    [txtVehicleYear addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfVehicleYear) shouldShowPlaceholder:YES];
    [txtVehicleYear setRightPaddingDownArrow];
    
    
    [txtVehiclePlateNo setPadding:8];
    txtVehiclePlateNo.delegate = self;
    [txtVehiclePlateNo setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtVehiclePlateNo.placeholder = @"Vehicle Plate no. (Optional)";
    
    //Custome Link Labledfg
    UIColor * fontColour = APP_GREEN_COLOR;
    
    
    A_ClickableElement *lblCheckEle1 = [A_ClickableElement create:@"Privacy Policy" withBuilder:[[A_AttributedStringBuilder createWithFontName:AppFont_Regular withSize:lblBGCheckLable.font.pointSize] setForegroundColor:fontColour] andClick:^(A_ClickableElement *element, A_ClickableLabel *sender, A_ClickedAdditionalInformation *info) {
            [self actionPrivacyPolicy:nil];
    }];
    
    
    A_ClickableElement *lblCheckEle2 = [A_ClickableElement create:@"I authorize Ahoy to initiate my background check" withBuilder:[[A_AttributedStringBuilder createWithFontName:AppFont_Regular withSize:lblBGCheckLable.font.pointSize] setForegroundColor:fontColour] andClick:^(A_ClickableElement *element, A_ClickableLabel *sender, A_ClickedAdditionalInformation *info) {
        [self checkUncheckBG:nil];
    }];
    
    [lblBGCheckLable setSentence:lblBGCheckLable.text withBuilder:[A_AttributedStringBuilder createWithFontName:AppFont_Regular withSize:lblBGCheckLable.font.pointSize] andElements: @[lblCheckEle1, lblCheckEle2]];
    
    
    
    A_ClickableElement *lblTCEle1 = [A_ClickableElement create:@"Terms and Conditions" withBuilder:[[A_AttributedStringBuilder createWithFontName:AppFont_Regular withSize:lblTCCheckLable.font.pointSize] setForegroundColor:fontColour] andClick:^(A_ClickableElement *element, A_ClickableLabel *sender, A_ClickedAdditionalInformation *info) {
        [self actionTermsCondition:nil];
    }];
    
    A_ClickableElement *lblTCEle2 = [A_ClickableElement create:@"I am 18 and above. I have read, understood and agreed to all" withBuilder:[[A_AttributedStringBuilder createWithFontName:AppFont_Regular withSize:lblTCCheckLable.font.pointSize] setForegroundColor:fontColour] andClick:^(A_ClickableElement *element, A_ClickableLabel *sender, A_ClickedAdditionalInformation *info) {
        [self checkUncheckTC:nil];
    }];
    
    [lblTCCheckLable setSentence:lblTCCheckLable.text withBuilder:[A_AttributedStringBuilder createWithFontName:lblTCCheckLable.font.fontName withSize:lblTCCheckLable.font.pointSize] andElements: @[lblTCEle1, lblTCEle2]];
    
    
    lblPIICheckLable.textColor = fontColour;
    lblLICCheckLAble.textColor = fontColour;
    
    
    lblPIICheckLable.font = [UIFont fontWithName:AppFont_Regular size:14];
    lblLICCheckLAble.font = [UIFont fontWithName:AppFont_Regular size:14];
    
    
    //PopView
    viewVerifying.frame = self.view.frame;
    [self.view addSubview:viewVerifying];
    viewVerifying.alpha = 0;
    viewVerifying.hidden = true;
    [lblPopHeader setFont:[UIFont fontWithName:AppFont_Regular size:POPUP_HEADER_FONTS_SIZE]];
    [lblPopUpInstruction setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [btnThankYou.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    
    
    
    [self setupTheBorderView];
    
    selectedYear = @"";
    
    viewBorderedLable.layer.borderWidth = 5.0;
    viewBorderedLable.layer.borderColor = [UIColor redColor].CGColor;
    lblVerificationMail.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegates -
-(void)setupTheBorderView {
    viewModelBorder.layer.borderWidth = 1.0;
    viewModelBorder.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewModelBorder.alpha = 0;
    
    viewBorderState.layer.borderWidth = 1.0;
    viewBorderState.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderState.alpha = 0;
    
    viewBorderYear.layer.borderWidth = 1.0;
    viewBorderYear.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderYear.alpha = 0;
    
    viewBorderPlate.layer.borderWidth = 1.0;
    viewBorderPlate.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderPlate.alpha = 0;
}

- (void) setBorderViewShow:(BOOL)isShow {
    UIView * editableView;
    if (activeTextField == txtVehicleModel){
        editableView = viewModelBorder;
    } else if (activeTextField == txtVehicleState) {
        editableView = viewBorderState;
    } else if (activeTextField == txtVehicleYear) {
        editableView = viewBorderYear;
    } else if (activeTextField == txtVehiclePlateNo) {
        editableView = viewBorderPlate;
    }
    
    [UIView animateWithDuration:isShow ? 0.5 : 0.2 animations:^{
        if (isShow){
            editableView.alpha = isShow;
        } else {
            editableView.alpha = activeTextField.hasText;
        }
    }];
}
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    [self setBorderViewShow:YES];
    
    if (txtVehicleModel == textField) {
        txtVehicleModel.placeholder = @"Make/Model/Color";
        txtVehicleModel.labelPlaceholder.text = @"Make/Model/Color";
    } else if (txtVehicleState == textField) {
        txtVehicleState.placeholder = @"Vehicle State";
        txtVehicleState.labelPlaceholder.text = @"Vehicle State";
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (txtVehicleModel == textField && [txtVehicleModel.text isEqualToString:@""]) {
        txtVehicleModel.placeholder = @"Make/Model/Color e.g.(Honda/Civic/Red)*";
        txtVehicleModel.labelPlaceholder.text = @"Make/Model/Color e.g.(Honda/Civic/Red)*";
    } else if (txtVehicleState == textField && [txtVehicleState.text isEqualToString:@""]) {
        txtVehicleState.placeholder = @"State vehicle registered in e.g.(VA MD DC)*";
        txtVehicleState.labelPlaceholder.text = @"State vehicle registered in e.g.(VA MD DC)*";
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self setBorderViewShow:NO];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == txtVehicleModel){
        
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"()1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,1234567890-/( "];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
            return YES;
        }
        else {
            return false;
        }
        
        
    }
    
    else if (textField == txtVehicleState){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 15){
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
            return YES;
        } else {
            return false;
        }
    }
    else if (textField == txtVehiclePlateNo){
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        return YES;
    }
    return true;
}

#pragma mark - Action Methods -

- (IBAction)actionBack:(id)sender {
    [APP_DELEGATE.navigationController popViewControllerAnimated:true];
}

- (IBAction)actionCheckUncheck:(UIButton *)sender {
    
    if (sender.tag == 0) {
        
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
        sender.tag = 1;
    }
    else {
        
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
        sender.tag = 0;
    }
}

- (IBAction)checkUncheckBG:(id)sender {
    if (btnBGCheckMark.tag == 0) {
        btnBGCheckMark.tag = 1;
        [btnBGCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else {
        btnBGCheckMark.tag = 0;
        [btnBGCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    }
}

- (IBAction)checkUncheckTC:(id)sender {
    if (btnTCCheckMark.tag == 0) {
        btnTCCheckMark.tag = 1;
        [btnTCCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else {
        btnTCCheckMark.tag = 0;
        [btnTCCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    }
}

- (IBAction)checkUncheckPII:(id)sender {
    if (btnPIICheckMark.tag == 0) {
        btnPIICheckMark.tag = 1;
        [btnPIICheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else {
        btnPIICheckMark.tag = 0;
        [btnPIICheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    }
}

- (IBAction)checkUncheckLIC:(id)sender {
    if (btnLICCheckMark.tag == 0) {
        btnLICCheckMark.tag = 1;
        [btnLICCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
    } else {
        btnLICCheckMark.tag = 0;
        [btnLICCheckMark setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
    }
}

- (IBAction)actionPrivacyPolicy:(id)sender {
    LoadURLViewController * loadURLView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
    loadURLView.cmsAPIMode = CMS_API_PRIVACY;
    loadURLView.isModeLoadString = true;
    [self presentViewController:loadURLView animated:true completion:nil];
}



- (IBAction)actionTermsCondition:(id)sender {
    LoadURLViewController * loadURLView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
    loadURLView.cmsAPIMode = CMS_API_TERMS;
    loadURLView.isModeLoadString = true;
    [self presentViewController:loadURLView animated:true completion:nil];
}

- (IBAction)actionBackgroundCheck:(id)sender {
    
    [self.view endEditing:YES];
    
    if (![self.dictUserDetails[@"userType"] isEqualToString:@"1"] && ![txtVehicleModel hasText]){
        [Singleton showSnakBar:@"Please enter vehicle make and model" multiline:NO];
    }
    else if (![self.dictUserDetails[@"userType"] isEqualToString:@"1"] && ![txtVehicleState hasText]){
        [Singleton showSnakBar:@"Please enter vehicle state" multiline:NO];
    } else if (btnBGCheckMark.tag == 0) {
        [Singleton showSnakBar:@"Please agree on all policies" multiline:NO];
    } else if (btnTCCheckMark.tag == 0) {
        [Singleton showSnakBar:@"Please agree on all policies" multiline:NO];
    } else if (btnPIICheckMark.tag == 0) {
        [Singleton showSnakBar:@"Please agree on all policies" multiline:NO];
        
    } else if (![self.dictUserDetails[@"userType"] isEqualToString:@"1"] && btnLICCheckMark.tag == 0) {
        [Singleton showSnakBar:@"Please agree on all policies" multiline:NO];
    } else {
        START_HUD
        [APP_DELEGATE.apiManager registerUserWithFirstName:self.dictUserDetails[@"firstName"] LastName:self.dictUserDetails[@"lastName"] CountryCode:self.dictUserDetails[@"countryCode"] mobileNumber:self.dictUserDetails[@"mobileNo"] Password:self.dictUserDetails[@"password"] Gender:self.dictUserDetails[@"userGender"] DateOfBirth:self.dictUserDetails[@"birthDate"] UserType:self.dictUserDetails[@"userType"] MetroArea:self.dictUserDetails[@"metroArea"] SSNNumber:@"" UserEmail:self.dictUserDetails[@"emailId"] allowedRiders:@"2" vehicleModel:txtVehicleModel.text VehicleYear:txtVehicleYear.text VehicleState:txtVehicleState.text VehiclePlate:txtVehiclePlateNo.text withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *errorMessage) {
            STOP_HUD
            if (success){
                [self showPopupView];
            } else {
                [Singleton showSnakBar:errorMessage multiline:true];
            }
        }];
    }
}

-(BOOL)isValidEmail:(NSString *)email{
    NSString * alphaNum = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alphaNum];
    return [regexTest evaluateWithObject:email];
}

- (IBAction)actionThankYou:(id)sender {
    exit(0);
}

#pragma mark - Custom Methods -

- (void) showPopupView {
    
    viewVerifying.hidden = false;
    
    NSURL *path = [[NSBundle mainBundle] URLForResource:@"PHBackgroundCheck" withExtension:@"gif"];
    FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:path]];
    imgGIFView.animatedImage = image;
    
    
    viewVerifying.alpha = 0;
    viewVerifyingInner.alpha = 0;
    constraintPopupY.constant = SCREEN_HEIGHT;
    
//    lblPopHeader.text = @"Check your emial !!";
//    lblPopUpInstruction.text = @"- You will receive an email from our partner company GoodHire.\n\n- Click on the link (in the email), fill-in your info and submit\n\n- Wait for text(SMS) with activation link";

    lblPopHeader.text = @"Check your emial !!";
    lblPopUpInstruction.text = @"- You will recieve a background verification link on your email.\n\n- Follow the instruction in the email and submit your background check.";
    
//    NSMutableAttributedString* mutableTitle1 = [[NSMutableAttributedString alloc] initWithString:lblPopUpInstruction.text];
//    [mutableTitle1 addAttribute:NSFontAttributeName value:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1] range:[lblPopUpInstruction.text rangeOfString:@"- Wait for text(SMS) with activation link"]];
//    [mutableTitle1 addAttribute:NSForegroundColorAttributeName value:[UIColor greenColor] range:[lblPopUpInstruction.text rangeOfString:@"- Wait for text(SMS) with activation link"]];
//    [lblPopUpInstruction setAttributedText:mutableTitle1];
    
    
    [UIView animateWithDuration:0.2 animations:^{
       
        viewVerifying.alpha = 1;
        viewVerifyingInner.alpha = 1;
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.5 animations:^{
            
            constraintPopupY.constant = 0;
            [self.view layoutIfNeeded];
        }];
    }];
}


#pragma mark - PickerView Delegate Methods -
- (void) txtVehicleYearTouched : (UITextField *) textField {


    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy"];
    arrayYears = [[NSMutableArray alloc]init];

    NSString * stringYear = [dateFormat stringFromDate:[NSDate date]];
    for (int i = 1968; i <= [stringYear doubleValue]; i++){
        [arrayYears addObject:[NSString stringWithFormat:@"%d",i]];
    }

    if (DEBUG_MODE){
        NSLog(@"Total Years : %@", arrayYears);
    }

    UIPickerView * pickerYears = [[UIPickerView alloc]init];
    pickerYears.delegate = self;
    pickerYears.dataSource = self;
    pickerYears.tag = 1101;
    textField.inputView.backgroundColor = [UIColor whiteColor];
    [txtVehicleYear setInputView:pickerYears];
    [pickerYears reloadAllComponents];
    if ([selectedYear isEqualToString:@""]){
        selectedYear = arrayYears.lastObject;
        [pickerYears selectRow:arrayYears.count - 1 inComponent:0 animated:true];
    } else {
        [pickerYears selectRow:[arrayYears indexOfObject:selectedYear]inComponent:0 animated:true];
    }
}

- (void) resignActiveInputField {
    
    [self.view endEditing:YES];
}

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return arrayYears.count;
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return arrayYears[row];
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    selectedYear = arrayYears[row];
}

- (void)doneOfVehicleYear
{
    txtVehicleYear.text = selectedYear;
    [txtVehicleYear resignFirstResponder];
}

#pragma mark - Status Bar
-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
