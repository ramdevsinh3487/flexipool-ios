//
//  RegisterViewController.m
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "RegisterViewController.h"
#import "BackgroundCheckViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInitialView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionClose:) name:@"dismissParentView" object:nil];
}

- (void) setupInitialView {
    
    strFromDate = @"";
    userGender = @"";
    userType = @"";
//    allowedRiders = @"";
//    selectedYear = @"2017";
    previouseDate = nil;
    
  
//    btnCheckUncheck.tag = 1;
    
    [self actionUserTypeSelection:btnBoth];
//    [self actionCheckUncheck:btnCheckUncheck];
    
//    viewCarDetailsContainer.hidden = false;
//    viewBorder.layer.borderWidth = 1.0;
//    viewBorder.layer.borderColor = [APP_BLUE_COLOR CGColor];
    
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    [txtFirstName setPadding:8];
    [txtFirstName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtFirstName.placeholder = @"What name you would like to go by ?*";
    
//    [txtLastName setPadding:8];
//    [txtLastName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
//    txtLastName.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"last_name")];
    
//    [txtCountryCode setPadding:8];
//    [txtCountryCode setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
//    txtCountryCode.placeholder = [NSString stringWithFormat:@"%@", GET_Language_Value(@"country_code")];
    
    [txtMobileNumber setPadding:8];
    [txtMobileNumber setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtMobileNumber.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"mobile_number")];
    
    lblCountryCode = [[UILabel alloc]initWithFrame:CGRectMake(6, 4, txtMobileNumber.frame.size.height - 5, txtMobileNumber.frame.size.height - 10)];
    lblCountryCode.text = [Singleton.sharedInstance findYourCurrentCountryCode];;
    lblCountryCode.textColor = [UIColor lightGrayColor];
    lblCountryCode.textAlignment = NSTextAlignmentCenter;
    lblCountryCode.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UILabel *lblPlusIcon = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, txtMobileNumber.frame.size.height - 10, txtMobileNumber.frame.size.height - 10)];
    lblPlusIcon.text = @"+";
    lblPlusIcon.textColor = [UIColor lightGrayColor];
    lblPlusIcon.textAlignment = NSTextAlignmentLeft;
    lblPlusIcon.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UIView * leftView = [[UIView alloc]initWithFrame:lblCountryCode.frame];
     [leftView addSubview:lblPlusIcon];
    [leftView addSubview:lblCountryCode];
    [txtMobileNumber setLeftView:leftView];
    
    
    
    [txtPassword setPadding:8];
    [txtPassword setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
     txtPassword.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"register_pwd")];
    
    [txtConfirmPassword setPadding:8];
    [txtConfirmPassword setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtConfirmPassword.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"confirm_password")];
    
    [txtDOB setPadding:8];
    [txtDOB setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtDOB.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"register_dob")];
    [txtDOB addTarget:self action:@selector(txtDateTouch:) forControlEvents:UIControlEventEditingDidBegin];
    [txtDOB addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfDOB) shouldShowPlaceholder:YES];
    [txtDOB setRightPaddingDownArrow];
    
    
//    [txtVehicleModel setPadding:8];
//    [txtVehicleModel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
//    txtVehicleModel.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"vehicle_make_&_model")];
//    
//    [txtVehicleYear setPadding:8];
//    [txtVehicleYear setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
//    txtVehicleYear.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"vehicle_year")];
//    [txtVehicleYear addTarget:self action:@selector(txtVehicleYearTouched:) forControlEvents:UIControlEventEditingDidBegin];
//    [txtVehicleYear addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfVehicleYear) shouldShowPlaceholder:YES];
//    [txtVehicleYear setRightPaddingDownArrow];
//    
//    
//    [txtVehicleState setPadding:8];
//    [txtVehicleState setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
//    txtVehicleState.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"vehicle_state")];
//    
//    [txtVehiclePlate setPadding:8];
//    [txtVehiclePlate setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
//    txtVehiclePlate.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"vehicle_plateNo")];
    
    [txtMetroArea setPadding:8];
    [txtMetroArea setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtMetroArea.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"redister_metroarea")];
    
    [txtEmaiID setPadding:8];
    [txtEmaiID setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    txtEmaiID.placeholder = [NSString stringWithFormat:@"%@",GET_Language_Value(@"redister_metroarea")];

//    [lblGenderTitle setFont:[UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE]];
    [lblMale setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    [lblFemale setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
    
    [lblUserTypeTitle setFont:[UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE]];
    [btnRider.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnDriver.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnBoth.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
//    [lblTermsAndCondition setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1]];
//    NSMutableAttributedString* mutableTitle = [[NSMutableAttributedString alloc] initWithString:lblTermsAndCondition.text];
//    [mutableTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE-1] range:[lblTermsAndCondition.text rangeOfString:@"Terms & Conditions"]];
//    [mutableTitle addAttribute:NSForegroundColorAttributeName value:APP_BLUE_COLOR range:[lblTermsAndCondition.text rangeOfString:@"Terms & Conditions"]];
//    [lblTermsAndCondition setAttributedText:mutableTitle];
//    
//    [mutableTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:AppFont_Bold size:TEXTFIELD_FONTS_SIZE-1] range:[lblTermsAndCondition.text rangeOfString:@"Privacy Policy"]];
//    [mutableTitle addAttribute:NSForegroundColorAttributeName value:APP_BLUE_COLOR range:[lblTermsAndCondition.text rangeOfString:@"Privacy Policy"]];
//    [lblTermsAndCondition setAttributedText:mutableTitle];
    
  //  lblHeader.text = GET_Language_Value(@"register");
    
    lblHeader.text = @"Registration (1 of 2)";
    
    lblMale.text = GET_Language_Value(@"male");
    lblFemale.text = GET_Language_Value(@"female");
    lblUserTypeTitle.text = GET_Language_Value(@"i_want_to_be");
    
    [btnRider setTitle:GET_Language_Value(@"ride") forState:UIControlStateNormal];
    [btnDriver setTitle:GET_Language_Value(@"drive") forState:UIControlStateNormal];
    [btnBoth setTitle:GET_Language_Value(@"both") forState:UIControlStateNormal];
    [btnRegister setTitle:GET_Language_Value(@"next") forState:UIControlStateNormal];

//    [self actionGender:btnMale];
//    [self actionUserTypeSelection:btnBoth];
    
    [btnRegister.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    /*I am 18 & above and I have read, understood and agreed to the Terms & Conditions and Privacy Policy of Ahoy*/
    
//    UIFont * lableFont = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-1];
//    
//    NSMutableArray * arrayData = [[NSMutableArray alloc]init];
    
    
//    A_ClickableElement *element = [A_ClickableElement create:@"Terms & Conditions" withBuilder:[[A_AttributedStringBuilder createWithFontName:lableFont.fontName withSize:lableFont.pointSize] setForegroundColor:APP_BLUE_COLOR] andClick:^(A_ClickableElement *element, A_ClickableLabel *sender, A_ClickedAdditionalInformation *info) {;
//        NSLog(@"Terms and Condition tapped");
//        [self actionTermsCondition:nil];
//    }];
//    
//    A_ClickableElement *element2 = [A_ClickableElement create:@"Privacy Policy" withBuilder:[[A_AttributedStringBuilder createWithFontName:lableFont.fontName withSize:lableFont.pointSize] setForegroundColor:APP_BLUE_COLOR] andClick:^(A_ClickableElement *element, A_ClickableLabel *sender, A_ClickedAdditionalInformation *info) {;
//        NSLog(@"Privacy Policy Tapped");
//        [self actionPrivacyPolicy:nil];
//    }];
//    
//    A_ClickableElement *element3 = [A_ClickableElement create:@"I am 18 & above and I have read, understood and agreed to the" withBuilder:[[A_AttributedStringBuilder createWithFontName:lableFont.fontName withSize:lableFont.pointSize] setForegroundColor:[UIColor blackColor]] andClick:^(A_ClickableElement *element, A_ClickableLabel *sender, A_ClickedAdditionalInformation *info) {;
//        [self actionCheckUncheck:btnCheckUncheck];
//    }];
    
//    [arrayData addObject:element];
//    [arrayData addObject:element2];
//    [arrayData addObject:element3];
//    
//    [lblTermsAndCondition setSentence:@"I am 18 & above and I have read, understood and agreed to the Terms & Conditions and Privacy Policy of Ahoy" withBuilder:[A_AttributedStringBuilder createWithFontName:lableFont.fontName withSize:lableFont.pointSize] andElements: arrayData];
    
    [self setupTheBorderView];
    
    
    txtMetroArea.text = @"Washington D.C";
    activeTextField = txtMetroArea;
    [self setBorderViewShow:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextField Delegates -
-(void)setupTheBorderView {
    viewBorderFirstName.layer.borderWidth = 1.0;
    viewBorderFirstName.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderFirstName.alpha = 0;
    
//    viewBorderLastName.layer.borderWidth = 1.0;
//    viewBorderLastName.layer.borderColor = [APP_BLUE_COLOR CGColor];
//    viewBorderLastName.alpha = 0;
    
    viewBorderMobileNo.layer.borderWidth = 1.0;
    viewBorderMobileNo.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderMobileNo.alpha = 0;
    
    viewBorderPassword.layer.borderWidth = 1.0;
    viewBorderPassword.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderPassword.alpha = 0;
    
    viewBorderConfirmPassword.layer.borderWidth = 1.0;
    viewBorderConfirmPassword.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderConfirmPassword.alpha = 0;
    
    viewBorderDOB.layer.borderWidth = 1.0;
    viewBorderDOB.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderDOB.alpha = 0;
    
    viewBorderMetroArea.layer.borderWidth = 1.0;
    viewBorderMetroArea.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderMetroArea.alpha = 0;
    
    viewBorderEmailId.layer.borderWidth = 1.0;
    viewBorderEmailId.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderEmailId.alpha = 0;
    
//    viewBorderModel.layer.borderWidth = 1.0;
//    viewBorderModel.layer.borderColor = [APP_BLUE_COLOR CGColor];
//    viewBorderModel.alpha = 0;
//    
//    viewBorderVehicleYear.layer.borderWidth = 1.0;
//    viewBorderVehicleYear.layer.borderColor = [APP_BLUE_COLOR CGColor];
//    viewBorderVehicleYear.alpha = 0;
//    
//    viewBorderVehicleState.layer.borderWidth = 1.0;
//    viewBorderVehicleState.layer.borderColor = [APP_BLUE_COLOR CGColor];
//    viewBorderVehicleState.alpha = 0;
//    
//    viewBorderVehiclePlate.layer.borderWidth = 1.0;
//    viewBorderVehiclePlate.layer.borderColor = [APP_BLUE_COLOR CGColor];
//    viewBorderVehiclePlate.alpha = 0;
}



- (void) setBorderViewShow:(BOOL)isShow {
    UIView * editableView;
    if (activeTextField == txtFirstName){
        editableView = viewBorderFirstName;
    }
//    else if (activeTextField == txtLastName) {
//        editableView = viewBorderLastName;
//    }
    else if (activeTextField == txtMobileNumber) {
        editableView = viewBorderMobileNo;
    } else if (activeTextField == txtPassword) {
        editableView = viewBorderPassword;
    } else if (activeTextField == txtConfirmPassword) {
        editableView = viewBorderConfirmPassword;
    } else if (activeTextField == txtDOB) {
        editableView = viewBorderDOB;
    } else if (activeTextField == txtMetroArea) {
        editableView = viewBorderMetroArea;
    } else if (activeTextField == txtEmaiID) {
        editableView = viewBorderEmailId;
    }
//    else if (activeTextField == txtVehicleModel) {
//        editableView = viewBorderModel;
//    } else if (activeTextField == txtVehicleYear) {
//        editableView = viewBorderVehicleYear;
//    } else if (activeTextField == txtVehicleState) {
//        editableView = viewBorderVehicleState;
//    } else if (activeTextField == txtVehiclePlate) {
//        editableView = viewBorderVehiclePlate;
//    }
    
    [UIView animateWithDuration:isShow ? 0.5 : 0.2 animations:^{
        if (isShow){
            editableView.alpha = isShow;
        } else {
            editableView.alpha = activeTextField.hasText;
        }
    }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (txtMetroArea == textField) {
        txtMetroArea.placeholder = @"Metro Area*";
        txtMetroArea.labelPlaceholder.text = @"Metro Area*";
    }
    activeTextField = textField;
    [self setBorderViewShow:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtFirstName){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30){
//            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz. "];
//            for (int i = 0; i < [string length]; i++)
//            {
//                unichar c = [string characterAtIndex:i];
//                if (![myCharSet characterIsMember:c])
//                {
//                    return NO;
//                }
//            }
            
            
            return YES;
            
        } else if ([string isEqualToString:@""]){
            return YES;
        } else {
            return NO;
        }
    }

    else if (textField == txtMobileNumber) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 10){
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            return YES;
        } else if ([string isEqualToString:@""]){
            return YES;
        } else {
            return NO;
        }
    } else if (textField == txtPassword || textField == txtConfirmPassword){
        
    } else if (textField == txtDOB){
        
    } else if (textField == txtMetroArea){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz. "];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
            return YES;
        }
    } else if (textField == txtEmaiID){
        
    }
//    else if (textField == txtVehicleModel){
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        if (newLength <= 20){
//            return true;
//        } else {
//            return false;
//        }
//    } else if (textField == txtVehicleState){
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        if (newLength <= 15){
//            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "];
//            for (int i = 0; i < [string length]; i++)
//            {
//                unichar c = [string characterAtIndex:i];
//                if (![myCharSet characterIsMember:c])
//                {
//                    return NO;
//                }
//            }
//            
//            return YES;
//        } else {
//            return false;
//        }
//    } else if (textField == txtVehiclePlate){
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        if (newLength <= 20){
//            return true;
//        } else {
//            return false;
//        }
//    }
////    else if (textField == txtMetroArea){
////        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"];
////        for (int i = 0; i < [string length]; i++)
////        {
////            unichar c = [string characterAtIndex:i];
////            if (![myCharSet characterIsMember:c])
////            {
////                return NO;
////            }
////        }
//    
//        //return YES;
////    }
//else if (textField == txtVehicleModel){
//        NSUInteger newLength = [textField.text length] + [string length] - range.length;
//        if (newLength <= 20){
//            return true;
//        } else {
//            return false;
//        }
//    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self setBorderViewShow:NO];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (txtMetroArea == textField && [txtMetroArea.text isEqualToString:@""]) {
        txtMetroArea.placeholder = @"Washington DC(Metro Area)*";
        txtMetroArea.labelPlaceholder.text = @"Washington DC(Metro Area)*";
    }
}



#pragma mark - Action Methods -

- (IBAction)actionGender:(id)sender {
    
    if ([sender tag] == 1) {
        
        [btnMale setImage:[UIImage imageNamed:@"user-icon-male-active"] forState:UIControlStateNormal];
        [btnFemale setImage:[UIImage imageNamed:@"user-icon-female-deactive"] forState:UIControlStateNormal];
        
        lblMale.textColor = APP_BLUE_COLOR;
        lblFemale.textColor = [UIColor darkGrayColor];
        userGender = @"1";
    }
    else {
        
        [btnMale setImage:[UIImage imageNamed:@"user-icon-male-deactive"] forState:UIControlStateNormal];
        [btnFemale setImage:[UIImage imageNamed:@"user-icon-female-active"] forState:UIControlStateNormal];
        
        lblMale.textColor = [UIColor darkGrayColor];
        lblFemale.textColor = APP_BLUE_COLOR;
        userGender = @"2";
    }
}

- (IBAction)actionCheckUncheck:(UIButton *)sender {
    
    if (sender.tag == 0) {
        
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueTrue"] forState:UIControlStateNormal];
        sender.tag = 1;
    }
    else {
        
        [sender setImage:[UIImage imageNamed:@"PHCheckBoxBlueFalse"] forState:UIControlStateNormal];
        sender.tag = 0;
    }
}

- (IBAction)actionTermsCondition:(id)sender {
    LoadURLViewController * loadURLView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
    loadURLView.cmsAPIMode = CMS_API_TERMS;
    loadURLView.isModeLoadString = true;
    [self presentViewController:loadURLView animated:true completion:nil];
}

- (IBAction)actionPrivacyPolicy:(id)sender {
    LoadURLViewController * loadURLView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadURLViewController"];
    loadURLView.cmsAPIMode = CMS_API_PRIVACY;
    loadURLView.isModeLoadString = true;
    [self presentViewController:loadURLView animated:true completion:nil];
}

- (IBAction)actionRegister:(id)sender {
    
    [self.view endEditing:YES];
//    if ([userGender isEqualToString:@""]){
//        [Singleton showSnakBar:@"Please select gender" multiline:NO];
//    }
    
    if (![txtFirstName hasText]) {
        [Singleton showSnakBar:@"Please enter name" multiline:NO];
    }
    else if (![txtMobileNumber hasText]) {
        [Singleton showSnakBar:@"Please enter Mobile No." multiline:NO];
    }
    else if ([txtMobileNumber.text length] < 10) {
        [Singleton showSnakBar:@"Please enter valid Mobile No." multiline:NO];
    }
    else if (![txtPassword hasText]) {
        [Singleton showSnakBar:@"Please enter password" multiline:NO];
    }
    else if ([txtPassword.text length] < 8) {
        [Singleton showSnakBar:@"Password must contain atleast 8 characters" multiline:IS_IPHONE_5 ? YES : NO];
    }
    else if (![txtConfirmPassword hasText]) {
        [Singleton showSnakBar:@"Please enter confirm password" multiline:NO];
    }
    else if (![txtPassword.text isEqualToString:txtConfirmPassword.text]) {
        [Singleton showSnakBar:@"Passwords don't match" multiline:NO];
    }
    else if (![txtDOB hasText]) {
        [Singleton showSnakBar:@"Please select birth month & year" multiline:NO];
    }
    else if ([userType isEqualToString:@""]){
        [Singleton showSnakBar:@"Please select role" multiline:NO];
    }

    else if (![txtEmaiID hasText]) {
        [Singleton showSnakBar:@"Please enter email Id" multiline:NO];
    } else if (![self isValidEmail:txtEmaiID.text]) {
        [Singleton showSnakBar:@"Please enter valid email Id" multiline:NO];
    } else if (![txtMetroArea hasText]) {
        [Singleton showSnakBar:@"Please enter metro area" multiline:NO];
    }
//    else if (btnCheckUncheck.tag == 0) {
//        [Singleton showSnakBar:@"Please agree on terms and condition" multiline:NO];
//    }
    else {
        START_HUD
        [APP_DELEGATE.apiManager verifyUserEneteredMobile:txtMobileNumber.text CountryCode:lblCountryCode.text withCallBack:^(BOOL success, NSString *message) {
            STOP_HUD
            if (success){
                NSDateFormatter * serverDateFormat = [[NSDateFormatter alloc]init];
                serverDateFormat.dateFormat = @"yyyy-MM-dd";
                
                NSString * dob = [serverDateFormat stringFromDate:previouseDate];
                
                BackgroundCheckViewController *obj = (BackgroundCheckViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"BackgroundCheckViewController"];
                obj.dictUserDetails = @{
                                        @"firstName":txtFirstName.text,
                                        @"lastName":@"",
                                        @"countryCode": !lblCountryCode ? @"" : [NSString stringWithFormat:@"+%@",lblCountryCode.text],
                                        @"mobileNo":txtMobileNumber.text,
                                        @"password":txtPassword.text,
                                        @"birthDate":dob,
                                        @"userGender":userGender,
                                        @"userType":userType,
                                        @"metroArea":txtMetroArea.text,
                                        @"emailId":txtEmaiID.text
//                                        @"allowedRiders":allowedRiders,
//                                        @"vehicleModel":txtVehicleModel.text,
//                                        @"vehicleYear":txtVehicleYear.text,
//                                        @"vehicleState":txtVehicleState.text,
//                                        @"vehiclePlate":txtVehiclePlate.text
                                        };
                [APP_DELEGATE.navigationController pushViewController:obj animated:true];
            } else {
                [Singleton showSnakBar:message multiline:true];
            }
        }];
    }
}

-(BOOL)isValidEmail:(NSString *)email{
    NSString * alphaNum = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *regexTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", alphaNum];
    return [regexTest evaluateWithObject:email];
}

- (IBAction)actionClose:(id)sender {
    [APP_DELEGATE.navigationController popViewControllerAnimated:true];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"dismissParentView" object:nil];
}

- (IBAction)actionUserTypeSelection:(UIButton *)sender {
    
    [btnRider setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnDriver setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnBoth setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    
//    [btnAllowedRiderOnePlus setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
//    allowedRiders = @"2";
    
    [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
    if (sender == btnRider){
        userType = @"1";
//        carContainerHeight.constant = 0;
    } else if (sender == btnDriver){
        userType = @"2";
//        carContainerHeight.constant = 275;
        
    } else {
        userType = @"0";
//        carContainerHeight.constant = 275;
        
    }
//    [UIView animateWithDuration:0.3 animations:^{
//        [self.view layoutIfNeeded];
//    }];
}

- (IBAction)allowRiderAction:(id)sender {
//    [btnAllowedRideOne setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
//    [btnAllowedRiderOnePlus setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
//    
//    [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
//    
//    if ([sender tag] == 101){
//        allowedRiders = @"1";
//    } else {
//        allowedRiders = @"2";
//    }
}


#pragma mark - Custom Methods -

- (void) txtDateTouch : (UITextField *) textField {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM yyyy"];
    
    NTMonthYearPicker *datePicker = [[NTMonthYearPicker alloc] init];
    
    textField.inputView.backgroundColor = [UIColor whiteColor];
//    datePicker.datePickerMode = UIDatePickerModeDate;
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth ;
    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:now];
    [comps setYear:[comps year] - 18];
    NSDate * maximumDate = [gregorian dateFromComponents:comps];
    if (!previouseDate){
        previouseDate = maximumDate;
    }
    
    datePicker.maximumDate = maximumDate;
    datePicker.date = previouseDate ? previouseDate : maximumDate;
    
    strFromDate = [dateFormat stringFromDate:[datePicker date]];
    [datePicker addTarget:self action:@selector(updateTextFieldDate:)
         forControlEvents:UIControlEventValueChanged];
    [txtDOB setInputView:datePicker];
}

-(void)updateTextFieldDate:(NTMonthYearPicker *)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM yyyy"];
    strFromDate = [dateFormat stringFromDate:sender.date];
    previouseDate = sender.date;
}

- (void) doneOfDOB {
    txtDOB.text = strFromDate;
    [txtDOB resignFirstResponder];
}

- (void) resignActiveInputField {
    
    [self.view endEditing:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - Status Bar
-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

//#pragma mark - PickerView Delegate Methods -
//- (void) txtVehicleYearTouched : (UITextField *) textField {
//    
////    UIPickerView *dataPickerView;
////    
////    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
////    [dateFormat setDateFormat:@"yyyy"];
////    arrayYears = [[NSMutableArray alloc]init];
////    
////    NSString * stringYear = [dateFormat stringFromDate:[NSDate date]];
////    for (int i = 1968; i <= [stringYear doubleValue]; i++){
////        [arrayYears addObject:[NSString stringWithFormat:@"%d",i]];
////    }
////    
////    if (DEBUG_MODE){
////        NSLog(@"Total Years : %@", arrayYears);
////    }
////    
////    UIPickerView * pickerYears = [[UIPickerView alloc]init];
////    pickerYears.delegate = self;
////    pickerYears.dataSource = self;
////    pickerYears.tag = 1101;
////    textField.inputView.backgroundColor = [UIColor whiteColor];
//////    [txtVehicleYear setInputView:pickerYears];
////    [pickerYears reloadAllComponents];
////    if ([selectedYear isEqualToString:@""]){
////        [pickerYears selectRow:arrayYears.count - 1 inComponent:0 animated:true];
////    } else {
////        [pickerYears selectRow:[arrayYears indexOfObject:selectedYear]inComponent:0 animated:true];
////    }
//}
//
//- (NSInteger)numberOfComponentsInPickerView:
//(UIPickerView *)pickerView
//{
//    return 1;
//}
//
//
//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
//{
//    return arrayYears.count;
//}
//
//
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    return arrayYears[row];
//}
//
//
//-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
//      inComponent:(NSInteger)component
//{
//    selectedYear = arrayYears[row];
//}
//
//- (void)doneOfVehicleYear
//{
//    txtVehicleYear.text = selectedYear;
//    [txtVehicleYear resignFirstResponder];
//}



@end
