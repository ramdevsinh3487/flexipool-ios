//
//  ForgetPassword.h
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetPassword : UIViewController<UITextFieldDelegate>
{
    
    __weak IBOutlet UILabel *lblHeader;
    __weak IBOutlet UIView *viewBOrder;
    __weak IBOutlet ACFloatingTextField *txtEmail;
    __weak IBOutlet UIButton *btnSubmit;
    
    UILabel * lblCountryCode;
}

@end
