//
//  ForgetPassword.m
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "ForgetPassword.h"

@interface ForgetPassword ()

@end

@implementation ForgetPassword
#pragma mark - viewDidLoad -
- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    [self setupInitialView];
    
    
    UITapGestureRecognizer * tapgesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissOnOutSideTouch)];
    [self.view addGestureRecognizer:tapgesture];
}
- (void) setupInitialView {
    
    viewBOrder.layer.borderWidth = 1.0;
    viewBOrder.layer.borderColor = [APP_BLUE_COLOR CGColor];

    [txtEmail setPadding:8];
    
    [txtEmail setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    
    [btnSubmit.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+2]];
    
    
    lblHeader.text = GET_Language_Value(@"forget_password");
    txtEmail.placeholder = GET_Language_Value(@"email");
    [btnSubmit setTitle:GET_Language_Value(@"submit") forState:UIControlStateNormal];
    
    lblCountryCode = [[UILabel alloc]initWithFrame:CGRectMake(6, 4, txtEmail.frame.size.height, txtEmail.frame.size.height - 10)];
    lblCountryCode.text = [Singleton.sharedInstance findYourCurrentCountryCode];;
    lblCountryCode.textColor = [UIColor lightGrayColor];
    lblCountryCode.textAlignment = NSTextAlignmentCenter;
    lblCountryCode.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UILabel *lblPlusIcon = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, txtEmail.frame.size.height - 10, txtEmail.frame.size.height - 10)];
    lblPlusIcon.text = @"+";
    lblPlusIcon.textColor = [UIColor lightGrayColor];
    lblPlusIcon.textAlignment = NSTextAlignmentLeft;
    lblPlusIcon.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UIView * leftView = [[UIView alloc]initWithFrame:lblCountryCode.frame];
    [leftView addSubview:lblPlusIcon];
    [leftView addSubview:lblCountryCode];
    [txtEmail setLeftView:leftView];
    
    txtEmail.delegate = self;
    
//    viewBOrder.frame = textField.frame;
}

-(void)dismissOnOutSideTouch {
    [self dismissViewControllerAnimated:true completion:nil];
}
#pragma mark - UITextField Delegates -

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    //viewBOrder.frame = textField.frame;
    [self setBorderViewShow:YES];
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self setBorderViewShow:NO];
    return YES;
}

- (void) setBorderViewShow:(BOOL)isShow {
    
    [UIView animateWithDuration:isShow ? 0.5 : 0 animations:^{
        if (isShow){
            viewBOrder.alpha = isShow;
        } else {
            viewBOrder.alpha = txtEmail.hasText;
        }
    }];
}


#pragma mark - Action methods -

- (IBAction)actionSubmit:(id)sender {
    [self.view endEditing:YES];
    
    if (![txtEmail hasText]) {
        [Singleton showSnakBar:@"Please enter Mobile No." multiline:NO];
    }
    else if ([txtEmail.text length] > 12 || [txtEmail.text length] < 8) {
        [Singleton showSnakBar:@"Please eneter valid Mobile No." multiline:NO];
    }
    else
    {
        START_HUD
        [APP_DELEGATE.apiManager recoverUserPasswordWithUserEmailId:txtEmail.text withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
            STOP_HUD
            if (success){
                [Singleton showSnakBar:serverMessange multiline:true];
                backAction()
            } else {
                [Singleton showSnakBar:serverMessange multiline:true];
            }
        }];
    }
}
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtEmail) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 10){
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            return YES;
        } else if ([string isEqualToString:@""]){
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}


@end
