//
//  ChangePassword.m
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "ChangePassword.h"

@interface ChangePassword ()

@end

@implementation ChangePassword
#pragma mark - viewDidLoad -
- (void)viewDidLoad {
    [super viewDidLoad];
   [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    
    [self setupInitialView];
}
- (void) setupInitialView {
    
    
    [txtOldPwd setPadding:8];
    [txtNewPwd setPadding:8];
    [txtConfirmPwd setPadding:8];
    
    [txtOldPwd setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txtNewPwd setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txtConfirmPwd setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];

    [btnSubmit.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE+2]];
    
    lblHeader.text = GET_Language_Value(@"change_pwd");
    txtOldPwd.placeholder = @"Password*";
    txtNewPwd.placeholder = @"New Password*";
    txtConfirmPwd.placeholder = @"Confirm Password*";
    [btnSubmit setTitle:GET_Language_Value(@"update") forState:UIControlStateNormal];
    
    [self setupTheBorderView];
}
#pragma mark - UITextField Delegates -

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    [self setBorderViewShow:YES];
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self setBorderViewShow:NO];
    return YES;
}
-(void)setupTheBorderView {
    viewBorderOldPassword.layer.borderWidth = 1.0;
    viewBorderOldPassword.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderOldPassword.alpha = 0;
    
    viewBorderNewPassword.layer.borderWidth = 1.0;
    viewBorderNewPassword.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderNewPassword.alpha = 0;
    
    viewBorderConfirmPassword.layer.borderWidth = 1.0;
    viewBorderConfirmPassword.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderConfirmPassword.alpha = 0;
}

- (void) setBorderViewShow:(BOOL)isShow {
    UIView * editableView;
    if (activeTextField == txtOldPwd){
        editableView = viewBorderOldPassword;
    } else if (activeTextField == txtNewPwd) {
        editableView = viewBorderNewPassword;
    } else if (activeTextField == txtConfirmPwd) {
        editableView = viewBorderConfirmPassword;
    }
    
    [UIView animateWithDuration:isShow ? 0.5 : 0.2 animations:^{
        if (isShow){
            editableView.alpha = isShow;
        } else {
            editableView.alpha = activeTextField.hasText;
        }
    }];
}




#pragma mark - action methods -
- (IBAction)actionBack:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)actionSubmit:(id)sender {
    
    [self.view endEditing:YES];
    
    if (![txtOldPwd hasText]) {
        [Singleton showSnakBar:@"Please enter Password" multiline:NO];
    } else if (txtOldPwd.text.length < 8){
        [Singleton showSnakBar:@"Old Password must contain atleast 8 characters" multiline:true];
    } else if (![txtOldPwd.text isEqualToString:APP_DELEGATE.userPassword]){
        [Singleton showSnakBar:@"Invalid Password" multiline:NO];
    } else if (![txtNewPwd hasText]) {
        [Singleton showSnakBar:@"Please enter new password" multiline:NO];
    } else if (txtNewPwd.text.length < 8){
        [Singleton showSnakBar:@"New Password must contain atleast 8 characters" multiline:true];
    } else if (![txtConfirmPwd hasText]) {
        [Singleton showSnakBar:@"Please enter confirm password" multiline:NO];
    } else if (txtConfirmPwd.text.length < 8){
        [Singleton showSnakBar:@"Confirm Password must contain atleast 8 characters" multiline:true];
    } else if (![txtNewPwd.text isEqualToString:txtConfirmPwd.text]) {
        [Singleton showSnakBar:@"Password dont match" multiline:NO];
    }
    else
    {
        START_HUD
        [APP_DELEGATE.apiManager updateUserPasswordWithOldPassword:txtOldPwd.text NewPassword:txtNewPwd.text withCallBack:^(BOOL success, NSString *serverMessange) {
            STOP_HUD
            if (success){
                [Singleton showSnakBar:serverMessange multiline:true];
                [self dismissViewControllerAnimated:true completion:^{
                    [APP_DELEGATE logoutTheUserWithSouldShowAlert:false];
                }];
            } else {
                [Singleton showSnakBar:serverMessange multiline:true];
            }
        }];
        
    }
}

#pragma mark - Status Bar

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


@end
