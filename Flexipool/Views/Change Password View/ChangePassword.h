//
//  ChangePassword.h
//  Flexipool
//
//  Created by Nick on 17/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePassword : UIViewController<UITextFieldDelegate>
{
    
    __weak IBOutlet UIButton *btnSubmit;
    __weak IBOutlet ACFloatingTextField *txtOldPwd;
    IBOutlet UIView *viewBorderOldPassword;
    __weak IBOutlet ACFloatingTextField *txtConfirmPwd;
    IBOutlet UIView *viewBorderNewPassword;
    __weak IBOutlet ACFloatingTextField *txtNewPwd;
    IBOutlet UIView *viewBorderConfirmPassword;
    __weak IBOutlet UILabel *lblHeader;
    UITextField * activeTextField;
}

@end
