//
//  FilterViewController.m
//  Flexipool
//
//  Created by Vishal Gohil on 18/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "FilterViewController.h"

@interface FilterViewController ()

@end

@implementation FilterViewController
@synthesize arraySelectedUsers,selectedDate;
- (void)viewDidLoad {
    [super viewDidLoad];
    [lblHeader setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    arrayFavoriteUsers = APP_DELEGATE.arrayFavoriteUsers;
    
    [lblDataMessage setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    if (!arraySelectedUsers){
        arraySelectedUsers = [[NSMutableArray alloc]init];
    }
    
    if (selectedDate){
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:DATE_FORMATE_NEW];
        strFromDate = [dateFormat stringFromDate:selectedDate];
        txtDateOfTravel.text = strFromDate;
    }
    
    [txtDateOfTravel setDelegate:self];
    [txtDateOfTravel setPadding:8.0];
    [txtDateOfTravel setRightPaddingCalender];
    [txtDateOfTravel addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfPickupDate) shouldShowPlaceholder:YES];
    
    [buttonFavoriteUser setBackgroundImage:[UIImage imageNamed:@"filter-active"] forState:UIControlStateSelected];
    [buttonFavoriteUser setBackgroundImage:[UIImage imageNamed:@"filter-deactive"] forState:UIControlStateNormal];
    
    [buttonDateOfTravel setBackgroundImage:[UIImage imageNamed:@"filter-active"] forState:UIControlStateSelected];
    [buttonDateOfTravel setBackgroundImage:[UIImage imageNamed:@"filter-deactive"] forState:UIControlStateNormal];
    
    
    float fontSize = (SCREEN_WIDTH * (TEXTFIELD_FONTS_SIZE - 2)) / 320;
    buttonDateOfTravel.titleLabel.font = [UIFont fontWithName:AppFont_Regular size:fontSize];
    buttonFavoriteUser.titleLabel.font = [UIFont fontWithName:AppFont_Regular size:fontSize];
    [buttonDateOfTravel setTitle:@"  Date Of Travel  " forState:UIControlStateNormal];
    [buttonFavoriteUser setTitle:@"  Favourite User  " forState:UIControlStateNormal];
    
     tblUserList.tableFooterView = [UIView new];
    
    [tblUserList reloadData];
    isModeFavorite = true;
    [self setupUserInterfaceWithIsModeFavoriteUser:isModeFavorite];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)backButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}
- (IBAction)clearButtonTapped:(id)sender {
    selectedDate = nil;
    txtDateOfTravel.text = @"";
    isModeFavorite = true;
    
    [arraySelectedUsers removeAllObjects];
    [tblUserList reloadData];
    
    [self setupUserInterfaceWithIsModeFavoriteUser:isModeFavorite];
}
- (IBAction)favoriteUserButtonTapped:(id)sender {
    isModeFavorite = true;
    [self setupUserInterfaceWithIsModeFavoriteUser:isModeFavorite];
}
- (IBAction)dateofTravelButtonTapped:(id)sender {
    isModeFavorite = false;
    [self setupUserInterfaceWithIsModeFavoriteUser:isModeFavorite];
}

- (IBAction)applyFilterTapped:(id)sender {
    [self dismissViewControllerAnimated:true completion:^{
        [self.delegate didApplyFilterWithDateOfTravel:selectedDate favoriteUsers:arraySelectedUsers];
    }];
}

-(void)setupUserInterfaceWithIsModeFavoriteUser:(BOOL)favoriteUser{
    if (favoriteUser){
        buttonFavoriteUser.selected = true;
        buttonDateOfTravel.selected = false;
        [self.view bringSubviewToFront:viewFilterContainer];
        [self.view bringSubviewToFront:buttonFavoriteUser];
        
        tblUserList.hidden = false;
        txtDateOfTravel.hidden = true;
    } else {
        buttonFavoriteUser.selected = false;
        buttonDateOfTravel.selected = true;
        [self.view bringSubviewToFront:viewFilterContainer];
        [self.view bringSubviewToFront:buttonDateOfTravel];

        tblUserList.hidden = true;
        txtDateOfTravel.hidden = false;
    }
    [tblUserList reloadData];
    [self.view bringSubviewToFront:buttonApplyFilter];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FORMATE_NEW];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.datePickerMode = UIDatePickerModeDate;
    if (selectedDate){
        [datePicker setDate:selectedDate];
    } else {
        [datePicker setDate:[NSDate date]];
    }
    
//    [datePicker setMaximumDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTextFieldDate:)
         forControlEvents:UIControlEventValueChanged];
    
   
    NSDateFormatter *dateFormate1 = [[NSDateFormatter alloc] init];
    [dateFormate1 setDateFormat:DATE_FORMATE_NEW];
    [dateFormate1 setDefaultDate:[NSDate date]];
    
    strFromDate = [dateFormat stringFromDate:[datePicker date]];
    selectedDate = [datePicker date];
    
    textField.inputView = datePicker;
    textField.inputView.backgroundColor = [UIColor whiteColor];
}

-(void)updateTextFieldDate:(UIDatePicker *)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:DATE_FORMATE_NEW];
    strFromDate = [dateFormat stringFromDate:sender.date];
    selectedDate = sender.date;
}

- (void) doneOfPickupDate {
    txtDateOfTravel.text = strFromDate;
    [txtDateOfTravel resignFirstResponder];
}

-(void)resignActiveInputField{
    [self.view endEditing:true];
}

#pragma mark - TableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (arrayFavoriteUsers.count > 0){
        if (isModeFavorite){
            lblDataMessage.hidden = true;
            tblUserList.hidden = false;
        } else {
            lblDataMessage.hidden = true;
            tblUserList.hidden = true;
        }
        return arrayFavoriteUsers.count;
    } else {
        if (isModeFavorite){
            lblDataMessage.hidden = false;
            tblUserList.hidden = true;
            lblDataMessage.text = @"No user found in your favoruite user list";
        } else {
            lblDataMessage.hidden = true;
            tblUserList.hidden = true;
        }
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    UILabel * userName = [cell viewWithTag:101];
    UIImageView * imageViewRadio = [cell viewWithTag:102];
    userName.text = arrayFavoriteUsers[indexPath.row][@"user_name"];
    
    if ([arraySelectedUsers containsObject:arrayFavoriteUsers[indexPath.row]]){
        imageViewRadio.image = [UIImage imageNamed:@"radio-active"];
    } else {
        imageViewRadio.image = [UIImage imageNamed:@"radio-deactive"];
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:true];
    if ([arraySelectedUsers containsObject:arrayFavoriteUsers[indexPath.row]]){
        [arraySelectedUsers removeObject:arrayFavoriteUsers[indexPath.row]];
    } else {
        [arraySelectedUsers addObject:arrayFavoriteUsers[indexPath.row]];
    }
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}
@end
