//
//  FilterViewController.h
//  Flexipool
//
//  Created by Vishal Gohil on 18/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FilterViewControllerDelegate <NSObject>

@optional
-(void)didApplyFilterWithDateOfTravel:(NSDate *)dateOfTravels favoriteUsers:(NSArray *)arrayFavoriteUsers;

@end

@interface FilterViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>{
    
    IBOutlet UILabel *lblHeader;
    IBOutlet UIButton *buttonFavoriteUser;
    IBOutlet UIButton *buttonDateOfTravel;
    IBOutlet UIButton *buttonClearAll;
    IBOutlet UITextField *txtDateOfTravel;
    IBOutlet UIButton *buttonApplyFilter;
    IBOutlet UIView *viewFilterContainer;
    IBOutlet UITableView *tblUserList;
    IBOutlet UILabel *lblDataMessage;
    
    
    NSArray * arrayFavoriteUsers;
    
    
    BOOL isModeFavorite;
    UITextField * activeTextField;
    NSString * strFromDate;
}

@property id <FilterViewControllerDelegate> delegate;
@property NSMutableArray * arraySelectedUsers;
@property NSDate * selectedDate;
@end
