//
//  SignInViewController.h
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegisterViewController.h"
#import "ForgetPassword.h"
@interface SignInViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet ACFloatingTextField *txtCountryCode;
    IBOutlet UIView *viewBorderMobile;
    IBOutlet ACFloatingTextField *txtMobileNumber;
    
    IBOutlet ACFloatingTextField *txtPassword;
    IBOutlet UIView *viewBorderPassword;
    
    IBOutlet UIButton *btnSignIn;
    IBOutlet UIButton *btnForgotPassword;
    IBOutlet UIButton *btnRegister;
    
    UILabel * lblCountryCode;
    
    UITextField * activeTextField;
}

- (IBAction)actionSignIn:(id)sender;
- (IBAction)actionForgotPassword:(id)sender;
- (IBAction)actionRegister:(id)sender;


@end
