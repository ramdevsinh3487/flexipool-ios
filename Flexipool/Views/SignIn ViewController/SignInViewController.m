//
//  SignInViewController.m
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "SignInViewController.h"
#import "RegisterViewController.h"
#import "HomeViewController.h"
#import "ForgetPassword.h"
@interface SignInViewController ()

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupInitialView];
}

- (void) setupInitialView {
    
    [txtCountryCode setPadding:8];
    [txtMobileNumber setPadding:8];
    [txtPassword setPadding:8];
    
    [txtCountryCode setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txtMobileNumber setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [txtPassword setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE]];
    [btnSignIn.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    [btnRegister.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE-2]];
    [btnForgotPassword.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE-4]];
    
    NSMutableAttributedString* mutableTitle = [[NSMutableAttributedString alloc] initWithString:btnRegister.titleLabel.text];
    [mutableTitle addAttribute:NSFontAttributeName value:[UIFont fontWithName:AppFont_Bold size:DONE_BUTTON_FONTS_SIZE-2] range:[btnRegister.titleLabel.text rangeOfString:@"Register"]];
    [mutableTitle addAttribute:NSForegroundColorAttributeName value:APP_BLUE_COLOR range:[btnRegister.titleLabel.text rangeOfString:@"Register"]];
    [btnRegister setAttributedTitle:mutableTitle forState:UIControlStateNormal];
    
    txtMobileNumber.placeholder = GET_Language_Value(@"mobile_number");
    txtPassword.placeholder = GET_Language_Value(@"password");
    
    [btnSignIn setTitle:GET_Language_Value(@"sign_in") forState:UIControlStateNormal];
    //[btnForgotPassword setTitle:GET_Language_Value(@"forget_password") forState:UIControlStateNormal];
     [btnRegister setTitle:GET_Language_Value(@"new_to_ahoy") forState:UIControlStateNormal];

    lblCountryCode = [[UILabel alloc]initWithFrame:CGRectMake(6, 4, txtMobileNumber.frame.size.height - 5, txtMobileNumber.frame.size.height - 10)];
    lblCountryCode.text = [Singleton.sharedInstance findYourCurrentCountryCode];;
    lblCountryCode.textColor = [UIColor lightGrayColor];
    lblCountryCode.textAlignment = NSTextAlignmentCenter;
    lblCountryCode.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UILabel *lblPlusIcon = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, txtMobileNumber.frame.size.height - 10, txtMobileNumber.frame.size.height - 10)];
    lblPlusIcon.text = @"+";
    lblPlusIcon.textColor = [UIColor lightGrayColor];
    lblPlusIcon.textAlignment = NSTextAlignmentLeft;
    lblPlusIcon.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    
    UIView * leftView = [[UIView alloc]initWithFrame:lblCountryCode.frame];
    [leftView addSubview:lblPlusIcon];
    [leftView addSubview:lblCountryCode];
    [txtMobileNumber setLeftView:leftView];
    
    
    txtMobileNumber.placeholder = GET_Language_Value(@"mobile_number");
    
    txtMobileNumber.text = @"";
    txtPassword.text = @"";
    [self setupTheBorderView];
}



-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - UITextField Delegates -
-(void)setupTheBorderView {
    viewBorderMobile.layer.borderWidth = 1.0;
    viewBorderMobile.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderMobile.alpha = 0;
    
    viewBorderPassword.layer.borderWidth = 1.0;
    viewBorderPassword.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderPassword.alpha = 0;
}

- (void) setBorderViewShow:(BOOL)isShow {
    UIView * editableView;
    if (activeTextField == txtMobileNumber){
        editableView = viewBorderMobile;
    } else if (activeTextField == txtPassword) {
        editableView = viewBorderPassword;
    }
    
    [UIView animateWithDuration:isShow ? 0.5 : 0.2 animations:^{
        if (isShow){
            editableView.alpha = isShow;
        } else {
            editableView.alpha = activeTextField.hasText;
        }
    }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    activeTextField = textField;
    [self setBorderViewShow:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == txtCountryCode){
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        return YES;
    } else if (textField == txtMobileNumber) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 10){
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            return YES;
        } else if ([string isEqualToString:@""]){
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    [self setBorderViewShow:NO];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(UIStatusBarStyle)preferredStatusBarStyle {
//    return UIStatusBarStyleLightContent;
//}


#pragma mark - Action Methods -

- (IBAction)actionSignIn:(id)sender {
    
    [self.view endEditing:YES];
    
    NSString * countryCode = !lblCountryCode ? @"":lblCountryCode.text;
    if (![txtMobileNumber hasText]) {
        [Singleton showSnakBar:@"Please enter Mobile No." multiline:NO];
    }
    else if ([txtMobileNumber.text length] > 12 || [txtMobileNumber.text length] < 8) {
        [Singleton showSnakBar:@"Please eneter valid Mobile No." multiline:NO];
    }
    else if (![txtPassword hasText]) {
        [Singleton showSnakBar:@"Please enter password" multiline:NO];
    } else {
        START_HUD
        [APP_DELEGATE.apiManager loginTheUserWithCountryCode:countryCode MobileNumber:txtMobileNumber.text Password:txtPassword.text withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *errorMessage) {
            STOP_HUD
            if (success){
                HomeViewController *obj = (HomeViewController *)[[self storyboard]instantiateViewControllerWithIdentifier:@"HomeViewController"];
                
                APP_DELEGATE.dictUserDetails = dictResponse;
                APP_DELEGATE.loggedUserId = dictResponse[@"user_id"];
                APP_DELEGATE.tokenString = dictResponse[@"token"];
                APP_DELEGATE.isUserLoggedIn = true;
                APP_DELEGATE.userPassword = txtPassword.text;
                if ([dictResponse[@"user_type"] isEqualToString:@"1"]){
                    APP_DELEGATE.isUserModeDriver = false;
                    APP_DELEGATE.loggedUserType = USER_TYPE_PASSENGER;
                } else if ([dictResponse[@"user_type"] isEqualToString:@"2"]){
                    APP_DELEGATE.isUserModeDriver = true;
                    APP_DELEGATE.loggedUserType = USER_TYPE_DRIVER;
                } else {
                    APP_DELEGATE.isUserModeDriver = false;
                    APP_DELEGATE.loggedUserType = USER_TYPE_BOTH;
                }
                
                if ([dictResponse[@"current_role"] isEqualToString:@"1"]){
                    APP_DELEGATE.isUserModeDriver = false;
                } else {
                    APP_DELEGATE.isUserModeDriver = true;
                }
                
                [APP_DELEGATE.apiManager updateUserModeWithIsModeDriver:APP_DELEGATE.isUserModeDriver withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
                    if (DEBUG_MODE){
                        NSLog(@"Mode Updated : %@",serverMessange);
                    }
                }];
                
                [APP_DELEGATE updateFavoriteUserListArray];
                [APP_DELEGATE updateTheArrayOfCancelReasons];
                
                [self.navigationController pushViewController:obj animated:YES];
            } else {
                [Singleton showSnakBar:errorMessage multiline:true];
            }
        }];
    }
}

- (IBAction)actionForgotPassword:(id)sender {
    ForgetPassword * forgetPass = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgetPassword"];
    forgetPass.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [self presentViewController:forgetPass animated:true completion:nil];
}

- (IBAction)actionRegister:(id)sender {
    RegisterViewController * registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [APP_DELEGATE.navigationController pushViewController:registerView animated:true];
}
@end
