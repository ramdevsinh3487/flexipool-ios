//
//  ReasonViewController.m
//  Flexipool
//
//  Created by HimAnshu on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "ReasonViewController.h"
#import "ReasonsCell.h"

@interface ReasonViewController ()

@end

@implementation ReasonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    selectedIndex = 0;
    self.viewPopup.alpha = 0;
    
    [self.lblReasonTitle setFont:[UIFont fontWithName:AppFont_Regular size:POPUP_HEADER_FONTS_SIZE]];
    
    [self.btnSubmit.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];
    
    _lblReasonTitle.text = [NSString stringWithFormat:@"%@?",GET_Language_Value(@"reasons")];
    [_btnSubmit setTitle:GET_Language_Value(@"submit") forState:UIControlStateNormal];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    self.viewPopup.alpha = 1;
    self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView animateWithDuration:0.3/2 animations:^{
        self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            self.viewPopup.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [_tblView reloadData];
            [_tblView layoutIfNeeded];
            _constraintTblHeight.constant = _tblView.contentSize.height;
            [self.view layoutIfNeeded];
        }];
    }];
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableview Delegate Methods -

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  _arrReasons.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReasonsCell *customcell = (ReasonsCell *)[tableView dequeueReusableCellWithIdentifier:@"ReasonsCell"];
    if (customcell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReasonsCell" owner:self options:nil];
        customcell = [nib objectAtIndex:0];
    }
    
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    customcell.lblReason.text = [_arrReasons objectAtIndex:indexPath.row][@"reason"];

    if (indexPath.row == selectedIndex) {
        
        [customcell.btnRadio setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
    }
    else {
        
        [customcell.btnRadio setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    }
    
    return customcell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedIndex = indexPath.row;
    [_tblView reloadData];
}

#pragma mark - Action Methods -

- (IBAction)actionSubmit:(id)sender {
    
    self.viewPopup.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.3/2 animations:^{
        self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.2, 1.2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.3/2 animations:^{
            self.viewPopup.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
        } completion:^(BOOL finished) {
            [self dismissViewControllerAnimated:false completion:^{
                [self.delegate reasonControllerButtonTappedWithIndex:selectedIndex];
            }];
        }];
    }];
}
@end
