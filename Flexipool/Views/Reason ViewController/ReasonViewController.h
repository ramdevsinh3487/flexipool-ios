//
//  ReasonViewController.h
//  Flexipool
//
//  Created by HimAnshu on 12/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ReasonViewControllerDelegate <NSObject>
@optional
-(void)reasonControllerButtonTappedWithIndex:(NSInteger)index;
@end

@interface ReasonViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSInteger selectedIndex;
}

@property id <ReasonViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *viewPopup;
@property (weak, nonatomic) IBOutlet UILabel *lblReasonTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTblHeight;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;

@property NSMutableArray *arrReasons;

- (IBAction)actionSubmit:(id)sender;


@end
