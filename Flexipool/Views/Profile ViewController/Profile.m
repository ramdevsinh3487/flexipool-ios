//
//  Profile.m
//  Flexipool
//
//  Created by Nick on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Profile.h"

@interface Profile ()

@end

@implementation Profile
#pragma mark - viewDidLoad -
- (void)viewDidLoad {
    [super viewDidLoad];
    START_HUD
    scrlView.alpha = 0.0;
    [lblProfile setFont:[UIFont fontWithName:AppFont_Regular size:HEADER_FONTS_SIZE]];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateNotificationsBadgeCount:) name:NOTI_BADGE_COUNT_UPDATED object:nil];
    [self updateNotificationsBadgeCount:nil];
    
    txtUserName.placeholder = @"Name";
    txtProfession.placeholder = @"Profession";
    txtDOB.placeholder = @"Date of Birth";
    txtAGe.placeholder = @"Age";
    txtMobNo.placeholder = @"Mobile No.";
    txtMetroArea.placeholder = @"Metro Area";
    
    txtVehicleModel.placeholder = @"Make/Model/Color";
    txtVehicleYear.placeholder = @"Vehicle Year";
    [txtDOB setRightPaddingDownArrow];
    txtVehicleState.placeholder = @"Vehicle State";
    txtVehiclePlateNo.placeholder = @"Vehicle Plate No.";
    
    txtCommute.placeholder = @"Commute to";
    
    [txtVehicleYear setRightPaddingDownArrow];
    
    lblProfile.text = @"Profile";
    lblRidingCompanion.text = @"Riding Companion References";
    lblAllowedRider.text = @"Allowed (1 Rider or 1+ Rider)";
    lblVehicleInfo.text = @"Vehicle Info";
    [lblGenderTitle setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    
    [btnMale setTitle:@"Male" forState:UIControlStateNormal];
    [btnFemale setTitle:@"Female" forState:UIControlStateNormal];
    [btnAny setTitle:@"Any" forState:UIControlStateNormal];
    
    [btnUpdate setTitle:@"Update" forState:UIControlStateNormal];
    
    
    [self setupInitialView];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self initTextFields];
    scrlView.alpha = 1.0;
    STOP_HUD
}

-(void)updateNotificationsBadgeCount:(NSNotification *)notif {
    [btnNotifications setShouldAnimateBadge:true];
    btnNotifications.badgeValue = APP_DELEGATE.badgeValue;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)initTextFields {
    activeTextField = txtUserName;
    [self setBorderViewShow:NO];
    
    activeTextField = txtProfession;
    [self setBorderViewShow:NO];
    
    activeTextField = txtDOB;
    [self setBorderViewShow:NO];
    
    activeTextField = txtAGe;
    [self setBorderViewShow:NO];
    
    activeTextField = txtMetroArea;
    [self setBorderViewShow:NO];
    
    activeTextField = txtMobNo;
    [self setBorderViewShow:NO];
    
    activeTextField = txtEmeMobNo;
    [self setBorderViewShow:NO];
    
    activeTextField = txtVehicleModel;
    [self setBorderViewShow:NO];
    
    activeTextField = txtVehicleYear;
    [self setBorderViewShow:NO];
    
    activeTextField = txtVehicleState;
    [self setBorderViewShow:NO];
    
    activeTextField = txtVehiclePlateNo;
    [self setBorderViewShow:NO];
    
    activeTextField = txtCommute;
    [self setBorderViewShow:NO];
}


- (void) setupInitialView {
   
    if (DEBUG_MODE) {
        NSLog(@"User Info : %@", APP_DELEGATE.dictUserDetails);
    }
    
    
    [txtDOB addTarget:self action:@selector(txtDateTouch:) forControlEvents:UIControlEventEditingDidBegin];
    [txtDOB addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfDOB) shouldShowPlaceholder:YES];
    
    [txtVehicleYear addTarget:self action:@selector(txtVehicleYearTouched:) forControlEvents:UIControlEventEditingDidBegin];
    [txtVehicleYear addCancelDoneOnKeyboardWithTarget:self cancelAction:@selector(resignActiveInputField) doneAction:@selector(doneOfVehicleYear) shouldShowPlaceholder:YES];
    

    
    [txtCountryCode setPadding:8];
    [txtMobNo setPadding:8];
    [txtAGe setPadding:8];
    [txtDOB setPadding:8];
    [txtEmeMobNo setPadding:8];
    [txtMetroArea setPadding:8];
    [txtProfession setPadding:8];
    [txtUserName setPadding:8];
    [txtVehicleYear setPadding:8];
    [txtVehicleModel setPadding:8];
    [txtVehicleState setPadding:8];
    [txtVehiclePlateNo setPadding:8];
    [txtCommute setPadding:8];

    txtUserName.delegate = self;
    [txtUserName setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    
    [txtCountryCode setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtMobNo setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtAGe setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtDOB setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtEmeMobNo setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtMetroArea setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtProfession setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtVehicleYear setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtVehicleModel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtVehicleState setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [txtVehiclePlateNo setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
     [txtCommute setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
   

    [btn1.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [btn1Plus.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [btnFemale.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [btnMale.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [btnAny.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 3]];
    [btnUpdate.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:DONE_BUTTON_FONTS_SIZE]];

    imgvwProfilePic.layer.cornerRadius = imgvwProfilePic.frame.size.width/2;
    imgvwProfilePic.layer.masksToBounds = YES;

     UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
    
//    UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//    NSString * gender = APP_DELEGATE.dictUserDetails[@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgvwProfilePic.frame.size.width, imgvwProfilePic.frame.size.height)];
   
    activity_indicator.tag = 11111;
    [[imgvwProfilePic viewWithTag:1111] removeFromSuperview];
    
    [imgvwProfilePic addSubview:activity_indicator];
    [activity_indicator startAnimating];
    [activity_indicator setColor:[UIColor blackColor]];
    
    NSString *strUrl = APP_DELEGATE.dictUserDetails[@"user_image"];
    strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    [imgvwProfilePic sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [activity_indicator stopAnimating];
        [activity_indicator removeFromSuperview];
        [[imgvwProfilePic viewWithTag:1111] removeFromSuperview];
        if (!image){
            canCheckProfile = true;
        } else {
            canCheckProfile = false;
        }
    }];
    
    txtUserName.text = [NSString stringWithFormat:@"%@%@", APP_DELEGATE.dictUserDetails[@"first_name"],APP_DELEGATE.dictUserDetails[@"last_name"]];
    
    
    if ([APP_DELEGATE.strCommuteTo length] != 0)
    {
        txtCommute.text = APP_DELEGATE.strCommuteTo;
    }
    

    NSDateFormatter * formtter = [[NSDateFormatter alloc]init];
    [formtter setDateFormat:@"MMM yyyy"];
    if ([formtter dateFromString:APP_DELEGATE.dictUserDetails[@"dob"]]) {
        strFromDate = APP_DELEGATE.dictUserDetails[@"dob"];
    } else {
        [formtter setDateFormat:@"yyyy-MM-dd"];
        NSDate * dateDOB = [formtter dateFromString:APP_DELEGATE.dictUserDetails[@"dob"]];
        [formtter setDateFormat:@"MMM yyyy"];
        strFromDate = [formtter stringFromDate:dateDOB];
    }
    
    txtDOB.text = strFromDate;
    [self calculateAndDisplayUserAge];
    

    txtProfession.text = APP_DELEGATE.dictUserDetails[@"profession"];
    txtMetroArea.text = APP_DELEGATE.dictUserDetails[@"metro_area"];
    txtMobNo.text = APP_DELEGATE.dictUserDetails[@"mobile_no"];
    
    NSLog(@"%@",APP_DELEGATE.dictUserDetails);
    
    
    if ([APP_DELEGATE.dictUserDetails[@"gender"] length] != 0)
        {
        
            if ([[APP_DELEGATE.dictUserDetails[@"gender"] lowercaseString]isEqualToString:@"male"] || [[APP_DELEGATE.dictUserDetails[@"gender"] lowercaseString]isEqualToString:@"1"]){
                [self genderbuttonChanged:btnGenderMale];
            } else {
                [self genderbuttonChanged:btnGenderFemale];
            }
    }
    else
    {
        selectedGender = @"";
        
        [btnGenderMale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
        [btnGenderFemale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    }
    
    
   
    
    txtEmeMobNo.text = APP_DELEGATE.dictUserDetails[@"emergency_mobile_number"];
    
    allowRefers = APP_DELEGATE.dictUserDetails[@"riding_companion_preferences"];
    if (!allowRefers || [allowRefers isEqualToString:@""]){
        allowRefers = @"0";
    }
    
    //Companion refers
    if ([APP_DELEGATE.dictUserDetails[@"riding_companion_preferences"] isEqualToString:@"2"]){
        //Select Only Female
        [btnMale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
        [btnFemale setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        [btnAny setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    } else if ([APP_DELEGATE.dictUserDetails[@"allow_rider"] isEqualToString:@"1"]){
        //Select Only Male
        [btnMale setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        [btnFemale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
        [btnAny setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    } else {
        //Select Only Any
        [btnMale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
        [btnFemale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
        [btnAny setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
    }
    
    if ([APP_DELEGATE.dictUserDetails[@"user_type"] isEqualToString:@"0"] || [APP_DELEGATE.dictUserDetails[@"user_type"] isEqualToString:@"2"]){
        //This is Also A driver User
        allowRiders = APP_DELEGATE.dictUserDetails[@"allow_rider"];
        if (!allowRiders || [allowRiders isEqualToString:@""]){
            allowRiders = @"1";
        }
        
        if ([APP_DELEGATE.dictUserDetails[@"allow_rider"] isEqualToString:@"1"]){
            //Select 1 rider
            [btn1 setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
            [btn1Plus setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
        } else {
            //Select 1+ Rider
            [btn1 setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
            [btn1Plus setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
        }
        
        txtVehicleModel.text = APP_DELEGATE.dictUserDetails[@"vehicle_model"];
        txtVehicleYear.text = APP_DELEGATE.dictUserDetails[@"vehicle_year"];
        txtVehicleState.text = APP_DELEGATE.dictUserDetails[@"vehicle_state"];
        txtVehiclePlateNo.text = APP_DELEGATE.dictUserDetails[@"vehicle_plate_no"];
        
        selectedYear = txtVehicleYear.text;
        

    } else {

        allowRiders = @"";
    }
    
    NSLog(@"%@",APP_DELEGATE.dictUserDetails);
    
   if ([APP_DELEGATE.dictUserDetails[@"user_type"] isEqualToString:@"0"] || [APP_DELEGATE.dictUserDetails[@"user_type"] isEqualToString:@"2"]){
        vehicleInfoContainerHeight.constant = 335;
        txtVehicleYear.userInteractionEnabled = true;
        txtVehicleModel.userInteractionEnabled = true;
        txtVehicleState.userInteractionEnabled = true;
        txtVehiclePlateNo.userInteractionEnabled = true;
        
        txtVehicleYear.hidden = false;
        txtVehicleModel.hidden = false;
        txtVehicleState.hidden = false;
        txtVehiclePlateNo.hidden = false;
        
        btn1.hidden = false;
        btn1.userInteractionEnabled = true;
        
        btn1Plus.hidden = false;
        btn1Plus.userInteractionEnabled = true;
    }else {
        vehicleInfoContainerHeight.constant = 0;
        
        txtVehicleYear.userInteractionEnabled = false;
        txtVehicleModel.userInteractionEnabled = false;
        txtVehicleState.userInteractionEnabled = false;
        txtVehiclePlateNo.userInteractionEnabled = false;
        
        txtVehicleYear.hidden = true;
        txtVehicleModel.hidden = true;
        txtVehicleState.hidden = true;
        txtVehiclePlateNo.hidden = true;
        
        btn1.hidden = true;
        btn1.userInteractionEnabled = false;
        
        btn1Plus.hidden = false;
        btn1Plus.userInteractionEnabled = false;
    }
    
    lblCountryCode = [[UILabel alloc]initWithFrame:CGRectMake(6, 4, txtMobNo.frame.size.height, txtMobNo.frame.size.height - 10)];
    lblCountryCode.text = [Singleton.sharedInstance findYourCurrentCountryCode];;
    lblCountryCode.textColor = [UIColor lightGrayColor];
    lblCountryCode.textAlignment = NSTextAlignmentCenter;
    lblCountryCode.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UILabel *lblPlusIcon = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, txtMobNo.frame.size.height - 10, txtMobNo.frame.size.height - 10)];
    lblPlusIcon.text = @"+";
    lblPlusIcon.textColor = [UIColor lightGrayColor];
    lblPlusIcon.textAlignment = NSTextAlignmentLeft;
    lblPlusIcon.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UIView * leftView = [[UIView alloc]initWithFrame:lblCountryCode.frame];
    [leftView addSubview:lblPlusIcon];
    [leftView addSubview:lblCountryCode];
    [txtMobNo setLeftView:leftView];
    
    lblCountryCode2 = [[UILabel alloc]initWithFrame:CGRectMake(6, 4, txtEmeMobNo.frame.size.height, txtEmeMobNo.frame.size.height - 10)];
    lblCountryCode2.text = [Singleton.sharedInstance findYourCurrentCountryCode];;
    lblCountryCode2.textColor = [UIColor lightGrayColor];
    lblCountryCode2.textAlignment = NSTextAlignmentCenter;
    lblCountryCode2.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UILabel *lblPlusIcon2 = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, txtEmeMobNo.frame.size.height - 10, txtEmeMobNo.frame.size.height - 10)];
    lblPlusIcon2.text = @"+";
    lblPlusIcon2.textColor = [UIColor lightGrayColor];
    lblPlusIcon2.textAlignment = NSTextAlignmentLeft;
    lblPlusIcon2.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE];
    
    UIView * leftView2 = [[UIView alloc]initWithFrame:lblCountryCode.frame];
    [leftView2 addSubview:lblPlusIcon2];
    [leftView2 addSubview:lblCountryCode2];
    [txtEmeMobNo setLeftView:leftView2];
    
    txtMobNo.placeholder = @"Mobile No.";
    txtMobNo.text = APP_DELEGATE.dictUserDetails[@"mobile_no"];
    
    txtEmeMobNo.placeholder = @"Emergency Mobile No.";
    txtEmeMobNo.text = APP_DELEGATE.dictUserDetails[@"emergency_mobile_number"];
    
    if (DEBUG_MODE){
        NSLog(@"User Data : %@",APP_DELEGATE.dictUserDetails);
    }

    [self setupTheBorderView];
}

-(void)calculateAndDisplayUserAge{
    NSDateFormatter * formater = [[NSDateFormatter alloc]init];
    [formater setDateFormat:@"MMM yyyy"];
    NSDate * dateOfBirth = [formater dateFromString:strFromDate];
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:dateOfBirth
                                       toDate:now
                                       options:0];
    NSInteger age = [ageComponents year];
    txtAGe.text = [NSString stringWithFormat:@"%ld Years",(long)age];
}

#pragma mark - Picker Methods -
- (void) txtTouch : (UITextField *) textField
{
    activeTextField = textField;
    
    if(dataPickerView)
    {
        [dataPickerView removeFromSuperview];
        dataPickerView = nil;
    }
    
    dataPickerView = [[UIPickerView alloc] init];
    [dataPickerView setDataSource: self];
    [dataPickerView setDelegate: self];
    dataPickerView.showsSelectionIndicator = YES;
    
    textField.inputView = dataPickerView;
    textField.inputView.backgroundColor = [UIColor whiteColor];
    [dataPickerView reloadAllComponents];
}
#pragma mark - PickerView Delegate Methods -
- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag == 1101){
        return arrayYears.count;
    } else {
        return 0;
    }
    
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView.tag == 1101){
        return arrayYears[row];
    } else {
        return @"";
    }
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    if (pickerView.tag == 1101){
        selectedYear = arrayYears[row];
    } else {
        selectedGender = @"0";
    }
}

#pragma mark - Custom Methods -

- (void) txtDateTouch : (UITextField *) textField {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM yyyy"];
    
    NTMonthYearPicker *datePicker = [[NTMonthYearPicker alloc] init];
    
    textField.inputView.backgroundColor = [UIColor whiteColor];
    //    datePicker.datePickerMode = UIDatePickerModeDate;
    
    unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth ;
    NSDate *now = [NSDate date];
    NSCalendar *gregorian = [NSCalendar currentCalendar];
    NSDateComponents *comps = [gregorian components:unitFlags fromDate:now];
    [comps setYear:[comps year] - 18];
    NSDate * maximumDate = [gregorian dateFromComponents:comps];
    NSDate * previouseDate = [txtDOB.text length] > 0 ? [dateFormat dateFromString:txtDOB.text] : [NSDate date];
    if (!previouseDate){
        previouseDate = maximumDate;
    }
    
    datePicker.maximumDate = maximumDate;
    datePicker.date = previouseDate ? previouseDate : maximumDate;
    
    strFromDate = [dateFormat stringFromDate:[datePicker date]];
    [datePicker addTarget:self action:@selector(updateTextFieldDate:)
         forControlEvents:UIControlEventValueChanged];
    [txtDOB setInputView:datePicker];
}

- (void) txtVehicleYearTouched : (UITextField *) textField {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy"];
    arrayYears = [[NSMutableArray alloc]init];
    
    NSString * stringYear = [dateFormat stringFromDate:[NSDate date]];
    for (int i = 1968; i <= [stringYear doubleValue]; i++){
        [arrayYears addObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    if (DEBUG_MODE){
        NSLog(@"Total Years : %@", arrayYears);
    }
    
    UIPickerView * pickerYears = [[UIPickerView alloc]init];
    pickerYears.delegate = self;
    pickerYears.dataSource = self;
    pickerYears.tag = 1101;
    textField.inputView.backgroundColor = [UIColor whiteColor];
    [txtVehicleYear setInputView:pickerYears];
    [dataPickerView reloadAllComponents];
    
    int index = (int)[arrayYears indexOfObject:selectedYear];
    if (index && index < arrayYears.count){
        [pickerYears selectRow:index inComponent:0 animated:true];
    } else {
        index = (int)(arrayYears.count - 1);
        [pickerYears selectRow:index inComponent:0 animated:true];
    }
}

-(void)updateTextFieldDate:(UIDatePicker *)sender {
//    dateOfBirth = sender.date;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM yyyy"];
    strFromDate = [dateFormat stringFromDate:sender.date];
}

- (void) doneOfDOB {
    
    txtDOB.text = strFromDate;
    [txtDOB resignFirstResponder];
    [self calculateAndDisplayUserAge];
}

- (void) doneOfVehicleYear {
    txtVehicleYear.text = selectedYear;
    [txtVehicleYear resignFirstResponder];
}
#pragma mark - UITextField Delegates -
-(void)setupTheBorderView {
    viewBorderUserName.layer.borderWidth = 1.0;
    viewBorderUserName.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderUserName.alpha = 0;
    
    viewBorderProfession.layer.borderWidth = 1.0;
    viewBorderProfession.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderProfession.alpha = 0;
    
    viewBorderDOB.layer.borderWidth = 1.0;
    viewBorderDOB.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderDOB.alpha = 0;
    
    viewBorderAge.layer.borderWidth = 1.0;
    viewBorderAge.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderAge.alpha = 0;
    
    viewBorderMetroArea.layer.borderWidth = 1.0;
    viewBorderMetroArea.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderMetroArea.alpha = 0;
    
    viewBorderMobile.layer.borderWidth = 1.0;
    viewBorderMobile.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderMobile.alpha = 0;
    
    viewBorderEmergancyMobile.layer.borderWidth = 1.0;
    viewBorderEmergancyMobile.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderEmergancyMobile.alpha = 0;
    
    viewBorderVehicleModel.layer.borderWidth = 1.0;
    viewBorderVehicleModel.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderVehicleModel.alpha = 0;
    
    viewBorderVehicleYear.layer.borderWidth = 1.0;
    viewBorderVehicleYear.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderVehicleYear.alpha = 0;
    
    viewBorderVehicleState.layer.borderWidth = 1.0;
    viewBorderVehicleState.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderVehicleState.alpha = 0;
    
    viewBorderVehiclePlate.layer.borderWidth = 1.0;
    viewBorderVehiclePlate.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderVehiclePlate.alpha = 0;
    
    
    viewBorderCommute.layer.borderWidth = 1.0;
    viewBorderCommute.layer.borderColor = [APP_BLUE_COLOR CGColor];
    viewBorderCommute.alpha = 0;
}

- (void) setBorderViewShow:(BOOL)isShow {
    UIView * editableView;

    if (activeTextField == txtUserName){
        editableView = viewBorderUserName;
    } else if (activeTextField == txtProfession){
        editableView = viewBorderProfession;
    } else if (activeTextField == txtDOB) {
        editableView = viewBorderDOB;
    } else if (activeTextField == txtAGe) {
        editableView = viewBorderAge;
    } else if (activeTextField == txtMetroArea) {
        editableView = viewBorderMetroArea;
    } else if (activeTextField == txtMobNo) {
        editableView = viewBorderMobile;
    } else if (activeTextField == txtEmeMobNo) {
        editableView = viewBorderEmergancyMobile;
    } else if (activeTextField == txtVehicleModel) {
        editableView = viewBorderVehicleModel;
    } else if (activeTextField == txtVehicleYear) {
        editableView = viewBorderVehicleYear;
    } else if (activeTextField == txtVehicleState) {
        editableView = viewBorderVehicleState;
    } else if (activeTextField == txtVehiclePlateNo) {
        editableView = viewBorderVehiclePlate;
    }
    
    else if (activeTextField == txtCommute) {
        editableView = viewBorderCommute;
    }
    
    [UIView animateWithDuration:isShow ? 0.5 : 0.2 animations:^{
        if (isShow){
            editableView.alpha = isShow;
        } else {
            editableView.alpha = activeTextField.hasText;
        }
    }];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
   // ViewBorder.frame = [self]
    activeTextField = textField;
    [self setBorderViewShow:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == txtUserName){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30){
            
            return YES;
        } else {
            return false;
        }
    }
    else if (textField == txtProfession){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 25){
            return YES;
        } else {
            return false;
        }
    }
    else if (textField == txtMobNo || textField == txtEmeMobNo) {
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 10){
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
            return YES;
        } else {
            return false;
        }
    }
    
    else if (textField == txtVehicleModel){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30)
        {
            
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"()1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,1234567890-/() "];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
            return true;
        } else {
            return false;
        }
    }
    else if (textField == txtVehicleState){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 15){
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz. "];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
            return YES;
        } else {
            return false;
        }
    }
    
    else if (textField == txtVehiclePlateNo){
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 20){
            
            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789"];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
        }
        else
            
        {
            return YES;
            
        }
        
       
    }

    else if (textField == txtCommute){
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 35){
            return true;
        } else {
            return false;
        }
    }
    
    else if (textField == txtMetroArea){
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if (newLength <= 30){

            NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@".ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "];
            for (int i = 0; i < [string length]; i++)
            {
                unichar c = [string characterAtIndex:i];
                if (![myCharSet characterIsMember:c])
                {
                    return NO;
                }
            }
            
        }
        else
        {
            return NO;
        }
        
    } else if (textField == txtProfession){
        NSCharacterSet *myCharSet = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz "];
        for (int i = 0; i < [string length]; i++)
        {
            unichar c = [string characterAtIndex:i];
            if (![myCharSet characterIsMember:c])
            {
                return NO;
            }
        }
        
        return YES;
    }
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    [self setBorderViewShow:NO];
    return YES;
}

- (void) resignActiveInputField {
    
    [self.view endEditing:YES];
}

#pragma mark - UIImagePickerView Delegates -

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary*)info{
    
    UIImage *imagePicked = info[UIImagePickerControllerEditedImage];
    imgUserProfile = imagePicked;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self updateTheProfileWithProfileImage:imagePicked];
    imgvwProfilePic.image = imgUserProfile;
}

-(void)updateTheProfileWithProfileImage:(UIImage *)image {
    NSString * userProfession = APP_DELEGATE.dictUserDetails[@"profession"];
    NSString * dob = APP_DELEGATE.dictUserDetails[@"dob"];
    NSString * metroArea = APP_DELEGATE.dictUserDetails[@"metro_area"];
    NSString * mobileNumber = APP_DELEGATE.dictUserDetails[@"mobile_no"];
    NSString * gender = selectedGender;//MALE : FEMALE
    NSString * emergancyMobile = APP_DELEGATE.dictUserDetails[@"emergency_mobile_number"];
    NSString * prefRides = APP_DELEGATE.dictUserDetails[@"riding_companion_preferences"];
    NSString * allsRiders = APP_DELEGATE.dictUserDetails[@"allow_rider"];
    NSString * vahicleModel = APP_DELEGATE.dictUserDetails[@"vehicle_model"];
    NSString * vehicleYear = APP_DELEGATE.dictUserDetails[@"vehicle_year"];
    NSString * vahiclePlate = APP_DELEGATE.dictUserDetails[@"vehicle_plate_no"];
    NSString * vahicleState = APP_DELEGATE.dictUserDetails[@"vehicle_state"];
    NSString * userImageName = APP_DELEGATE.dictUserDetails[@"user_image"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMddhhmmss"];
    NSString *strDate = [dateFormatter stringFromDate:[NSDate date]];
    userImageName = [NSString stringWithFormat:@"%@-%@_img.png",APP_DELEGATE.loggedUserId,strDate];
    
    Singleton *obj = [[Singleton alloc] init];
    [obj uploadToS3WithImage:imgUserProfile orFileWithLocalPath:nil fileName:[NSString stringWithFormat:@"upload/user/%@",userImageName] completion:^(BOOL finished) {
        //        dob formate : @"yyyy-MM-dd";
        START_HUD
        [APP_DELEGATE.apiManager updateUserProfileWithUserName:txtUserName.text UserProfession:userProfession DateOfBirth:dob MetroArea:metroArea MobileNumber:mobileNumber gender:gender EmergancyMobile:emergancyMobile PrefersRide:prefRides AllowedRiders:allsRiders VehcleModel:vahicleModel VehicleYear:vehicleYear VehiclePlate:vahiclePlate vehicleState:vahicleState UserImageName:userImageName commute:txtCommute.text withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
            if (success){
                APP_DELEGATE.dictUserDetails = dictResponse;
                
                 UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
                
//                UIImage * dummyImage = [UIImage imageNamed:@"PHUserMale"];
//                NSString * gender = APP_DELEGATE.dictUserDetails[@"gender"];
//                if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//                    dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//                }
                
                UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, imgvwProfilePic.frame.size.width, imgvwProfilePic.frame.size.height)];
                
                activity_indicator.tag = 11111;
                [[imgvwProfilePic viewWithTag:1111] removeFromSuperview];
                
                [imgvwProfilePic addSubview:activity_indicator];
                [activity_indicator startAnimating];
                [activity_indicator setColor:[UIColor blackColor]];
                
                NSString *strUrl = APP_DELEGATE.dictUserDetails[@"user_image"];
                strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
                
                [imgvwProfilePic sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    [activity_indicator stopAnimating];
                    [activity_indicator removeFromSuperview];
                    [[imgvwProfilePic viewWithTag:1111] removeFromSuperview];
                    if (!image){
                        canCheckProfile = true;
                    } else {
                        canCheckProfile = false;
                    }
                }];

//                [Singleton showSnakBar:serverMessange multiline:true];
            } else {
//                SHOW_ALERT_WITH_CAUTION(@"Unable to update your profile image, please try again later")
            }
            STOP_HUD
        }];
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Action Method -

- (IBAction)actionMenu:(id)sender {
    
    [APP_DELEGATE showMenu];
}

- (IBAction)actionNotofication:(id)sender {    
    [APP_DELEGATE NavigateToNotification];
}

- (IBAction)genderbuttonChanged:(UIButton *)sender {
    [btnGenderMale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnGenderFemale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    
    [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
    
    if (sender == btnGenderMale){
        selectedGender = @"1";
    } else {
        selectedGender = @"2";
    }
}

- (IBAction)actionUpdate:(id)sender {
    
    [self.view endEditing:YES];
    BOOL canCheckVehicleData = APP_DELEGATE.loggedUserType == USER_TYPE_DRIVER || APP_DELEGATE.loggedUserType == USER_TYPE_BOTH;
    if (!imgUserProfile && canCheckProfile) {
        [Singleton showSnakBar:@"Please select profile image" multiline:NO];
    } else if(![txtUserName hasText]) {
        [Singleton showSnakBar:@"Please enter name" multiline:NO];
    } else if(![txtProfession hasText]) {
        [Singleton showSnakBar:@"Please enter profession" multiline:NO];
    }
    else if (![txtDOB hasText]) {
        [Singleton showSnakBar:@"Please enter date of birth" multiline:NO];
    }
    else if (![txtAGe hasText]) {
        [Singleton showSnakBar:@"Please enter age" multiline:NO];
    }
    else if (![txtMetroArea hasText]) {
       [Singleton showSnakBar:@"Please enter metro area" multiline:NO];
    }
    else if (![txtMobNo hasText]) {
        [Singleton showSnakBar:@"Please enter Mobile No." multiline:NO];
    }
//    else if ([selectedGender isEqualToString:@""]) {
//         [Singleton showSnakBar:@"Please select gender" multiline:NO];
//    }
    else if ([txtEmeMobNo hasText] && txtEmeMobNo.text.length < 10) {
        [Singleton showSnakBar:@"Please enter valid emergency Mobile No." multiline:NO];
    }
    else if (canCheckVehicleData && ![txtVehicleModel hasText]) {
        [Singleton showSnakBar:@"Please enter vehicle make and model" multiline:NO];
    }
    else if (canCheckVehicleData && ![txtVehicleYear hasText]) {
        [Singleton showSnakBar:@"Please enter vehicle year" multiline:NO];
    }
    else if (canCheckVehicleData && ![txtVehicleState hasText]) {
        [Singleton showSnakBar:@"Please enter vehicle state" multiline:NO];
    }
    else if (canCheckVehicleData && ![txtVehiclePlateNo hasText]) {
        [Singleton showSnakBar:@"Please enter vehicle plate no" multiline:NO];
    } else {
        START_HUD
        
        
        [APP_DELEGATE.apiManager updateUserProfileWithUserName:txtUserName.text UserProfession:txtProfession.text DateOfBirth:txtDOB.text MetroArea:txtMetroArea.text MobileNumber:txtMobNo.text gender:selectedGender EmergancyMobile:txtEmeMobNo.text PrefersRide:allowRefers AllowedRiders:allowRiders VehcleModel:txtVehicleModel.text VehicleYear:txtVehicleYear.text VehiclePlate:txtVehiclePlateNo.text vehicleState:txtVehicleState.text UserImageName:@"" commute:txtCommute.text withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
            STOP_HUD
            if (success){
                APP_DELEGATE.dictUserDetails = dictResponse;
                [Singleton showSnakBar:@"Your profile has been updated" multiline:true];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    [APP_DELEGATE.navigationController popViewControllerAnimated:true];
                });
            } else {
                [Singleton showSnakBar:serverMessange multiline:true];
            }
        }];
    }
}

- (IBAction)actionUpdateProfile:(id)sender {
    
    [UIImagePickerController showActionOfImagePicker:self];
    
}
- (IBAction)actionRidingPreference:(id)sender {
    
    [btnMale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnFemale setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btnAny setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    
    [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
    
    if (sender == btnMale){
        allowRefers = @"1";
    } else if (sender == btnFemale){
        allowRefers = @"2";
    } else {
        allowRefers = @"0";
    }
}
- (IBAction)actionAllowDriver:(id)sender {
    [btn1 setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    [btn1Plus setImage:[UIImage imageNamed:@"radio-deactive"] forState:UIControlStateNormal];
    
    [sender setImage:[UIImage imageNamed:@"radio-active"] forState:UIControlStateNormal];
    
    if (sender == btn1){
        allowRiders = @"1";
    } else {
        allowRiders = @"2";
    }
}


#pragma mark - Status Bar
-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - didReceiveMemoryWarning -
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
