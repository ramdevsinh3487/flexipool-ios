//
//  Profile.h
//  Flexipool
//
//  Created by Krishna on 13/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <NTMonthYearPicker/NTMonthYearPicker.h>
@interface Profile : UIViewController<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
{

    IBOutlet UIImageView *imgvwProfilePic;
    

    IBOutlet ACFloatingTextField *txtUserName;
    IBOutlet UIView *viewBorderUserName;
    IBOutlet ACFloatingTextField *txtProfession;
    IBOutlet UIView *viewBorderProfession;
    IBOutlet ACFloatingTextField *txtDOB;
    IBOutlet UIView *viewBorderDOB;
    IBOutlet ACFloatingTextField *txtAGe;
    IBOutlet UIView *viewBorderAge;
    IBOutlet ACFloatingTextField *txtMetroArea;
    IBOutlet UIView *viewBorderMetroArea;
    IBOutlet ACFloatingTextField *txtMobNo;
    IBOutlet UIView *viewBorderMobile;
    IBOutlet ACFloatingTextField *txtEmeMobNo;
    IBOutlet UIView *viewBorderEmergancyMobile;
    
    IBOutlet ACFloatingTextField *txtVehicleModel;
    IBOutlet UIView *viewBorderVehicleModel;
    IBOutlet ACFloatingTextField *txtVehicleYear;
    IBOutlet UIView *viewBorderVehicleYear;
    IBOutlet ACFloatingTextField *txtVehicleState;
    IBOutlet UIView *viewBorderVehicleState;
    IBOutlet ACFloatingTextField *txtVehiclePlateNo;
    IBOutlet UIView *viewBorderVehiclePlate;
    
    
    IBOutlet ACFloatingTextField *txtCommute;
    IBOutlet UIView *viewBorderCommute;

    
    IBOutlet ACFloatingTextField *txtCountryCode;
    
    IBOutlet UIButton *btnUpdate;

    IBOutlet UIButton *btnAny;
    IBOutlet UIButton *btnFemale;
    IBOutlet UIButton *btn1Plus;
    IBOutlet UIButton *btn1;
    IBOutlet UIButton *btnMale;

    IBOutlet UILabel *lblRidingCompanion;
    IBOutlet UILabel *lblAllowedRider;
    IBOutlet UILabel *lblVehicleInfo;
    
    __weak IBOutlet UILabel *lblProfile;
    NSString *strFromDate;

    __weak IBOutlet UIScrollView *scrlView;
    UITextField *activeTextField;
    
    UIPickerView *dataPickerView;
    
    NSString * selectedGender;
    
    NSMutableArray * arrayYears;
    NSString * selectedYear;
    
    UIImage * imgUserProfile;
    BOOL canCheckProfile;
    
    UILabel * lblCountryCode, * lblCountryCode2;
    
    IBOutlet NSLayoutConstraint *vehicleInfoContainerHeight;
    
    NSString * allowRiders, * allowRefers;
    IBOutlet UIButton *btnNotifications;
    
    IBOutlet UILabel *lblGenderTitle;
    IBOutlet UIButton *btnGenderMale;
    IBOutlet UIButton *btnGenderFemale;
}

- (IBAction)actionMenu:(id)sender;
- (IBAction)actionNotofication:(id)sender;
- (IBAction)genderbuttonChanged:(UIButton *)sender;

@end
