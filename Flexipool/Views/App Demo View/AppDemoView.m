//
//  AppDemoView.m
//  Flexipool
//
//  Created by Vishal Gohil on 07/12/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AppDemoView.h"

@interface AppDemoView ()

@end

@implementation AppDemoView

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayScreenNames = @[@"first",@"demo_2",@"demo_3",@"demo_4",@"demo_5",@"demo_6",@"demo_7",@"demo_8",@"demo_9"];
    collDemoScreens.allowsSelection = false;
    pgCurrentPage.numberOfPages = arrayScreenNames.count;
    pgCurrentPage.currentPage = 0;
    [collDemoScreens reloadData];
    // Doany additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrayScreenNames.count;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(SCREEN_WIDTH, SCREEN_HEIGHT);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell= [collectionView dequeueReusableCellWithReuseIdentifier:@"AppDemoViewCell" forIndexPath:indexPath];
    UIImageView * imgView = [cell viewWithTag:101];
    imgView.image = [UIImage imageNamed:arrayScreenNames[indexPath.row]];
    
    
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
    pgCurrentPage.currentPage = indexPath.row;
    if (indexPath.row == arrayScreenNames.count - 1){
        if (centerBtnDone.constant != 20){
            centerBtnDone.constant = 20;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [btnDone setTitle:@"DONE" forState:UIControlStateNormal];
    } else {
        if (indexPath.row == 0)
        {
          
            [btnDone setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        }
        else
        {
            
            [btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        [btnDone setTitle:@"SKIP" forState:UIControlStateNormal];
        if (centerBtnDone.constant != -50){
            centerBtnDone.constant = -50;
            [UIView animateWithDuration:0.3 animations:^{
                [self.view layoutIfNeeded];
            }];
        }
        
    }
    
    
}

- (IBAction)btnDoneTapped:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}
@end
