//
//  AppDemoView.h
//  Flexipool
//
//  Created by Vishal Gohil on 07/12/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDemoView : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UIScrollViewDelegate, UICollectionViewDelegateFlowLayout> {
    
    IBOutlet UICollectionView *collDemoScreens;
    IBOutlet UIPageControl *pgCurrentPage;
    IBOutlet UIButton *btnDone;
    IBOutlet NSLayoutConstraint *centerBtnDone;
    NSArray * arrayScreenNames;
}
- (IBAction)btnDoneTapped:(id)sender;

@end
