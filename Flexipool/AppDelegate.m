//
//  AppDelegate.m
//  Flexipool
//
//  Created by HimAnshu on 10/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuCustomCell.h"
#import "WelcomeViewController.h"
#import "HomeViewController.h"
#import "NoNetworkManager.h"
#import "ChangePassword.h"
#import "Notification.h"
#import <UserNotifications/UserNotifications.h>
#import <AWSCore/AWSCore.h>
#import <MessageUI/MessageUI.h>
#import "BackgroundCheckViewController.h"


static NSString *const CUSTOM_URL_SCHEME = @"bundleIDDL";
static NSString *const DYNAMIC_LINK_DOMAIN = @"h3zx6.app.goo.gl";


#define MENU_WIDTH (SCREEN_WIDTH * 0.85)
@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate
//@synthesize viewMenu, tblMenu;
@synthesize arrLanguage;
@synthesize strCommuteTo;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.arrayHomeScreenStatus = [[NSMutableArray alloc]initWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
    
    strCommuteTo = @"";
    
    self.apiManager = [[APIManager alloc]init];
    [self registerUserDefaults];
    [self setupPushNoification];
    [self initializeAndSetupLibraryIntigrations];
    [self initializeDatabaseAndLanguageSetup];
    [self setupApplicationRootViewController];
    [self updateTheArrayOfCancelReasons];
    [self updateFavoriteUserListArray];
    
    self.strLatitude = @"0.000000";
    self.strLongitude = @"0.000000";
    [self getCurrentLocation];
    
    if (DEBUG_MODE){
        NSLog(@"Logged User Id : %@",APP_DELEGATE.loggedUserId);
        NSLog(@"Token String : %@", APP_DELEGATE.tokenString);
        NSLog(@"User Details : %@", APP_DELEGATE.dictUserDetails);
    }
    
    self.badgeValue = @"0";
    
    // Initialize the Amazon Cognito credentials provider
  //  AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc]initWithAccessKey:@"AKIAJ2RD53CCDSMGOQ5A" secretKey:@"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"];
      AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc]initWithAccessKey:@"AKIAIYVGT6I2FVVRJVCQ" secretKey:@"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"];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    
    // Temp Show Passenger Call View
    //[self performSelector:@selector(gotPassengerRequest) withObject:nil afterDelay:5];
    [NoNetworkManager enableLackOfNetworkTakeover];
    
    // Firebase Dynamic Links
    [FIROptions defaultOptions].deepLinkURLScheme = CUSTOM_URL_SCHEME;
    [FIRApp configure];

    self.canNavigateToLogin = false;
    
    
    NSDictionary *notificationPayload = launchOptions[UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notificationPayload){
        self.canNavigateToNotifications = true;
    } else {
        self.canNavigateToNotifications = false;
    }
    
    if (APP_DELEGATE.isRideRunning == false){
        [self clearPreviousRideDetailsFromTheAPP];
    }
    
    
    
    
    [UIApplication sharedApplication].idleTimerDisabled = true;
    
    return YES;
}

-(void)updateHomeStatusArrayWithIndex:(NSString *)stringIndex {
    self.arrayHomeScreenStatus = [[NSMutableArray alloc]initWithArray:@[@"0",@"0",@"0",@"0",@"0",@"0",@"0"]];
    if (stringIndex && ![stringIndex isEqualToString:@""]){
        int index = [stringIndex intValue];
        if (index < self.arrayHomeScreenStatus.count){
            [self.arrayHomeScreenStatus replaceObjectAtIndex:index withObject:@"1"];
        }
    }
}
-(void)updateTheArrayOfCancelReasons {
    if (APP_DELEGATE.isUserLoggedIn){
        [APP_DELEGATE.apiManager getAllCancelationReasonsWithPageNumber:@"0" withCallBack:^(BOOL success, NSArray *arrayResponse, NSString *serverMessange) {
            if (success){
                if (arrayResponse.count > 0){
                    APP_DELEGATE.arrayCancelationReasons = [[NSMutableArray alloc]initWithArray:arrayResponse];
                }
            } else {
                if (DEBUG_MODE){
                    NSLog(@"Can not Update Reasons : %@",serverMessange);
                }
            }
            
            if (APP_DELEGATE.arrayCancelationReasons.count == 0){
                
                NSArray * staticReasons = @[
                                            @{
                                                @"reason_id": @"1",
                                                @"reason": @"Driver is late"
                                                },
                                            @{
                                                @"reason_id": @"2",
                                                @"reason": @"Personal reasons"
                                                },
                                            @{
                                                @"reason_id": @"3",
                                                @"reason": @"Bad weather"
                                                }];
                
                APP_DELEGATE.arrayCancelationReasons = [[NSMutableArray alloc]initWithArray:staticReasons];
            }
        }];
    }
}



-(void)updateFavoriteUserListArray {
    if (APP_DELEGATE.isUserLoggedIn){
        [APP_DELEGATE.apiManager getMyAllFavoriteUsersListWithCallBack:^(BOOL success, NSArray *arrayResponse, NSString *serverMessange) {
            if (success){
                APP_DELEGATE.arrayFavoriteUsers = [[NSMutableArray alloc]initWithArray:arrayResponse];
            } else {
                APP_DELEGATE.arrayFavoriteUsers = [[NSMutableArray alloc]init];
                if (DEBUG_MODE){
                    NSLog(@"Can't Get Favoruite Users : %@",serverMessange);
                }
            }
        }];
    }
}

#pragma mark - PushNotificationCorner
#pragma mark - PushNotificationCorner
- (void) setupPushNoification
{
    
    float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    if (sysVer >= 10.0)
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if( !error ){
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            }
        }];
    }
    else
    {
        if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
            
            UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
            [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
        } else {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
             (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
        }
        
    }
    
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *str = [NSString stringWithFormat:@"%@",deviceToken];
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@""];
    str = [str stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (DEBUG_MODE){
        NSLog(@"Device Token : %@",str);
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:str forKey:DEFAULT_PUSH_TOKEN];
    [[NSUserDefaults standardUserDefaults]synchronize];
    if (APP_DELEGATE.isUserLoggedIn){
        [APP_DELEGATE.apiManager upfateDeviceTokenWithToken:str withCallBack:^(BOOL success, NSString *messange) {
            if (success){
                if (DEBUG_MODE){
                    NSLog(@"Device Token Updated");
                }
            }
        }];
    }
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString   *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
}

-(void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    NSDictionary *aps = (NSDictionary *)[userInfo objectForKey:@"aps"];
    NSString* alertValue = [aps valueForKey:@"badge"];
    NSInteger badgeValue= [alertValue integerValue];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeValue];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    
    NSDictionary *userInfo = (NSDictionary *)[notification.request.content.userInfo objectForKey:@"aps"];
    
    if (DEBUG_MODE){
        NSLog(@"Push notification : %@",userInfo);
    }
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RIDE_STATUS_UPDATED object:nil userInfo:userInfo];
    
    NSString *str = [userInfo valueForKey:@"alert"];
    
    if ([str containsString:@"declined"]) {
        
        //completionHandler(UNAuthorizationOptionNone);
    }
    else {
        
        completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    }
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    //Called to let your app know which action was selected by the user for a given notification.
    NSDictionary *userInfo = (NSDictionary *)[response.notification.request.content.userInfo objectForKey:@"aps"];
    
    if (DEBUG_MODE){
        NSLog(@"Push notification data : %@",userInfo);
    }
    
    if (self.canNavigateToNotifications){
        
//            Notification *obj = (Notification *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"Notification"];
//            self.navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
//            [self.navigationController setNavigationBarHidden:YES];
//            [self.window setRootViewController:self.navigationController];
//            [self createMenu:self.navigationController];
//            [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
//            [self.window makeKeyAndVisible];
//        
//        
//            SET_DEFAULT_VALUE(@"0", @"badge_count");
//            
//            NSString* alertValue = @"0";
//            NSInteger badgeValue= [alertValue integerValue];
//            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeValue];
       
    }
    else
    {
//        UIViewController *rvc= self.navigation.visibleViewController;
//        if ([rvc isMemberOfClass:NSClassFromString(@"NotificationVC")])
//        {
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationReload" object:nil];
//        }
//        else
//        {
//            [self.navigation pushViewController:mNotification animated:YES];
//        }
    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_RIDE_STATUS_UPDATED object:nil userInfo:userInfo];
}


- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    if (DEBUG_MODE){
        NSLog(@"Failled to remote notification : %@",err.localizedDescription);
    }
}

-(void)updateDeviceTokenToServer{
    [APP_DELEGATE.apiManager upfateDeviceTokenWithToken:[[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_PUSH_TOKEN] withCallBack:^(BOOL success, NSString *messange) {
        if (success){
            if (DEBUG_MODE){
                NSLog(@"Device token successfully submited");
            }
        }
    }];
}
#pragma mark end


-(void)initializeAndSetupLibraryIntigrations{
    [GMSServices provideAPIKey:GOOGLE_API_KEY];
    [GMSPlacesClient provideAPIKey:GOOGLE_API_KEY];
    [GMSServices provideAPIKey:GOOGLE_API_KEY];
    
    //Clienr Sandbox DEFAULT_TRIP_DETAILS
    
    //    [PayPalMobile initializeWithClientIdsForEnvironments:
    //     @{PayPalEnvironmentProduction : @"",
    //       PayPalEnvironmentSandbox : @"AU8hwRj8wXqKLlUMUmtRLpgJoy7ESXkDDVylYmMFFiMn6aWUrQ6MM1ln9R-iVN0jLEHYYHPYyRMeCMeO"}];
    
    
    //    Vrinsoft Sandbox Details
//    [PayPalMobile initializeWithClientIdsForEnvironments: @{PayPalEnvironmentProduction : @"", PayPalEnvironmentSandbox : @"ARIfxFRkz3vNc_LiLtxF_4cXNBHOENAC8D0_UtK7Ulha_XJuZkRhUVwQPlCaiYL5D21ZkSZjhn6D-WTz"}];

    [PayPalMobile initializeWithClientIdsForEnvironments: @{PayPalEnvironmentProduction : @"AS0ViVkxgFMY3QPtljnzRjoPXQbwTKLYf_78Xz_vYU9tQ0WB_jIhJf7hEx58zkCeSfo8KhG-lxin31qC", PayPalEnvironmentSandbox : @"ARIfxFRkz3vNc_LiLtxF_4cXNBHOENAC8D0_UtK7Ulha_XJuZkRhUVwQPlCaiYL5D21ZkSZjhn6D-WTz"}];
    
}

-(void)speakStringForNavigation:(NSString *)textToSpeech {
    if (NAVIGATION_MODE && APP_DELEGATE.canSpeakNavigation && APP_DELEGATE.isUserModeDriver){
        AVSpeechSynthesizer * speechSynthesizer = [[AVSpeechSynthesizer alloc]init];
        AVSpeechUtterance * speechUtterance = [AVSpeechUtterance speechUtteranceWithString:textToSpeech];
        [speechSynthesizer speakUtterance:speechUtterance];
    }
}

-(void)initializeDatabaseAndLanguageSetup{
    //Database Object
    [[Database shareDatabase]createEditableCopyOfDatabaseIfNeeded];
    self.db=[Database shareDatabase];
    
    arrLanguage = [[NSMutableArray alloc] init];
    NSString *strQuery = [NSString stringWithFormat:@"SELECT * from tbl_language"];
    APP_DELEGATE.arrLanguage = [[APP_DELEGATE.db SelectAllFromTable:strQuery] mutableCopy];
}

-(void)setupApplicationRootViewController {
    if (APP_DELEGATE.isUserLoggedIn){
        HomeViewController *obj = (HomeViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"HomeViewController"];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
        [self.navigationController setNavigationBarHidden:YES];
        [self.window setRootViewController:self.navigationController];
        [self createMenu:self.navigationController];
        [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
        [self.window makeKeyAndVisible];
    } else {
        WelcomeViewController *obj = (WelcomeViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
        self.navigationController = [[UINavigationController alloc] initWithRootViewController:obj];
        [self.navigationController setNavigationBarHidden:YES];
        [self.window setRootViewController:self.navigationController];
        [self createMenu:self.navigationController];
        [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0]];
        [self.window makeKeyAndVisible];
    }
}

// Temp Show Passenger Call View
- (void) gotPassengerRequest {
    
    PassengerRequestViewController *obj = (PassengerRequestViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"PassengerRequestViewController"];
    obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [[APP_DELEGATE.navigationController.viewControllers firstObject] presentViewController:obj animated:NO completion:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Firebase DynamicLinks -

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *, id> *)options {
    return [self application:app openURL:url sourceApplication:nil annotation:@{}];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    FIRDynamicLink *dynamicLink =
    [[FIRDynamicLinks dynamicLinks] dynamicLinkFromCustomSchemeURL:url];
    
    if (dynamicLink) {
        // Handle the deep link. For example, show the deep-linked content or
        // apply a promotional offer to the user's account.
        // ...
        
        
        
        return YES;
    }
    
    return NO;
}

- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
 restorationHandler:(void (^)(NSArray *))restorationHandler {
    
    BOOL handled = [[FIRDynamicLinks dynamicLinks]
                    handleUniversalLink:userActivity.webpageURL
                    completion:^(FIRDynamicLink * _Nullable dynamicLink,
                                 NSError * _Nullable error) {
                        // ...
                        
                        if (!APP_DELEGATE.isUserLoggedIn) {
                            @try {
                                NSString * absoluteString = dynamicLink.url.absoluteString;
                                if ([absoluteString containsString:@"HOME"]){
                                    [Singleton showSnakBar:@"Please loing first" multiline:false];
                                } else if ([absoluteString containsString:@"LOGIN"]){
                                    NSArray *arrLoginData = [dynamicLink.url.absoluteString componentsSeparatedByString:@"&"];
                                    
                                    NSArray * mobileData = [arrLoginData[1] componentsSeparatedByString:@"="];
                                    NSString * mobile = mobileData[1];
                                    NSArray * passwordData = [arrLoginData[2] componentsSeparatedByString:@"="];
                                    NSString * password = passwordData[1];
                                    
                                    NSString * countryCode = [NSString stringWithFormat:@"+%@",[Singleton.sharedInstance findYourCurrentCountryCode]];
                                    START_HUD
                                    [APP_DELEGATE.apiManager loginTheUserWithCountryCode:countryCode MobileNumber:mobile Password:password withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *errorMessage) {
                                        STOP_HUD
                                        if (success){
                                            HomeViewController *obj = (HomeViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"HomeViewController"];
                                            
                                            APP_DELEGATE.dictUserDetails = dictResponse;
                                            APP_DELEGATE.loggedUserId = dictResponse[@"user_id"];
                                            APP_DELEGATE.tokenString = dictResponse[@"token"];
                                            APP_DELEGATE.isUserLoggedIn = true;
                                            APP_DELEGATE.userPassword = [arrLoginData objectAtIndex:1];
                                            if ([dictResponse[@"user_type"] isEqualToString:@"1"]){
                                                APP_DELEGATE.isUserModeDriver = false;
                                                APP_DELEGATE.loggedUserType = USER_TYPE_PASSENGER;
                                            } else if ([dictResponse[@"user_type"] isEqualToString:@"2"]){
                                                APP_DELEGATE.isUserModeDriver = true;
                                                APP_DELEGATE.loggedUserType = USER_TYPE_DRIVER;
                                            } else {
                                                APP_DELEGATE.isUserModeDriver = false;
                                                APP_DELEGATE.loggedUserType = USER_TYPE_BOTH;
                                            }
                                            
                                            if ([dictResponse[@"current_role"] isEqualToString:@"1"]){
                                                APP_DELEGATE.isUserModeDriver = false;
                                            } else {
                                                APP_DELEGATE.isUserModeDriver = true;
                                            }
                                            
                                            [APP_DELEGATE.apiManager updateUserModeWithIsModeDriver:APP_DELEGATE.isUserModeDriver withCallBack:^(BOOL success, NSDictionary *dictResponse, NSString *serverMessange) {
                                                if (DEBUG_MODE){
                                                    NSLog(@"Mode Updated : %@",serverMessange);
                                                }
                                            }];
                                            
                                            [APP_DELEGATE updateFavoriteUserListArray];
                                            [APP_DELEGATE updateTheArrayOfCancelReasons];
                                            
                                            [self.navigationController pushViewController:obj animated:YES];
                                        } else {
                                            [Singleton showSnakBar:errorMessage multiline:true];
                                        }
                                        STOP_HUD
                                    }];
                                } else {
                                    
                                }
                            } @catch (NSException *exception) {
                                
                            } @finally {
                                
                            }
                        }
                    }];
    
    
    return handled;
}

- (void) createDynamicLink : (NSString *)Link
{
    NSURL *link = [NSURL URLWithString:Link];
    FIRDynamicLinkComponents *components =
    [FIRDynamicLinkComponents componentsWithLink:link
                                          domain:DYNAMIC_LINK_DOMAIN];
    
    
    
    NSString *copyStringverse = components.url.absoluteString;
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:copyStringverse];
}

#pragma mark - Menu -

-(void)createMenu : (UINavigationController *)navigation {
    // Setup Menu Data
    arrMenutitleNames = [[NSMutableArray alloc] initWithObjects:@"Ride Now",@"Plan Ride", @"Share My Ride Info",@"Ride History",@"Fav Spots",@"Share",@"FAQs",@"Contact Us",@"Settings",@"Sign Out", nil];
    
    arrImageIconNames = [[NSMutableArray alloc] initWithObjects:@"menu-ride-now",@"menu-plan-ride",@"menu-sharemyride-info",@"menu-ride-history",@"menu-fav-spot",@"menu-share",@"menu-faq",@"menu-contact",@"menu-settings",@"menu-sign-out", nil];
    
    dictMenuList = [[NSMutableDictionary alloc] init];
    [dictMenuList setValue:@"HomeViewController" forKey:@"Ride Now"];
    [dictMenuList setValue:@"PlanRide" forKey:@"Plan Ride"];
    
    [dictMenuList setValue:@"ShareMyRideVC" forKey:@"Share My Ride Info"];
    [dictMenuList setValue:@"RideHistory" forKey:@"Ride History"];
    
    [dictMenuList setValue:@"FavouriteSpots" forKey:@"Fav Spots"];
    [dictMenuList setValue:@"Share" forKey:@"Share"];
    [dictMenuList setValue:@"FAQViewController" forKey:@"FAQs"];
    [dictMenuList setValue:@"ContactUs" forKey:@"Contact Us"];
    [dictMenuList setValue:@"Settings" forKey:@"Settings"];
    
    
    self.viewLeftMenu = [[[NSBundle mainBundle] loadNibNamed:@"MenuView" owner:self options:nil] firstObject];
    self.viewLeftMenu.tag = 1111111111;
    [self.viewLeftMenu removeFromSuperview];
    self.viewLeftMenu.frame = [UIScreen mainScreen].bounds;
    self.viewLeftMenu.clipsToBounds = YES;
    
//    viewMenu = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];

    self.viewLeftMenu.widthMenuView.constant = MENU_WIDTH;
    self.viewLeftMenu.leftSpaceMenuView.constant = 0 - MENU_WIDTH;
    self.viewLeftMenu.heightTblMenuView.constant = arrMenutitleNames.count * (IS_IPHONE?45:55);
    self.viewLeftMenu.tblMenuOptions.scrollEnabled = false;
    [_viewLeftMenu layoutIfNeeded];
    
//    viewBackground = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    viewBackground.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
//    [navigation.view addSubview:viewBackground];
//    [navigation.view bringSubviewToFront:viewBackground];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    tapGestureRecognizer.delegate = self;
    [self.viewLeftMenu addGestureRecognizer:tapGestureRecognizer];
    
    [navigation.view addSubview:self.viewLeftMenu];
    [navigation.view bringSubviewToFront:self.viewLeftMenu];
    
//    viewHeader = [[UIView alloc] init];
//    viewHeader.backgroundColor = [UIColor whiteColor];
//    viewHeader.frame = CGRectMake(0, 0, MENU_WIDTH, IS_IPHONE ? 125 : 120);
    
  
    
    UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
//    UIImage * dummyImage = [UIImage imageNamed:@"PHMaleActive"];
//    NSString * gender = APP_DELEGATE.dictUserDetails[@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHFemaleActive"];
//    }

    self.viewLeftMenu.imgUserProfile.image = dummyImage;
//    imgViewUser.frame = CGRectMake(15, 22, IS_IPHONE ? 70 : 70, IS_IPHONE ? 70 : 70);
//    imgViewUser.center = CGPointMake(imgViewUser.center.x, viewHeader.center.y+5);
    self.viewLeftMenu.imgUserProfile.contentMode = UIViewContentModeScaleAspectFill;
    self.viewLeftMenu.imgUserProfile.backgroundColor = [UIColor whiteColor];
    self.viewLeftMenu.imgUserProfile.clipsToBounds = true;
    [self.viewLeftMenu.imgUserProfile setCornerRadius:2];
    
    if (self.viewLeftMenu.imgUserProfile.tag != 111111){
        self.viewLeftMenu.imgUserProfile.tag = 111111;
        
        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.viewLeftMenu.imgUserProfile.frame.size.width, self.viewLeftMenu.imgUserProfile.frame.size.height)];
        
        activity_indicator.tag = 11111;
        [[self.viewLeftMenu.imgUserProfile viewWithTag:1111] removeFromSuperview];
        
        [self.viewLeftMenu.imgUserProfile addSubview:activity_indicator];
        [activity_indicator startAnimating];
        [activity_indicator setColor:[UIColor blackColor]];
        
        NSString *strUrl = APP_DELEGATE.dictUserDetails[@"user_image"];
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        [self.viewLeftMenu.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activity_indicator stopAnimating];
            [activity_indicator removeFromSuperview];
            [[self.viewLeftMenu.imgUserProfile viewWithTag:1111] removeFromSuperview];
            self.viewLeftMenu.imgUserProfile.tag = 0;
        }];
    }
    
    
//    lblName = [[UILabel alloc] initWithFrame:CGRectMake(imgViewUser.frame.origin.x + imgViewUser.frame.size.width + 8, imgViewUser.frame.origin.y, MENU_WIDTH - (imgViewUser.frame.origin.x + imgViewUser.frame.size.width + 16), 25)];
    
    self.viewLeftMenu.lblUserName.font = [UIFont fontWithName:AppFont_Semibold size:TEXTFIELD_FONTS_SIZE + 4];
    self.viewLeftMenu.lblUserName.textColor = [UIColor blackColor];
    self.viewLeftMenu.lblUserName.text = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictUserDetails[@"first_name"],APP_DELEGATE.dictUserDetails[@"last_name"]];
    
   
    self.viewLeftMenu.lblUserProfession.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE - 2];
    self.viewLeftMenu.lblUserProfession.textColor = [UIColor blackColor];
    self.viewLeftMenu.lblUserProfession.text = APP_DELEGATE.dictUserDetails[@"profession"];
    
    [self.viewLeftMenu.btnEditProfile addTarget:self action:@selector(actionViewProfile) forControlEvents:UIControlEventTouchUpInside];
    
    float fontSize = (SCREEN_WIDTH * (TEXTFIELD_FONTS_SIZE - 2)) / 320;
    
    [self.viewLeftMenu.btnChangePassword.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:fontSize]];
    self.viewLeftMenu.btnChangePassword.titleLabel.textColor = [UIColor blackColor];
    [self.viewLeftMenu.btnChangePassword addTarget:self action:@selector(actionChangePwd) forControlEvents:UIControlEventTouchUpInside];
    NSMutableAttributedString *strOrigPrc = [[NSMutableAttributedString alloc] initWithString:@"Change Password"];
    
    
    
//    // Wallet icon
//    [btnWallet setImage:[UIImage imageNamed:@"menu-wallet"] forState:UIControlStateNormal];
//    [btnWallet sizeToFit];
//    btnWallet.frame = CGRectMake(viewHeader.frame.size.width - btnWallet.frame.size.width - 15, 24, btnWallet.frame.size.width, btnWallet.frame.size.height);
//    btnWallet.center = CGPointMake(btnWallet.center.x, imgViewUser.center.y + 25);
//    [btnWallet sizeToFit];
//    
//    btnWalletAmount = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btnWalletAmount setBackgroundImage:[UIImage imageNamed:@"menu-wallet-amount"] forState:UIControlStateNormal];
//    [btnWalletAmount setTitle:[APP_DELEGATE formatTheCostWithInputValue:[APP_DELEGATE.WalletAmount doubleValue] isModeValue:false] forState:UIControlStateNormal];
//    
//    btnWalletAmount.titleEdgeInsets = UIEdgeInsetsMake(0, -12, 0, 0);
//    [btnWalletAmount.titleLabel setFont:[UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE-3]];
//    [btnWalletAmount sizeToFit];
//    [btnWalletAmount.titleLabel setTextColor:[UIColor whiteColor]];
//    btnWalletAmount.frame = CGRectMake(viewHeader.frame.size.width - btnWalletAmount.frame.size.width - 20, btnWallet.center.y - (btnWalletAmount.frame.size.height / 2), btnWalletAmount.frame.size.width, btnWalletAmount.frame.size.height);
    
    [self.viewLeftMenu.btnWallet addTarget:self action:@selector(actionWallet) forControlEvents:UIControlEventTouchUpInside];
    
    if (IS_iOS10) {
        [strOrigPrc addAttribute:NSBaselineOffsetAttributeName
                           value:[NSNumber numberWithInteger: NSUnderlineStyleNone]
                           range:NSMakeRange(0,[strOrigPrc length])];
        
    }
    [strOrigPrc addAttribute:NSUnderlineStyleAttributeName
                       value:@1
                       range:NSMakeRange(0,[strOrigPrc length])];
    [self.viewLeftMenu.btnChangePassword setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.viewLeftMenu.btnChangePassword setAttributedTitle:strOrigPrc forState:UIControlStateNormal];
    
    self.viewLeftMenu.tblMenuOptions.backgroundColor = [UIColor whiteColor];
    self.viewLeftMenu.tblMenuOptions.dataSource = self;
    self.viewLeftMenu.tblMenuOptions.delegate = self;
    self.viewLeftMenu.tblMenuOptions.showsVerticalScrollIndicator = NO;
    self.viewLeftMenu.tblMenuOptions.bounces = NO;
    self.viewLeftMenu.tblMenuOptions.tableFooterView = [UIView new];
    [self.viewLeftMenu.tblMenuOptions reloadData];
    
    [self.viewLeftMenu setHidden:YES];
    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        lblName.frame = CGRectMake(imgViewUser.frame.origin.x + imgViewUser.frame.size.width + 8, imgViewUser.frame.origin.y, MENU_WIDTH - (imgViewUser.frame.origin.x + imgViewUser.frame.size.width + 16), 25);
//        lblEmail.frame = CGRectMake(lblName.frame.origin.x, lblName.frame.origin.y + lblName.frame.size.height + 5, lblName.frame.size.width, 15);
//    });
}

- (void)actionChangePwd
{
    [self hideMenu];
    
    ChangePassword *obj = (ChangePassword *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"ChangePassword"];
    obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
    [APP_DELEGATE.navigationController presentViewController:obj animated:true completion:nil];
}

-(void)NavigateToNotification{
    
    [self hideMenu];
    Notification *obj = (Notification *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"Notification"];
    [APP_DELEGATE.navigationController pushViewController:obj animated:YES];
    
}
- (void) setupMenuValues {
    self.viewLeftMenu.lblUserName.text = [NSString stringWithFormat:@"%@ %@",APP_DELEGATE.dictUserDetails[@"first_name"],APP_DELEGATE.dictUserDetails[@"last_name"]];
    self.viewLeftMenu.lblUserProfession.text = APP_DELEGATE.dictUserDetails[@"profession"];
    
    UIImage * dummyImage = [UIImage imageNamed:@"NoGender"];
//    NSString * gender = APP_DELEGATE.dictUserDetails[@"gender"];
//    if ([[gender lowercaseString] isEqualToString:@"female"] || [gender isEqualToString:@"2"]){
//        dummyImage = [UIImage imageNamed:@"PHUserFemale"];
//    }
    
    
    if (self.viewLeftMenu.imgUserProfile.tag != 111111){
        self.viewLeftMenu.imgUserProfile.tag = 111111;
        
        UIActivityIndicatorView *activity_indicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, self.viewLeftMenu.imgUserProfile.frame.size.width, self.viewLeftMenu.imgUserProfile.frame.size.height)];
        
        activity_indicator.tag = 11111;
        [[self.viewLeftMenu.imgUserProfile viewWithTag:1111] removeFromSuperview];
        
        [self.viewLeftMenu.imgUserProfile addSubview:activity_indicator];
        [activity_indicator startAnimating];
        [activity_indicator setColor:[UIColor blackColor]];
        
        NSString *strUrl = APP_DELEGATE.dictUserDetails[@"user_image"];
        strUrl = [strUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
        
        [self.viewLeftMenu.imgUserProfile sd_setImageWithURL:[NSURL URLWithString:strUrl] placeholderImage:dummyImage completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            [activity_indicator stopAnimating];
            [activity_indicator removeFromSuperview];
            [[self.viewLeftMenu.imgUserProfile viewWithTag:1111] removeFromSuperview];
            self.viewLeftMenu.imgUserProfile.tag = 0;
        }];
    }
    
    NSLog(@"%ld",[APP_DELEGATE.WalletAmount length]);
    self.viewLeftMenu.lblWalletAmount.text = [APP_DELEGATE formatTheCostWithInputValue:[APP_DELEGATE.WalletAmount doubleValue] isModeValue:false];
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    [self hideMenu];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isDescendantOfView:self.viewLeftMenu.tblMenuOptions]) {
        return NO;
    }
    return YES;
}

- (void) showMenu
{
    [self setupMenuValues];
    
    [self.window endEditing:YES];
    [self.viewLeftMenu setHidden:NO];

    [self.viewLeftMenu.tblMenuOptions reloadData];
    self.viewLeftMenu.leftSpaceMenuView.constant = 0;
//    [UIView animateWithDuration:0.3 animations:^{
//        [self.viewLeftMenu layoutIfNeeded];
//    }];
//    return;
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveLinear animations:^{
        [UIView animateWithDuration:0.4 delay:0 options:UIViewAnimationOptionBeginFromCurrentState animations:(void (^)(void)) ^{
                             [self.viewLeftMenu layoutIfNeeded];
                             self.viewLeftMenu.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
                     completion:^(BOOL finished){
                     }];
}

- (void) hideMenu
{
    self.viewLeftMenu.leftSpaceMenuView.constant = 0 - MENU_WIDTH;
    [UIView animateWithDuration:0.2
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:(void (^)(void)) ^{
                         
                         self.viewLeftMenu.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
                         
                         [UIView animateWithDuration:0.2 delay:0 options: UIViewAnimationOptionBeginFromCurrentState animations:(void (^)(void)) ^{
                             [self.viewLeftMenu layoutIfNeeded];
                                              
                                          }
                                          completion:^(BOOL finished){
                                              [self.viewLeftMenu setHidden:YES];
                                          }];
                     }
                     completion:^(BOOL finished){
                         
                         
                     }];
}

- (void) actionWallet {
    
    [self hideMenu];
    
    AhoyWallet *obj = (AhoyWallet *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"AhoyWallet"];
    obj.isModeRefillFromPayment = false;
    [self.navigationController pushViewController:obj animated:NO];
}

- (void) actionViewProfile {
    
    [self hideMenu];
    
    Profile *obj = (Profile *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"Profile"];
    [self.navigationController pushViewController:obj animated:NO];
}

#pragma mark - UITableview Delegate Methods -

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  arrMenutitleNames.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_IPHONE?45:55;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MenuCustomCell *customcell = (MenuCustomCell *)[tableView dequeueReusableCellWithIdentifier:@"MenuCustomCell"];
    if (customcell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MenuCustomCell" owner:self options:nil];
        customcell = [nib objectAtIndex:0];
    }
    
    customcell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    customcell.lblTitle.text = [arrMenutitleNames objectAtIndex:indexPath.row];
    customcell.lblTitle.font = [UIFont fontWithName:AppFont_Regular size:TEXTFIELD_FONTS_SIZE + 1];
    customcell.imgMenu.image = [UIImage imageNamed:[arrImageIconNames objectAtIndex:indexPath.row]];
    
    return customcell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self hideMenu];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_MENU_ITEM_SELECTED object:nil];
    
    if (indexPath.row == 2){
        if (APP_DELEGATE.isRideRunning && APP_DELEGATE.runningRideStatus == RIDE_STATUS_ON_GOING) {
            [self shareMyRideUsingDefaultMessageApp];
        } else {
            [Singleton showSnakBar:@"You can share you ride while your ride is running" multiline:true];
        }
    } else if (indexPath.row == 5) {
        // UIImage *image = [UIImage imageNamed:@"AppIcon"];
        NSArray *activityItems = @[@"I am enjoying Ahoy for my carpooling! Click the link to install the App", ItunesAppLink];
        UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
        activityViewControntroller.excludedActivityTypes = @[];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            activityViewControntroller.popoverPresentationController.sourceView = self.window;
            activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.window.bounds.size.width/2, self.window.bounds.size.height/4, 0, 0);
        }
        [APP_DELEGATE.navigationController presentViewController:activityViewControntroller animated:true completion:nil];
        
        return;
    } else if (indexPath.row == arrMenutitleNames.count - 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self logoutTheUserWithSouldShowAlert:true];
        });
    }
    else
    {
        Class className = NSClassFromString([dictMenuList valueForKey:[arrMenutitleNames objectAtIndex:indexPath.row]]);
        
        for (UIViewController *view in self.navigationController.viewControllers) {
            if ([view isKindOfClass:className]) {
                
                [self.navigationController popToViewController:view animated:NO];
                if (indexPath.row == 0){
                    [APP_DELEGATE CheckLocationPermissionWithSourceView:view];
                }
                return;
            }
        }
        
        id viewController = [MAIN_STORYBOARD instantiateViewControllerWithIdentifier:[dictMenuList valueForKey:[arrMenutitleNames objectAtIndex:indexPath.row]]];
        [self.navigationController pushViewController:viewController animated:NO];
        if (indexPath.row == 0){
            [APP_DELEGATE CheckLocationPermissionWithSourceView:viewController];
        }
    }
}

-(void)shareMyRideUsingDefaultMessageApp {
    //Message
    if(![MFMessageComposeViewController canSendText]) {
        SHOW_ALERT_WITH_CAUTION(@"Unable to send text messages \n Please check your device settings");
        return;
    } else {
        START_HUD
        [APP_DELEGATE.apiManager didShareMyRideInfoWithRideId:APP_DELEGATE.runningRideId ContactNumber:@"" withCallBack:^(BOOL success, NSString *serverMessage) {
            STOP_HUD
            if (success) {
                serverMessage = [serverMessage stringByAppendingString:APP_DELEGATE.tripFormatedDuration];
                MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
                messageController.messageComposeDelegate = self;
                //                [messageController setRecipients:@[@""]];
                [messageController setBody:serverMessage];
                [self.navigationController presentViewController:messageController animated:YES completion:nil];
            }
        }];
    }
}

//-(void)shareMyRideUsingDefaultMessageApp {
//    //Message
//    if(![MFMessageComposeViewController canSendText]) {
//        SHOW_ALERT_WITH_CAUTION(@"Unable to send text messages \n Please check your device settings");
//        return;
//    } else {
//        START_HUD
//        [APP_DELEGATE.apiManager didShareMyRideInfoWithRideId:APP_DELEGATE.runningRideId ContactNumber:@"" withCallBack:^(BOOL success, NSString *serverMessage) {
//            STOP_HUD
//            if (success) {
//                MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
//                messageController.messageComposeDelegate = self;
////                [messageController setRecipients:@[@""]];
//                [messageController setBody:serverMessage];
//                [self.navigationController presentViewController:messageController animated:YES completion:nil];
//            } else {
//                
//            }
//        }];
//    }
//}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    NSLog(@"Result : %ld", (long)result);
    [controller dismissViewControllerAnimated:true completion:nil];
}

- (void)alertControllerButtonTappedWithIndex:(NSInteger)index {
    
    if (index == 1) {
        
        [self logoutUser];
    }
}


#pragma mark - Custom Methods -
- (void) logoutTheUserWithSouldShowAlert:(BOOL)shouldShowAlert {
    [self hideMenu];
    if (shouldShowAlert){
        AlertViewController *obj = (AlertViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"AlertViewController"];
        obj.modalPresentationStyle = UIModalPresentationOverFullScreen;
        obj.delegate = self;
        obj.alertTitles = @"Signout ?";
        obj.alertMessages = @"Are you sure you want to Signout ?";
        obj.yesTitle = @"Signout";
        obj.noTitle = @"Cancel";
        [APP_DELEGATE.navigationController presentViewController:obj animated:NO completion:nil];
    } else {
        [self logoutUser];
    }
}

- (void) logoutUser
{
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
    if (APP_DELEGATE.isUserLoggedIn){
        START_HUD
        [APP_DELEGATE.apiManager logoutTheLoggedUserFromTheAppWithCallBack:^(BOOL success, NSString *serverMessange) {
            STOP_HUD
            [APP_DELEGATE.navigationController dismissViewControllerAnimated:true completion:nil];
            APP_DELEGATE.canNavigateToLogin = true;
            [self resetDefaults];
        }];
    }
}

- (void) resetNavigationStack
{
    WelcomeViewController *obj = (WelcomeViewController *)[MAIN_STORYBOARD instantiateViewControllerWithIdentifier:@"WelcomeViewController"];
    
    for (UIViewController *view in self.navigationController.viewControllers) {
        if ([view isKindOfClass:obj.classForCoder]) {
            [self.navigationController popToViewController:view animated:NO];
            return;
        }
    }

    [self.navigationController pushViewController:obj animated:NO];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray: APP_DELEGATE.navigationController.viewControllers];
    for (UIViewController *controller in APP_DELEGATE.navigationController.viewControllers)
    {
        if (![controller isKindOfClass:[WelcomeViewController class]])
        {
            [allViewControllers removeObjectIdenticalTo:controller];
        }
    }
    APP_DELEGATE.navigationController.viewControllers = allViewControllers;
}

#pragma mark - UserLocationManager -

-(void)getCurrentLocation {
    
    self.locationManager = [[CLLocationManager alloc] init];
    if ([CLLocationManager locationServicesEnabled]) {
        self.locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        
        [self.locationManager setDelegate:self];
        
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        
        float sysVer = [[[UIDevice currentDevice] systemVersion] floatValue];
        if (sysVer >= 8.0)
        {
            [self.locationManager requestWhenInUseAuthorization];
            [self.locationManager requestAlwaysAuthorization];
            if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
                [self.locationManager requestWhenInUseAuthorization];
            }
        }
        
//        [self.locationManager startMonitoringSignificantLocationChanges];
        [self.locationManager startUpdatingLocation];
        
        if ([CLLocationManager headingAvailable]) {
            self.locationManager.headingFilter = 5;
            [self.locationManager startUpdatingHeading];
        }


        [self.locationManager startUpdatingHeading];
        
        self.strLatitude =[NSString stringWithFormat:@"%f",self.locationManager.location.coordinate.latitude];
        self.strLongitude =[NSString stringWithFormat:@"%f",self.locationManager.location.coordinate.longitude];
        self.userLocation = self.locationManager.location.coordinate;
        
        if (self.userLocation.latitude > 0 && self.userLocation.longitude > 0 && APP_DELEGATE.isUserLoggedIn){
            [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_USER_LOCATION_UPDATES object:nil];
        }
    }
}

- (void)requestWhenInUseAuthorization {
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    
    // If the status is denied or only granted for when in use, display an alert
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse || status == kCLAuthorizationStatusDenied) {
        //        [self showEnableGPSScreen];
    } else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    CLLocation *location = [locations lastObject];
    self.userLocation = location.coordinate;
    self.strLatitude = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    self.strLongitude = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    if (self.userLocation.latitude > 0 && self.userLocation.longitude > 0 && APP_DELEGATE.isUserLoggedIn){
        [[NSNotificationCenter defaultCenter]postNotificationName:NOTI_USER_LOCATION_UPDATES object:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"Some how Location is not updating");
}

-(void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    self.trueHeading = [newHeading trueHeading];
    self.magneticeading = [newHeading magneticHeading];
}


-(void)moveMarker:(GMSMarker *)marker WithAnimationFromCoordinate:(CLLocationCoordinate2D)oldLocation toCoordinate:(CLLocationCoordinate2D)newLocation{
//    if (APP_DELEGATE.isUserModeDriver && APP_DELEGATE.runningRideStatus != RIDE_STATUS_ON_GOING){
//        [CATransaction begin];
//        [CATransaction setAnimationDuration:10.0];
//        marker.position = newLocation;
//        [CATransaction commit];
//    } else {
//        [CATransaction begin];
//        [CATransaction setAnimationDuration:2.0];
//        marker.rotation = [self angleFromCoordinate:oldLocation toCoordinate:newLocation] * (180.0 / M_PI);
//        [CATransaction commit];
//        
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [CATransaction begin];
//            [CATransaction setAnimationDuration:8.0];
//            marker.position = newLocation;
//            [CATransaction commit];
//        });
//    }
    
    marker.position = newLocation;
}

- (float)angleFromCoordinate:(CLLocationCoordinate2D)first toCoordinate:(CLLocationCoordinate2D)second {
    
    float deltaLongitude = second.longitude - first.longitude;
    float deltaLatitude = second.latitude - first.latitude;
    float angle = (M_PI * .5f) - atan(deltaLatitude / deltaLongitude);
    
    if (deltaLongitude > 0)      return angle;
    else if (deltaLongitude < 0) return angle + M_PI;
    else if (deltaLatitude < 0)  return M_PI;
    
    return 0.0f;
}

-(BOOL)CheckLocationPermissionWithSourceView:(UIViewController *)sourceView {
    
    BOOL isValid = FALSE;
    
    if(![CLLocationManager locationServicesEnabled])
    {
        NSString *strLocationAlert = [NSString stringWithFormat:@"Turn On Location Services to Allow \"%@\" to Determine Your Location", AlertTitle];
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:strLocationAlert
                                     message:nil
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Settings"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                        
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                    }];
        
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                   }];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        [sourceView presentViewController:alert animated:YES completion:nil];
        
        isValid = FALSE;
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        
        NSString *strMessages = [NSString stringWithFormat:@"Allow %@ to access your location while you are using the app?",AlertTitle];

        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:[NSString stringWithFormat:AlertTitle]
                                     message:strMessages
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* whileUsingApp = [UIAlertAction
                                        actionWithTitle:@"Okay"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
//                                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                        }];
        
        UIAlertAction* AlwaysAllow = [UIAlertAction
                                      actionWithTitle:@"Settings"
                                      style:UIAlertActionStyleDefault
                                      handler:^(UIAlertAction * action) {
                                          
                                          [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                      }];
//
//        
//        UIAlertAction* DoNotAllow = [UIAlertAction
//                                     actionWithTitle:@"Don't Allow"
//                                     style:UIAlertActionStyleDefault
//                                     handler:^(UIAlertAction * action) {
//                                         
//                                     }];
        
        
        [alert addAction:whileUsingApp];
        [alert addAction:AlwaysAllow];
        //[alert addAction:DoNotAllow];
        
        [sourceView presentViewController:alert animated:YES completion:nil];
        
        isValid = FALSE;
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        isValid = FALSE;
    }
    else
    {
        isValid = TRUE;
    }
    
    return isValid;
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Flexipool"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - NSUserDefaultsCorner
- (void) resetDefaults {
    
    NSString * deviceToken = [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_PUSH_TOKEN];
    
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        if (![key isEqualToString:DEFAULT_PUSH_TOKEN] && ![key isEqualToString:DEFAULT_CURRENY_SYMBOL] && ![key isEqualToString:DEFAULT_DEMO_PRESENTED]){
             [defs removeObjectForKey:key];
        }
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:deviceToken forKey:DEFAULT_PUSH_TOKEN];
    [defs synchronize];
    [self registerUserDefaults];
    [self clearPreviousRideDetailsFromTheAPP];
    [self resetNavigationStack];
}

-(void)registerUserDefaults{
    [[NSUserDefaults standardUserDefaults]registerDefaults:@{
                                                             DEFAULT_IS_LOGGED:@false,
                                                             DEFAULT_USER_ID:@"",
                                                             DEFAULT_DEMO_PRESENTED:@false,
                                                             DEFAULT_USER_TYPE:@"0",
                                                             DEFAULT_IS_MODE_SRIVER:@false,
                                                             DEFAULT_USER_DATA:@{},
                                                             DEFAULT_TOKEN_STRING:@"",
                                                             DEFAULT_PUSH_TOKEN:@"",
                                                             DEFAULT_LAST_SELECTION:@{},
                                                             DEFAULT_CURRENY_SYMBOL:@"$",
                                                             DEFAULT_DECIMAL_POINT:@"2",
                                                             DEFAULT_WALLET_AMOUNT:@0,
                                                
                                                             DEFAULT_ACTUAL_ROUTE:@[],
                                                             DEFAULT_RIDE_RATES:@10.0,
                                                             DEFAULT_GLOBAL_TIMEOUT:@120,
                                                             DEFAULT_IS_RIDE_RUNNING:@false,
                                                             DEFAULT_RIDE_STATUS:@"0",
                                                             DEFAULT_TRIP_START_LOCATION:@{
                                                                     @"lattitude":@"00.00000",
                                                                     @"longitude":@"00.00000"},
                                                             DEFAULT_TRIP_END_LOCATION:@{
                                                                     @"lattitude":@"00.00000",
                                                                     @"longitude":@"00.00000"},
                                                             DEFAULT_CANCEL_REASONS:@[],
                                                             DEFAULT_WALLET_AMOUNT:@"0",
                                                             
                                                             DEFAULT_TRIP_START_DETAILS:@{
                                                                     @"title":@"",
                                                                     @"fullAddress":@"",
                                                                     @"lattitude":@"00.00000",
                                                                     @"longitude":@"00.00000"
                                                                     },
                                                             DEFAULT_TRIP_END_DETAILS:@{
                                                                     @"title":@"",
                                                                     @"fullAddress":@"",
                                                                     @"lattitude":@"00.00000",
                                                                     @"longitude":@"00.00000"
                                                                     },
                                                             DEFAULT_CAN_AVOID_TOLLS:@false,
                                                             DEFAULT_CAN_AVOID_HIGHWAYS:@false,
                                                             DEFAULT_CAN_SPEAK_NAVIGATION:@true,
                                                             }];
}

-(void)clearPreviousRideDetailsFromTheAPP {
    APP_DELEGATE.sendRequestDate = [NSDate date];
    APP_DELEGATE.isRideRunning = false;
    APP_DELEGATE.runningRideStatus = RIDE_STATUS_NONE;
    APP_DELEGATE.tripStartLocation = APP_DELEGATE.userLocation;
    APP_DELEGATE.tripEndLocation = CLLocationCoordinate2DMake(00.00000, 00.000000);
    
    APP_DELEGATE.dictPickupDetails = @{
                                       @"title":@"",
                                       @"fullAddress":@"",
                                       @"lattitude":@"00.00000",
                                       @"longitude":@"00.00000"
                                       };
    APP_DELEGATE.dictDropoffDetails = @{
                                        @"title":@"",
                                        @"fullAddress":@"",
                                        @"lattitude":@"00.00000",
                                        @"longitude":@"00.00000"
                                        };
    APP_DELEGATE.arrayActualRoute = @[];
    [APP_DELEGATE updateHomeStatusArrayWithIndex:@""];
}


-(NSString *)findActualRouteDistance {
    float distanceMeter = 0;
    for (int i = 0; i < APP_DELEGATE.arrayActualRoute.count; i ++){
        if (i + 1 < APP_DELEGATE.arrayActualRoute.count){
            NSDictionary * dictStartCoordinate = APP_DELEGATE.arrayActualRoute[i];
            NSDictionary * dictEndCoordinate = APP_DELEGATE.arrayActualRoute[i + 1];
            
            double startLattitude = [dictStartCoordinate[@"lattitude"] doubleValue];
            double startLongitude = [dictStartCoordinate[@"longitude"] doubleValue];
            
            double endLattitude = [dictEndCoordinate[@"lattitude"] doubleValue];
            double endLongitude = [dictEndCoordinate[@"longitude"] doubleValue];
            
            CLLocationCoordinate2D startCoor = CLLocationCoordinate2DMake(startLattitude, startLongitude);
            CLLocationCoordinate2D endCoor = CLLocationCoordinate2DMake(endLattitude, endLongitude);
            
            MKMapPoint point1 = MKMapPointForCoordinate(startCoor);
            MKMapPoint point2 = MKMapPointForCoordinate(endCoor);
            float distance = MKMetersBetweenMapPoints(point1, point2);
            distanceMeter = distanceMeter + distance;
        }
    }
    
    if (DEBUG_MODE){
        NSLog(@"Actual Distance is : %@ KM", [NSString stringWithFormat:@"%0.1f", distanceMeter / 1000]);
    }
    
    return [NSString stringWithFormat:@"%0.1f", distanceMeter / 1000];
    
}

//Setter || Getter
-(void)setIsUserLoggedIn:(BOOL)isUserLoggedIn{
    [[NSUserDefaults standardUserDefaults]setBool:isUserLoggedIn forKey:DEFAULT_IS_LOGGED];
}
-(BOOL)isUserLoggedIn{
    return [[NSUserDefaults standardUserDefaults]boolForKey:DEFAULT_IS_LOGGED];
}

-(void)setIsDemoPresented:(BOOL)isDemoPresented{
    [[NSUserDefaults standardUserDefaults]setBool:isDemoPresented forKey:DEFAULT_DEMO_PRESENTED];
}
-(BOOL)isDemoPresented{
    return [[NSUserDefaults standardUserDefaults]boolForKey:DEFAULT_DEMO_PRESENTED];
}

-(void)setLoggedUserId:(NSString *)loggedUserId{
    [[NSUserDefaults standardUserDefaults]setObject:loggedUserId forKey:DEFAULT_USER_ID];
}
-(NSString *)loggedUserId{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_USER_ID];
}
-(void)setLoggedUserType:(LoggedUserType )loggedUserType{
    NSString * userType = @"0";
    if (loggedUserType == USER_TYPE_PASSENGER){
        userType = @"1";
    } else if (loggedUserType == USER_TYPE_DRIVER){
        userType = @"2";
    } else {
        userType = @"0";
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:userType forKey:DEFAULT_USER_TYPE];
}
-(LoggedUserType)loggedUserType{
    NSString * userType = [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_USER_TYPE];
    if ([userType isEqualToString:@"1"]){
        return USER_TYPE_PASSENGER;
    } else if ([userType isEqualToString:@"2"]){
        return USER_TYPE_DRIVER;
    } else {
        return USER_TYPE_BOTH;
    }
}

-(void)setIsUserModeDriver:(BOOL)isUserModeDriver{
    [[NSUserDefaults standardUserDefaults]setBool:isUserModeDriver forKey:DEFAULT_IS_MODE_SRIVER];
}
-(BOOL)isUserModeDriver{
    return [[NSUserDefaults standardUserDefaults]boolForKey:DEFAULT_IS_MODE_SRIVER];
}

-(void)setDictUserDetails:(NSDictionary *)dictUserDetails{
    [[NSUserDefaults standardUserDefaults]setObject:dictUserDetails forKey:DEFAULT_USER_DATA];
}
-(NSDictionary *)dictUserDetails{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_USER_DATA];
}

-(void)setUserPassword:(NSString *)userPassword{
    [[NSUserDefaults standardUserDefaults]setObject:userPassword forKey:DEFAULT_USER_PASSWORD];
}

-(NSString *)userPassword{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_USER_PASSWORD];
}

-(void)setTokenString:(NSString *)tokenString{
    [[NSUserDefaults standardUserDefaults]setObject:tokenString forKey:DEFAULT_TOKEN_STRING];
}
-(NSString *)tokenString{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_TOKEN_STRING];
}

-(void)setDictLastSelectedLocation:(NSDictionary *)dictLastSelectedLocation{
    [[NSUserDefaults standardUserDefaults]setObject:dictLastSelectedLocation forKey:DEFAULT_LAST_SELECTION];
}
-(NSDictionary *)dictLastSelectedLocation{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_LAST_SELECTION];
}

-(void)setCurrencySymbol:(NSString *)currencySymbol{
    [[NSUserDefaults standardUserDefaults]setObject:currencySymbol forKey:DEFAULT_CURRENY_SYMBOL];
}
-(NSString *)currencySymbol{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_CURRENY_SYMBOL];
}

-(void)setDecimapPoints:(NSString *)decimapPoints{
    [[NSUserDefaults standardUserDefaults]setObject:decimapPoints forKey:DEFAULT_DECIMAL_POINT];
}
-(NSString *)decimapPoints{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_DECIMAL_POINT];
}

-(void)setArrayCancelationReasons:(NSMutableArray *)arrayCancelationReasons{
    [[NSUserDefaults standardUserDefaults]setObject:arrayCancelationReasons forKey:DEFAULT_CANCEL_REASONS];
}
-(NSMutableArray *)arrayCancelationReasons{
    return [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]arrayForKey:DEFAULT_CANCEL_REASONS]];
}

-(void)setWalletAmount:(NSString *)WalletAmount{
    [[NSUserDefaults standardUserDefaults]setObject:WalletAmount forKey:DEFAULT_WALLET_AMOUNT];
}
-(NSString *)WalletAmount{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_WALLET_AMOUNT];
}

-(void)setArrayFavoriteUsers:(NSMutableArray *)arrayFavoriteUsers{
    NSMutableArray * testArray = [[NSMutableArray alloc]init];
    for (NSDictionary * data in arrayFavoriteUsers){
        if (data[@"user_id"] != nil && ![data[@"user_id"] isKindOfClass:[NSNull class]] && ![data[@"user_id"] isEqualToString:@""] && data[@"user_name"] != nil && ![data[@"user_name"] isKindOfClass:[NSNull class]] && ![data[@"user_name"] isEqualToString:@""]){
            [testArray addObject:data];
        } else {
            if (DEBUG_MODE){
                NSLog(@"Found Bugs while save fav users : %@",data);
            }
        }
    }
    
    [[NSUserDefaults standardUserDefaults]setObject:testArray forKey:DEFAULT_FAVORITE_USERS];
}
-(NSMutableArray *)arrayFavoriteUsers{
    return [[NSMutableArray alloc]initWithArray:[[NSUserDefaults standardUserDefaults]arrayForKey:DEFAULT_FAVORITE_USERS]];
}

-(void)setPhoneMessageNumber:(NSString *)phoneMessageNumber{
    [[NSUserDefaults standardUserDefaults]setObject:phoneMessageNumber forKey:DEFAULT_PHONE_MESSAGE];
}

-(NSString *)phoneMessageNumber{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_PHONE_MESSAGE];
}

-(void)setFacebookURL:(NSString *)facebookURL {
    [[NSUserDefaults standardUserDefaults]setObject:facebookURL forKey:DEFAULT_FACEBOOK_URL];
}

-(NSString *)facebookURL{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_FACEBOOK_URL];
}

-(void)setWhatsApp:(NSString *)whatsApp {
    [[NSUserDefaults standardUserDefaults]setObject:whatsApp forKey:DEFAULT_WHATS_APP];
}
-(NSString *)whatsApp{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_WHATS_APP];
}


//Ride Management Here
-(void)setArrayActualRoute:(NSArray *)arrayActualRoute {
    [[NSUserDefaults standardUserDefaults]setObject:arrayActualRoute forKey:DEFAULT_ACTUAL_ROUTE];
}
-(NSArray *)arrayActualRoute{
    return [[NSUserDefaults standardUserDefaults]arrayForKey:DEFAULT_ACTUAL_ROUTE];
}\

-(void)setGlobalTimeout:(int)globalTimeout{
    [[NSUserDefaults standardUserDefaults]setInteger:globalTimeout forKey:DEFAULT_GLOBAL_TIMEOUT];
}
-(int)globalTimeout{
    return (int)[[NSUserDefaults standardUserDefaults]integerForKey:DEFAULT_GLOBAL_TIMEOUT];
}

-(void)setRideRates:(double)rideRates{
    [[NSUserDefaults standardUserDefaults]setDouble:rideRates forKey:DEFAULT_RIDE_RATES];
}
-(double)rideRates{
    return [[NSUserDefaults standardUserDefaults]doubleForKey:DEFAULT_RIDE_RATES];
}

-(void)setIsRideRunning:(BOOL)isRideRunning{
    [[NSUserDefaults standardUserDefaults]setBool:isRideRunning forKey:DEFAULT_IS_RIDE_RUNNING];
}
-(BOOL)isRideRunning{
    return [[NSUserDefaults standardUserDefaults]boolForKey:DEFAULT_IS_RIDE_RUNNING];
}

-(void)setRunningRideId:(NSString *)runningRideId{
    [[NSUserDefaults standardUserDefaults]setObject:runningRideId forKey:DEFAULT_RIDE_ID];
}
-(NSString *)runningRideId{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_RIDE_ID];
}

-(void)setRecieverId:(NSString *)recieverId{
    [[NSUserDefaults standardUserDefaults]setObject:recieverId forKey:DEFAULT_RECIEVER_ID];
}
-(NSString *)recieverId{
    return [[NSUserDefaults standardUserDefaults]stringForKey:DEFAULT_RECIEVER_ID];
}

-(void)setRunningRideStatus:(RunningRideStatusUpdate)runningRideStatus {
    int valueToStore = 0;
    if (runningRideStatus == RIDE_STATUS_NONE){
        valueToStore = 0;
    } else if (runningRideStatus == RIDE_STATUS_REQUEST){
        valueToStore = 1;
    } else if (runningRideStatus == RIDE_STATUS_ACCEPTED){
        valueToStore = 2;
    } else if (runningRideStatus == RIDE_STATUS_ARRIVAL){
        valueToStore = 3;
    } else if (runningRideStatus == RIDE_STATUS_ON_GOING){
        valueToStore = 4;
    } else if (runningRideStatus == RIDE_STATUS_FINISHED){
        valueToStore = 5;
    } else if (runningRideStatus == RIDE_STATUS_CANCELED){
        valueToStore = 6;
    } else if (runningRideStatus == RIDE_STATUS_DECLINED){
        valueToStore = 7;
    } else {
        valueToStore = 0;
    }
    [[NSUserDefaults standardUserDefaults]setInteger:valueToStore forKey:DEFAULT_RIDE_STATUS];
}

-(RunningRideStatusUpdate)runningRideStatus{
    int valueToCompar = (int)[[NSUserDefaults standardUserDefaults]integerForKey:DEFAULT_RIDE_STATUS];
    if (valueToCompar == 0){
        return RIDE_STATUS_NONE;
    } else if (valueToCompar == 1){
        return RIDE_STATUS_REQUEST;
    } else if (valueToCompar == 2){
        return RIDE_STATUS_ACCEPTED;
    } else if (valueToCompar == 3){
        return RIDE_STATUS_ARRIVAL;
    } else if (valueToCompar == 4){
        return RIDE_STATUS_ON_GOING;
    } else if (valueToCompar == 5){
        return RIDE_STATUS_FINISHED;
    } else if (valueToCompar == 6){
        return RIDE_STATUS_CANCELED;
    }else if (valueToCompar == 7){
        return RIDE_STATUS_DECLINED;
    } else {
        return RIDE_STATUS_REQUEST;
    }
}

-(void)setTripStartLocation:(CLLocationCoordinate2D)tripStartLocation{
    NSDictionary * dictStartLocData = @{
                                        @"lattitude":[NSString stringWithFormat:@"%f",tripStartLocation.latitude],
                                        @"longitude":[NSString stringWithFormat:@"%f",tripStartLocation.longitude]
                                        };
    [[NSUserDefaults standardUserDefaults]setObject:dictStartLocData forKey:DEFAULT_TRIP_START_LOCATION];
}
-(CLLocationCoordinate2D)tripStartLocation{
    NSDictionary * dictStartLocData = [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_TRIP_START_LOCATION];
    double lattitude = [dictStartLocData[@"lattitude"] doubleValue];
    double longitude = [dictStartLocData[@"longitude"] doubleValue];
    return CLLocationCoordinate2DMake(lattitude, longitude);
}

-(void)setDictPickupDetails:(NSDictionary *)dictPickupDetails{
    [[NSUserDefaults standardUserDefaults]setObject:dictPickupDetails forKey:DEFAULT_TRIP_START_DETAILS];
}
-(NSDictionary *)dictPickupDetails{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_TRIP_START_DETAILS];
}

-(void)setTripEndLocation:(CLLocationCoordinate2D)tripEndLocation{
    NSDictionary * dictEndLocData = @{
                                      @"lattitude":[NSString stringWithFormat:@"%f",tripEndLocation.latitude],
                                      @"longitude":[NSString stringWithFormat:@"%f",tripEndLocation.longitude]
                                      };
    [[NSUserDefaults standardUserDefaults]setObject:dictEndLocData forKey:DEFAULT_TRIP_END_LOCATION];
}
-(CLLocationCoordinate2D)tripEndLocation{
    NSDictionary * dictEndLocData = [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_TRIP_END_LOCATION];
    double lattitude = [dictEndLocData[@"lattitude"] doubleValue];
    double longitude = [dictEndLocData[@"longitude"] doubleValue];
    return CLLocationCoordinate2DMake(lattitude, longitude);
}

-(void)setDictDropoffDetails:(NSDictionary *)dictDropoffDetails{
    [[NSUserDefaults standardUserDefaults]setObject:dictDropoffDetails forKey:DEFAULT_TRIP_END_DETAILS];
}
-(NSDictionary *)dictDropoffDetails{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_TRIP_END_DETAILS];
}

-(void)setSendRequestDate:(NSDate *)sendRequestDate{
    [[NSUserDefaults standardUserDefaults]setDouble:[[NSDate date] timeIntervalSince1970] forKey:DEFAULT_SEND_REQUEST_DATE];
}
-(NSDate *)sendRequestDate{
    double timeStamp = [[NSUserDefaults standardUserDefaults]doubleForKey:DEFAULT_SEND_REQUEST_DATE];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeStamp];
    return date;
}

-(void)setDictTripDetails:(NSDictionary *)dictTripDetails{
    [[NSUserDefaults standardUserDefaults]setObject:dictTripDetails forKey:DEFAULT_TRIP_DETAILS];
}
-(NSDictionary *)dictTripDetails{
    return [[NSUserDefaults standardUserDefaults]dictionaryForKey:DEFAULT_TRIP_DETAILS];
}

-(void)setCanAvoidTolls:(BOOL)canAvoidTolls{
    [[NSUserDefaults standardUserDefaults]setBool:canAvoidTolls forKey:DEFAULT_CAN_AVOID_TOLLS];
}
-(BOOL)canAvoidTolls{
    return [[NSUserDefaults standardUserDefaults]boolForKey:DEFAULT_CAN_AVOID_TOLLS];
}

-(void)setCanAvoidHighways:(BOOL)canAvoidHighways{
    [[NSUserDefaults standardUserDefaults]setBool:canAvoidHighways forKey:DEFAULT_CAN_AVOID_HIGHWAYS];
}
-(BOOL)canAvoidHighways{
    return [[NSUserDefaults standardUserDefaults]boolForKey:DEFAULT_CAN_AVOID_HIGHWAYS];
}

-(void)setCanSpeakNavigation:(BOOL)canSpeakNavigation{
    [[NSUserDefaults standardUserDefaults]setBool:canSpeakNavigation forKey:DEFAULT_CAN_SPEAK_NAVIGATION];
}
-(BOOL)canSpeakNavigation{
    return [[NSUserDefaults standardUserDefaults]boolForKey:DEFAULT_CAN_SPEAK_NAVIGATION];
}


-(NSString *)countEstimatedCostFromETAInMinutes:(int)duration DistanceInKM:(float)distance isModeValue:(BOOL)isModeValue{
    if (DEBUG_MODE){
        NSLog(@"Distance : %f",distance);
        NSLog(@"Duration : %d",duration);
        NSLog(@"Multiply Rate : %f",PER_METRES_TO_MULTIPLY);
        NSLog(@"Ride Rate : %f",APP_DELEGATE.rideRates);
    }
    
    double totalCost = (PER_METRES_TO_MULTIPLY * distance) * APP_DELEGATE.rideRates;
    return [self formatTheCostWithInputValue:totalCost isModeValue:isModeValue];
}

-(NSString *)formatTheCostWithInputValue:(double)totalCost isModeValue:(BOOL)isModeValue{
//    NSString * amount = [NSString stringWithFormat:@"%f",totalCost];
//    NSString * decimalPoints = [amount componentsSeparatedByString:@"."][1];
//    amount = [amount componentsSeparatedByString:@"."][0];
//    
//    NSString * currencySymbol = @"";
//    if (isModeValue){
//        currencySymbol = @"";
//    } else {
//        currencySymbol = APP_DELEGATE.currencySymbol;
//    }
////    NSString * returnValue = [NSString stringWithFormat:@"%@ %0.0f",currencySymbol, totalCost];
//    if ([APP_DELEGATE.decimapPoints isEqualToString:@"0"]){
//        decimalPoints = @"";
//    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"1"]){
//        decimalPoints = [NSString stringWithFormat:@".%@",[decimalPoints substringToIndex:1]];
//    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"2"]){
//        decimalPoints = [NSString stringWithFormat:@".%@",[decimalPoints substringToIndex:2]];
//    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"3"]){
//        decimalPoints = [NSString stringWithFormat:@".%@",[decimalPoints substringToIndex:3]];
//    } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"4"]){
//        decimalPoints = [NSString stringWithFormat:@".%@",[decimalPoints substringToIndex:4]];
//    } else {
//        decimalPoints = [NSString stringWithFormat:@".%@",[decimalPoints substringToIndex:1]];
//    }
//    
//    return [NSString stringWithFormat:@"%@ %@%@", currencySymbol, amount, decimalPoints];
    
    
    /*------- OLD CODE --------*/
     NSString * currencySymbol = @"";
     if (isModeValue){
     currencySymbol = @"";
     } else {
     currencySymbol = APP_DELEGATE.currencySymbol;
     }
     NSString * returnValue = [NSString stringWithFormat:@"%@ %0.0f",currencySymbol, totalCost];
     if ([APP_DELEGATE.decimapPoints isEqualToString:@"0"]){
     returnValue =  [NSString stringWithFormat:@"%@ %0.0f",currencySymbol, totalCost];
     } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"1"]){
     returnValue =  [NSString stringWithFormat:@"%@ %0.1f",currencySymbol, totalCost];
     } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"2"]){
     returnValue =  [NSString stringWithFormat:@"%@ %0.2f",currencySymbol, totalCost];
     } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"3"]){
     returnValue =  [NSString stringWithFormat:@"%@ %0.3f",currencySymbol, totalCost];
     } else if ([APP_DELEGATE.decimapPoints isEqualToString:@"4"]){
     returnValue =  [NSString stringWithFormat:@"%@ %0.4f",currencySymbol, totalCost];
     } else {
     returnValue = [NSString stringWithFormat:@"%@ %0.1f",currencySymbol, totalCost];
     }
     return returnValue;

}
@end
