//
//  defines.h
//  Flexipool
//
//  Created by Himanshu on 05/01/2017.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "AppDelegate.h"

#ifndef Flexipool_defines_h
#define Flexipool_defines_h

//#define GOOGLE_API_KEY @"AIzaSyDGasbGCWb3s-yw9nx7j4s64ywS6-XZlqw" // Original
//#define GOOGLE_DIRECTION_API_KEY @"AIzaSyAmIISRKr35fqxy6MRvgQ9Sd8jzHaHXO14"

//Client ID
#define GOOGLE_API_KEY @"AIzaSyAuNslPTAToLEEqTo-IBYUxA4MunDlYlVg"
#define GOOGLE_DIRECTION_API_KEY @"AIzaSyBB1bR7tM410XRQrKb7P1kWjLj5nPDdSQ0"


#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

#define MAIN_STORYBOARD  (UIStoryboard *)[UIStoryboard storyboardWithName:@"Main" bundle:nil]

#define PER_METRES_TO_MULTIPLY 0.000621371
//for iphone 4 or 5
#define IS_IPHONE ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
#define IS_HEIGHT_GTE_568 ([[UIScreen mainScreen ] bounds].size.height >= 568.0f)
#define IS_IPHONE_5 ( IS_IPHONE && [[UIScreen mainScreen ] bounds].size.height == 568.0f )
#define IS_iOS6  ([[[UIDevice currentDevice] systemVersion] floatValue]==6.0)
#define IS_iOS7 ([[[UIDevice currentDevice] systemVersion] floatValue]==7.0)
#define IS_iOS9 ([[[UIDevice currentDevice] systemVersion] floatValue]==9.0)
#define IS_iOS10 ([[[UIDevice currentDevice] systemVersion] floatValue]>=10.0)

#define IS_IPHONE_5_OR_LESS ([[UIScreen mainScreen ] bounds].size.height <= 568.0f)
#define IS_IPHONE_4_OR_LESS ([[UIScreen mainScreen ] bounds].size.height < 568.0f)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6_GTE_667 (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
#define IS_IPHONE_6_GTE (IS_IPHONE && SCREEN_MAX_LENGTH >= 667.0)
#define IS_IPHONE_6P_GTE (IS_IPHONE && SCREEN_MAX_LENGTH >= 736.0)
#define SET_VIEW_NAME(strName) IS_IPHONE?strName:[NSString stringWithFormat:@"%@_iPad",strName]

//#define INCRESE_FONT_SIZE IS_IPHONE ? 1 : 2
#define INCRESE_FONT_SIZE IS_IPHONE_5 ? 0 : (IS_IPHONE_6P_GTE || !IS_IPHONE ? 2 : 1)

#define TEXTFIELD_FONTS_SIZE (13 + (INCRESE_FONT_SIZE))
#define DONE_BUTTON_FONTS_SIZE (16 + (INCRESE_FONT_SIZE))
#define HEADER_FONTS_SIZE (20 + (INCRESE_FONT_SIZE))
#define POPUP_HEADER_FONTS_SIZE (24 + (INCRESE_FONT_SIZE))

#define PUSH_VIEW_CONTROLLER(viewName) viewName *obj = (viewName *)[[self storyboard]instantiateViewControllerWithIdentifier:SET_VIEW_NAME(NSStringFromClass([viewName class]))]; [self.navigationController pushViewController:obj animated:YES];

#define GET_DEFAULT_VALUE(string_value) [[NSUserDefaults standardUserDefaults] valueForKey:string_value]

#define SET_DEFAULT_VALUE(object,string_value) [[NSUserDefaults standardUserDefaults] setObject:object forKey:string_value];[[NSUserDefaults standardUserDefaults] synchronize];

//#define START_HUD [[Singleton sharedInstance] showGlobalHUD]; [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
//#define STOP_HUD [[Singleton sharedInstance] dismissGlobalHUD]; [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

#define START_HUD [[Singleton sharedInstance] shouldShowSVProgressHud:true]; [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
#define STOP_HUD [[Singleton sharedInstance] shouldShowSVProgressHud:false]; [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:false];


#define SHOW_NO_DATA_LABEL [APP_DELEGATE showNoDataLabel];
#define HIDE_NO_DATA_LABEL [APP_DELEGATE hideNoDataLabel];

#define SHOW_TABBAR [APP_DELEGATE.tabBarController.tabBar setHidden:NO];
#define HIDE_TABBAR [APP_DELEGATE.tabBarController.tabBar setHidden:YES];

#define CHECK_INTERNET if (![APP_DELEGATE CheckInternet]){ STOP_HUD; return; }

#define SHOW_ALERT_WITH_CAUTION(message) [[Singleton sharedInstance] showAlertViewWithMessage:message];

#define SHOW_ALERT_WITH_SUCCESS(message) [[Singleton sharedInstance] showAlertViewWithMessageSuccess:message];

#define CHECK_VALID_CONTACT_NUMBER(textField,string,range) [[Singleton sharedInstance] checkValidContactNumber:textField string:string range:range];

#define CHECK_TEXTFIELD_BLANK(txtField,message) [[Singleton sharedInstance] showAlertForBlankTextfield:txtField message: message]

#define SET_ACTIVE_VC(vc) APP_DELEGATE.strActiveViewController = NSStringFromClass([vc class]); NSLog(@"Active View Controller - %@",APP_DELEGATE.strActiveViewController);

#define GET_Language_Value(key) [Singleton setAlertText:key]

#define GET_Language_Value_FROM_ARRAY(key) [[APP_DELEGATE.arrLanguage objectAtIndex:[[APP_DELEGATE.arrLanguage valueForKey:@"label_key"] indexOfObject:key]] valueForKey:@"label_value"]

#define SHOW_MENU [kMainViewController showLeftViewAnimated:YES completionHandler:nil];

#define DATE_FORMAT @"MM/dd/yyyy"

#define DATE_FORMATE_NEW @"dd MMM yyyy"

#define DATE_FORMATE_YYYY_MM_DD @"yyyy-MM-dd"

#define TIME_FORMAT @"hh:mm a"

#define CHECK_USER_TYPE [APP_DELEGATE.strUserType isEqualToString:@"3"]

#define HEADER_HEIGHT 64

#define PASSWORD_LENGTH 8

#define Password_Alert [NSString stringWithFormat:@"Password must be %d character long", PASSWORD_LENGTH]

#define PULL_TO_REFRESH_COLOR [UIColor lightGrayColor]

#define NO_DATA_FOUND @"No records to display"
#define GREEN_COLOR [[UIColor blackColor] colorWithAlphaComponent:0.5]

#define CUSTOM_NO_DATA_CELL UITableViewCell *cell = [[UITableViewCell alloc] init]; cell.textLabel.textAlignment = NSTextAlignmentCenter; cell.textLabel.font = [UIFont fontWithName:AppFont_Regular size:IS_IPHONE ? 15 : 18]; cell.textLabel.textColor = GREEN_COLOR; cell.textLabel.text = NO_DATA_FOUND; cell.selectionStyle = UITableViewCellSelectionStyleNone; tableView.separatorStyle = UITableViewCellSeparatorStyleNone; return cell;

#define GET_STRING(strString) [Singleton getValidObject:strString]

#define AlertTitle @"Ahoy Carpool"
#define AlertInternet @"Please check your internet connection"
#define AlertNoresult @"No data found"
#define AlertServerError @"Cannot connect to server, please try again later."

#define AppFont_Regular @"CenturyGothic"
#define AppFont_Light @"CenturyGothic"
#define AppFont_Semibold @"CenturyGothic-Bold"
#define AppFont_Bold @"CenturyGothic-Bold"
#define AppFont_Bold_Italic @"CenturyGothic-BoldItalic"
#define AppFont_traditional @"BlendaScript"

#define backAction() [self.navigationController popViewControllerAnimated:YES];

#define APP_BLUE_COLOR [UIColor colorWithRed:29.0/255.0 green:106.0/255.0 blue:168.0/255.0 alpha:1]
#define APP_GREEN_COLOR [UIColor colorWithRed:75.0/255.0 green:170/255.0 blue:8/255.0 alpha:1]
#define APP_YELLOW_COLOR [UIColor colorWithRed:255/255.0 green:214/255.0 blue:10/255.0 alpha:1]

#define NAVI_LIGH_GREEN [UIColor colorWithRed:75/255.0 green:170/255.0 blue:8/255.0 alpha:1]
#define NAVI_DARK_GREEN [UIColor colorWithRed:0.0/255.0 green:158/255.0 blue:41/255.0 alpha:1]
#define NAVI_ACTIVE_ROUTE [UIColor colorWithRed:102/255.0 green:51.0/255.0 blue:153.0/255.0 alpha:1]
#define NAVI_ALTERNATE_ROUTE [UIColor colorWithRed:81.0/255 green:81.0/255 blue:81.0/255 alpha:0.7]

#define NAVI_ZOOM_RATE 18.0
#define NAVI_BEARING_RATE 180.0
#define NAVI_ANGLE_RATE 65.0
#define NAVI_DISTANCE_RATE 50.0
#define NAVI_STEP_DISTANCE_RATE 250.0




#endif
