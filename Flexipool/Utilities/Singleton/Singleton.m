//
//  Singleton.m
//  PeopleScience
//
//  Created by HimAnshu on 04/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "Singleton.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#define IS_IPHONE_6P_GTE (IS_IPHONE && SCREEN_MAX_LENGTH >= 736.0)
//#define kPayPalEnvironment PayPalEnvironmentSandbox
#define kPayPalEnvironment PayPalEnvironmentProduction

@implementation Singleton

+(Singleton *)sharedInstance{
    static Singleton *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[Singleton alloc]init];
    });
    
    return _sharedInstance;
}

- (void) createActivityIndicatorView
{
    // Paypal Configure
    [self configurePayPal];
    
    _activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:APP_DELEGATE.window.bounds];
    [_activityIndicator setColor:[UIColor blackColor]];
    _activityIndicator.tintColor = [UIColor colorWithRed:(170.0/255.0) green:(211.0/255.0) blue:(11.0/255.0) alpha:1];
    [APP_DELEGATE.window addSubview:_activityIndicator];
}

- (BOOL)checkValidContactNumber : (UITextField *)textField string :(NSString *)string range : (NSRange)range
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *invalidCharSet = [[NSCharacterSet characterSetWithCharactersInString:@"+0123456789"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:invalidCharSet] componentsJoinedByString:@""];
    return ([string isEqualToString:filtered] && (newLength <= 12));
}

- (void)showGlobalHUD {

    if (!_activityIndicator) {
        
        [self createActivityIndicatorView];
    }
    
    [_activityIndicator startAnimating];
    APP_DELEGATE.window.userInteractionEnabled = NO;
}

-(void)shouldShowSVProgressHud:(BOOL)shouldShow{
    if (shouldShow){
        [SVProgressHUD show];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    } else {
        [SVProgressHUD dismiss];
    }
}



- (void)showAlertViewWithTwoButtonWithViewController:(id)viewController
{
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.delegate = viewController;
    alert.colorScheme = [UIColor colorWithRed:44.0f/255.0f green:121.0f/255.0f blue:255.0f/255.0f alpha:1];
    
    
    alert.detachButtons = YES;
    [alert bounceAnimations];
    alert.dismissOnOutsideTouch = TRUE;
    
    
    
    [alert showAlertInView:viewController
                 withTitle:@"Delete"
              withSubtitle:@"Are you sure you want to delete this ?"
           withCustomImage:[UIImage imageNamed:@"header-delete"]
       withDoneButtonTitle:@"Delete"
                andButtons:@[@"cancel"]];
    //    alert.autoHideSeconds = 2;
}

- (void)showAlertViewforRetunrTool:(id)viewController
{
 
    FCAlertView *alert = [[FCAlertView alloc] init];
    alert.delegate = viewController;
    alert.colorScheme = [UIColor colorWithRed:44.0f/255.0f green:121.0f/255.0f blue:255.0f/255.0f alpha:1];
    
    
    alert.detachButtons = YES;
    [alert bounceAnimations];
    alert.dismissOnOutsideTouch = TRUE;
    
    
    
    [alert showAlertInView:viewController
                 withTitle:@"Delete"
              withSubtitle:@"Are you sure you want to return this ?"
           withCustomImage:[UIImage imageNamed:@"header-delete"]
       withDoneButtonTitle:@"Yes"
                andButtons:@[@"cancel"]];
    
}


- (void) resetTumblerHUD
{
    [_activityIndicator removeFromSuperview];
    _activityIndicator = nil;
}


- (void)dismissGlobalHUD {
    
    APP_DELEGATE.window.userInteractionEnabled = YES;
    [_activityIndicator stopAnimating];
}


- (void)showAlertViewWithMessage : (NSString *)message
{
    [APP_DELEGATE.window endEditing:YES];
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    [alert makeAlertTypeCaution];
    
    //        alert.blurBackground = YES;
    alert.dismissOnOutsideTouch = YES;
    alert.bounceAnimations = YES;
    alert.hideAllButtons = YES;
    [alert showAlertInWindow:APP_DELEGATE.window
                   withTitle:AlertTitle
                withSubtitle:message
             withCustomImage:nil
         withDoneButtonTitle:nil
                  andButtons:nil];
    alert.delegate = self;
    
    alert.autoHideSeconds = 2.0;
}


- (void)showAlertViewWithMessageSuccess : (NSString *)message
{
    [APP_DELEGATE.window endEditing:YES];
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    [alert makeAlertTypeSuccess];
    
    //        alert.blurBackground = YES;
    alert.dismissOnOutsideTouch = YES;
    alert.bounceAnimations = YES;
    alert.hideAllButtons = YES;
    [alert showAlertInWindow:APP_DELEGATE.window
                   withTitle:AlertTitle
                withSubtitle:message
             withCustomImage:nil
         withDoneButtonTitle:nil
                  andButtons:nil];
    alert.delegate = self;

    alert.autoHideSeconds = 2.0;
}

- (void) setDynamicHeightOfLabel :(UILabel *)lbl
{
    lbl.numberOfLines = 0;
    
    CGRect frame = lbl.frame;
    
    lbl.frame = CGRectMake(lbl.frame.origin.x, lbl.frame.origin.y, lbl.frame.size.width, lbl.frame.size.height);
    
    [lbl sizeToFit];
    frame.size.height = lbl.frame.size.height;
    lbl.frame = frame;
}

- (UIImage *) getThumbImageFromVideoURL : (NSURL *)videoURL
{
    AVAsset *asset = [AVAsset assetWithURL:videoURL];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);

    return thumbnail;
}

- (void)setShadowtoUIView : (UIView *)view radius :(NSInteger) radius
{
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(0, 0);
    view.layer.shadowRadius = radius;
    view.layer.shadowOpacity = 0.6;
}

-(void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews
{
    NSInteger increase = INCRESE_FONT_SIZE;
    
    if ([view isKindOfClass:[UILabel class]])
    {
        UILabel *lbl = (UILabel *)view;
        
//        NSLog(@"----------Label-------%f",[[lbl font] pointSize]);
        
        [lbl setFont:[UIFont fontWithName:lbl.font.fontName size:[[lbl font] pointSize] + increase]];
        
//        NSLog(@"----------Label-------%f",[[lbl font] pointSize]);
    }
    
    else if ([view isKindOfClass:[UITextField class]])
    {
        UITextField *lbl = (UITextField *)view;
        
//        NSLog(@"----------UITextField-------%f",[[lbl font] pointSize]);
        
        [lbl setFont:[UIFont fontWithName:lbl.font.fontName size:[[lbl font] pointSize] + increase]];
        
//        NSLog(@"----------UITextField-------%f",[[lbl font] pointSize]);
    }
    
    else if ([view isKindOfClass:[UITextView class]])
    {
        UITextView *lbl = (UITextView *)view;
        
        //        NSLog(@"----------UITextField-------%f",[[lbl font] pointSize]);
        
        [lbl setFont:[UIFont fontWithName:lbl.font.fontName size:[[lbl font] pointSize] + increase]];
        
        //        NSLog(@"----------UITextField-------%f",[[lbl font] pointSize]);
    }
    
    else if ([view isKindOfClass:[UIButton class]])
    {
        UIButton *btn = (UIButton *)view;
        
//        NSLog(@"----------UIButton-------%f",btn.titleLabel.font.pointSize);
        
        [btn.titleLabel setFont:[UIFont fontWithName:btn.titleLabel.font.fontName size:btn.titleLabel.font.pointSize + increase]];
        
//        NSLog(@"----------UIButton-------%f",btn.titleLabel.font.pointSize);
    }
    
    if (isSubViews)
    {
        for (UIView *sview in view.subviews)
        {
            [self setFontforView:sview andSubViews:YES];
        }
    }
}

- (BOOL) showAlertForBlankTextfield:(UITextField *)txtField message:(NSString *)message
{
    if (![txtField hasText]) {
        [self showAlertViewWithMessage:message];
        return NO;
    }
    return YES;
}

+ (NSString *)getCurrentDate {
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"ddMMyyyyHHmmss"];
    NSLog(@"%@",[dateFormatter stringFromDate:currDate]);
    
    return [dateFormatter stringFromDate:currDate];
}

+ (NSString *) properStringFormat :(NSString *)strString
{
    if (strString && ![strString isEqual:[NSNull null]]) {
        if ([strString isEqualToString:@"<null>"] || [strString length] <= 0) {
            
            return @"";
        }
        else
            return [NSString stringWithFormat:@"%@",strString];
    }
    
    return [NSString stringWithFormat:@"%@",strString];
}

+ (NSString *) properStringFormatFromInteger :(NSInteger)strString
{
    if (strString > 0) {
        return [NSString stringWithFormat:@"%ld",(long)strString];
    }
    
    return @"";
}

+ (NSString *) properStringFormatFromFloat :(float)strString
{
    if (strString > 0) {
        return [NSString stringWithFormat:@"%f",strString];
    }
    
    return @"";
}

+ (NSString *) getValidObject : (id) object{
    BOOL isValid = true;
    if ([object isKindOfClass:[NSNull class]]) {
        isValid = false;
    }
    else if (object == nil) {
        isValid = false;
    }
    else if ([object isKindOfClass:[NSString class]]) {
        // Check string condition here
        NSString *strValue = (NSString *)object;
        if ([strValue isEqualToString:@""]) {
            isValid = false;
        } else if ([strValue isEqualToString:@"null"]) {
            isValid = false;
        } else if ([strValue isEqualToString:@"(null)"]) {
            isValid = false;
        }
    }
    else if ([object isKindOfClass:[NSArray class]]) {
        NSArray *arrayValue = (NSArray *)object;
        if (arrayValue.count > 0) {
            isValid = true;
        }else{
            isValid = false;
        }
    }
    else if ([object isKindOfClass:[NSNumber class]]) {
        NSNumber *numberValue = (NSNumber *)object;
        if (numberValue == nil ) {
            isValid = false;
        }
    }
    
    if (isValid) {
        
        return object;
    }
    else
        return @"";
}

+ (NSAttributedString *)setAttributedString:(NSString *)str forLabel: (UILabel *)lbl
{
    NSArray *arr = [str componentsSeparatedByString:@" : "];
    
    if (arr.count != 2) {
        
        str = [str stringByReplacingOccurrencesOfString:@": " withString:@":"];
        str = [str stringByReplacingOccurrencesOfString:@" :" withString:@":"];
        
        arr = [str componentsSeparatedByString:@":"];
    }
    
    NSMutableAttributedString *attString =
    [[NSMutableAttributedString alloc]
     initWithString:[NSString stringWithFormat:@"%@ : ",[arr objectAtIndex:0]]];
    
    [attString addAttribute: NSFontAttributeName
                      value: [UIFont fontWithName:AppFont_Semibold size:lbl.font.pointSize]
                      range: NSMakeRange(0,[attString length] )];
    
    NSMutableAttributedString *attString1 =
    [[NSMutableAttributedString alloc]
     initWithString:[arr objectAtIndex:1]];
    
    [attString1 addAttribute: NSFontAttributeName
                       value: [UIFont fontWithName:AppFont_Regular size:lbl.font.pointSize]
                       range: NSMakeRange(0,[attString1 length] )];
    
    [attString appendAttributedString:attString1];
    
    return attString;
}

#pragma mark - Button Password
- (void) setButtonToTextField:(UITextField *)txtField button:(UIButton *)btnPassword imageName : (NSString *)imgView
{
    
    [btnPassword setImage:[UIImage imageNamed:imgView] forState:UIControlStateNormal];
    btnPassword.frame = CGRectMake(0, 0, txtField.frame.size.width * 0.15, txtField.frame.size.height);
    UIView *viewTemp1 = [[UIView alloc] initWithFrame:CGRectMake(4, 0, btnPassword.frame.size.width, txtField.frame.size.height)];
    [viewTemp1 addSubview:btnPassword];
    btnPassword.center = viewTemp1.center;
    viewTemp1.userInteractionEnabled = YES;
    txtField.rightView = viewTemp1;
    txtField.rightViewMode = UITextFieldViewModeAlways;
}

+ (void) clearDocumentDirectory
{
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error]) {
        [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
    }
}

#pragma mark - Play Video With AVPlayerView -

- (void) playVideoWithLink : (NSString *) strURL
{
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",strURL]];
    
    AVPlayer *player = [AVPlayer playerWithURL:url];
    AVPlayerViewController *playerController = [[AVPlayerViewController alloc] init];
    playerController.player = player;
    
    player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[player currentItem]];
    
    AVPlayerLayer *playerLayer = [AVPlayerLayer playerLayerWithPlayer:player];
    playerLayer.frame = APP_DELEGATE.window.bounds;
    //    [self.view.layer addSublayer:playerLayer];
    [player play];
    
    [APP_DELEGATE.window.rootViewController presentViewController:playerController animated:YES completion:nil];
}


- (void) playerItemDidReachEnd:(NSNotification *)notification {

//    AVPlayer *player = [AVPlayer playerWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[arrContents objectAtIndex:currentIndex] valueForKey:@"content_url"]]]];
//    playerController.player = player;
//    [player seekToTime:kCMTimeZero];
//    [player play];
}

- (void)GeneratePasswordScore : (NSString *)mStrPassword {
    
    /* Password Score Requirements
     Minimum 8 characters in length
     Contains 3/4 of the following items:
     - Uppercase Letters
     - Lowercase Letters
     - Numbers
     - Symbols*/
    
    char mPreviousChar = '0';
    NSInteger mPasswordLength = 0, mUpperCaseCounter = 0, mLowerCaseCounter = 0, mNumberCounter = 0, mSymbolCounter = 0, mMiddleNumbersSymbolCounter = 0, mRequirementsCounter = 1, mRequirementsTotal = 0;
    NSInteger mLettersOnly = 0, mNumbersOnly = 0, mConsecutiveUpperCaseLetters = 0, mConsecutiveLowerCaseLetters = 0, mConsecutiveNumbers = 0, mSequentialLetters = 0, mSequentialNumbers = 0, mSequentialSymbols = 0;
    NSInteger mConsUpperCheck = 0, mConsLowerCheck = 0, mConsNumberCheck = 0; // 1 or more consecutive
    NSInteger mSeqLetterCheck = 0, mSeqNumberCheck = 0, mSeqSymbolCheck = 0; // 3 or 3 plus sequence
    NSInteger mAddition = 0, mDeductions = 0, mPasswordScore = 0;
    double nRepInc = 0, nRepChar = 0, nUnqChar = 0;
    NSString *mStrComplexity = @"";
    mPasswordLength = [mStrPassword length];
    
    
    for (int i = 0; i < mPasswordLength; i++) {
        char ch = [mStrPassword characterAtIndex:i];
        NSLog(@"%c",ch);
        if (ch >= 'A' && ch <= 'z') {
            
            if (ch >= 'A' && ch <= 'Z') {
                mUpperCaseCounter += 1;
                
                if (mConsUpperCheck > 0)
                    mConsecutiveUpperCaseLetters += 1;
                
                mConsUpperCheck = 1;
                mConsLowerCheck = 0;
            } else {
                mLowerCaseCounter += 1;
                
                if (mConsLowerCheck > 0) {
                    mConsecutiveLowerCaseLetters += 1;
                }
                
                mConsUpperCheck = 0;
                mConsLowerCheck = 1;
            }
            
            
            mLettersOnly += 1;
            mPreviousChar = ch;
            mConsNumberCheck = 0;
            mSeqNumberCheck = 0;
            mSeqSymbolCheck = 0;
            
        } else if (ch >= '0' && ch <= '9') {
            mNumberCounter += 1;
            mNumbersOnly += 1;
            
            if ((i == 0 || i == mPasswordLength - 1) && mNumberCounter > 0) {
                
                mMiddleNumbersSymbolCounter = mMiddleNumbersSymbolCounter + 1;
            }
            
            if (mConsNumberCheck > 0) {
                mConsecutiveNumbers += 1;
            }
            mConsNumberCheck = 1;
            
            
            if (mPreviousChar + 1 == ch) {
                
                mSeqNumberCheck += 1;
                
                if (mSeqNumberCheck > 1) {
                    mSequentialNumbers += 1;
                }
            } else {
                mSeqNumberCheck = 0;
            }
            
            mPreviousChar = ch;
            mConsUpperCheck = 0;
            mConsLowerCheck = 0;
            mSeqLetterCheck = 0;
            mSeqSymbolCheck = 0;
            
        } else {
            mSymbolCounter += 1;
            
            if ((i == 0 || i == mPasswordLength - 1) && mSymbolCounter > 0) {
                mMiddleNumbersSymbolCounter = mMiddleNumbersSymbolCounter + 1;
            }
            
            if (mPreviousChar + 1 == ch) {
                
                mSeqSymbolCheck += 1;
                
                if (mSeqSymbolCheck > 1) {
                    mSequentialSymbols += 1;
                }
            }
            
            mPreviousChar = ch;
            mConsUpperCheck = 0;
            mConsLowerCheck = 0;
            mConsNumberCheck = 0;
            mSeqLetterCheck = 0;
            mSeqNumberCheck = 0;
        }
        
        
        BOOL nextCharExists = false;
        for (int b = 0; b < mPasswordLength; b++) {
            char newCh = [mStrPassword characterAtIndex:b];
            if (mPreviousChar == newCh && i != b) { /* repeat character exists */
                nextCharExists = true;
                /*
                 Calculate increment deduction based on proximity to identical characters
                 Deduction is incremented each time a new match is discovered
                 Deduction amount is based on total password length divided by the
                 difference of distance between currently selected match
                 */
                nRepInc += labs(mPasswordLength / (b - i));
                //                    System.out.println("nRepInc                    "+i+"  : "+nRepInc);
            }
        }
        if (nextCharExists) {
            nRepChar++;
            nUnqChar = mPasswordLength - nRepChar;
            nRepInc = (nUnqChar > 0) ? fabs(nRepInc / nUnqChar) : nRepInc;
        }
    }
    
    mMiddleNumbersSymbolCounter = (mNumberCounter + mSymbolCounter) - mMiddleNumbersSymbolCounter;
    
    if (mPasswordLength >= 8) {
        
        if (mUpperCaseCounter > 0) {
            mRequirementsCounter += 1;
        }
        
        if (mLowerCaseCounter > 0) {
            mRequirementsCounter += 1;
        }
        
        if (mNumberCounter > 0) {
            mRequirementsCounter += 1;
        }
        
        if (mSymbolCounter > 0) {
            mRequirementsCounter += 1;
        }
        
        if (mRequirementsCounter < 4) {
            mRequirementsCounter = 0;
            mRequirementsTotal = 0;
        } else {
            mRequirementsTotal = mRequirementsCounter * 2;
        }
    }
    if (mUpperCaseCounter > 0 || mLowerCaseCounter > 0) {
        mNumbersOnly = 0;
    }
    
    if (mNumberCounter > 0 || mSymbolCounter > 0) {
        mLettersOnly = 0;
    }
    
    mAddition = (mPasswordLength * 4) + ((mUpperCaseCounter == 0) ? 0 : ((mPasswordLength - mUpperCaseCounter) * 2)) + ((mLowerCaseCounter == 0) ? 0 : ((mPasswordLength - mLowerCaseCounter) * 2)) +
    (mNumberCounter * 4) + (mSymbolCounter * 6) + (mMiddleNumbersSymbolCounter * 2) + (mRequirementsTotal);
    
    mDeductions = mLettersOnly + mNumbersOnly + (int) nRepInc + (mConsecutiveUpperCaseLetters * 2) + (mConsecutiveLowerCaseLetters * 2) +
    (mConsecutiveNumbers * 2) + (mSequentialLetters * 3) + (mSequentialNumbers * 3) + (mSequentialSymbols * 3);
    
    mPasswordScore = mAddition - mDeductions;
    
    if (mPasswordScore <= 0) {
        mPasswordScore = 0;
    }
    
    if (mPasswordScore > 100) {
        mPasswordScore = 100;
    } else if (mPasswordScore < 0) {
        mPasswordScore = 0;
    }
    
    if (mPasswordScore >= 0 && mPasswordScore < 20) {
        NSLog(@"Very Weak");
        mStrComplexity = @"Very Weak";
    } else if (mPasswordScore >= 20 && mPasswordScore < 40) {
        NSLog(@"Weak");

        mStrComplexity = @"Weak";
    } else if (mPasswordScore >= 40 && mPasswordScore < 60) {
        NSLog(@"Good");

        mStrComplexity = @"Good";
    } else if (mPasswordScore >= 60 && mPasswordScore < 80) {
        NSLog(@"Strong");

        mStrComplexity = @"Strong";
    } else if (mPasswordScore >= 80 && mPasswordScore <= 100) {
        NSLog(@"Very Strong");

        mStrComplexity = @"Very Strong";
    }
    
    NSLog(@"%ld",(long)mPasswordScore);
}
+(NSString *)createRandomOrderNumber : (NSString *)strProjectName {
    
    
    NSString *strOderNumber = @"";
    
//    NSNumber *index = [NSNumber numberWithInt:arc4random_uniform(555)];
//   NSString *randomString = [self randomStringWithLength:3 project:strProjectName].uppercaseString;
   
//    strOderNumber = [NSString stringWithFormat:@"%@%@%@",randomString,index,APP_DELEGATE.strUserId];
    
    return strOderNumber;
    
   
}


+(NSString *)randomStringWithLength:(int)len project:(NSString *)projectname
{
    NSString *letters = projectname;
    
    letters = [letters substringToIndex:len];
    
    return letters;
}

+ (NSAttributedString *)setAttributedStringWithBLueColor:(NSString *)str
{
    NSArray *arr = [str componentsSeparatedByString:@" : "];
    
    if (arr.count != 2) {
        
        str = [str stringByReplacingOccurrencesOfString:@": " withString:@":"];
        str = [str stringByReplacingOccurrencesOfString:@" :" withString:@":"];
        
        arr = [str componentsSeparatedByString:@":"];
    }
    
    NSMutableAttributedString *attString =
    [[NSMutableAttributedString alloc]
     initWithString:[NSString stringWithFormat:@"%@ : ",[arr objectAtIndex:0]]];
    
    [attString addAttribute: NSForegroundColorAttributeName
                      value: [UIColor blackColor]
                      range: NSMakeRange(0,[attString length] )];
    
    NSMutableAttributedString *attString1 =
    [[NSMutableAttributedString alloc]
     initWithString:[arr objectAtIndex:1]];
    
    [attString1 addAttribute: NSForegroundColorAttributeName
                       value: [UIColor colorWithRed:34.0/255.0 green:118.0/255.0 blue:188.0/255.0 alpha:1.0]
                       range: NSMakeRange(0,[attString1 length] )];
    
    [attString appendAttributedString:attString1];
    
    return attString;
}

+(void) showToast :(NSString *)message
{
    MDToast *t = [[MDToast alloc]
                  initWithText:message
                  duration:kMDToastDurationLong];
    [t show];
}

+(void) showSnakBar :(NSString *)message multiline: (BOOL)isMultiline
{
    MDSnackbar *snackbar = [[MDSnackbar alloc] initWithText: message actionTitle:@""];
    snackbar.multiline = isMultiline;
    [snackbar show];
}

+(NSString *)daySuffixForDate:(NSDate* )date {
    
    NSString* string=@"";
    NSDateComponents *components = [[NSCalendar currentCalendar] components: NSCalendarUnitDay fromDate:date];
    
    if(components.day == 1 || components.day == 21 || components.day == 31){
        string = @"st";
    }else if (components.day == 2 || components.day == 22){
        string = @"nd";
    }else if (components.day == 3 || components.day == 23){
        string = @"rd";
    }else{
        string = @"th";
    }
    NSDateFormatter *prefixDateFormatter = [[NSDateFormatter alloc] init];    [prefixDateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [prefixDateFormatter setDateFormat:[NSString stringWithFormat:@" dd'%@' MMM yyyy",string]];
    
    NSString *dateString = [prefixDateFormatter stringFromDate:date];
    
    return dateString;
}

#pragma mark - Paypal Methods -

- (void)configurePayPal
{
    //    // Set up payPalConfig
    //    _payPalConfig = [[PayPalConfiguration alloc] init];
    //    _payPalConfig.acceptCreditCards = YES;
    //    _payPalConfig.merchantName = @"iHart Developers";
    //    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    //     _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    //
    //      // Setting the languageOrLocale property is optional.
    //      //
    //      // If you do not set languageOrLocale, then the PayPalPaymentViewController will present
    //      // its user interface according to the device's current language setting.
    //      //
    //      // Setting languageOrLocale to a particular language (e.g., @"es" for Spanish) or
    //      // locale (e.g., @"es_MX" for Mexican Spanish) forces the PayPalPaymentViewController
    //      // to use that language/locale.
    //      //
    //      // For full details, including a list of available languages and locales, see PayPalPaymentViewController.h.
    //
    //      _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    //
    //
    //      // Setting the payPalShippingAddressOption property is optional.
    //      //
    //      // See PayPalConfiguration.h for details.
    //
    //      _payPalConfig.payPalShippingAddressOption =
    //      PayPalShippingAddressOptionPayPal;
    //
    //      // use default environment, should be Production in real life
    //      [PayPalMobile preconnectWithEnvironment:kPayPalEnvironment];
    
    
    
    _payPalConfig = [[PayPalConfiguration alloc] init];
    _payPalConfig.acceptCreditCards = YES;
    _payPalConfig.merchantName = @"Awesome Shirts, Inc.";
    _payPalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
    _payPalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
    
    _payPalConfig.languageOrLocale = [NSLocale preferredLanguages][0];
    
    
    NSLog(@"%@",[NSLocale preferredLanguages]);
    
    // Setting the payPalShippingAddressOption property is optional.
    //
    // See PayPalConfiguration.h for details.
    
    //  _payPalConfig.payPalShippingAddressOption = PayPalShippingAddressOptionPayPal;
    
    // Do any additional setup after loading the view, typically from a nib.
    
    //        self.successView.hidden = YES;
    
    // use default environment, should be Production in real life
    self.environment = kPayPalEnvironment;
    
    [PayPalMobile preconnectWithEnvironment:kPayPalEnvironment];
}

- (void) PayPalCallWithAmount:(NSString *)amount delegatedView:(UIViewController *)view
{
    [self configurePayPal];
    walletAmount = amount;
    
    // Note: For purposes of illustration, this example shows a payment that includes
    //       both payment details (subtotal, shipping, tax) and multiple items.
    //       You would only specify these if appropriate to your situation.
    //       Otherwise, you can leave payment.items and/or payment.paymentDetails nil,
    //       and simply set payment.amount to your total charge.
    
    // Optional: include multiple items
    PayPalItem *item1 = [PayPalItem
                         itemWithName:@"iOS Application Development"
                         withQuantity:1
                         withPrice:[NSDecimalNumber
                                    decimalNumberWithString:amount]
                         withCurrency:@"USD"
                         withSku:@"Hip-00037"];
    
    
    
    NSArray *items = @[item1];
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:items];
    
    //    // Optional: include payment details
    //    NSDecimalNumber *shipping = [[NSDecimalNumber alloc]
    //                                 initWithString:@"0.50"];
    //    NSDecimalNumber *tax = [[NSDecimalNumber alloc]
    //                            initWithString:@"0.50"];
    PayPalPaymentDetails *paymentDetails =
    [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal
                                        withShipping:nil
                                             withTax:nil];
    //
    //    NSDecimalNumber *total = [[subtotal
    //                               decimalNumberByAdding:shipping] decimalNumberByAdding:tax];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = subtotal;
    payment.currencyCode = @"USD";
    payment.shortDescription = @"Subscription Fee";
    payment.items = items;
    // if not including multiple items, then leave payment.items as nil
    
    payment.paymentDetails = paymentDetails;
    // if not including payment details, then leave payment. paymentDetails as nil
    
    if (!payment.processable) {
        // This particular payment will always be processable. If, for
        // example, the amount was negative or the shortDescription was
        // empty, this payment wouldn't be processable, and you'd want
        // to handle that here.
        [Singleton showSnakBar:@"Currency / Amount is not supported by PayPal, Please select bank transfer for your payment" multiline:true];
        return;
    }
    
    PayPalPaymentViewController *paymentViewController =
    [[PayPalPaymentViewController alloc]
     initWithPayment:payment
     configuration:self.payPalConfig
     delegate:(id)[view self]];
    
    [[view self] presentViewController:paymentViewController
                       animated:YES completion:nil];
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    walletAmount = @"";
    [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    SET_DEFAULT_VALUE(walletAmount, DEFAULT_WALLET_AMOUNT);
    NSLog(@"PayPal Payment Success! : %@", completedPayment.confirmation);
    [APP_DELEGATE.navigationController dismissViewControllerAnimated:YES completion:nil];
//    
//    
//    NSDictionary *dic = @{
//                          @"InvoiceID" : self.InvoiceID,
//                          @"InvoiceStatus" : @"a",
//                          @"PaymentID" : [[completedPayment.confirmation valueForKey:@"response"] valueForKey:@"id"]
//                          };
    
    /*
     [APP_DELEGATE.apiManager invoiceStatusChange:dic withCallBack:^(BOOL success, NSMutableArray *response, NSString *errorMessage) {
     
     
     NSInteger responseCode = [[[response objectAtIndex:0] valueForKey:@"code"] integerValue];
     
     if (responseCode == 1)
     {
     [Singleton showSnakBar:@"Payment done successfully" multiline:NO];
     
     NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
     dict = [APP_DELEGATE.arrInvoiceDetails objectAtIndex:_indexRow];
     [dict setValue:@"Paid" forKey:@"Status"];
     [APP_DELEGATE.arrInvoiceDetails replaceObjectAtIndex:_indexRow withObject:dict];
     }
     
     [self.navigationController popViewControllerAnimated:YES];
     
     
     STOP_HUD
     }];
     
     */
}

+ (NSString *) getWalletAmount {
    
    NSInteger walletValue = [[Singleton getValidObject:GET_DEFAULT_VALUE(DEFAULT_WALLET_AMOUNT)] integerValue];
    walletValue = walletValue > 0 ? walletValue : 0;
    
    return [NSString stringWithFormat:@"$%ld",walletValue];
}

#pragma mark - FTP Delegate -

-(void)ftpRequest:(NSString*)filename ftpFolder :(NSString *)ftpFolder
{
    [SVProgressHUD show];
    
    NSLog(@"%@",filename);
    //make url
    NSURL *url = nil;
    
    url = [NSURL URLWithString:[[NSString stringWithFormat:@"ftp://104.238.100.77/%@/",ftpFolder] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]];
    
    SCRFTPRequest *ftpRequest = [[SCRFTPRequest alloc] initWithURL:url toUploadFile:filename];
    ftpRequest.username = @"livecrm";
    ftpRequest.password = @"5Mlw5~e0";
    
    ftpRequest.delegate = self;
    
    //start
    [ftpRequest startAsynchronous];
    [ftpRequest setTimeOutSeconds:60];
}

- (void)ftpRequestDidFinish:(SCRFTPRequest *)request
{
    NSLog(@"File uploading is finish now");
    [SVProgressHUD showWithStatus:@"Loading..."];
    [Singleton clearDocumentDirectory];
    [[NSNotificationCenter defaultCenter] postNotificationName:APP_DELEGATE.strActiveNotification object:nil];
    [SVProgressHUD dismiss];
}

- (void)ftpRequest:(SCRFTPRequest *)request didFailWithError:(NSError *)error
{
    NSLog(@"%@",@"Document upload failed, Please try again");
    [SVProgressHUD showWithStatus:@"Loading..."];
    [[NSNotificationCenter defaultCenter] postNotificationName:[APP_DELEGATE.strActiveNotification stringByAppendingString:@"fail"] object:nil];
    [SVProgressHUD dismiss];
}

- (void)ftpRequestWillStart:(SCRFTPRequest *)request
{
    totalBytesUploaded = 0;
    [SVProgressHUD showWithStatus:@"Uploading..."];
}

- (void)ftpRequest:(SCRFTPRequest *)request didChangeStatus:(SCRFTPRequestStatus)status
{
    
}

- (void)ftpRequest:(SCRFTPRequest *)request didWriteBytes:(NSUInteger)bytesWritten
{
    totalBytesUploaded = totalBytesUploaded + request.bytesWritten;
    
    [SVProgressHUD showProgress:(float)totalBytesUploaded/(float)request.fileSize status:@"Uploading..."];
}

+(NSString *)setAlertText :(NSString *)key
{
    @try {
        return GET_Language_Value_FROM_ARRAY(key) ? GET_Language_Value_FROM_ARRAY(key) : [[key capitalizedString] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    } @catch (NSException *exception) {
        return [[key capitalizedString] stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    }
}

- (NSString *)getCurrentDate {
    
    NSDate *currDate = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"ddMMyyyyHHmmss"];
    NSLog(@"%@",[dateFormatter stringFromDate:currDate]);
    
    return [dateFormatter stringFromDate:currDate];
}

#pragma mark - S3 stuff -

- (void) uploadToS3WithImage : (UIImage *)image orFileWithLocalPath : (NSURL *)filePath fileName : (NSString *)fileName completion:(void (^)(BOOL finished))completion {
    [SVProgressHUD showWithStatus:@"Loading..."];
    NSURL *url;
    
    if (image) {
        
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:@"image.png"];
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:path atomically:YES];
        url = [NSURL fileURLWithPath:path];
    }
    else
    {
        url = filePath;
    }
    
    _uploadRequest = [AWSS3TransferManagerUploadRequest new];
    //    _uploadRequest.bucket = @"snapsells";
    _uploadRequest.bucket = @"flexiprod";
    _uploadRequest.ACL = AWSS3ObjectCannedACLPublicRead;
    _uploadRequest.key = fileName;
    _uploadRequest.body = url;
    
    __weak Singleton *weakSelf = self;
    
    
    
    _uploadRequest.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
        dispatch_sync(dispatch_get_main_queue(), ^{
            amountUploaded = totalBytesSent;
            filesize = totalBytesExpectedToSend;
            [weakSelf update];
        });
    };
    
    
    transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    [[transferManager upload:_uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        
        [SVProgressHUD dismiss];
        
        if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
            
            
            
            switch (task.error.code) {
                case AWSS3TransferManagerErrorCancelled:
                {
                    break;
                }
                case AWSS3TransferManagerErrorPaused:
                {
                    break;
                }
                default:
                    NSLog(@"Upload failed: [%@]", task.error);
                    break;
            }
            
            completion(NO);
            
        } else{
            
            NSLog(@"https://s3.amazonaws.com/%@/%@",_uploadRequest.bucket,_uploadRequest.key);
            
            NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:NSTemporaryDirectory() error:NULL];
            for (NSString *file in tmpDirectory) {
                [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@", NSTemporaryDirectory(), file] error:NULL];
            }
            
            NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSError *error = nil;
            for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error]) {
                [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:file] error:&error];
            }
            
            completion(YES);
            
        }
        
        return nil;
    }];
}

- (IBAction) pauseTasks
{
    
    [[_uploadRequest pause] continueWithBlock:^id(AWSTask *task) {
        if (task.error) {
            NSLog(@"Error: %@",task.error);
        } else {
            //Pause the upload.
        }
        return nil;
    }];
    
    [SVProgressHUD showProgress:((float)amountUploaded/ (float)filesize) status:@"Paused..."];
}

- (IBAction) resumeTasks
{
    
    [SVProgressHUD showProgress:((float)amountUploaded/ (float)filesize) status:@"Resuming..."];
    
    [transferManager upload:_uploadRequest];
}

-(void) stopTasks
{
    [SVProgressHUD showProgress:((float)amountUploaded/ (float)filesize) status:@"Stopped"];
}

- (void) update{
    
    [SVProgressHUD showProgress:((float)amountUploaded/ (float)filesize) status:@"Uploading..."];
}

-(NSString *)findYourCurrentCountryCode {
    
    CTCarrier *carrier = [[CTTelephonyNetworkInfo new] subscriberCellularProvider];
    NSString *countryCode = carrier.isoCountryCode;
    if (!countryCode || ![countryCode isEqualToString:@""]){
        countryCode = [[NSLocale currentLocale] objectForKey:NSLocaleCountryCode];
    }
    if (DEBUG_MODE){
        NSLog(@"Country ID : %@", countryCode);
    }
    
    NSString * plistPath = [[NSBundle mainBundle] pathForResource:@"DiallingCodes" ofType:@"plist"];
    NSString * telephonyCode = [[NSDictionary dictionaryWithContentsOfFile:plistPath] objectForKey:countryCode.lowercaseString];
    
    if (DEBUG_MODE){
        NSLog(@"Country Telephony Code : %@", telephonyCode);
    }
    
    if (telephonyCode && telephonyCode.length > 0){
        return telephonyCode;
    } else {
        return @"+ 91";
    }
}
@end
