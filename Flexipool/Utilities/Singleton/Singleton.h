//
//  Singleton.h
//  PeopleScience
//
//  Created by HimAnshu on 04/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FCAlertView.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>
#import "SCRFTPRequest.h"
#import "PayPalConfiguration.h"
#import "PayPalMobile.h"
#import "AWSCore.h"
#import <AWSS3/AWSS3.h>
#import "SVProgressHUD.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@interface Singleton : NSObject <FCAlertViewDelegate,SCRFTPRequestDelegate, PayPalPaymentDelegate>
{
    UInt64 totalBytesUploaded;
    NSString *walletAmount;
    
    AWSS3TransferManager *transferManager;
    AWSS3TransferManagerUploadRequest *_uploadRequest;
    uint64_t filesize;
    uint64_t amountUploaded;
}
@property (nonatomic,retain) UIActivityIndicatorView *activityIndicator;

+ (Singleton *)sharedInstance;

+(NSString *)createRandomOrderNumber : (NSString *)strProjectName;

- (BOOL)checkValidContactNumber : (UITextField *)textField string :(NSString *)string range : (NSRange)range;
- (void)showGlobalHUD;
- (void)resetTumblerHUD;
- (void)dismissGlobalHUD;
- (void)showAlertViewWithMessage : (NSString *)message;
- (void)showAlertViewWithMessageSuccess : (NSString *)message;
- (void)setDynamicHeightOfLabel :(UILabel *)lbl;

- (UIImage *)getThumbImageFromVideoURL : (NSURL *)videoURL;
- (void)setFontforView:(UIView*)view andSubViews:(BOOL)isSubViews;
- (void) setButtonToTextField:(UITextField *)txtField button:(UIButton *)btnPassword imageName : (NSString *)imgView;
- (void)showAlertViewWithTwoButtonWithViewController:(id)viewController;


- (void)showAlertViewforRetunrTool:(id)viewController;

- (void)setShadowtoUIView : (UIView *)view radius :(NSInteger) radius;
- (void)GeneratePasswordScore : (NSString *)mStrPassword;
- (BOOL) showAlertForBlankTextfield:(UITextField *)txtField message:(NSString *)message;
+ (NSAttributedString *)setAttributedStringWithBLueColor:(NSString *)str;
+ (void) clearDocumentDirectory;
-(void)ftpRequest:(NSString*)filename ftpFolder :(NSString *)ftpFolder;

+ (NSString *)getCurrentDate;

+ (NSString *) properStringFormat :(NSString *)strString;
+ (NSString *) properStringFormatFromInteger :(NSInteger)strString;
+ (NSString *) properStringFormatFromFloat :(float)strString;
+ (NSString *) getValidObject : (id) object;
+ (NSAttributedString *)setAttributedString:(NSString *)str forLabel: (UILabel *)lbl;

+(void) showToast :(NSString *)message;
+(void) showSnakBar :(NSString *)message multiline: (BOOL)isMultiline;
+(NSString *)daySuffixForDate:(NSDate* )date;

@property(nonatomic, strong, readwrite) PayPalConfiguration *payPalConfig;
@property(nonatomic, strong, readwrite) NSString *environment;
@property(nonatomic, strong, readwrite) NSString *InvoiceID;
+(NSString *)setAlertText :(NSString *)key;
- (void) PayPalCallWithAmount:(NSString *)amount delegatedView:(UIViewController *)view;
+ (NSString *) getWalletAmount;

- (NSString *)getCurrentDate;

- (void) uploadToS3WithImage : (UIImage *)image orFileWithLocalPath : (NSURL *)filePath fileName : (NSString *)fileName completion:(void (^)(BOOL finished))completion;

-(void)shouldShowSVProgressHud:(BOOL)shouldShow;
    
-(NSString *)findYourCurrentCountryCode;
@end

