//
//  UILabel+DynamicHeight.m
//  CRM
//
//  Created by HimAnshu on 05/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "UILabel+DynamicHeight.h"

@implementation UILabel (DynamicHeight)

- (void)resizeToFit{
//    float height = [self expectedHeight];
////    float height = [self getLabelHeight:self];
//    CGRect newFrame = [self frame];
//    newFrame.size.height = height;
//    [self setFrame:newFrame];
    
    [self setHeight];
}

- (void) setHeight
{
    self.numberOfLines = 0;
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    [self sizeToFit];
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
}

-(float)expectedHeight{
    [self setNumberOfLines:0];
    [self setLineBreakMode:NSLineBreakByWordWrapping];
    
    CGSize maximumLabelSize = CGSizeMake(self.frame.size.width,9999);
    
    CGRect expectedLabelSize = [[self text] boundingRectWithSize:maximumLabelSize
                                                         options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading
                                                      attributes:@{NSFontAttributeName:[self font]}
                                                         context:nil];

    return expectedLabelSize.size.height;
}

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, 9999);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

@end
