//
//  UIButton+buttonCurve.h
//  PeopleScience
//
//  Created by HimAnshu on 06/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (buttonCurve)

-(void) setRightCornerRadius;
-(void) setLeftCornerRadius;

@end
