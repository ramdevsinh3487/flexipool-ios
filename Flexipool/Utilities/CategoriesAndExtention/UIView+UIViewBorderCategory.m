//
//  AppDelegate.h
//  DeeraCofee
//
//  Created by Naman Vaishnav on 23/01/17.
//  Copyright © 2017 Vrinsoft. All rights reserved.
//


#import "UIView+UIViewBorderCategory.h"

@implementation UIView (UIViewBorderCategory)
@dynamic borderColor,borderWidth,cornerRadius,shadowColor;


//- (void) setcolor : (UIColor *)border : (CGFloat) bWidth : (CGFloat) CRadius{
//    
//    self.layer.borderColor = border.CGColor;
//    self.layer.borderWidth = bWidth;
//    self.layer.cornerRadius = CRadius;
//    
//}
- (void) setBorderColor :(UIColor *)border borderWidth : (CGFloat)bWidth boarderRadius : (CGFloat)cRadius{
    
    self.layer.borderColor = border.CGColor;
    self.layer.borderWidth = bWidth;
    self.layer.cornerRadius = cRadius;
    
}
//- (void) setBorderColorForValidation : (UIColor *)borderColor{
//    self.layer.borderColor = borderColor.CGColor;
//}

-(void)setBorderColor:(UIColor *)borderColor{
    [self.layer setBorderColor:borderColor.CGColor];
}

-(void)setBorderWidth:(CGFloat)borderWidth{
    [self.layer setBorderWidth:borderWidth];
    self.layer.borderColor = [UIColor blackColor].CGColor;
}

-(void)setCornerRadius:(CGFloat)cornerRadius{
    if (cornerRadius == 1) {
        [self.layer setCornerRadius:self.frame.size.height/2+(IS_IPHONE_5 ? 0 : 2)];
    }
    else if (cornerRadius == 2) {
        
        [self.layer setCornerRadius:self.frame.size.height/2];
    }
    else {
        
        [self.layer setCornerRadius:cornerRadius];
    }
}

- (void)setShadowColor:(UIColor *)shadowColor{
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.2;
    self.layer.shadowRadius = 0.8;
    self.layer.shadowOffset = CGSizeMake(1, 3);
    self.layer.masksToBounds = NO;
}


-(void) setWindow:(UIWindow * _Nullable)window{
    
}
- (void)awakeFromNib{
    [super awakeFromNib];
}
@end
