//
//  AppDelegate.h
//  DeeraCofee
//
//  Created by Naman Vaishnav on 23/01/17.
//  Copyright © 2017 Vrinsoft. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface UIView (UIViewBorderCategory){
    
}

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable UIColor *shadowColor;

//- (void) setcolor : (UIColor *)border : (CGFloat) bWidth : (CGFloat) CRadius;
- (void) setBorderColor :(UIColor *)border borderWidth : (CGFloat)bWidth boarderRadius : (CGFloat)cRadius;
//- (void) setBorderColorForValidation : (UIColor *)borderColor;
//@property (nonatomic) IBInspectable UISwitch *changeBoarder;

@end
