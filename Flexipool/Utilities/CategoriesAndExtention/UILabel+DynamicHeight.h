//
//  UILabel+DynamicHeight.h
//  CRM
//
//  Created by HimAnshu on 05/07/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (DynamicHeight)

- (void)resizeToFit;

@end
