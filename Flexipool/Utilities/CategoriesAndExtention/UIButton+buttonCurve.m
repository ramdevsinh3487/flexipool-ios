//
//  UIButton+buttonCurve.m
//  PeopleScience
//
//  Created by HimAnshu on 06/03/17.
//  Copyright © 2017 Himanshu. All rights reserved.
//

#import "UIButton+buttonCurve.h"

@implementation UIButton (buttonCurve)


-(void) setRightCornerRadius
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(self.frame.size.height / 2, self.frame.size.height / 2)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.layer.mask = maskLayer;
}


-(void) setLeftCornerRadius
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(self.frame.size.height / 2, self.frame.size.height / 2)];
    
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = self.bounds;
    maskLayer.path  = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

@end
