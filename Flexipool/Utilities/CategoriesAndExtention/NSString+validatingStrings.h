//
//  NSString+validatingStrings.h
//  DeeraCofee
//
//  Created by Himanshu on 2/14/17.
//  Copyright © 2017 Vrinsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (validatingStrings)
- (BOOL)isValidEmail;
- (BOOL)isValidPassword;
@end
