//
//  NSString+validatingStrings.m
//  DeeraCofee
//
//  Created by Himanshu on 2/14/17.
//  Copyright © 2017 Vrinsoft. All rights reserved.
//

#import "NSString+validatingStrings.h"

@implementation NSString (validatingStrings)


// Email Validation . . .
-(BOOL)isValidEmail{
    
    // Regex Style 
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

- (BOOL)isValidPassword{
  
    /// Condition :  Password should contain atleast 8 characters, 1 uppercase, 1 special character and 1 number
    /// @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{8,}";
    NSString *regularExpression = @"^[A-Za-z0-9-\\d$@$!%*?&#]{6,}";
    NSPredicate *passwordValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regularExpression];
    return [passwordValidation evaluateWithObject:self];
    
}

@end
